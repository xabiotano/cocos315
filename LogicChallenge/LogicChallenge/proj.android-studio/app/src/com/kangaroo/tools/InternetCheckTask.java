package com.kangaroo.tools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;

import android.os.AsyncTask;

public class InternetCheckTask extends AsyncTask<String, Void, Boolean> {

	IConnection iconnection;

	public InternetCheckTask(IConnection iconnection){
		this.iconnection=iconnection;
	}
	
	
@Override
protected void onPreExecute() {
    super.onPreExecute();

}

@Override
protected Boolean doInBackground(String... urls) {
    try {

        //------------------>>
    	HttpURLConnection urlc = (HttpURLConnection) (new URL("https://www.google.com").openConnection());
         urlc.setRequestProperty("User-Agent", "Test");
         urlc.setRequestProperty("Connection", "close");
         urlc.setConnectTimeout(3500); 
         urlc.connect();
         return (urlc.getResponseCode() == 200 || urlc.getResponseCode()==302);
         
    } catch (IOException e) {
        e.printStackTrace();
    } catch(Exception e){
    	
    }
    return false;
}

	protected void onPostExecute(Boolean result) {
		iconnection.resultadoPost(result);
	}
}