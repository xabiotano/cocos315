package com.kangaroo.tools.notifications;



import org.cocos2dx.cpp.AppActivity;
import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.kangaroo.logicchallenge.R;

public class LocalNotificationReceiver extends BroadcastReceiver {

	public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    
	@Override
	public void onReceive(Context context, Intent intent) {
		
		int notificationId = intent.getIntExtra("notification_id", 0);
		String message = intent.getStringExtra("message");
	

		Log.e("LocalNotificationRece",""+notificationId+"   "+message);
		
		Intent intent2 = new Intent(context, AppActivity.class);
		intent2.putExtra("started_from","notification");
		intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent2,
				PendingIntent.FLAG_UPDATE_CURRENT);
		

		Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(),
				R.mipmap.ic_launcher);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setContentTitle(context.getString(R.string.app_name));
		builder.setContentText(message);
		builder.setSmallIcon(R.mipmap.ic_launcher);
		builder.setLargeIcon(largeIcon);
		builder.setTicker(message);
		builder.setAutoCancel(true);
		builder.setDefaults(Notification.DEFAULT_ALL);
		builder.setContentIntent(pendingIntent);
		

		NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		 Notification notification = intent.getParcelableExtra(NOTIFICATION);
		 
		
		 
		manager.notify(notificationId, builder.build());
	}

}