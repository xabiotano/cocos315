package com.kangaroo.tools.ads;

import android.app.Activity;

import com.kangaroo.tools.AdsActvity;

/**
 * Created by xabiotano on 4/6/17.
 */

public interface IAdHelper {


    void showIntersitial();
    void cacheIntersitial();
    void showBanner();
    void hideBanner();
    void setIntersitialCachedI();
    boolean getIntersitialCachedI();
    void destroy();


}
