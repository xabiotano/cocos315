package com.kangaroo.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.kangaroo.tools.ads.AdHelperMopub;
import com.kangaroo.tools.ads.IAdHelper;
import com.kangaroo.tools.ads.IAdNative;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHandler;

import java.lang.ref.WeakReference;

/**
 * Created by xabiotano on 4/6/17.
 */

public class AdsActvity  extends Cocos2dxActivity implements IAdNative {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!getRemoveAdsPurchased())
        {
            AdHelperMopub.getInstance(this).showBanner();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(AdHelperMopub.getInstance(this)!=null)
        {
            AdHelperMopub.getInstance(this).destroy();
        }

    }

    private static void shareAmigos(String path)
    {
        ShareHelper.takeScreenshot((Cocos2dxActivity)Cocos2dxActivity.getContext(),path);
    }

    private static void showIntersitial() {
        AdHelperMopub.getInstance().showIntersitial();
    }


    private static void cacheIntersitial() {
        AdHelperMopub.getInstance().cacheIntersitial();
    }


    private static void showBanner() {
        AdHelperMopub.getInstance().showBanner();
    }


    private static void hideBanner() {
        AdHelperMopub.getInstance().hideBanner();
    }

    private static boolean getIntersitialCached() {
        return AdHelperMopub.getInstance().getIntersitialCachedI();
    }

    @Override
    public native void interstitialDidAppear(String device_token) ;

    @Override
    public native void setIntersitialCached(boolean cacheado);

    @Override
    public native boolean getRemoveAdsPurchased();

    @Override
    public Cocos2dxActivity getActivity() {
        return ((Cocos2dxActivity)Cocos2dxActivity.getContext());
    }
}