package com.kangaroo.tools.notifications;

/**
 * Created by xabiotano on 8/6/17.
 */

public interface INotificationHelper {
     void showLocalNotification(String message, int interval, int tag);
     void cancelLocalNotification(int tag);
     void showGiftNotificationI(boolean mostrado);

}
