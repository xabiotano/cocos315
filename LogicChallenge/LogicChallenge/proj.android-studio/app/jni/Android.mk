LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)
$(call import-add-path, $(LOCAL_PATH))

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame



LOCAL_SRC_FILES := hellocpp/main.cpp \
../../../Classes/AppDelegate.cpp \
../../../Classes/HelloWorldScene.cpp \
../../../Classes/BaseScene.cpp \
../../../Classes/InicioScene.cpp \
../../../Classes/Levels.cpp \
../../../Classes/LevelsBase.cpp \
../../../Classes/Levelsv2.cpp \
../../../Classes/Variables.cpp \
../../../Classes/balanzas/Balanza.cpp \
../../../Classes/balanzas/BalanzasScene.cpp \
../../../Classes/balanzas/Cake.cpp \
../../../Classes/botellas/BotellasScene.cpp \
../../../Classes/botellas/Bottle.cpp \
../../../Classes/bridge/BridgeScene.cpp \
../../../Classes/bridge/HumanB.cpp \
../../../Classes/canibals/CanibalsScene.cpp \
../../../Classes/canibals/Canoe.cpp \
../../../Classes/canibals/Human.cpp \
../../../Classes/elevators/Elevator.cpp \
../../../Classes/elevators/ElevatorsScene.cpp \
../../../Classes/fivefrogs/FiveFrogs.cpp \
../../../Classes/fivefrogs/Frog.cpp \
../../../Classes/goatlettucewolf/Animal.cpp \
../../../Classes/goatlettucewolf/Boat.cpp \
../../../Classes/goatlettucewolf/GoatLettuceWolfScene.cpp \
../../../Classes/hanoi/Disk.cpp \
../../../Classes/hanoi/DisksScene.cpp \
../../../Classes/hanoi/Stick.cpp \
../../../Classes/hardbridge/Barca.cpp \
../../../Classes/hardbridge/HardBridgeScene.cpp \
../../../Classes/hardbridge/Protagonista.cpp \
../../../Classes/hardbridge/LayerSituacionNoOk.cpp \
../../../Classes/layers/LayerDesbloqueadoNivel.cpp \
../../../Classes/layers/LayerExplicarItems.cpp \
../../../Classes/layers/LayerBase.cpp \
../../../Classes/layers/LayerBlock.cpp \
../../../Classes/layers/LayerGameOver.cpp \
../../../Classes/layers/LayerGanarItem.cpp \
../../../Classes/layers/LayerGastarPista.cpp \
../../../Classes/layers/LayerHelp.cpp \
../../../Classes/layers/LayerHint.cpp \
../../../Classes/layers/LayerHintOk.cpp \
../../../Classes/layers/LayerHintSteps.cpp \
../../../Classes/layers/LayerHintWithButton.cpp \
../../../Classes/layers/LayerInfo.cpp \
../../../Classes/layers/LayerNecesitasItems.cpp \
../../../Classes/layers/LayerShop.cpp \
../../../Classes/layers/LayerSuccess.cpp \
../../../Classes/layers/LayerTutorial.cpp \
../../../Classes/lightsout/Diamond.cpp \
../../../Classes/lightsout/LightsOutScene.cpp \
../../../Classes/psico/PsicoScene.cpp \
../../../Classes/sacks/Banderin.cpp \
../../../Classes/sacks/Palo.cpp \
../../../Classes/sacks/Sack.cpp \
../../../Classes/sacks/SacksScene.cpp \
../../../Classes/SandClock/SandClock.cpp \
../../../Classes/SandClock/SandClockScene.cpp \
../../../Classes/series/SeriesScene.cpp \
../../../Classes/sql/sqlite3.c \
../../../Classes/sql/SQLiteHelper.cpp \
../../../Classes/twofrogs/TwoFrogs.cpp \
../../../Classes/twofrogs/Frogtwo.cpp \
../../../Classes/volcan/Baloon.cpp \
../../../Classes/volcan/Bear.cpp \
../../../Classes/volcan/VolcanScene.cpp \
../../../Classes/widgets/Boton.cpp \
../../../Classes/widgets/CCSlidingLayer.cpp \
../../../Classes/widgets/LabelButton.cpp \
../../../Classes/widgets/LevelButton.cpp \
../../../Classes/widgets/NivelItem.cpp \
../../../Classes/widgets/StarLocked.cpp \
../../../Classes/widgets/CCLocalizedString/CCLocalizedString.cpp \
../../../Classes/jni/InterfaceJNI.cpp \
../../../Classes/helpers/LogicSQLHelper.cpp \
../../../Classes/frutas/FrutasScene.cpp \
../../../Classes/tutorial/TutorialScene.cpp \
../../../Classes/layers/LayerNecesitasSuperar.cpp \
../../../Classes/layers/LayerPuedesUsarPista.cpp \
../../../Classes/layers/LayerPuedesVotar.cpp \
../../../Classes/localnotifications/LocalNotification_Android.cpp \
../../../Classes/widgets/CCGetFont.cpp \
../../../Classes/widgets/LanguageManager.cpp \
../../../Classes/AdHelper.cpp


LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes
LOCAL_WHOLE_STATIC_LIBRARIES := PluginIAP \
sdkbox \
android_native_app_glue \
PluginGoogleAnalytics \
PluginReview

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./pluginiap)
$(call import-module, ./plugingoogleanalytics)
$(call import-module, ./pluginreview)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
