//
//  Sack.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Sack.h"
#include "SimpleAudioEngine.h"





Sack::Sack(SacksScene* parent,int index){
    mTipo=SacksTypes(kNone);
    mParent=parent;
    mIndex=index;
    noActionLaunch();
    _color=Color3B(255,255,255);
    mFixed=false;
}

Sack::~Sack(void){}


Sack* Sack::create(SacksScene* parent,int index)
{
    Sack *devolver= new Sack(parent,index);
    devolver->autorelease();
    devolver->initBoton(NULL, NULL, "sacks/sack.png");
    return devolver;
}



SacksTypes Sack::getType(){
    return SacksTypes(mTipo);
}


void Sack::setType(SacksTypes tipo){
    Sprite* mercancia=Sprite::create();
    if(tipo==SacksTypes(kTomato)){
        mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/tomatoes_sack.png"));
        
        //  mercancia->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().heigth*0.8f));
    }else if(tipo==SacksTypes(kBanana)){
        mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/bananas_sack.png"));
    }else if(tipo==SacksTypes(kTennis)){
        mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/tennis_sack.png"));
    }else if(tipo==SacksTypes(kWaterMelon)){
        mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/watermelon_sack.png"));
    }else if(tipo==SacksTypes(kCoins)){
        mercancia->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/coins_sack.png"));
    }
    this->mTipo=tipo;
    mercancia->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height*0.95f));
    this->addChild(mercancia);
}




void Sack::onTouchEnded(Touch* touch, Event* event){
   
   
    Boton::onTouchEnded(touch, event);
    //revelar contenido

    if(mParent->getTurn()==0){
        //siempre correcto
        //aqui desvelo la mercancia
        mParent->nextTurn(this);
    }
   
}

int Sack::getIndex(){
    return mIndex;
}


bool Sack::getLocked(){
    return mFixed;
}
void Sack::setLocked(bool fixed){
    mFixed=fixed;
}







