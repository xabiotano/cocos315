//
//  Palo.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Palo__
#define __logicmaster2__Palo__

#include <iostream>
#include "SacksScene.h"
#include "SacksTypes.h"



class Palo: public Sprite
{
    
    
    public:
        Palo();
        virtual ~Palo(void);
        static Palo* create();
        PaloState getState();
    void setState(PaloState state);
    
    

    
   

    private:
        PaloState mState;
        SacksScene* mParent;
    
    
};


#endif 
