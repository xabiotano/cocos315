#ifndef __SacksScene_SCENE_H__
#define __SacksScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Sack.fwd.h"
#include "Banderin.fwd.h"
#include "SacksTypes.h"
#include "Palo.fwd.h"
USING_NS_CC;


class SacksScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(SacksScene);
    static SacksScene* getInstance();
    ~SacksScene();  // This is the destructor: declaration
   
    
    int getTurn();
    
    
    virtual void delayedCheckFinish();
    void checkFinish();
    virtual void HelpClose();

    virtual void Retry(Ref* sender) override;
    virtual void Hint(Ref* sender) override ;
    virtual void HintCallback(bool accepted) override;
    virtual void HintStart(Ref* sender) override;
    virtual void HintNext(Ref* sender) override;
    virtual void TutorialStart(){};
    
    void nextTurn(Sack* sender);
    void checkCollision(Banderin* sender);
    int isInCollisionWithBanderin(Banderin* sender);
    void finishAnimations();
    bool setBanderin(Banderin* banderin);
    
    const int TAG_STARS=234;
    const int NUMERO_SUCCESS=4;
    void onEnterTransitionDidFinish() override;
    
private:
    Dictionary* mBanderinesDictionary;
    Dictionary* mSacksDictionary;
    Label* mCounter;
    Sack* mSaco1,*mSaco2,*mSaco3,*mSaco4,*mSaco5,*mSaco6;
    Banderin* mBanderin1,*mBanderin2,*mBanderin3;
    SacksTypes getTipeOfPosicion(int index);
    Palo *mPalo1,*mPalo2,*mPalo3;
    Sack* mSackFirstRevealed;
    void pintarSacos();
    void pintarBanderines();
    int mTurn;
    void crearPartida();
    bool mPaloAccion1,mPaloAccion2,mPaloAccion3;
    bool checkSuccesOfMove(Banderin* sender,int palo_colisionado);
    
    void fixBanderinToStick(Banderin* sender,int numero);
   
    void setContentOfSacks(Banderin* banderin,int palo_destino);
   
    void  pintarEstrellas();
    void sumarNivelSuperado();
    int getNumSuccess();

    TipoEscenario mTiposEscenario;
    bool mEsTipoDoble;
    int mSacoTipoDoble;
    int getNumberOfSackRevealed();
    int mNumeroIntentos;
    void dibujarPanelInfo(int paso);
    void dibujarPanelInfo1(float dt);
    void dibujarPanelInfo2(float dt);
    
    Sack* getHintSacoRevelar();
    Banderin* getHintBanderinPrimerMove();
  
};


#endif // __SacksScene_SCENE_H__
