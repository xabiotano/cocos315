//
//  Banderin.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Banderin__
#define __logicmaster2__Banderin__

#include <iostream>
#include "../widgets/Boton.h"
#include "SacksTypes.h"






class Banderin: public Boton
{
    
    
    public:
        Banderin(SacksTypes tipo1,SacksTypes tipo2,int index);
        virtual ~Banderin(void);
        static Banderin* create(SacksTypes tipo1,SacksTypes tipo2,int index);
        SacksTypes getType1();
        SacksTypes getType2();
        Point mPuntoOrigen;
        int getIndex();
        BanderinState getState();
        void  setState(BanderinState state);
    
    private:
        SacksTypes mTipo1;
        SacksTypes mTipo2;
        int mIndex;
        BanderinState mState;
    protected:
        bool onTouchBegan(Touch* touch, Event* event) override;
        void onTouchMoved(Touch* touch, Event* event) override;
        void onTouchEnded(Touch* touch, Event* event) override;
    
};


#endif /* defined(__logicmaster2__Banderin__) */
