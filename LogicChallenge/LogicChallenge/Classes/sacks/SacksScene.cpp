#include "SacksScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Sack.h"
#include "Banderin.h"
#include "Palo.h"

using namespace cocos2d;
using namespace CocosDenshion;


SacksScene* instancia_sack;



SacksScene::~SacksScene(void)
{
    CCLOG("destructor called");
    CC_SAFE_RELEASE(mSacksDictionary);
    CC_SAFE_RELEASE(mBanderinesDictionary);
   
    
}


Scene* SacksScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    SacksScene *layer = SacksScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool SacksScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_sack=this;
    mNombrePantalla="SacksScene";
    mTipoNivel=kNivelLogica;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    mBanderinesDictionary=Dictionary::create();
    mBanderinesDictionary->retain();
    mSacksDictionary=Dictionary::create();
    mSacksDictionary->retain();
    // Director::getInstance()->setContentScaleFactor(640/visibleSize.width);
    
    // add "SplashScene" splash screen"
    Sprite* mFondo = Sprite::create("sacks/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    crearPartida();
    
    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_SACKS");;
    mRetryVisibleDuringGame=true;
    
  
    return true;
}

void SacksScene::dibujarPanelInfo(int paso){
    LayerInfo* info;
    std::string texto;
    
    if(paso==1){
        texto= LanguageManager::getInstance()->getStringForKey("SACKS_TOUCH_ONE_SACK");
        
    }else if(paso==2){
        texto= LanguageManager::getInstance()->getStringForKey("SACKS_ASIGNA");
    }
    else if(paso==3){
        texto=LanguageManager::getInstance()->getStringForKey("SACKS_TOUCH_ONE_SACK");
                               
    }
    info= Info(texto);
    Sprite* fondo=(Sprite*)info->getCanvas();
    if(paso==1){
        Sprite* saco=Sprite::create("sacks/sack.png");
        saco->setPosition(Point(fondo->getContentSize().width/2,fondo->getContentSize().height/2 ));
        saco->setScale(0.5f);
        fondo->addChild(saco);
    }
    else if(paso==2){
        Sprite* saco=Sprite::create("sacks/paso2.png");
        saco->setPosition(Point(fondo->getContentSize().width/2,fondo->getContentSize().height/2 ));
        fondo->addChild(saco);
     
    }
    
    
}




void SacksScene::crearPartida(){
    mTurn=0;
    mPaloAccion1=false;
    mPaloAccion2=false;
    mPaloAccion3=false;
    pintarSacos();
    pintarBanderines();
    pintarEstrellas();
   
}

void SacksScene::pintarEstrellas(){
    
    
    float x_percent=0.3f;
    float x_plus=0.07f;
    mNumeroIntentos=0;
    for(int i=1;i<=4;i++){
        char temp[200];
        sprintf(temp,"SacksScene_stars %i",i);
        bool blocked= UserDefault::getInstance()->getBoolForKey(temp);
        if(!blocked){
            mNumeroIntentos=i;
        }
        StarLocked* locked=StarLocked::create(blocked);
        locked->setPosition(Point(mVisibleSize.width*(x_percent + (x_plus*i)),mVisibleSize.height*0.9f));
        this->addChild(locked);
        locked->setTag((TAG_STARS + i));
    }
}

//encuentro la primera estrella no superada para fijarla como unlock
void SacksScene::sumarNivelSuperado(){
    for(int i=1;i<=4;i++){
        char temp[200];
        sprintf(temp,"SacksScene_stars %i",i);
        bool blocked= UserDefault::getInstance()->getBoolForKey(temp);
        if(blocked){
             UserDefault::getInstance()->setBoolForKey(temp, false);
             UserDefault::getInstance()->flush();
            int tag=TAG_STARS + i;
            StarLocked* star=(StarLocked*)this->getChildByTag(tag);
            star->unlock();
            return;
        }
        
    }
    
        
    
    
    
}

int SacksScene::getNumSuccess(){
    int num=0;
    for(int i=1;i<=4;i++){
        char temp[200];
        sprintf(temp,"SacksScene_stars %i",i);
        bool blocked= UserDefault::getInstance()->getBoolForKey(temp);
        if(!blocked){
            num++;
        }
        
    }
    return num;
}

void SacksScene::pintarBanderines(){
}


void SacksScene::pintarSacos(){
    /*Limpieza de la escena*/
    
    mTurn=0;
    mPaloAccion1=false;
    mPaloAccion2=false;
    mPaloAccion3=false;
    
    if(mSaco1!=NULL){
        mSaco1->removeFromParent();
        mSaco1=NULL;
    }
    if(mSaco2!=NULL){
        mSaco2->removeFromParent();
        mSaco2=NULL;
    }
    if(mSaco3!=NULL){
        mSaco3->removeFromParent();
        mSaco3=NULL;
    }
    
    if(mSaco4!=NULL){
        mSaco4->removeFromParent();
        mSaco4=NULL;
    }
    if(mSaco5!=NULL){
        mSaco5->removeFromParent();
        mSaco5=NULL;
    }
    if(mSaco6!=NULL){
        mSaco6->removeFromParent();
        mSaco6=NULL;
    }
    if(mPalo1!=NULL){
        mPalo1->removeFromParent();
        mPalo1=NULL;
    }
    if(mPalo2!=NULL){
        mPalo2->removeFromParent();
        mPalo2=NULL;
    }
    if(mPalo3!=NULL){
        mPalo3->removeFromParent();
        mPalo3=NULL;
    }
    
    if(mBanderin1!=NULL){
        mBanderin1->removeFromParent();
        mBanderin1=NULL;
    }if(mBanderin2!=NULL){
        mBanderin2->removeFromParent();
        mBanderin2=NULL;
    }if(mBanderin3!=NULL){
        mBanderin3->removeFromParent();
        mBanderin3=NULL;
    }
    mBanderinesDictionary->removeAllObjects();
    mSacksDictionary->removeAllObjects();
    /*FIN Limpieza de la escena*/
    
    mSaco1=Sack::create(this,1);
    mSaco1->setPosition(Point(mVisibleSize.width*0.10f,mVisibleSize.height*0.4f));
    this->addChild(mSaco1);
    mSaco2=Sack::create(this,2);
    mSaco2->setPosition(Point(mVisibleSize.width*0.25f,mVisibleSize.height*0.4f));
    this->addChild(mSaco2);
    
    
    mSaco3=Sack::create(this,3);
    mSaco3->setPosition(Point(mVisibleSize.width*0.45f,mVisibleSize.height*0.4f));
    this->addChild(mSaco3);
    mSaco4=Sack::create(this,4);
    mSaco4->setPosition(Point(mVisibleSize.width*0.55f,mVisibleSize.height*0.4f));
    this->addChild(mSaco4);
    
    
    mSaco5=Sack::create(this,5);
    mSaco5->setPosition(Point(mVisibleSize.width*0.75f,mVisibleSize.height*0.4f));
    this->addChild(mSaco5);
    mSaco6=Sack::create(this,6);
    mSaco6->setPosition(Point(mVisibleSize.width*0.9f,mVisibleSize.height*0.4f));
    this->addChild(mSaco6);
    
    
    mPalo1=Palo::create();
    mPalo2=Palo::create();
    mPalo3=Palo::create();
    
    mPalo1->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.15f));
    mPalo2->setPosition(Point(mVisibleSize.width*0.50f,mVisibleSize.height*0.15f));
    mPalo3->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.15f));
    
    this->addChild(mPalo1);
    this->addChild(mPalo2);
    this->addChild(mPalo3);

    
    
    
    //tipos
    //*11 21 22
    //*12 11 22
    //*11 22 12
    //*11 22 21
        /*
     kTomato,
     kBanana,
     kCoins,
     kWaterMelon,
     kTennis

     */
    int tipo=getNumSuccess();

    if(tipo==0){
        mBanderin1=Banderin::create(SacksTypes(kTomato), SacksTypes(kTomato),1);
        mBanderin2=Banderin::create(SacksTypes(kBanana), SacksTypes(kBanana),2);
        mBanderin3=Banderin::create(SacksTypes(kBanana), SacksTypes(kTomato),3);
    }else if(tipo==1){
        mBanderin1=Banderin::create(SacksTypes(kCoins), SacksTypes(kCoins),1);
        mBanderin2=Banderin::create(SacksTypes(kWaterMelon), SacksTypes(kCoins),2);
        mBanderin3=Banderin::create(SacksTypes(kWaterMelon), SacksTypes(kWaterMelon),3);
    }else if(tipo==2){
        mBanderin1=Banderin::create(SacksTypes(kTennis), SacksTypes(kTomato),1);
        mBanderin2=Banderin::create(SacksTypes(kTennis), SacksTypes(kTennis),2);
        mBanderin3=Banderin::create(SacksTypes(kTomato), SacksTypes(kTomato),3);
    }else if(tipo==3){
        mBanderin1=Banderin::create(SacksTypes(kBanana), SacksTypes(kBanana),1);
        mBanderin2=Banderin::create(SacksTypes(kWaterMelon), SacksTypes(kWaterMelon),2);
        mBanderin3=Banderin::create(SacksTypes(kBanana), SacksTypes(kWaterMelon),3);
    }else{
        for(int i=0;i<=3;i++){
            char temp[200];
            sprintf(temp,"SacksScene_stars %i",i);
            UserDefault::getInstance()->setBoolForKey(temp, true);
            UserDefault::getInstance()->flush();
        }
        //ESTO ESTA BIEN??
        mBanderin1=Banderin::create(SacksTypes(kBanana), SacksTypes(kBanana),1);
        mBanderin2=Banderin::create(SacksTypes(kWaterMelon), SacksTypes(kWaterMelon),2);
        mBanderin3=Banderin::create(SacksTypes(kBanana), SacksTypes(kWaterMelon),3);
    }
    
    
    mBanderin1->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.25f));
    mBanderin2->setPosition(Point(mVisibleSize.width*0.50f,mVisibleSize.height*0.25f));
    mBanderin3->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.25f));
    
    this->addChild(mBanderin1);
    this->addChild(mBanderin2);
    this->addChild(mBanderin3);
    

    
    mBanderinesDictionary->setObject(mBanderin1, 1);
    mBanderinesDictionary->setObject(mBanderin2, 2);
    mBanderinesDictionary->setObject(mBanderin3, 3);
    
    mSacksDictionary->setObject(mSaco1, 1);
    mSacksDictionary->setObject(mSaco2, 2);
    mSacksDictionary->setObject(mSaco3, 3);
    mSacksDictionary->setObject(mSaco4, 4);
    mSacksDictionary->setObject(mSaco5, 5);
    mSacksDictionary->setObject(mSaco6, 6);
    
}

Sack* SacksScene::getHintSacoRevelar(){
    int tipo=getNumSuccess();
    if(tipo==0){
        return mSaco2;
    }else if(tipo==1){
        return mSaco2 ;
    }else if(tipo==2){
        return mSaco2 ;
    }else if(tipo==3){
         return mSaco1 ;
    }
}

Banderin* SacksScene::getHintBanderinPrimerMove(){
    int tipo=getNumSuccess();
    if(tipo==0){
        if(mBanderin2->getType2()==mSaco2->getType()){
            return mBanderin2 ;
        }else{
            return mBanderin3 ;
        }
    }else if(tipo==1){
        if(mBanderin2->getType2()==mSaco2->getType()){
            return mBanderin2 ;
        }else{
            return mBanderin3 ;
        }
    }else if(tipo==2){
        if(mBanderin2->getType2()==mSaco2->getType()){
            return mBanderin2 ;
        }else{
            return mBanderin3 ;
        }
    }else if(tipo==3){
        if(mBanderin2->getType1()==mSaco1->getType()){
            return mBanderin2 ;
        }else{
            return mBanderin3 ;
        }
    }
}

int SacksScene::getTurn(){
    return mTurn;
}


void SacksScene::nextTurn(Sack* sender){
   
    if(mTurn==0){
        mSackFirstRevealed=sender;
        int posicionDelTipo=-1;
        int random=(double) rand();
        int posibilidades[2];
        //siempre acierto, elijo una fruta de entre las dos posibles
        if(sender==mSaco1){
            //solo puedo elegir el tipo de 2 o 4
            posibilidades[0]=3;
            posibilidades[1]=5;
        }else if(sender==mSaco2){
            //3 o 5
            posibilidades[0]=4;
            posibilidades[1]=6;
           
            
        }else if(sender==mSaco3){
            //1 o 4
            posibilidades[0]=1;
            posibilidades[1]=5;
            
        }else if(sender==mSaco4){
            //1 o 5
            posibilidades[0]=2;
            posibilidades[1]=6;
            
        }else if(sender==mSaco5){
            //0 o 2
            posibilidades[0]=1;
            posibilidades[1]=3;
            
        }else if(sender==mSaco6){
            //1 o 3
            posibilidades[0]=2;
            posibilidades[1]=4;
            
        }
       
        int valor=-1;
        if(random%2==0){
            posicionDelTipo=posibilidades[0];
            
            mSacoTipoDoble=posibilidades[1];
        }else{
             posicionDelTipo=posibilidades[1];
             mSacoTipoDoble=posibilidades[0];
        }
        
        if(getTipeOfPosicion(posibilidades[0])==getTipeOfPosicion(posibilidades[1])){
            mEsTipoDoble=true;
            
        }else{
            mEsTipoDoble=false;
        }
        
        //kDosTresUno
        //kTresUnoDos
        valor=posicionDelTipo;
        
        
        if(posicionDelTipo==1){
            if(sender==mSaco3){
                mTiposEscenario=TipoEscenario(kTresUnoDos);
            }else if(sender==mSaco5){
                mTiposEscenario=TipoEscenario(kDosTresUno);
            }
            
        }else if(posicionDelTipo==2){
            if(sender==mSaco4){
               mTiposEscenario=TipoEscenario(kTresUnoDos);
            }else if(sender==mSaco6){
              mTiposEscenario=TipoEscenario(kDosTresUno);
            }
            
        }
         //12  34  56
        else if(posicionDelTipo==3){
           if(sender==mSaco1){
              mTiposEscenario=TipoEscenario(kDosTresUno);
            }
            if(sender==mSaco5){
                 mTiposEscenario=TipoEscenario(kTresUnoDos);
            }
            
            
        }else if(posicionDelTipo==4){
            if(sender==mSaco2){
                mTiposEscenario=TipoEscenario(kDosTresUno);
            }
            if(sender==mSaco6){
                mTiposEscenario=TipoEscenario(kTresUnoDos);
            }
        }else if(posicionDelTipo==5){
            if(sender==mSaco1){
                mTiposEscenario=TipoEscenario(kTresUnoDos);
            }
            if(sender==mSaco3){
                mTiposEscenario=TipoEscenario(kDosTresUno);
            }
            
        }else if(posicionDelTipo==6){
            if(sender==mSaco2){
                mTiposEscenario=TipoEscenario(kTresUnoDos);
            }else if(sender==mSaco4){
                mTiposEscenario=TipoEscenario(kDosTresUno);
            }
        }
        
        //la fruta es del tipo del index posicionDelTipo
        SacksTypes tipo= getTipeOfPosicion(posicionDelTipo);
        sender->setType(tipo);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/opensack.mp3",false);
        
        mBanderin1->mPuntoOrigen= Point(mBanderin1->getPositionX(),mVisibleSize.height*0.75);
        mBanderin2->mPuntoOrigen= Point(mBanderin2->getPositionX(),mVisibleSize.height*0.75);
        mBanderin3->mPuntoOrigen= Point(mBanderin3->getPositionX(),mVisibleSize.height*0.75);
        
        mBanderin1->runAction(MoveTo::create(0.5f,mBanderin1->mPuntoOrigen));
        mBanderin2->runAction(MoveTo::create(0.5f,mBanderin2->mPuntoOrigen ));
        mBanderin3->runAction(MoveTo::create(0.5f,mBanderin3->mPuntoOrigen));
        
        
                              
        
        mBanderin1->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.4f, 10), RotateBy::create(0.4f, -10))));
         mBanderin2->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.4f, 10), RotateBy::create(0.4f, -10))));
         mBanderin3->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.4f, 10), RotateBy::create(0.4f, -10))));
    
        mBanderin1->setState(kUnlocked);
        mBanderin2->setState(kUnlocked);
        mBanderin3->setState(kUnlocked);
        
        int intentos=LogicSQLHelper::getInstance().getIntentos(Variables::CURRENT_SCENE,mTipoNivel);
        if(intentos<=2 && !mSituacionHint){
            this->scheduleOnce(schedule_selector(SacksScene::dibujarPanelInfo2),0.5f);
        }
        
    }
    
    if(mTurn==0 && mSituacionHint ){
        if( getHintSacoRevelar()!=sender){
            LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("SACKS_NO_SEGUISTE_LA_PISTA"));
            BaseScene::HintStop(NULL);
        }else{
            mSituacionHint=true;
            mHintStep=3;
            HintNext(NULL);
        }
        getHintSacoRevelar()->stopAllActions();
        getHintSacoRevelar()->setScale(1.f);
    }
    
    mTurn++;
    
    
   
    
    
}

SacksTypes  SacksScene::getTipeOfPosicion(int index){
    
    if(index==1){
        return mBanderin1->getType1();
    }else if(index==2){
        return mBanderin1->getType2();
    }else if(index==3){
        return mBanderin2->getType1();
    }else if(index==4){
        return mBanderin2->getType2();
    }else if(index==5){
        return mBanderin3->getType1();
    }else if(index==6){
       return mBanderin3->getType2();
    }
}

void SacksScene::checkCollision(Banderin* sender){
    int colisionado=isInCollisionWithBanderin(sender);
    
    if (colisionado==1){
        if(!mPaloAccion1){
            mPalo1->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
            mPaloAccion1=true;
            mSaco1->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            mSaco2->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
        }
        
        
        
    }else if (colisionado==2){
        if(!mPaloAccion2){
            mPalo2->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
            mPaloAccion2=true;
            mSaco3->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            mSaco4->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            
        }
        
    }else if (colisionado==3){
        if(!mPaloAccion3){
            mPalo3->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
            mPaloAccion3=true;
            mSaco5->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            mSaco6->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
        }
        
    }else{
        
        finishAnimations();
    }
}



int SacksScene::isInCollisionWithBanderin(Banderin* sender){
    
    Rect senderRect;
    senderRect = Rect(sender->getPosition().x - (sender->getContentSize().width*0.5f),
                            sender->getPosition().y - (sender->getContentSize().height*0.5f),
                            sender->getContentSize().width,
                            sender->getContentSize().height);
    
    
    
    //update status
    Rect palo1 = Rect(mPalo1->getPosition().x - (mPalo1->getContentSize().width/2),
                                mPalo1->getPosition().y - (mPalo1->getContentSize().height/2),
                                mPalo1->getContentSize().width,
                                mPalo1->getContentSize().height);
    
    Rect palo2 = Rect(mPalo2->getPosition().x - (mPalo2->getContentSize().width/2),
                              mPalo2->getPosition().y - (mPalo2->getContentSize().height/2),
                              mPalo3->getContentSize().width,
                              mPalo3->getContentSize().height);
    
    Rect palo3 = Rect(mPalo3->getPosition().x - (mPalo3->getContentSize().width/2),
                              mPalo3->getPosition().y - (mPalo3->getContentSize().height/2),
                              mPalo3->getContentSize().width,
                              mPalo3->getContentSize().height);
    
    
    
    if (palo1.intersectsRect(senderRect)){
        if(!mPaloAccion1){
            mPalo1->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
            mPaloAccion1=true;
            mSaco1->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            mSaco2->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
        }
        
        return 1;
        
    }else if (palo2.intersectsRect(senderRect)){
        if(!mPaloAccion2){
            mPalo2->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
            mPaloAccion2=true;
            mSaco3->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
             mSaco4->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
            
        }
         return 2;
        
    }else if (palo3.intersectsRect(senderRect)){
        if(!mPaloAccion3){
            mPalo3->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.3f), ScaleBy::create(0.2f, 0.7f))));
             mPaloAccion3=true;
            mSaco5->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
             mSaco6->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 200, 100),TintTo::create(0.5f, 255.f, 255.f, 255.f) )));
        }
         return 3;
        
    }
    return -1;
    
 
    
    
}
void SacksScene::finishAnimations(){
    mPaloAccion1=false;
    mPaloAccion2=false;
    mPaloAccion3=false;
    mPalo1->stopAllActions();
    mPalo2->stopAllActions();
    mPalo3->stopAllActions();
    mSaco1->stopAllActions();
    mSaco2->stopAllActions();
    mSaco3->stopAllActions();
    mSaco4->stopAllActions();
    mSaco5->stopAllActions();
    mSaco6->stopAllActions();

    mPalo1->setScale(1.f);
    mPalo2->setScale(1.f);
    mPalo3->setScale(1.f);
    mSaco1->setColor(Color3B(255,255,255));
    mSaco2->setColor(Color3B(255,255,255));
    mSaco3->setColor(Color3B(255,255,255));
    mSaco4->setColor(Color3B(255,255,255));
    mSaco5->setColor(Color3B(255,255,255));
    mSaco6->setColor(Color3B(255,255,255));
  
}





//devuelve si ha sido gestionado el movimiento o no
bool SacksScene::setBanderin(Banderin* banderin_movido){
    stopAllActions();
   
    
    int palo_colisionado=isInCollisionWithBanderin(banderin_movido);
    if(palo_colisionado==-1){
        //no esta con ningun palo
        return false;
    }
    if(    (palo_colisionado==1 && mPalo1->getState()==PaloState(kPaloAsigned))
        || (palo_colisionado==2 && mPalo2->getState()==PaloState(kPaloAsigned))
        || (palo_colisionado==3 && mPalo3->getState()==PaloState(kPaloAsigned))
       )
    {
        CCLOG("MOVIDO A PALO OCUPADO");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return false;
    }
    
    fixBanderinToStick(banderin_movido,palo_colisionado );
    
    int palo_alternativa;
    bool alarma=false;
    bool mismo_palo=false;
    
    if(banderin_movido->getIndex()==palo_colisionado){
        mismo_palo=true;
        alarma=true;
        //mismo palo osea que debo dar error
        CCLOG("MISMO PALO");
        if(palo_colisionado==1){
            if(mTiposEscenario==TipoEscenario(kTresUnoDos)){
                palo_alternativa=3;
                 CCLOG("palo_colisionado 1 => 3");
            }else if(mTiposEscenario==TipoEscenario(kDosTresUno))
            {
                palo_alternativa=2;
                 CCLOG("palo_colisionado 1 => 2");
            }
        }
        else if(palo_colisionado==2){
            if(mTiposEscenario==TipoEscenario(kTresUnoDos)){
                 palo_alternativa=1;
                 CCLOG("palo_colisionado 2 => 1");
            }else if(mTiposEscenario==TipoEscenario(kDosTresUno))
            {
                 palo_alternativa=3;
                 CCLOG("palo_colisionado 2 => 3");
            }
            
        }else if(palo_colisionado==3){
            if(mTiposEscenario==TipoEscenario(kTresUnoDos)){
                 palo_alternativa=2;
                 CCLOG("palo_colisionado 3 => 2");
                
            }else if(mTiposEscenario==TipoEscenario(kDosTresUno))
            {
                 palo_alternativa=1;
                 CCLOG("palo_colisionado 3 => 1");
            }
            
        }
    }
    
    
    //se usa luego
    bool desveladoSacoSinReveleacionPrevia=false;
    if(palo_colisionado==1){
         desveladoSacoSinReveleacionPrevia=mSaco1->getType()== SacksTypes(kNone) && mSaco2->getType()== SacksTypes(kNone);
    }
    else if(palo_colisionado==2){
        desveladoSacoSinReveleacionPrevia=mSaco3->getType()== SacksTypes(kNone) && mSaco4->getType()== SacksTypes(kNone);
    }
    else if(palo_colisionado==3){
          desveladoSacoSinReveleacionPrevia=mSaco5->getType()== SacksTypes(kNone) && mSaco6->getType()== SacksTypes(kNone);
    }
   //actualizo a asignado
    if(banderin_movido->getIndex()==1){
        mBanderin1->setState(kAsigned);
       
    }else if(banderin_movido->getIndex()==2){
        mBanderin2->setState(kAsigned);
       
    }else if(banderin_movido->getIndex()==3){
        mBanderin3->setState(kAsigned);
      
    }
    
    int palo_destino=palo_colisionado;
    int palo_origen=banderin_movido->getIndex();
 
    
   
    if(!alarma){
        if(palo_destino==1){
            if(palo_origen==2){
                if(mTiposEscenario!=TipoEscenario(kDosTresUno)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=3;
                }
                
            }if(palo_origen==3){
                if(mTiposEscenario!=TipoEscenario(kTresUnoDos)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=2;
                }
            }
            
            
            
        }else if(palo_destino==2){
            if(palo_origen==1){
                if(mTiposEscenario!=TipoEscenario(kTresUnoDos)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=3;
                }
            }
            if(palo_origen==3){
                if(mTiposEscenario!=TipoEscenario(kDosTresUno)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=1;
                }
            }
        }else if(palo_destino==3){
            if(palo_origen==1){
                if(mTiposEscenario!=TipoEscenario(kDosTresUno)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=2;
                }
            }
            if(palo_origen==2){
                if(mTiposEscenario!=TipoEscenario(kTresUnoDos)){
                    CCLOG("ALARMAAA");
                    alarma=true;
                    palo_alternativa=1;
                }
            }
        }
    }
    
   
    
    //PENDIENTE LO DE DOBLE
    //solo si hay un saco revelado
    if(!alarma && getNumberOfSackRevealed()==1){
        CCLOG("SOLO SE HA DESCUBIERTO UN SACO");
        if(mEsTipoDoble){
            CCLOG("ES TIPO DOBLE");
            if(mSacoTipoDoble==1 || mSacoTipoDoble==2)
            {
                if(palo_destino!=1)
                {
                    CCLOG("BANDERIN DOBLE 1");
                    alarma=true;
                    palo_alternativa=1;

                }
            }
            if(mSacoTipoDoble==3 || mSacoTipoDoble==4)
            {
                if(palo_destino!=2)
                {
                    CCLOG("BANDERIN DOBLE 2");
                    alarma=true;
                    palo_alternativa=2;
                }
            }
            if(mSacoTipoDoble==5 || mSacoTipoDoble==6)
            {
                if(palo_destino!=3)
                {
                    CCLOG("BANDERIN DOBLE 3");
                    alarma=true;
                    palo_alternativa=3;
                }
                
            }
            if(!alarma){
                //si es el primerm movimiento
                //sino ha encontrado alterntiva
                //y se ha asignado un palo que no tenia ningun saco desvelado
               
                //kDosTresUno
                //kTresUnoDos
                
                if(desveladoSacoSinReveleacionPrevia){
                    CCLOG("desveladoSacoSinReveleacionPrevia TRUE");
                    if(palo_destino==1) {
                        if(banderin_movido->getIndex()==2){
                            CCLOG("Destino 1 , banderin_movido 2");
                            alarma=true;
                            palo_alternativa=3;

                        }
                        if(banderin_movido->getIndex()==3){
                            CCLOG("Destino 1 , banderin_movido 3");
                            alarma=true;
                            palo_alternativa=2;
                            
                        }
                    }
                    else if(palo_destino==2){
                        if(banderin_movido->getIndex()==1){
                            CCLOG("Destino 2 , banderin_movido 1");
                            alarma=true;
                            palo_alternativa=3;
                            
                        }
                        if(banderin_movido->getIndex()==3){
                            CCLOG("Destino 2 , banderin_movido 3");
                            alarma=true;
                            palo_alternativa=1;
                            
                        }
                    }
                    else if(palo_destino==3){
                        if(banderin_movido->getIndex()==1){
                            CCLOG("Destino 3 , banderin_movido 1");
                            alarma=true;
                            palo_alternativa=2;
                            
                        }
                        if(banderin_movido->getIndex()==2){
                            CCLOG("Destino 3 , banderin_movido 2");
                            alarma=true;
                            palo_alternativa=1;
                            
                        }
                    }
                   
                }
            }
        }
        if(mSituacionHint && banderin_movido==getHintBanderinPrimerMove()){
            getHintBanderinPrimerMove()->stopAllActions();
            getHintBanderinPrimerMove()->setColor(Color3B(255,255,255));
            mSituacionHint=true;
            mHintStep=6;
            LayerHintOk::mostrar(true);
            HintStart(NULL);
        }
       
    }
    
    Banderin* alternativa=NULL;
    if(alarma){
        if(palo_alternativa==1){
             alternativa=mBanderin1;
        }else if(palo_alternativa==2) {
              alternativa=mBanderin2;
        }else if(palo_alternativa==3){
              alternativa=mBanderin3;
        }
    }
    
    if(alternativa!=NULL){
        //compruebo que la alternativa seleccionada encaja en algun saco restante
        setContentOfSacks(alternativa,palo_colisionado);
        mGameover=true;
        if(mismo_palo){
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("SACKS_NO_OF_THESIGNALS");
           
        }else{
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("SACKS_NOT_CORRECT_SIGNAL");
        }
        delayedCheckFinish();
        return true;
       
    }else{
        //revelamos los sacos correctamente
        setContentOfSacks(banderin_movido, palo_colisionado);
        if(mSaco1->getType()!=SacksTypes(kNone) &&
           mSaco2->getType()!=SacksTypes(kNone) &&
           mSaco3->getType()!=SacksTypes(kNone) &&
           mSaco4->getType()!=SacksTypes(kNone) &&
           mSaco5->getType()!=SacksTypes(kNone)&&
           mSaco6->getType()!=SacksTypes(kNone)){
            delayedCheckFinish();
            sumarNivelSuperado();
        }

        
        return true;
    }
    
   
    return false;
}

void SacksScene::setContentOfSacks(Banderin* banderin,int palo_destino){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter.mp3",false);
    Sack* saco1, *saco2;
    if(palo_destino==1){
        saco1=mSaco1;
        saco2=mSaco2;
    }else if(palo_destino==2){
        saco1=mSaco3;
        saco2=mSaco4;
    }else if(palo_destino==3){
        saco1=mSaco5;
        saco2=mSaco6;
    }
    saco1->setType(banderin->getType1());
    saco2->setType(banderin->getType2());
}






void SacksScene::fixBanderinToStick(Banderin* sender,int colisionado){
    
    sender->stopAllActions();
    sender->setColorBoton(Color3B(170.f,170.f,170.f));
    sender->setState(BanderinState(kAsigned));
    if(colisionado==1){
        sender->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.25f));
        mSaco1->setLocked(true);
        mSaco2->setLocked(true);
        mPalo1->setState(PaloState(kPaloAsigned));
        
    }else if(colisionado==2){
        sender->setPosition(Point(mVisibleSize.width*0.50f,mVisibleSize.height*0.25f));
        mSaco3->setLocked(true);
        mSaco4->setLocked(true);
        mPalo2->setState(PaloState(kPaloAsigned));
    }else if(  colisionado==3){
        sender->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.25f));
        mSaco5->setLocked(true);
        mSaco6->setLocked(true);
        mPalo3->setState(PaloState(kPaloAsigned));
    }
    mPalo1->stopAllActions();
    mPalo2->stopAllActions();
    mPalo3->stopAllActions();
    mSaco1->stopAllActions();
    mSaco2->stopAllActions();
    mSaco3->stopAllActions();
    mSaco4->stopAllActions();
    mSaco5->stopAllActions();
    mSaco6->stopAllActions();
    mSaco1->setColor(Color3B(255.f,255.f,255.f));
    mSaco2->setColor(Color3B(255.f,255.f,255.f));
    mSaco3->setColor(Color3B(255.f,255.f,255.f));
    mSaco4->setColor(Color3B(255.f,255.f,255.f));
    mSaco5->setColor(Color3B(255.f,255.f,255.f));
    mSaco6->setColor(Color3B(255.f,255.f,255.f));
    this->nextTurn(NULL);
   
    
    
}


bool SacksScene::checkSuccesOfMove(Banderin* sender,int palo_colisionado){
    CCLOG("checkSuccesOfMove palo_colisionado:%i",palo_colisionado);
    if(sender==NULL){
        return false;
    }
    bool exito=false;
    //establezco un banderin
   
    
    if(palo_colisionado==1){
        if(
           ( mSaco1->getType()==SacksTypes(kNone) ||
            (mSaco1->getType()!= SacksTypes(kNone) &&  mSaco1->getType()==sender->getType1()))
           &&
           ( mSaco2->getType()==SacksTypes(kNone) ||
            (mSaco2->getType()!= SacksTypes(kNone)&& mSaco2->getType()==sender->getType2())
            )
           )
            
        {
            CCLOG("checkSuccesOfMove %i OK",palo_colisionado);
            exito=true;
        }
        
    }else if(palo_colisionado==2){
        if(
           ( mSaco3->getType()==SacksTypes(kNone) ||
            (mSaco3->getType()!= SacksTypes(kNone) &&  mSaco3->getType()==sender->getType1()))
           &&
           ( mSaco4->getType()==SacksTypes(kNone) ||
            (mSaco4->getType()!= SacksTypes(kNone)&& mSaco4->getType()==sender->getType2())
            )
           )
        {
           CCLOG("checkSuccesOfMove %i OK",palo_colisionado);
            exito=true;
        }
        
    }else if(palo_colisionado==3){
        if(
           ( mSaco5->getType()==SacksTypes(kNone) ||
            (mSaco5->getType()!= SacksTypes(kNone) &&  mSaco5->getType()==sender->getType1()))
           &&
           ( mSaco6->getType()==SacksTypes(kNone) ||
            (mSaco6->getType()!= SacksTypes(kNone)&& mSaco6->getType()==sender->getType2())
            )
           ){
             CCLOG("checkSuccesOfMove %i OK",palo_colisionado);
            exito=true;
        }
    }else{
        CCLOG("NO action");
         exito=true;
    }
    
    
    
    return exito;
}




SacksScene* SacksScene::getInstance(){
    return instancia_sack;
};

void SacksScene::delayedCheckFinish(){
    CallFunc* funcion=CallFunc::create([this]() { this->checkFinish(); });
    this->runAction(Sequence::createWithTwoActions(DelayTime::create(1.f),funcion));
   
};

void SacksScene::checkFinish(){
    if(mSaco1->getType()!=SacksTypes(kNone) &&
       mSaco2->getType()!=SacksTypes(kNone) &&
       mSaco3->getType()!=SacksTypes(kNone) &&
       mSaco4->getType()!=SacksTypes(kNone) &&
       mSaco5->getType()!=SacksTypes(kNone)&&
       mSaco6->getType()!=SacksTypes(kNone)){
        mSuccess=true;
        int numero_restante=NUMERO_SUCCESS- this->getNumSuccess()-1;
        if(numero_restante>0){
            char temp[200];
            sprintf(temp,LanguageManager::getInstance()->getStringForKey("SACKSSCENE_EXITO_MAS_VECES").c_str(),numero_restante);
            mTextoSuccess=std::string(temp);
        }
       
        
    }
    
    mSmallGameOver=true;
    BaseScene::checkFinish(NULL);
}


void SacksScene::HelpClose()
{
    dibujarPanelInfo1(0.f);
}
void SacksScene::Retry(Ref* sender){
    
    Director::getInstance()->replaceScene(SacksScene::scene());
}




void SacksScene::dibujarPanelInfo1(float dt){
    if(!mGameover && mTurn<=1){
        dibujarPanelInfo(1);
    }
}

void SacksScene::dibujarPanelInfo2(float dt)
{
    if(!mGameover && mTurn<=1){
        dibujarPanelInfo(2);
    }
    
    
  
        
}

                                             

int SacksScene::getNumberOfSackRevealed(){
    int numero=0;
    if(mSaco1->getType()!=SacksTypes(kNone)){
        numero ++;
    } if(mSaco2->getType()!=SacksTypes(kNone)){
        numero ++;
    } if(mSaco3->getType()!=SacksTypes(kNone)){
        numero ++;
    } if(mSaco4->getType()!=SacksTypes(kNone)){
        numero ++;
    } if(mSaco5->getType()!=SacksTypes(kNone)){
        numero ++;
    } if(mSaco6->getType()!=SacksTypes(kNone)){
        numero ++;
    }
    return numero;
}


void SacksScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void SacksScene::HintNext(Ref* sender)
{
    mHintStep++;
    HintStart(NULL);
}

void SacksScene::HintStart(Ref* sender){
    switch(mHintStep){
        case 0:
            LayerHintWithButton::mostrar(LanguageManager::getInstance()->getStringForKey("SACKSCENE_HINT_0"));
                break;
        case 1:
            LayerHintWithButton::ocultar(NULL);
            LayerHintWithButton::mostrar(LanguageManager::getInstance()->getStringForKey("SACKSCENE_HINT_1"));
            
            break;
        case 2:
            LayerHintWithButton::ocultar(NULL);
            LayerHintWithButton::mostrar(LanguageManager::getInstance()->getStringForKey("SACKSCENE_HINT_2"));
            getHintSacoRevelar()->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.1f), ScaleTo::create(0.5f, 1.f))));
            break;
            
        case 3:
            LayerHintWithButton::ocultar(NULL);
            LayerHintOk::ocultar(NULL);
          
            break;

        case 4:
            LayerHintWithButton::mostrar(LanguageManager::getInstance()->getStringForKey("SACKSCENE_HINT_4"));
            break;
        
        case 5:
            LayerHintWithButton::ocultar(NULL);
            LayerHintOk::ocultar(NULL);
            getHintBanderinPrimerMove()->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f,100.f,200.f,100.f), TintTo::create(0.5f,255,255,255))));
            break;
        case 6:
            LayerHintWithButton::ocultar(NULL);
            LayerHintWithButton::mostrar(LanguageManager::getInstance()->getStringForKey("SACKSCENE_HINT_5"));
            break;

    }
    if(mHintStep>=7){
        BaseScene::HintStop(NULL);
    }
   

    
}

void SacksScene::HintCallback(bool accepted){
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        pintarSacos();
        mSituacionHint=true;
        mHintStep=0;
        LayerHintOk::mostrar(true);
        SacksScene::HintStart(NULL);
    
    }
}

void SacksScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}


