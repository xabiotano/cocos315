//
//  Palo.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Palo.h"
#include "SimpleAudioEngine.h"





Palo::Palo(){
    mState=PaloState(kPaloUnasigned);
}


Palo::~Palo(void){}


Palo* Palo::create()
{
    Palo *devolver= new Palo();
    devolver->autorelease();
    devolver->initWithTexture( Director::getInstance()->getTextureCache()->addImage("sacks/cartel_palo.png"));
    
  
    return devolver;
}



PaloState Palo::getState(){
    return mState;
}

void Palo::setState(PaloState state){
    this->mState=state;
}










