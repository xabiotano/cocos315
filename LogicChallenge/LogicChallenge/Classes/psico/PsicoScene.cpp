#include "PsicoScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "../widgets/Boton.h"
#include "../sql/SQLiteHelper.h"
#include "../helpers/LogicSQLHelper.h"
#include "../Levelsv2.h"


using namespace cocos2d;
using namespace CocosDenshion;


PsicoScene* instancia_psico;



Scene* PsicoScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    PsicoScene *layer = PsicoScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool PsicoScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    mNombrePantalla="PsicoScene";
    instancia_psico=this;
    mLuchaPorItems=false;
    mTipoNivel=kNivelElixir;
    mEsPosibleCompartirEnWhatssap=true;
    
    mOpcionElegida=false;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();

    Sprite* mFondo = Sprite::create("psico/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    
    mFondoBlack = Sprite::create("psico/background_black.png");
    
    // position the sprite on the center of the screen
    mFondoBlack->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(mFondoBlack,5);
    mFondoBlack->setOpacity(0.f);
    
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    mFondoBlack->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    
    
    cargarPrueba();
    
    cargarEnergyDrinks();
    LogicSQLHelper::getInstance().sumarIntentoSQL(Variables::LEVELS_ELIXIR_CLICKADO);
   
    srand(time(NULL));
    mTipoAnuncio= rand() % 2;
    //num_pistas=1;
    CCLOG("mTipoAnuncio %i",mTipoAnuncio);
    

  
    
    
    
    return true;
}

int RandomInt2(int min, int max)
{
    return min + (rand() % (int)(max - min + 1));
}

void PsicoScene::cargarPrueba(){
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    int mIdNivel=Variables::LEVELS_ELIXIR_CLICKADO;
    sprintf (sqlgenerado, "SELECT * from niveles where idnivel=%i",mIdNivel);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, cargarNiveltexto,this, &SQLiteHelper::zErrMsg);
    CCLOG("%s",sqlgenerado);
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }

}
// This is the callback function to display the select data in the table
int PsicoScene::cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName)
{
    PsicoScene* psicoScene= (PsicoScene*) NotUsed;
    for(int i=0; i<argc; i++)
    {
        /*if(strcmp( azColName[i],"id")==0){
            
        }else */
        if(strcmp( azColName[i],"textonivel")==0){
            psicoScene->mNumeroPrueba=atoi(argv[i]);
        }else if(strcmp( azColName[i],"solucion")==0){
            psicoScene->mSolucion=atoi(argv[i]);
        }else if(strcmp( azColName[i],"intentos")==0){
            psicoScene->mIntentos=atoi(argv[i]);
        }else if(strcmp( azColName[i],"superado")==0){
            psicoScene->mSuperado=atoi(argv[i]);
        }else if(strcmp( azColName[i],"estrellas")==0){
            psicoScene->mNumeroEstrellas=atoi(argv[i]);
        }
    }
    psicoScene->crearEscena();
    return 0;
}



void PsicoScene::crearEscena(){

    CCLOG("mNumeroPrueba %i",mNumeroPrueba);
    mShowButtons=Boton::createBoton(this, menu_selector(PsicoScene::showButtons), "psico/solve_button.png");
    mShowButtons->setPosition(Point(mVisibleSize.width*0.5f,mVisibleSize.height*0.25f));
    mShowButtons->setTexto(LanguageManager::getInstance()->getStringForKey("PSICOSCENE_BUTTON", "PSICOSCENE_BUTTON"));
    mShowButtons->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateTo::create(0.75f,2),RotateTo::create(0.75f,-2))));
    mShowButtons->setSound("sounds/serieslogic/insert_letter.mp3");
    this->addChild(mShowButtons);
    
    char memory[200];
    sprintf(memory,"psico/pruebas/%i/prueba%i.png",mNumeroPrueba,mNumeroPrueba);
    
    Sprite* prueba=Sprite::create(memory);
    prueba->setPosition(Point(mVisibleSize.width*0.5f,mVisibleSize.height*0.60f));
    this->addChild(prueba,5);
    
    
    
    
    Sprite* arana=Sprite::create("psico/arana.png");
    int r3 = rand();
    float destinoY;
    if(r3 % 2==0){
        destinoY=0.25f;
    }else{
        destinoY=0.7f;
    }
    arana->setPosition(Point(mVisibleSize.width*destinoY,mVisibleSize.height+ arana->getContentSize().height/2));
    this->addChild(arana);
    arana->runAction(MoveBy::create(1.f, Point(0,-arana->getContentSize().height*0.7f)));

}




void PsicoScene::showButtons(Ref* sender){
    
    if(mLayerBotones!=NULL){
        mLayerBotones->removeFromParent();
        mLayerBotones=NULL;
    }
    mFondoBlack->runAction(FadeIn::create(0.5f));
    mLayerBotones=LayerColor::create(Color4B(0,0,0,0),  mVisibleSize.width, mVisibleSize.width*0.4f);
    mLayerBotones->setPosition(Point(0,-mLayerBotones->getContentSize().height/2));
 
    
    mBoton1=Boton::createBoton(this, menu_selector(PsicoScene::opcion1),"psico/button_psico_verde.png" );
    mBoton2=Boton::createBoton(this, menu_selector(PsicoScene::opcion2),"psico/button_psico_naranja.png" );
    mBoton3=Boton::createBoton(this, menu_selector(PsicoScene::opcion3),"psico/button_psico_azul.png" );
    mBoton4=Boton::createBoton(this, menu_selector(PsicoScene::opcion4),"psico/button_psico_morado.png" );
   
    
    mBoton1->setPosition(Point(mLayerBotones->getContentSize().width*0.125f,mLayerBotones->getContentSize().height*0.5f));
    mBoton2->setPosition(Point(mLayerBotones->getContentSize().width*0.375f,mLayerBotones->getContentSize().height*0.5f));
    mBoton3->setPosition(Point(mLayerBotones->getContentSize().width*0.625f,mLayerBotones->getContentSize().height*0.5f));
    mBoton4->setPosition(Point(mLayerBotones->getContentSize().width*0.875f,mLayerBotones->getContentSize().height*0.5f));

    
   // mBoton1->setColorBoton(Color3B((48,135,39));
   // mBoton2->setColorBoton(Color3B((70,140,210));
    //mBoton3->setColorBoton(Color3B((231,61,87));
   // mBoton4->setColorBoton(Color3B((201,86,179));
    
    mBoton1->setOpacity(255);
    mBoton2->setOpacity(255);
    mBoton3->setOpacity(255);
    mBoton4->setOpacity(255);
    
    char memory[200];
    sprintf(memory,"psico/pruebas/%i/%i_opcion1.png",mNumeroPrueba,mNumeroPrueba);
    mOpcion1Dibu=Sprite::create(memory);
    mOpcion1Dibu->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mBoton1->addChild(mOpcion1Dibu);
    
    sprintf(memory,"psico/pruebas/%i/%i_opcion2.png",mNumeroPrueba,mNumeroPrueba);
    mOpcion2Dibu=Sprite::create(memory);
    mOpcion2Dibu->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mBoton2->addChild(mOpcion2Dibu);
    
    sprintf(memory,"psico/pruebas/%i/%i_opcion3.png",mNumeroPrueba,mNumeroPrueba);
    CCLOG("%s",memory);
    mOpcion3Dibu=Sprite::create(memory);
    mOpcion3Dibu->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mBoton3->addChild(mOpcion3Dibu);
    
     sprintf(memory,"psico/pruebas/%i/%i_opcion4.png",mNumeroPrueba,mNumeroPrueba);
    mOpcion4Dibu=Sprite::create(memory);
    mOpcion4Dibu->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mBoton4->addChild(mOpcion4Dibu);
    
    
    
    mBoton1->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton2->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton3->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton4->setSound("sounds/ui/elegirrespuesta.mp3");
    
    
    
    Boton* close=Boton::createBoton(this, menu_selector(PsicoScene::closeButtons),"psico/close_button.png");
    close->setPosition(Point(mLayerBotones->getContentSize().width*0.9f,mLayerBotones->getContentSize().height*0.95f));
    mLayerBotones->addChild(close,2);
    close->setSound("sounds/ui/popup_close.mp3");
    close->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.5f, 15), RotateBy::create(0.5f, -15))));
    
    mLayerBotones->addChild(mBoton1);
    mLayerBotones->addChild(mBoton2);
    mLayerBotones->addChild(mBoton3);
    mLayerBotones->addChild(mBoton4);
    
    mLayerBotones->runAction(MoveTo::create(0.5f, Point(0,0)));
    mShowButtons->runAction(MoveTo::create(0.5f, Point(mShowButtons->getPositionX(),-mShowButtons->getContentSize().height/2)));
    
    this->addChild(mLayerBotones,6);
    
}

void PsicoScene::opcion1(Ref* sender){
    opcionClickada(1);
}
void PsicoScene::opcion2(Ref* sender){
     opcionClickada(2);
}
void PsicoScene::opcion3(Ref* sender){
     opcionClickada(3);
}
void PsicoScene::opcion4(Ref* sender){
     opcionClickada(4);
}
void PsicoScene::opcionClickada(int opcion){
    if(mOpcionElegida){
        return;
    }
    
    mOpcionElegida=true;
    CCLOG("%i",opcion);
    if(opcion==mSolucion){
        mSuccess=true;
    }else{
        mGameover=true;
        mTextoGameOver=std::string(LanguageManager::getInstance()->getStringForKey("PSICOSCENE_GAMEOVER","Sorry, you choosed a incorrect answer"));
    }
    // this->scheduleOnce(schedule_selector(PsicoScene::checkFinish),0.2f);
   
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(PsicoScene::checkFinish,this));
    this->runAction(Sequence::create(DelayTime::create(0.2f), funcion,NULL));

}


void PsicoScene::closeButtons(Ref* sender){
    mLayerBotones->runAction(MoveTo::create(0.5f, Point(mLayerBotones->getPositionX(),-mVisibleSize.height)));
    
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(PsicoScene::removePanel,this));
    this->runAction(Sequence::create(DelayTime::create(0.5f), funcion,NULL));
    
  //  this->scheduleOnce(schedule_selector(PsicoScene::removePanel), 0.5f);
    mFondoBlack->runAction(FadeOut::create(0.5f));

}

void PsicoScene::removePanel(Ref* sender){
    mShowButtons->runAction(MoveTo::create(0.5f, Point(mShowButtons->getPositionX(),mVisibleSize.height*0.25f)));
};

PsicoScene* PsicoScene::getInstance(){
    return instancia_psico;
};



void PsicoScene::Help(Ref* sender){
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("PSICOSCENE_HELP","Click solve button and choose the right answer!"));
    BaseScene::Help(NULL);
};


void PsicoScene::Retry(Ref* sender){
    Director::getInstance()->replaceScene(PsicoScene::scene());
};

void PsicoScene::Hint(Ref* sender){
    if(mLayerBotones!=NULL){
        closeButtons(NULL);
    }
    BaseScene::Hint(NULL);
}


void PsicoScene::HintStart(Ref* sender){
    
    if(mHintStep==1){
        if(mSolucion!=1){
            mBoton1->runAction(FadeOut::create(0.5f));
            mOpcion1Dibu->runAction(FadeOut::create(0.5f));
        }
        if(mSolucion!=2){
            mBoton2->runAction(FadeOut::create(0.5f));
            mOpcion2Dibu->runAction(FadeOut::create(0.5f));
        }
        if(mSolucion!=3){
            mBoton3->runAction(FadeOut::create(0.5f));
            mOpcion3Dibu->runAction(FadeOut::create(0.5f));
        }
        if(mSolucion!=4){
            mBoton4->runAction(FadeOut::create(0.5f));
            mOpcion4Dibu->runAction(FadeOut::create(0.5f));
        }
    }
    else if(mHintStep==2){
        if(mSolucion!=1){
            mBoton1->removeFromParent();
            
        }
        if(mSolucion!=2){
             mBoton2->removeFromParent();
        }
        if(mSolucion!=3){
             mBoton3->removeFromParent();
        }
        if(mSolucion!=4){
             mBoton4->removeFromParent();
        }
    }else if(mHintStep==2){
        BaseScene::HintStop(NULL);
    }
    mHintStep++;
}



void PsicoScene::HintCallback(bool accepted){
    
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        mOpcionElegida=false;
        mSituacionHint=true;
        mHintStep=1;
        showButtons(NULL);
        
        
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(PsicoScene::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(0.5f), funcion,DelayTime::create(0.5f),funcion,NULL));

        
        
        //runAction(Sequence::create(DelayTime::create(0.5f), CCCallFunc::create(this, callfunc_selector(PsicoScene::HintStart)),DelayTime::create(0.5f),CCCallFunc::create(this, callfunc_selector(PsicoScene::HintStart)),NULL));
      /*  mScheudleHint=schedule_selector(PsicoScene::HintStart);
        this->schedule(mScheudleHint,1.f);*/
        
    }else{
        if(mOpcionElegida){
            //si habia elegido alguna opcion resetamos el nivel
            Retry(NULL);
        }
    }
}

void PsicoScene::Back(Ref* sender){
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, Levelsv2::scene()));
}




void PsicoScene::cargarEnergyDrinks()
{
   
    if(mIntentos==0)
    {
        mLuchaPorItems=true;
        //temporizador
        mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
        mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.08f,mVisibleSize.height*0.7f));
        addChild(mFondo_temporizador);
        mFondo_temporizador->setScale(0.7f);
        
     
        
        if(mSuperado){
            //los que gane en su dia
            mEnergyDrinkAspirables=mNumeroEstrellas;
        }else{
            //los que pueda aspirar
            if(mIntentos>0){
                mEnergyDrinkAspirables=1;
            }else{
                mEnergyDrinkAspirables=3;
            }
        }
        mStars=mEnergyDrinkAspirables;
        dibujarEnergyDrink(mEnergyDrinkAspirables);
        
        NUMERO_SEGUNDOS_MAX=50.f;
        mNumeroSegundos=NUMERO_SEGUNDOS_MAX;
        mProgresoEnergyDrink = ProgressTimer::create(Sprite::create("elixircomun/temporizador_energy.png"));
        mProgresoEnergyDrink->setType(ProgressTimer::Type::RADIAL);
        mProgresoEnergyDrink->setPercentage(100.f);
        mProgresoEnergyDrink->setPosition(mFondo_temporizador->getPosition());
        addChild(mProgresoEnergyDrink,2);
        mProgresoEnergyDrink->setScale(0.7f);
        
        if(mEnergyDrinkAspirables>1 && mIntentos==0){
            mSchedhuleCountdown=schedule_selector(PsicoScene::countDown);
            this->schedule(mSchedhuleCountdown, 1.f);
            
        }else{
            mProgresoEnergyDrink->setPercentage(0.f);
        }
    }else{
        
        
        float scale=0.5f;
        float rotateValue=20.f;
        float scaleCorona=0.8f;
        if(!mSuperado){
            
            mEnergyDrinkAspirables=1;
            mStars=mEnergyDrinkAspirables;
            mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
            mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.08f,mVisibleSize.height*0.7f));
            addChild(mFondo_temporizador);
            mFondo_temporizador->setScale(0.7f);
            dibujarEnergyDrink(mEnergyDrinkAspirables);
        }else{
            /*mStars=mNumeroEstrellas;
            if(mNumeroEstrellas>=1){
                Sprite* granoganado1=Sprite::create("series/energy_drink.png");
                granoganado1->setScale(scale);
               // granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.10f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                Sprite* corona=Sprite::create("ui/layerganaritem/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }
            if(mNumeroEstrellas>=2){
                Sprite* granoganado1=Sprite::create("series/energy_drink.png");
                granoganado1->setScale(scale);
                //  granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                 Sprite* corona=Sprite::create("ui/layerganaritem/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona,2);
            }
            if(mNumeroEstrellas>=3){
                Sprite* granoganado1=Sprite::create("series/energy_drink.png");
                granoganado1->setScale(scale);
                 // granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.20f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                 Sprite* corona=Sprite::create("ui/layerganaritem/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }*/
        }
    }
    
}



void PsicoScene::countDown(float dt){
    mNumeroSegundos--;
    float from=(mNumeroSegundos/NUMERO_SEGUNDOS_MAX)*100.f;
    float to=((mNumeroSegundos-1)/NUMERO_SEGUNDOS_MAX)*100.f;
    mProgresoEnergyDrink->runAction(ProgressFromTo::create(1.f, from,to));
    if(mNumeroSegundos<=NUMERO_SEGUNDOS_MAX/2 && mStars==3){
        Sprite* cafe2= (Sprite*) mFondo_temporizador->getChildByTag(PsicoScene::TAG_ENERGY_DRINK_2);
        cafe2->runAction(FadeTo::create(1.f,0.f));
        Sprite* x3=(Sprite*)mFondo_temporizador->getChildByTag(PsicoScene::TAG_X3);
        if(x3!=NULL){
            x3->runAction(FadeOut::create(0.5f));
            x3->runAction(ScaleTo::create(0.5f, 0.f));
        }
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(PsicoScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(Sequence::createWithTwoActions(DelayTime::create(0.7f), FadeIn::create(0.8f)));
        }
        
        mStars--;
        CCLOG("reduccion starts %i",mStars);
    }
    if(mNumeroSegundos<=0 && mStars==2){
        Sprite* cafe1=(Sprite*) mFondo_temporizador->getChildByTag(PsicoScene::TAG_ENERGY_DRINK_3);
        cafe1->runAction(FadeTo::create(1.f,0.f));
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(PsicoScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(FadeOut::create(0.5f));
            x2->runAction(ScaleTo::create(0.5f, 0.f));
        }
        
        mStars--;
        this->unschedule(mSchedhuleCountdown);
        mSchedhuleCountdown=NULL;
        CCLOG("reduccion starts %i",mStars);
    }
    
    
}



void PsicoScene::dibujarEnergyDrink(int numero){
    Texture2D * elixirTex=Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_s.png");
    
    if(numero==1){
        Sprite* cafe1=Sprite::createWithTexture(elixirTex);
        cafe1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.5f));
        mFondo_temporizador->addChild(cafe1);
        cafe1->setScale(0.8f);
        cafe1->setRotation(-20);
        
    }else if(numero>1){
        Sprite* cafe1=Sprite::createWithTexture(elixirTex);
        cafe1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.32f,mFondo_temporizador->getContentSize().height*0.38f));
        mFondo_temporizador->addChild(cafe1);
        cafe1->setScale(0.6f);
        cafe1->setRotation(40);
        
        
    }
    if(numero>=2){
        Sprite* cafe2=Sprite::createWithTexture(elixirTex);
        cafe2->setPosition(Point(mFondo_temporizador->getContentSize().width*0.7f,mFondo_temporizador->getContentSize().height*0.4f));
        mFondo_temporizador->addChild(cafe2);
        cafe2->setScale(0.6f);
        cafe2->setRotation(-40);
        cafe2->setTag(PsicoScene::TAG_ENERGY_DRINK_2);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x2.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setOpacity(0.f);
        multiplicador->setTag(PsicoScene::TAG_X2);
        multiplicador->setScale(1.5f);
        
    }
    if(numero==3){
        
        Sprite* cafe3=Sprite::createWithTexture(elixirTex);
        cafe3->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.7f));
        mFondo_temporizador->addChild(cafe3);
        cafe3->setScale(0.6f);
        cafe3->setRotation(90);
        cafe3->setTag(PsicoScene::TAG_ENERGY_DRINK_3);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x3.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setTag(PsicoScene::TAG_X3);
        multiplicador->setScale(1.5f);
        
    }
    
}

void PsicoScene::checkFinish(Ref* sender){
    if(mSuccess){
        if(!mSuperado){
            LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(true,Variables::LEVELS_ELIXIR_CLICKADO,1);
            //bloqueo que puedan pretar los botones de atras
            this->addChild(LayerBlock::create(),5);
            LayerGanarItem::mostrar(mStars);
            return;
        }
    }
    BaseScene::checkFinish(NULL);
};

template <typename T> std::string PsicoScene::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }


bool PsicoScene::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}


void PsicoScene::onEnterTransitionDidFinish(){
   LogicSQLHelper::getInstance().sumarIntentoSQL(Variables::LEVELS_ELIXIR_CLICKADO);
    BaseScene::onEnterTransitionDidFinish();
}

