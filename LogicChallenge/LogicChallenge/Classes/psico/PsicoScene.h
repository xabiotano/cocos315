#ifndef __PsicoScene_SCENE_H__
#define __PsicoScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"

USING_NS_CC;


class PsicoScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(PsicoScene);
    static PsicoScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Back(Ref* sender);
    virtual void Help(Ref* sender);
    virtual void Retry(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
     virtual void TutorialStart(){};
    static int cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName);
    int mNumeroPrueba;
    void crearEscena();
private:
  
    Boton* mBoton1,*mBoton2,*mBoton3,*mBoton4;
    Sprite* mOpcion1Dibu,*mOpcion2Dibu,*mOpcion3Dibu,*mOpcion4Dibu;
    Label* mCounter;
    Boton* mShowButtons;
    LayerColor* mLayerBotones;
    void closeButtons(Ref* sender);
    void removePanel(Ref* sender);
    
    void showButtons(Ref* sender);
    void opcion1(Ref* sender);
    void opcion2(Ref* sender);
    void opcion3(Ref* sender);
    void opcion4(Ref* sender);
    void opcionClickada(int opcion);
    bool mOpcionElegida;
    void cargarPrueba();
    int mIdNivel;
    int mSolucion;
    bool mSuperado;
    Sprite* mFondoBlack;
    
    void cargarEnergyDrinks();
    //contador energy drink
    Sprite* mFondo_serie;
    ProgressTimer* mProgresoEnergyDrink;
    
    bool mGameFinished;
    
    int mEnergyDrinkAspirables;
    Sprite *mFondo_temporizador;
    void dibujarEnergyDrink(int numero);
    static  std::string SERIES_NUMERO_ENERGY_DRINK_CONSEGUIDOS;
    float mNumeroSegundos;
    float NUMERO_SEGUNDOS_MAX;
    void countDown(float dt);
    SEL_SCHEDULE mSchedhuleCountdown;

    
    const int TAG_ENERGY_DRINK_2=840;
    const int TAG_ENERGY_DRINK_3=841;
    const int TAG_X2=8472;
    const int TAG_X3=29402;
    
    bool mLuchaPorItems;
    int mTipoAnuncio;
    int mIntentos;
    int mNumeroEstrellas;
    template <typename T> std::string tostr(const T& t);

    virtual bool onTouchBegan(Touch* touch, Event* event);
    void onEnterTransitionDidFinish();
    void HintStartTemp(float dt);
};


#endif // __PsicoScene_SCENE_H__
