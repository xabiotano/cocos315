//
//  Bottle.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Bottle__
#define __logicmaster2__Bottle__

#include <iostream>
#include "../widgets/Boton.h"
#include "Bottle.fwd.h"


typedef enum bottleState
{
    kBottleBoxLeft,
    kBottleBoxRight,
    kNone
    
} BottleState;


class Bottle: public Boton
{
    
    
    public:
        Bottle(int capacity,float limitY);
        virtual ~Bottle(void);
        static cocos2d::Scene* scene();
        static Bottle* create(Node* parent,int capacity,float limitY);
        void click(Ref* sender);
        int mCapacity;
        int getCapacity();
        int getWaterInside();
        int mZIndex;
        int mWaterInside;
        float mLimitY;
        void setLimitY(float limit);
        void setInCollision(bool collisioned,bool clockWise);
        bool getIsTurned();
        BottleState mState;
         void loseMilk(int quantity);
        void setNumberBig();
   
      
    private:
        void finFilling();
        bool mIsFilling;
        unsigned int mSoundEffect;
        void onTouchMoved(Touch* touch, Event* event) override;
        void onTouchEnded(Touch* touch, Event* event) override;
        bool mIsTurned;
        Bottle* mBottleCollisioned;
       // CCProgressFromTo *mProgresoLlenado;
        ProgressTimer* mProgresoLlenado;
        int fillWithMilk(int quantity);
        Sprite* mNumero;
        template <typename T> std::string tostr(const T& t);
        void mostarAlerta(bool clockWise);
        void quitarAlerta();
        Sprite* mFondoAlert;

    
};


#endif /* defined(__logicmaster2__Bottle__) */
