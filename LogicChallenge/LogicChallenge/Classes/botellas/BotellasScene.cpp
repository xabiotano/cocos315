#include "BotellasScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../layers/LayerBlock.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Bottle.h"

using namespace cocos2d;
using namespace CocosDenshion;


BotellasScene* instancia_botellas;


BotellasScene::~BotellasScene(void){
    CC_SAFE_RELEASE(mListaPasos);
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}
Scene* BotellasScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    BotellasScene *layer = BotellasScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool BotellasScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_botellas=this;
    mNombrePantalla="BotellasScene";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("bottles/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);

    
   
    
    mBotella3=Bottle::create(this, 3,mSize.height*0.1f);
    mBotella3->setPosition(Point(mSize.width/2,mSize.height/2));
    this->addChild(mBotella3,2);
   
    
    mBotella5=Bottle::create(this, 5,mSize.height*0.1f);
    mBotella5->setPosition(Point(mSize.width*0.25f,mSize.height/2));
    this->addChild(mBotella5,2);
    
    
    mBotella8=Bottle::create(this, 8,mSize.height*0.1f);
    mBotella8->setPosition(Point(mSize.width*0.75f,mSize.height/2));
    this->addChild(mBotella8,2);
    
    
    
    
    mBackBoxBlue=Sprite::create("bottles/back_box_blue.png");
    mBackBoxBlue->setPosition(Point(mSize.width*0.25f,mSize.height*0.16f));
    this->addChild(mBackBoxBlue,1);
    Sprite* front_box_blue=Sprite::create("bottles/front_box_blue.png");
    front_box_blue->setPosition(Point(mSize.width*0.25f,mSize.height*0.15f));
    this->addChild(front_box_blue,3);
    
    mBackBoxPink=Sprite::create("bottles/back_box_pink.png");
    mBackBoxPink->setPosition(Point(mSize.width*0.75f,mSize.height*0.16f));
    this->addChild(mBackBoxPink,1);
    Sprite* front_box_pink=Sprite::create("bottles/front_box_pink.png");
    front_box_pink->setPosition(Point(mSize.width*0.75f,mSize.height*0.15f));
    this->addChild(front_box_pink,3);
    
    
    mBotella3->setLimitY(mSize.height*0.09f );
    mBotella5->setLimitY(mSize.height*0.09f);
    mBotella8->setLimitY(mSize.height*0.09f);
    
    Boton* checkWeight=Boton::createBoton(this, menu_selector(BotellasScene::cargarAnimacionPesaje), "bottles/check.png");
    checkWeight->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height*0.87f));
    this->addChild(checkWeight);
    
    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_BOTELLAS");

    
    
    
    return true;
}





BotellasScene* BotellasScene::getInstance(){
    return instancia_botellas;
};

Bottle* BotellasScene::checkIfIstouching(Bottle* sender){
   
    Rect senderRect;
    if(sender->getIsTurned()){
        senderRect = Rect(sender->getPosition().x - (sender->getBoundingBox().size.width/2),
                                sender->getPosition().y - (sender->getBoundingBox().size.height/2),
                                sender->getBoundingBox().size.width,
                                sender->getBoundingBox().size.height);
    

    }
       
        senderRect = Rect(sender->getPosition().x - (sender->getBoundingBox().size.width*0.5f/2.f),
                                sender->getPosition().y - (sender->getBoundingBox().size.height*1.2f/2.f),
                                sender->getBoundingBox().size.width*0.5f,
                                sender->getBoundingBox().size.height*1.2f);
    
    
    
    Rect botella3Rect = Rect(mBotella3->getPosition().x - (mBotella3->getBoundingBox().size.width/2),
                                 mBotella3->getPosition().y - (mBotella3->getBoundingBox().size.height/2),
                                 mBotella3->getBoundingBox().size.width,
                                 mBotella3->getBoundingBox().size.height);
    Rect botella5Rect = Rect(mBotella5->getPosition().x - (mBotella5->getBoundingBox().size.width/2),
                                 mBotella5->getPosition().y - (mBotella5->getBoundingBox().size.height/2),
                                 mBotella5->getBoundingBox().size.width,
                                 mBotella5->getBoundingBox().size.height);

    Rect botella8Rect = Rect(mBotella8->getPosition().x - (mBotella8->getBoundingBox().size.width/2),
                                 mBotella8->getPosition().y - (mBotella8->getBoundingBox().size.height/2),
                                 mBotella8->getBoundingBox().size.width,
                                 mBotella8->getBoundingBox().size.height);

    
    
    //update status
    Rect boxLeft = Rect(mBackBoxBlue->getPosition().x - (mBackBoxBlue->getBoundingBox().size.width/2),
                                     mBackBoxBlue->getPosition().y - (mBackBoxBlue->getBoundingBox().size.height/2),
                                     mBackBoxBlue->getBoundingBox().size.width,
                                     mBackBoxBlue->getBoundingBox().size.height);
    
    Rect boxRight = Rect(mBackBoxPink->getPosition().x - (mBackBoxPink->getBoundingBox().size.width/2),
                                mBotella8->getPosition().y - (mBackBoxPink->getBoundingBox().size.height/2),
                                mBackBoxPink->getBoundingBox().size.width,
                                mBackBoxPink->getBoundingBox().size.height);

    if (boxLeft.intersectsRect(senderRect))
    {
         sender->mState=BottleState(kBottleBoxLeft);
        
    }else if(boxRight.intersectsRect(senderRect)){
         sender->mState=BottleState(kBottleBoxRight);
    }else{
          sender->mState=BottleState(kNone);
    }
    
    bool clockWise=false;
    if (senderRect.intersectsRect(botella3Rect) && sender->getCapacity()!=3)
    {
        clockWise=(sender->getPositionX()>mBotella3->getPositionX());
        sender->setInCollision(true,clockWise);
        //interseccion con la botella3
        CCLOG("3");
        return mBotella3;
    }else if (senderRect.intersectsRect(botella5Rect) && sender->getCapacity()!=5)
    {
        clockWise=(sender->getPositionX()>mBotella5->getPositionX());
        sender->setInCollision(true,clockWise);
        //interseccion con la botella5
        CCLOG("5");
        return mBotella5;
    }else if (senderRect.intersectsRect(botella8Rect) && sender->getCapacity()!=8)
    {
        //interseccion con la botella8
        clockWise=(sender->getPositionX()>mBotella8->getPositionX());
        sender->setInCollision(true,clockWise);
         CCLOG("8");
        return mBotella8;
    }else{
        CCLOG("NONE");
        sender->setInCollision(false,false);
        return NULL;
    }

    
   
    
}

void BotellasScene::cargarAnimacionPesaje(Ref* sender){
    
    int weightLeft=0;
    int weightRight=0;
    
    if(mBotella3->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella3->getWaterInside();
    }else if(mBotella3->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella3->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
        return;
    }
    
    if(mBotella5->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella5->getWaterInside();
    }else if(mBotella5->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella5->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
        return;
    }
    
    if(mBotella8->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella8->getWaterInside();
    }else if(mBotella8->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella8->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
        return;
    }
    
    

    LayerColor* degradado=LayerColor::create(Color4B(0,0,0,200), mVisibleSize.width, mVisibleSize.height);
    this->addChild(degradado,4);
    
    Sprite* weight_icon_left=Sprite::create("bottles/weight_icon.png");
    weight_icon_left->setPosition(Point(mVisibleSize.width*0.24f,mVisibleSize.height*0.35f));
    this->addChild(weight_icon_left,5);
    
    Sprite* weight_icon_right=Sprite::create("bottles/weight_icon.png");
     weight_icon_right->setPosition(Point(mVisibleSize.width*0.74f,mVisibleSize.height*0.35f));
    this->addChild(weight_icon_right,5);
    
    
    /*CCLabelStroke* pesoIzq=CCLabelStroke::create(weight_icon_left, "", CCGetFont(), 40, Size(100, 100), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    weight_icon_left->addChild(pesoIzq,2);
    pesoIzq->setPosition(Point(weight_icon_left->getContentSize().width/2,weight_icon_left->getContentSize().height*0.4f));
   
   
    
    pesoIzq->setString(memory, Color3B((0,0,0));*/
    char memory[200];
    sprintf(memory,"%i L",weightLeft);
    
    Label* pesoIzq= Label::createWithTTF(memory, CCGetFont(), 40);
    pesoIzq->setWidth(100);
    pesoIzq->setHeight(100);
    pesoIzq->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    pesoIzq->enableOutline(Color4B(0,0,0,255),2);
    weight_icon_left->addChild(pesoIzq,2);
    pesoIzq->setPosition(Point(weight_icon_left->getContentSize().width/2,weight_icon_left->getContentSize().height*0.4f));
   

    
    
    
    
    
   /* CCLabelStroke* pesoDch=CCLabelStroke::create(weight_icon_right, "8", CCGetFont(), 40, Size(100, 100), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    weight_icon_right->addChild(pesoDch,2);
    pesoDch->setPosition(Point(weight_icon_right->getContentSize().width/2,weight_icon_right->getContentSize().height*0.4f));
  
     sprintf(memory,"%i L",weightRight);
     pesoDch->setString(memory, Color3B((0,0,0));
    */
    sprintf(memory,"%i L",weightRight);
    
    
    
    Label* pesoDch= Label::createWithTTF(memory, CCGetFont(), 40);
    pesoDch->setColor(Color3B(255,255,255));
    pesoDch->setWidth(100);
    pesoDch->setHeight(100);
    pesoDch->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    pesoDch->enableOutline(Color4B(0,0,0,255),2);
   weight_icon_right->addChild(pesoDch,2);
    pesoDch->setPosition(Point(weight_icon_right->getContentSize().width/2,weight_icon_right->getContentSize().height*0.4f));

   

    
    
    
    
    LayerBlock* layerBlock=LayerBlock::create();
    this->addChild(layerBlock,6);
    layerBlock->setPosition(Point(0,0));
    layerBlock->setTag(LayerBlock::TAG_LAYER_BLOCK);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter.mp3", false);
    
     
    
    //this->scheduleOnce(schedule_selector(BotellasScene::checkFinish), 2.f);
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(BotellasScene::checkFinish,this));
    this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));

    
    
    
    
    
    
    
}

void BotellasScene::InfomoverBotellaBasket(){
    LayerInfo* layer=LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("DISTRIBUTE_BOTELLAS"));
    Sprite* fondo=(Sprite*)layer->getCanvas();
    Sprite* bottle= Sprite::create("bottles/bottle.png");
    Sprite* basket=Sprite::create("bottles/front_box_blue.png");
    bottle->setScale(0.15f);
    basket->setScale(0.2f);
    
   
    fondo->addChild(bottle);
    fondo->addChild(basket);
    
    bottle->setPosition(Point(fondo->getContentSize().width/2, fondo->getContentSize().height*0.2f));
    basket->setPosition(Point(fondo->getContentSize().width/2, fondo->getContentSize().height*0.15f));
    bottle->runAction(RepeatForever::create(Sequence::createWithTwoActions(MoveBy::create(1.f, Point(0,fondo->getContentSize().height*0.2f)), MoveTo::create(1.f, Point(fondo->getContentSize().width/2,fondo->getContentSize().height*0.2f)))));
                     

}

void BotellasScene::checkFinish(Ref* sender){
    
    this->removeChildByTag(LayerBlock::TAG_LAYER_BLOCK);
    int weightLeft=0;
    int weightRight=0;
    if(mBotella3->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella3->getWaterInside();
    }else if(mBotella3->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella3->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
        return;
    }
    
    if(mBotella5->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella5->getWaterInside();
    }else if(mBotella5->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella5->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
         return;
    }
    
    if(mBotella8->mState==BottleState(kBottleBoxLeft)){
        weightLeft+=mBotella8->getWaterInside();
    }else if(mBotella8->mState==BottleState(kBottleBoxRight)){
        weightRight+=mBotella8->getWaterInside();
    }else{
        BotellasScene::InfomoverBotellaBasket();
         return;
    }
    
    if(weightLeft==4 && weightRight==4){
        mSuccess=true;
        mStars=3;
    }else{
        mTextoGameOver= LanguageManager::getInstance()->getStringForKey("GAME_OVER_BOTELLAS");
        mGameover=true;
    }
  
    BaseScene::checkFinish(NULL);
    
};


void BotellasScene::Retry(Ref* sender){
    
    Director::getInstance()->replaceScene(BotellasScene::scene());
   
    
}


void BotellasScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void BotellasScene::HintCallback(bool accepted){
    if(mSituacionHint){
        return;
    }
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        mSituacionHint=true;
        mListaPasos=CCArray::create();
        mListaPasos->retain();
        //paso1
        Sprite* item =Sprite::create("ui/item_hint_panel.png");
        Size tamanoItem= item->getContentSize();
        float scale8=0.18;
        float scale5=0.13;
        float scale3=0.08;
        float fontSize=30;
        Color3B color=Color3B(167,126,88);
        
        Bottle* botella8_01=Bottle::create(this, 8,mSize.height*0.1f);
        botella8_01->loseMilk(8);
        botella8_01->setScale(scale8);
        botella8_01->setNumberBig();
        item->addChild(botella8_01);
        botella8_01->setEnabled(false);
        botella8_01->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label1=Label::createWithTTF("pours-to --->", CCGetFont(), fontSize);
        //CCLabelTTF::create("pours to --->", CCGetFont(), fontSize);
        label1->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item->addChild(label1);
        label1->setColor(color);
        
        Bottle* botella5_01=Bottle::create(this, 5,mSize.height*0.1f);
        item->addChild(botella5_01);
        botella5_01->setScale(scale5);
        botella5_01->setNumberBig();
        botella5_01->setEnabled(false);
        botella5_01->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        mListaPasos->addObject(item);
        
        //paso 2
        Sprite* item2 =Sprite::create("ui/item_hint_panel.png");
        
        Bottle* botella5_2=Bottle::create(this, 5,mSize.height*0.1f);
        item2->addChild(botella5_2);
        botella5_2->setNumberBig();
        botella5_2->setScale(scale5);
        botella5_2->setEnabled(false);
        botella5_2->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label2=Label::createWithTTF("pours to --->", CCGetFont(), fontSize);
        label2->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item2->addChild(label2);
        label2->setColor(color);
        
        Bottle* botella3_02=Bottle::create(this, 3,mSize.height*0.1f);
        item2->addChild(botella3_02);
        botella3_02->setNumberBig();
        botella3_02->setScale(scale3);
        botella3_02->setEnabled(false);
        botella3_02->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        
        mListaPasos->addObject(item2);
        

        //paso 3
        Sprite* item3 =Sprite::create("ui/item_hint_panel.png");
        
        Bottle* botella3_3=Bottle::create(this, 3,mSize.height*0.1f);
        item3->addChild(botella3_3);
        botella3_3->setNumberBig();
        botella3_3->setScale(scale3);
        botella3_3->setEnabled(false);
        botella3_3->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label3=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("POURS_TO"), CCGetFont(), fontSize);
        label3->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item3->addChild(label3);
        label3->setColor(color);
        
        Bottle* botella8_03=Bottle::create(this, 8,mSize.height*0.1f);
        item3->addChild(botella8_03);
        botella8_03->setNumberBig();
        botella8_03->setScale(scale8);
        botella8_03->loseMilk(8);
        botella8_03->setEnabled(false);
        botella8_03->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        
        mListaPasos->addObject(item3);
        
        
        
        //paso 4
        Sprite* item4 =Sprite::create("ui/item_hint_panel.png");
        
        Bottle* botella5_4=Bottle::create(this, 5,mSize.height*0.1f);
        item4->addChild(botella5_4);
        botella5_4->setNumberBig();
        botella5_4->setScale(scale5);
        botella5_4->setEnabled(false);
        botella5_4->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label4=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("POURS_TO"), CCGetFont(), fontSize);
        label4->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item4->addChild(label4);
        label4->setColor(color);
        
        Bottle* botella3_04=Bottle::create(this, 3,mSize.height*0.1f);
        item4->addChild(botella3_04);
        botella3_04->setNumberBig();
        botella3_04->setScale(scale3);
        botella3_04->setEnabled(false);
        botella3_04->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        
        mListaPasos->addObject(item4);

        //paso 5
        Sprite* item5 =Sprite::create("ui/item_hint_panel.png");
        
        Bottle* botella8_05=Bottle::create(this, 8,mSize.height*0.1f);
        botella8_05->setScale(scale8);
        botella8_05->setNumberBig();
        item5->addChild(botella8_05);
        botella8_05->loseMilk(8);
        botella8_05->setEnabled(false);
        botella8_05->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label5=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("POURS_TO"), CCGetFont(), fontSize);
        label5->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item5->addChild(label5);
        label5->setColor(color);
        
        Bottle* botella5_05=Bottle::create(this, 5,mSize.height*0.1f);
        item5->addChild(botella5_05);
        botella5_05->setNumberBig();
        botella5_05->setScale(scale5);
        botella5_05->setEnabled(false);
        botella5_05->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        mListaPasos->addObject(item5);

        //paso 6
        Sprite* item6 =Sprite::create("ui/item_hint_panel.png");
        
        Bottle* botella5_6=Bottle::create(this, 5,mSize.height*0.1f);
        item6->addChild(botella5_6);
        botella5_6->setNumberBig();
        botella5_6->setScale(scale5);
        botella5_6->setEnabled(false);
        botella5_6->setPosition(Point(tamanoItem.width*0.25f, tamanoItem.height/2));
        
        Label* label6=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("POURS_TO"), CCGetFont(), fontSize);
        label6->setPosition(Point(tamanoItem.width*0.5f, tamanoItem.height/2));
        item6->addChild(label6);
        label6->setColor(color);
        
        Bottle* botella3_06=Bottle::create(this, 3,mSize.height*0.1f);
        item6->addChild(botella3_06);
        botella3_06->setNumberBig();
        botella3_06->setScale(scale3);
        botella3_06->setEnabled(false);
        botella3_06->setPosition(Point(tamanoItem.width*0.75f, tamanoItem.height/2));
        mListaPasos->addObject(item6);
        
        //8-->5
        //5-->3
        //3-->8
        //5-->3
        //8-->5
        //5-->3
        LayerHintSteps::mostrar(mListaPasos);
    }
    
    
}

void BotellasScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/botellas/fondo_pajaros.mp3",true);
    BaseScene::onEnterTransitionDidFinish();
}








