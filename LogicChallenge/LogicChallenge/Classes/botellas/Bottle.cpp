//
//  Bottle.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Bottle.h"
#include "SimpleAudioEngine.h"
#include "BotellasScene.h"




Bottle::Bottle(int  capacity,float limitY){
    mCapacity=capacity;
    mLimitY=limitY;
    mIsTurned=false;
    mState=BottleState(kNone);
    
  

}

Bottle::~Bottle(void){}


Bottle* Bottle::create(Node* parent,int capacity,float limitY)
{
    Bottle *devolver= new Bottle(capacity,limitY);
    devolver->autorelease();
    devolver->initBoton(devolver, menu_selector(Bottle::click), "bottles/bottle.png");
    Texture2D *pTexture = Director::getInstance()->getTextureCache()->addImage("bottles/milk.png");
    devolver->mProgresoLlenado= ProgressTimer::create(Sprite::createWithTexture(pTexture));
    devolver->mProgresoLlenado->setType(ProgressTimer::Type::BAR);
    
    devolver->mProgresoLlenado->setMidpoint(Point(100,0));
    devolver->mProgresoLlenado->setBarChangeRate(Point(0,1));
    devolver->mProgresoLlenado->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().height/2));
    devolver->addChild(devolver->mProgresoLlenado,2);

    
    
    if(capacity==3){
        devolver->setScale(0.37f);
        devolver->mNumero=Sprite::create("bottles/3.png");
        devolver->mProgresoLlenado->setPercentage(0.f);
        devolver->mWaterInside=0;
       
    }else if(capacity==5){
        devolver->setScale(0.5f);
        devolver->mNumero=Sprite::create("bottles/5.png");
        devolver->mProgresoLlenado->setPercentage(0.f);
         devolver->mWaterInside=0;
    }else if(capacity==8){
        devolver->setScale(0.8f);
        devolver->mNumero=Sprite::create("bottles/8.png");
        devolver->mProgresoLlenado->setPercentage(100.f);
        devolver->mWaterInside=8;
    }
    devolver->mNumero->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().height/2));
    devolver->addChild(devolver->mNumero,3);
    return devolver;
}


void Bottle::setNumberBig(){
    std::string s= tostr(mCapacity);
    //CCLabelTTF* label= CCLabelTTF::create(s.c_str(), CCGetFont(), 120);
    Label* label=Label::createWithTTF(s.c_str(), CCGetFont(), 120);
    label->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height/2));
    this->addChild(label,3);
    label->setColor(Color3B(113,91,55));
    label->setScale(3.f);
    mNumero->removeFromParent();
}

void Bottle::click(Ref* sender)
{
    if(mIsFilling){
        return;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop.mp3",false);
    
}

void Bottle::finFilling(){
    mIsFilling=false;
 
}

int Bottle::getCapacity(){
    return mCapacity;
    
}

int Bottle::getWaterInside(){
    return mWaterInside;
    
}

void Bottle::setLimitY(float limit){
    mLimitY=limit;
}


void Bottle::onTouchMoved(Touch* touch, Event* event){
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
    if(puntonuevo.y - this->getBoundingBox().size.height/2>mLimitY ){
        this->setPosition(puntonuevo);
    }else{
         this->setPositionX(puntonuevo.x);
    }
    mBottleCollisioned=BotellasScene::getInstance()->checkIfIstouching(this);
    
   
}
void Bottle::onTouchEnded(Touch* touch, Event* event){
    if(mBottleCollisioned!=NULL){
        //hay que hacer la animacion de rellenado de botella a mBottleCollisioned
        
        int restante=mBottleCollisioned->fillWithMilk(this->mWaterInside);
        this->loseMilk(restante);
        mBottleCollisioned=NULL;
    }
    setInCollision(false,false);
}

void Bottle::setInCollision(bool collisioned,bool clockWise){
    
    if(collisioned){
        if(mIsTurned){
            return;
        }
        
        mostarAlerta(clockWise);
        if(clockWise){
             this->runAction(RotateTo::create(0.2f, -120));
        }else{
             this->runAction(RotateTo::create(0.2f, 120));
        }
        mIsTurned=true;
    }else{
        quitarAlerta();
        if(!mIsTurned){
            return;
        }
        this->runAction(RotateTo::create(0.2f, 0));
        mIsTurned=false;
    }
}



int Bottle::fillWithMilk(int quantity){
    int restante=0;
    int pendingTofill=quantity;
    if(quantity>0){
          CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/botellas/volcaragua.mp3", false);
    }
    //si no cabe toda
    if(quantity>mCapacity-mWaterInside){
        pendingTofill=mCapacity-mWaterInside;
        restante=quantity-(mCapacity-mWaterInside);
    }
    CCLOG("DESTINATION:pendingTofill:%i,restante:%i",pendingTofill,restante);
    mWaterInside+=pendingTofill;
    restante=quantity-pendingTofill;
    //lanzo la accion de llenado
    float percentage=((float)mWaterInside/(float)mCapacity) *100.f;
    mProgresoLlenado->setPercentage(percentage);
    return pendingTofill;
}


void Bottle::loseMilk(int quantity){
    CCLOG("ORIGIN:loseMilk %i",quantity);
    mWaterInside-=quantity;
    float percentaje=((float)mWaterInside/(float)mCapacity) *100.f;
    CCLOG("ORIGIN:%i %i percentaje %f",mWaterInside,mCapacity,percentaje);
    this->mProgresoLlenado->setPercentage(percentaje);
}




bool Bottle::getIsTurned(){
    return mIsTurned;
}
template <typename T> std::string Bottle::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }


void Bottle::mostarAlerta(bool clockWise){
    Size visibleSize = this->getContentSize();
    if(mFondoAlert!=NULL && this->getChildByTag(8252)!=NULL){
        this->removeChildByTag(8252);
    }
  
  
    mFondoAlert=Sprite::create("bottles/release_alert.png");
    mFondoAlert->setOpacity(0.f);
    mFondoAlert->runAction(RepeatForever::create(Sequence::createWithTwoActions(EaseOut::create(FadeIn::create(1.f), 3),EaseOut::create(FadeOut::create(0.2f), 3))));
    mFondoAlert->setPosition(Point(visibleSize.width*0.5f,0 - visibleSize.height*0.18f));
    this->addChild(mFondoAlert);
    mFondoAlert->setTag(8252);
    
    /*CCLabelStroke* label= CCLabelStroke::create(mFondoAlert, "Hola", CCGetFont(), 30, Size(mFondoAlert->getContentSize().width*0.8,mFondoAlert->getContentSize().height*0.8f), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mFondoAlert->addChild(label,2);
    label->setPosition(Point(mFondoAlert->getContentSize().width/2,mFondoAlert->getContentSize().height/2));
    label->setColor(Color3B(255,255,255));
    label->setString(LanguageManager::getInstance()->getStringForKey("BOTTLE_TEXTO_VERTER", "BOTTLE_TEXTO_VERTER"), Color3B((0,0,0));
    */
    
    Label* label = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("BOTTLE_TEXTO_VERTER"), CCGetFont(), 25);
    label->setOverflow(Label::Overflow::SHRINK);
    label->setWidth(mFondoAlert->getContentSize().width*0.75);
    label->setHeight(mFondoAlert->getContentSize().height*0.75f);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255),2);
       label->setPosition(Point(mFondoAlert->getContentSize().width/2,mFondoAlert->getContentSize().height/2));
    label->setColor(Color3B(255,255,255));
    mFondoAlert->addChild(label,2);

    
    
     if(clockWise){
        mFondoAlert->setRotation(90+80);
    }else{
       mFondoAlert->setRotation(-90-80);
    }
    mFondoAlert->setScale(1/getScale());
    
}

void Bottle::quitarAlerta(){
    
    if(mFondoAlert!=NULL && this->getChildByTag(8252)!=NULL){
        this->removeChildByTag(8252);
    }
    
}



