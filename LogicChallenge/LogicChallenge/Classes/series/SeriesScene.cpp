#include "SeriesScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../layers/LayerBlock.h"
#include "../Variables.h"
#include "../Levels.h"
#include "../sql/SQLiteHelper.h"
#include "../helpers/LogicSQLHelper.h"
#include "../Levelsv2.h"
#include "../AdHelper.h"

using namespace cocos2d;
using namespace CocosDenshion;



SeriesScene* instancia_series;
std::string SeriesScene::SERIES_NUMERO_GRANOS_CAFE_CONSEGUIDOS=std::string("GRANOS_CAFE_CONSEGUIDOS_");


Scene* SeriesScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    SeriesScene *layer = SeriesScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool SeriesScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    mNombrePantalla="SeriesScene";
    instancia_series=this;
    mTipoNivel=kNivelElixir ;
    mLuchaPorElixir=false;
    mZoomActivado=false;
    mEsPosibleCompartirEnWhatssap=true;
    
    Size frame=Director::getInstance()->getOpenGLView()->getFrameSize();
    //CCLOG("frame size%f %f ",frame.width,frame.height );
    mVisibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
   
    
 
    
    Sprite* mFondo = Sprite::create("series/background.png");
    mFondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    this->addChild(mFondo);
    
    float scalex=mVisibleSize.width/mFondo->getContentSize().width;
    float scaley=mVisibleSize.height/mFondo->getContentSize().height;
   // CCLOG("content size%f %f ",layer->getContentSize().width,layer->getContentSize().height);
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
   
    
    cargarControles();
    cargarSerie();
    
   
    srand(time(NULL));
    mTipoAnuncio= rand() % 2;
    //num_pistas=1;
    CCLOG("mTipoAnuncio %i",mTipoAnuncio);
    if(mTipoAnuncio>0){
        //sdkbox::PluginChartboost::cache(sdkbox::CB_Location_LevelComplete);
    }else{
       // sdkbox::PluginChartboost::cache(sdkbox::CB_Location_Default);
    }
    return true;
}

int RandomInt(int min, int max)
{
    return min + (rand() % (int)(max - min + 1));
}

void SeriesScene::cargarSerie(){
    SQLiteHelper::openDB();
 
    char sqlgenerado [400];
    int mIdNivel=Variables::LEVELS_ELIXIR_CLICKADO;
    
    sprintf (sqlgenerado, "SELECT * from niveles where idnivel=%i",mIdNivel);
    CCLOG("%s",sqlgenerado);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, cargarNivelSeries,this, &SQLiteHelper::zErrMsg);
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
    

}

void SeriesScene::cargarElixires()
{
    
    if(mNumeroIntentos==0)
    {
        mLuchaPorElixir=true;
        //temporizador
        mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
        mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.7f));
        addChild(mFondo_temporizador);
        mFondo_temporizador->setScale(0.7f);
    
        if(mSuperado){
            //los que gane en su dia
            mGranosAspirables=mNumEstrellas;
        }else{
            //los que pueda aspirar
            if(mNumeroIntentos>0){
                mGranosAspirables=1;
            }else{
                mGranosAspirables=3;
            }
        }
        
        mStars=mGranosAspirables;
        dibujarGranosCafe(mGranosAspirables);
    
        NUMERO_SEGUNDOS_MAX=50.f;
        mNumeroSegundos=NUMERO_SEGUNDOS_MAX;
        mProgresoCafeina = ProgressTimer::create(Sprite::create("elixircomun/temporizador_energy.png"));
        mProgresoCafeina->setType(ProgressTimer::Type::RADIAL); // This is for round progress timer. Possible value for horizontal bar
        mProgresoCafeina->setPercentage(100.f);
        mProgresoCafeina->setPosition(mFondo_temporizador->getPosition());// It's your position
        addChild(mProgresoCafeina,2);
        mProgresoCafeina->setScale(0.7f);
        
        if(mGranosAspirables>1 && mNumeroIntentos==0){
            mSchedhuleCountdown=schedule_selector(SeriesScene::countDown);
            this->schedule(mSchedhuleCountdown, 1.f);

        }else{
            mProgresoCafeina->setPercentage(0.f);
        }
    }else{
        
        
        float scale=0.4f;
        float rotateValue=20.f;
        float scaleCorona=0.8f;
        if(!mSuperado){
            mGranosAspirables=1;
            mStars=mGranosAspirables;
            mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
            mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.7f));
            addChild(mFondo_temporizador);
            mFondo_temporizador->setScale(0.7f);
            dibujarGranosCafe(mGranosAspirables);
            mLuchaPorElixir=false;

        }else{
            mStars=mNumEstrellas;
            /*if(mNumEstrellas>=1){
                Sprite* granoganado1=Sprite::create("series/cofee.png");
                granoganado1->setScale(scale);
                granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.10f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                Sprite* corona=Sprite::create("ui/layerganaritem/king.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }
            if(mNumEstrellas>=2){
                Sprite* granoganado1=Sprite::create("series/cofee.png");
                granoganado1->setScale(scale);
                granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                Sprite* corona=Sprite::create("ui/layerganaritem/king.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }
            if(mNumEstrellas>=3){
                Sprite* granoganado1=Sprite::create("series/cofee.png");
                granoganado1->setScale(scale);
                granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.20f,mVisibleSize.height*0.75f));
                addChild(granoganado1);
                Sprite* corona=Sprite::create("ui/layerganaritem/king.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }*/
        }
    }
   
}

void SeriesScene::countDown(float dt){
    mNumeroSegundos--;
    float from=(mNumeroSegundos/NUMERO_SEGUNDOS_MAX)*100.f;
    float to=((mNumeroSegundos-1)/NUMERO_SEGUNDOS_MAX)*100.f;
    mProgresoCafeina->runAction(ProgressFromTo::create(1.f, from,to));
    //mProgresoCafeina->setPercentage((mNumeroSegundos/NUMERO_SEGUNDOS_MAX)*100.f);
    if(mNumeroSegundos<=NUMERO_SEGUNDOS_MAX/2 && mStars==3){
        Sprite* cafe2= (Sprite*) mFondo_temporizador->getChildByTag(SeriesScene::TAG_GRANO_CAFE_2);
        cafe2->runAction(FadeTo::create(1.f,0.f));
        Sprite* x3=(Sprite*)mFondo_temporizador->getChildByTag(SeriesScene::TAG_X3);
        if(x3!=NULL){
            x3->runAction(FadeOut::create(0.5f));
            x3->runAction(ScaleTo::create(0.5f, 0.f));
        }
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(SeriesScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(Sequence::createWithTwoActions(DelayTime::create(0.7f), FadeIn::create(0.8f)));
        }
      
        mStars--;
        CCLOG("reduccion starts %i",mStars);
    }
    if(mNumeroSegundos<=0 && mStars==2){
        Sprite* cafe1=(Sprite*) mFondo_temporizador->getChildByTag(SeriesScene::TAG_GRANO_CAFE_3);
        cafe1->runAction(FadeTo::create(1.f,0.f));
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(SeriesScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(FadeOut::create(0.5f));
            x2->runAction(ScaleTo::create(0.5f, 0.f));
        }
       
        mStars--;
        this->unschedule(mSchedhuleCountdown);
        mSchedhuleCountdown=NULL;
        CCLOG("reduccion starts %i",mStars);
        mLuchaPorElixir=false;
    }
   
    
}



void SeriesScene::dibujarGranosCafe(int numero){
    Texture2D * elixirTex=Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_s.png");

    
    if(numero==1){
        Sprite* cafe1=Sprite::createWithTexture(elixirTex);
        cafe1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.5f));
        mFondo_temporizador->addChild(cafe1);
        cafe1->setScale(0.8f);
        cafe1->setRotation(-20);
        
    }else if(numero>1){
        Sprite* cafe1=Sprite::createWithTexture(elixirTex);
        cafe1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.32f,mFondo_temporizador->getContentSize().height*0.38f));
        mFondo_temporizador->addChild(cafe1);
        cafe1->setScale(0.6f);
        cafe1->setRotation(40);
        
        
    }
    if(numero>=2){
        Sprite* cafe2=Sprite::createWithTexture(elixirTex);
        cafe2->setPosition(Point(mFondo_temporizador->getContentSize().width*0.7f,mFondo_temporizador->getContentSize().height*0.4f));
        mFondo_temporizador->addChild(cafe2);
        cafe2->setScale(0.6f);
        cafe2->setRotation(-40);
        cafe2->setTag(SeriesScene::TAG_GRANO_CAFE_2);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x2.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setOpacity(0.f);
        multiplicador->setTag(SeriesScene::TAG_X2);
        multiplicador->setScale(1.5f);

    }
    if(numero==3){
        
        Sprite* cafe3=Sprite::createWithTexture(elixirTex);
        cafe3->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.7f));
        mFondo_temporizador->addChild(cafe3);
        cafe3->setScale(0.6f);
        cafe3->setRotation(90);
        cafe3->setTag(SeriesScene::TAG_GRANO_CAFE_3);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x3.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setTag(SeriesScene::TAG_X3);
        multiplicador->setScale(1.5f);

    }
    
}






void SeriesScene::cargarControles(){
    
    mLayerSerie=LayerColor::create(Color4B(0.f, 0.f, 0.f, 0.f), mVisibleSize.width, mVisibleSize.height*0.48f);
    mLayerSerie->setPosition(Point(0,mVisibleSize.height*0.55f));
    this->addChild(mLayerSerie,30);
    
    
    mFondo_serie=Sprite::create("series/fondo_serie.png");
    mFondo_serie->setPosition(Point(mVisibleSize.width/2,mLayerSerie->getContentSize().height*0.42f));
    mLayerSerie->addChild(mFondo_serie,29);
   
   
    mZoom=Boton::createBoton(this, menu_selector(SeriesScene::zoom), "elixircomun/zoom.png");
    mZoom->setSwallowTouches(true);
   
    mZoom->setPosition(Point(mFondo_serie->getContentSize().width,mFondo_serie->getContentSize().height-mZoom->getContentSize().height/2));
    mFondo_serie->addChild(mZoom);
    mZoom->setSound("sounds/serieslogic/zoom.mp3");
    
    
    mSerie= Label::createWithTTF("1, 2 ,3 ,4 ,5, ....","fonts/KGBlankSpaceSketch.ttf", 65);
    mSerie->setDimensions(mFondo_serie->getContentSize().width*0.85f, mFondo_serie->getContentSize().height*0.4f);
    mSerie->setHorizontalAlignment(TextHAlignment::CENTER);
    mSerie->setVerticalAlignment(TextVAlignment::CENTER);
    mSerie->setOverflow(Label::Overflow::SHRINK);
    mSerie->setPosition(Point(mFondo_serie->getContentSize().width/2,mFondo_serie->getContentSize().height/2));
    mSerie->enableOutline(Color4B(76,58,8,255),4);
    mFondo_serie->addChild(mSerie,4);
    
 
  
    
    
    mOpcion0= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_0.png", "", 0);
    mOpcion1= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_1.png", "", 1);
    mOpcion2= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_2.png", "", 2);
    mOpcion3= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_3.png", "", 3);
    mOpcion4= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_4.png", "", 4);
    mOpcion5= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_5.png", "", 5);
    mOpcion6= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_6.png", "", 6);
    mOpcion7= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_7.png", "", 7);
    mOpcion8= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_8.png", "", 8);
    mOpcion9= LabelButton::create(this, menu_selector(SeriesScene::opcionElegida), "series/boton_9.png", "", 9);
    
    
    
    
    mOpcion0->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion1->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion2->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion3->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion4->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion5->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion6->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion7->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion8->setSound("sounds/serieslogic/insert_letter.mp3");
    mOpcion9->setSound("sounds/serieslogic/insert_letter.mp3");
    
    
    
    float scale=0.5f;
    mOpcion0->setScale(scale);
    mOpcion1->setScale(scale);
    mOpcion2->setScale(scale);
    mOpcion3->setScale(scale);
    mOpcion4->setScale(scale);
    mOpcion5->setScale(scale);
    mOpcion6->setScale(scale);
    mOpcion7->setScale(scale);
    mOpcion8->setScale(scale);
    mOpcion9->setScale(scale);

    
    
    
    float initial_width=0.087f;
    float temp_add_width=0.14f;
    float height=0.44f;
    mOpcion1->setPosition(Point(mVisibleSize.width*(initial_width+0*temp_add_width),mVisibleSize.height*height));
    mOpcion2->setPosition(Point(mVisibleSize.width*(initial_width+1*temp_add_width),mVisibleSize.height*height));
    mOpcion3->setPosition(Point(mVisibleSize.width*(initial_width+2*temp_add_width),mVisibleSize.height*height));
    mOpcion4->setPosition(Point(mVisibleSize.width*(initial_width+3*temp_add_width),mVisibleSize.height*height));
    mOpcion5->setPosition(Point(mVisibleSize.width*(initial_width+4*temp_add_width),mVisibleSize.height*height));
    
    initial_width=0.087f;
    height=0.24f;
    
    mOpcion6->setPosition(Point(mVisibleSize.width*(initial_width+0*temp_add_width),mVisibleSize.height*height));
    mOpcion7->setPosition(Point(mVisibleSize.width*(initial_width+1*temp_add_width),mVisibleSize.height*height));
    mOpcion8->setPosition(Point(mVisibleSize.width*(initial_width+2*temp_add_width),mVisibleSize.height*height));
    mOpcion9->setPosition(Point(mVisibleSize.width*(initial_width+3*temp_add_width),mVisibleSize.height*height));
    mOpcion0->setPosition(Point(mVisibleSize.width*(initial_width+4*temp_add_width),mVisibleSize.height*height));
    
    fondo_label=Sprite::create("series/fondo_texto.png");
    fondo_label->setPosition(Point(mVisibleSize.width*0.85f,mVisibleSize.height*0.48f));
    this->addChild(fondo_label);
    
    this->addChild(mOpcion1);
    this->addChild(mOpcion2);
    this->addChild(mOpcion3);
    this->addChild(mOpcion4);
    this->addChild(mOpcion5);
    
    this->addChild(mOpcion6);
    this->addChild(mOpcion7);
    this->addChild(mOpcion8);
    this->addChild(mOpcion9);
    this->addChild(mOpcion0);
    
    
    
    
    ok_button= Boton::createBoton(this,  menu_selector(SeriesScene::ok), "series/ok.png");
    ok_button->setPosition(Point(mVisibleSize.width*0.79f,mVisibleSize.height*0.3f));
    this->addChild(ok_button);
    ok_button->setSound("sounds/ui/elegirrespuesta.mp3");
    
    borrar_button= Boton::createBoton(this,  menu_selector(SeriesScene::borrar), "series/delete.png");
    borrar_button->setPosition(Point(mVisibleSize.width*0.91f,mVisibleSize.height*0.3f));
    this->addChild(borrar_button);
    borrar_button->setSound("sounds/serieslogic/delete.mp3");
    
    
    /*mLabelPrevision=LabelTTF::create(" ", "fonts/KGBlankSpaceSketch.ttf", 50,Size(fondo_label->getContentSize().width * 0.8f, fondo_label->getContentSize().height),kCCTextAlignmentRight,kCCVerticalTextAlignmentCenter);
    */
    
    mLabelPrevision=Label::createWithTTF(" ", "fonts/KGBlankSpaceSketch.ttf", 50);
    mLabelPrevision->setWidth(fondo_label->getContentSize().width * 0.8f);
    mLabelPrevision->setHeight(fondo_label->getContentSize().height);
    mLabelPrevision->setHorizontalAlignment(TextHAlignment::RIGHT);
    mLabelPrevision->setVerticalAlignment(TextVAlignment::CENTER);
    
    mLabelPrevision->setPosition(fondo_label->getPosition());
    this->addChild(mLabelPrevision);
    mLabelPrevision->setColor(Color3B(0,0,0));
    
    
   

    
}

void SeriesScene::ok(Ref* sender){
    int longitud= mPrevisionSerie.length();
    if(longitud==0){
         fondo_label->runAction(Sequence::createWithTwoActions(TintTo::create(0.25f, 250, 180,180), TintTo::create(0.25f, 255, 255, 255)));
        return;
    }
    if(mGameFinished){
        return;
    }
   
   
    valorPorpuesto=atoi(mPrevisionSerie.c_str());
    CCLOG("Valor Propuesto%i",valorPorpuesto);
    
    if(valorPorpuesto!=mSolucion){
        //sino pelea por granos mostramos anuncio reward
        
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/bad.mp3");
        mNumeroSegundos=mNumeroSegundos-NUMERO_SEGUNDOS_MAX/3;//quito 1/3 del tiempo
        mFondo_serie->runAction(Sequence::createWithTwoActions(TintTo::create(0.7f, 200, 100, 100), TintTo::create(0.5f,255,255,255)));
        mFondo_serie->runAction(Sequence::create(RotateTo::create(0.2f, -5),RotateTo::create(0.2f, 5),RotateTo::create(0.2f, 0),NULL));
        if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
            AdHelper::showIntersitial();
        }
      
    }else{
        mGameFinished=true;
        mSuccess=true;
        if(mSchedhuleCountdown!=NULL){
            this->unschedule(mSchedhuleCountdown);
        }
        //bloqueo que puedan pretar los botones de atras
        this->addChild(LayerBlock::create(),5);
        
        LayerColor* layer=LayerColor::create(Color4B(237, 217,182 , 150), mVisibleSize.width, mVisibleSize.height);
        layer->setPosition(0,0);
        this->addChild(layer, 30);
        
        Sprite* doctor=Sprite::create("series/doctor.png");
        doctor->setPosition(Point(0-doctor->getContentSize().width/2,doctor->getContentSize().height/2));
        doctor->runAction(MoveTo::create(0.4f, Point(doctor->getContentSize().width/2,doctor->getContentSize().height/2)));
        this->addChild(doctor,31);
        
        
        Label* mNumero;
        if(valorPorpuesto==mSolucion ){
            mNumero=Label::createWithTTF("OK!", "fonts/KGBlankSpaceSketch.ttf", 40);
            mNumero->setColor(Color3B(0,0,0));
        }else{
            mNumero=Label::createWithTTF("NO!", "fonts/KGBlankSpaceSketch.ttf", 40);
            mNumero->setColor(Color3B(198,54,88));
        }
        
        mNumero->setPosition(Point(doctor->getContentSize().width*0.7f,doctor->getContentSize().height * 0.8f));
        doctor->addChild(mNumero);
        
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(SeriesScene::checkFinish,this));
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
        
        //this->scheduleOnce(schedule_selector(SeriesScene::checkFinish),2.f);
    }
}





void SeriesScene::borrar(Ref* sender){
    if(mGameFinished){
        return;
    }
    int longitud= mPrevisionSerie.length();
    if(longitud==0){
        return;
    }
    mPrevisionSerie=mPrevisionSerie.substr(0,longitud-1);
    mLabelPrevision->setString(mPrevisionSerie.c_str());
}


// This is the callback function to display the select data in the table
int SeriesScene::cargarNivelSeries(void *NotUsed, int argc, char **argv, char **azColName)
{
    SeriesScene* seriesScene= (SeriesScene*) NotUsed;
    
    for(int i=0; i<argc; i++)
    {
        if(strcmp( azColName[i],"idnivel")==0){
        
        }else if(strcmp( azColName[i],"textonivel")==0){
            CCLOG("textonivel %s",argv[i]);
            std::string cadena=std::string(argv[i]);
            seriesScene->mSerie->setString(cadena.c_str());
            /*if(cadena.length()>37){
                seriesScene->mSerie->setSystemFontSize(20);
            }else if(cadena.length()>33){
                seriesScene->mSerie->setSystemFontSize(24);
            }else if(cadena.length()>30){
                seriesScene->mSerie->setSystemFontSize(26);
            }else if(cadena.length()>25){
                seriesScene->mSerie->setSystemFontSize(28);
            }else if(cadena.length()>22){
                seriesScene->mSerie->setSystemFontSize(30);
            }else if(cadena.length()>20){
                 seriesScene->mSerie->setSystemFontSize(35);
            }else if(cadena.length()>15){
                seriesScene->mSerie->setSystemFontSize(40);
            }
            */
            //seriesScene->mSerie->setScale(seriesScene->mFondo_serie->getContentSize().width*0.8f/seriesScene->mSerie->getContentSize().width);
            
            
        }else if(strcmp( azColName[i],"solucion")==0){
            
            seriesScene->mSolucion=atoi(argv[i]);
            CCLOG("SOLUCION %i",seriesScene->mSolucion);
        }else if(strcmp( azColName[i],"intentos")==0){
            seriesScene->mNumeroIntentos=atoi(argv[i]);
            CCLOG("mNumeroIntentos %i",seriesScene->mNumeroIntentos);
        }else if(strcmp( azColName[i],"estrellas")==0){
            seriesScene->mNumEstrellas=atoi(argv[i]);
            CCLOG("mNumEstrellas %i",seriesScene->mNumEstrellas);
        }else if(strcmp( azColName[i],"superado")==0){
            seriesScene->mSuperado=atoi(argv[i]);
            CCLOG("SOLUCION %i",seriesScene->mSuperado);
        }

     }
    seriesScene->cargarElixires();
    
    return 0;
}


void SeriesScene::opcionElegida(Ref* sender){
    int longitud= mPrevisionSerie.length();
    if(longitud==5){
        return;
    }
    if(mGameFinished){
        return;
    }
    
    char memory[200];
    sprintf(memory,"%s%i",mPrevisionSerie.c_str(),LabelButton::CLICKADO);
    mLabelPrevision->setString(memory);
    mPrevisionSerie=memory;
    
    
}


SeriesScene* SeriesScene::getInstance(){
    return instancia_series;
};

void SeriesScene::checkFinish(Ref* sender){
    if(mSuccess){
        if(!mSuperado){
            //LogicSQLHelper::getInstance().setNivelSuperadoElixir(Variables::LEVELS_ELIXIR_CLICKADO, mStars);
            LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(true,Variables::LEVELS_ELIXIR_CLICKADO,1);
            LayerGanarItem::mostrar(mStars);
            return;
     
        }
    }
    BaseScene::checkFinish(NULL);
    
    
};
void SeriesScene::Help(Ref* sender){
    std::string temp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_SERIES","HELP_SERIES"));
    this->mTextoHelp=temp;
    BaseScene::Help(NULL);
};

void SeriesScene::zoom(Ref* sender){
    if(mGameFinished){
        return;
    }
    if(mZoomActivandose){
        return;
    }
    
    if(!mZoomActivado){
        mLayerSerie->runAction(ScaleTo::create(0.3f, mVisibleSize.width*0.85f/mFondo_serie->getContentSize().width ));
        mLayerSerie->runAction(MoveTo::create(0.3f, Point(0,mVisibleSize.height*0.35f)));
        mZoom->setTexture(Director::getInstance()->getTextureCache()->addImage("elixircomun/zoom-less.png"));
        mZoom->runAction(ScaleTo::create(0.3, 0.5f));
        mZoomActivado=true;
         mShare->setEnabled(false);
        
    }else{
        mLayerSerie->runAction(ScaleTo::create(0.3f, 1.f));
        mLayerSerie->runAction(MoveTo::create(0.3f, Point(0,mVisibleSize.height*0.55f)));
        mZoom->setTexture(Director::getInstance()->getTextureCache()->addImage("elixircomun/zoom.png"));
        mZoom->runAction(ScaleTo::create(0.3, 1.f));
        mZoomActivado=false;
         mShare->setEnabled(true);
    }
    scheduleOnce(schedule_selector(SeriesScene::zoomStop), 0.3f);
    mZoomActivandose=true;
    
    
    
}

void SeriesScene::zoomStop(float dt){
    mZoomActivandose=false;
}

void SeriesScene::Retry(Ref* sender){
   Director::getInstance()->replaceScene(SeriesScene::scene());
};



void SeriesScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}


void SeriesScene::HintStart(Ref* sender)
{
    
}
void SeriesScene::HintCallback(bool accepted){
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        char memory[200];
        sprintf(memory,"%i",mSolucion);
        mLabelPrevision->setString(memory);
        mPrevisionSerie=memory;
        fondo_label->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.15f),ScaleTo::create(0.5f, 1.f)));
    }
    
}
void SeriesScene::Back(Ref* sender){
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, Levelsv2::scene()));
}

template <typename T> std::string SeriesScene::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }




bool SeriesScene::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}
 




void SeriesScene::onEnterTransitionDidFinish(){
    BaseScene::onEnterTransitionDidFinish();
    LogicSQLHelper::getInstance().sumarIntentoSQL(Variables::LEVELS_ELIXIR_CLICKADO);
}

