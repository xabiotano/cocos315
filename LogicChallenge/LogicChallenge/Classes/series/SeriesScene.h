#ifndef __SeriesScene_SCENE_H__
#define __SeriesScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "../widgets/LabelButton.h"

USING_NS_CC;


class SeriesScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(SeriesScene);
    static SeriesScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Help(Ref* sender);
    virtual void Retry(Ref* sender);
    virtual void Back(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
     virtual void TutorialStart(Ref* sender){};
   
    //SQL
    // This is the callback function to display the select data in the table
    static int cargarNivelSeries(void *NotUsed, int argc, char **argv, char **azColName);
    void opcionElegida(Ref* sender);
    void ok(Ref* sender);
    void borrar(Ref* sender);
    
    Label* mSerie;
    int mSolucion;
    
    virtual bool onTouchBegan(Touch* touch, Event* event);
    bool mSuperado;
    int nNumIntentos;
    int mNumEstrellas;
    
    
private:
  
    
    LabelButton *mOpcion0,*mOpcion1,*mOpcion2,*mOpcion3,*mOpcion4,*mOpcion5,*mOpcion6,*mOpcion7,*mOpcion8,*mOpcion9;
    void cargarSerie();
    void cargarControles();
    void cargarElixires();
    Sprite* fondo_label;
    Boton *ok_button,*borrar_button;
    
    std::string mPrevisionSerie;
    
    Label* mLabelPrevision;
    
    
    int valorPorpuesto;
    int mNumeroIntentos;
    
    LayerColor* mLayerSerie;
    Boton* mZoom;
    bool mZoomActivado;
    bool mZoomActivandose;
    void zoom(Ref* sender);
    void zoomStop(float dt);
    bool mGameFinished;
    Sprite* mFondo_serie;
    ProgressTimer* mProgresoCafeina;
    
    int mGranosAspirables;
    Sprite *mFondo_temporizador;
    void dibujarGranosCafe(int numero);
    static  std::string SERIES_NUMERO_GRANOS_CAFE_CONSEGUIDOS;
    float mNumeroSegundos;
    float NUMERO_SEGUNDOS_MAX;
    void countDown(float dt);
    SEL_SCHEDULE mSchedhuleCountdown;
    
    const int TAG_GRANO_CAFE_2=840;
    const int TAG_GRANO_CAFE_3=841;
    const int TAG_X2=8472;
    const int TAG_X3=29402;
    
    bool mLuchaPorElixir;
    int mTipoAnuncio;
   
    
    template <typename T> std::string tostr(const T& t);
    void onEnterTransitionDidFinish();
    
};


#endif // __SeriesScene_SCENE_H__
