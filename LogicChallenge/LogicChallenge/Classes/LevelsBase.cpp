#include "SimpleAudioEngine.h"
#include "LevelsBase.h"
#include "layers/LayerInfo.h"
#include "layers/LayerShop.h"
#include "helpers/LogicSQLHelper.h"
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#include "PluginIAP/PluginIAP.h"
#include "InicioScene.h"
#include "AdHelper.h"
using namespace cocos2d;
using namespace sdkbox;

template <typename T> std::string LevelsBase::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

//LevelsBase* LevelsBase::_instancia;

LevelsBase::~LevelsBase(void){}


bool LevelsBase::initWithColor(const Color4B& color)
{
    if (!LayerColor::initWithColor(color) )
    {
        return false;
    }
    customInit();
    return true;
}

void LevelsBase::Back(Ref* sender){
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, Inicio::createScene()));
}

void LevelsBase::customInit(){
    mVisibleSize = Director::getInstance()->getVisibleSize();
    
    //boton atras
    Boton* mBack=Boton::createBoton(this,menu_selector(LevelsBase::Back), "ui/back.png");
    mBack->setPosition(Point(mVisibleSize.width*0.10f,mVisibleSize.height*0.9f));
    this->addChild(mBack,30);
    mBack->setSound("sounds/ui/click1.mp3");
    
   
    mFondoItems=Boton::createBoton(this,menu_selector(LevelsBase::Shop),"elixircomun/back_numero_items.png");
    mFondoItems->setPosition(Point(mVisibleSize.width*0.85f, mVisibleSize.height*0.9f));
    this->addChild(mFondoItems,100);
    int numeroElixiresUsuario= LogicSQLHelper::getInstance().getElixiresGanados();
    mLabelNumeroItems=Label::createWithTTF(tostr(numeroElixiresUsuario).c_str(),"fonts/JungleFever.ttf", 40);
    mLabelNumeroItems->setDimensions(mFondoItems->getContentSize().width*0.8f,mFondoItems->getContentSize().height*0.7f);
    mLabelNumeroItems->setAlignment(TextHAlignment::RIGHT, TextVAlignment::CENTER);
    mLabelNumeroItems->setPosition(Point(mFondoItems->getContentSize().width*0.45f,mFondoItems->getContentSize().height/2));
    mLabelNumeroItems->enableOutline(Color4B(0,0,0,255),2);
    mFondoItems->addChild(mLabelNumeroItems,3);
    
    Sprite* itemganadoSimbolo= Sprite::create("elixircomun/elixir_item_small.png");
    mFondoItems->addChild(itemganadoSimbolo);
    itemganadoSimbolo->setPosition(Point(mFondoItems->getContentSize().width*0.12f,mFondoItems->getContentSize().height*0.5f));
   
    
}



void LevelsBase::Shop(Ref* sender){
    CCLOG("Shop");
    LayerShop::mostrar(this);
    
}


void LevelsBase::onEnterTransitionDidFinish(){
   if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
       AdHelper::showBanner();
   }
   
}


void LevelsBase::onExit()
{
    LayerColor::onExit();
}







/*ILayerShop*/
void LevelsBase::onElixirComprado(int numero)
{
    int numeroElixiresUsuario= LogicSQLHelper::getInstance().getElixiresGanados();
    CCLOG("onElixirComprado %i", numeroElixiresUsuario );
    mLabelNumeroItems->setString(tostr(numeroElixiresUsuario).c_str());
}

void LevelsBase::onCompleteGameComprado()
{
    int numeroElixiresUsuario= LogicSQLHelper::getInstance().getElixiresGanados();
    mLabelNumeroItems->setString(tostr(numeroElixiresUsuario).c_str());
     AdHelper::hideBanner();
}
void LevelsBase::onRemoveAdsComprado()
{
    AdHelper::hideBanner();
    
}




