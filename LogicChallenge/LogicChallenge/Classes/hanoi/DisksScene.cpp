#include "DisksScene.h"
#include "SimpleAudioEngine.h"
#include "Stick.h"
#include "Disk.h"

using namespace cocos2d;
using namespace CocosDenshion;

DisksScene* instancia_Disks;

DisksScene* DisksScene::getInstance(){
    return instancia_Disks;
}






Scene* DisksScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    DisksScene *layer = DisksScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool DisksScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_Disks=this;
    mNombrePantalla="DisksScene";
    mTipoNivel=kNivelLogica;
    mMovements=44;
  
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("hanoi/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    Sprite* mFondoAbajo = Sprite::create("hanoi/background_down.png");
    // position the sprite on the center of the screen
    mFondoAbajo->setPosition(Point(visibleSize.width/2 , mFondoAbajo->getContentSize().height/2));
    this->addChild(mFondoAbajo, 1);

    
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    
   
    
    mStick1=Stick::create(this, std::string("hanoi/wood_stick.png"), 0);
    mStick1->setPosition(Point(mSize.width*0.20f,mSize.height*0.35f));
    this->addChild(mStick1);
 
    mStick2=Stick::create(this, std::string("hanoi/wood_stick.png"), 0);
    mStick2->setPosition(Point(mSize.width*0.5f,mSize.height*0.35f));
    this->addChild(mStick2);
    
    mStick3=Stick::create(this, std::string("hanoi/wood_stick.png"), 0);
    mStick3->setPosition(Point(mSize.width*0.8f,mSize.height*0.35f));
    this->addChild(mStick3);
    crearPanel();
    colocarDisks();
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_DISKS", "HELP_DISKS"));
    mRetryVisibleDuringGame=true;
    

    return true;
}



void DisksScene::colocarDisks()
{
    if(disk1!=NULL){
        disk1->removeFromParent();
    }
    if(disk2!=NULL){
        disk2->removeFromParent();
    }
    if(disk3!=NULL){
        disk3->removeFromParent();
    }
    if(disk4!=NULL){
        disk4->removeFromParent();
    }
    if(disk5!=NULL){
        disk5->removeFromParent();
    }
    mStick1->removeAllDisks();
    mStick2->removeAllDisks();
    mStick3->removeAllDisks();
   
   
    disk1=Disk::create(this, "hanoi/disk_in_stick.png", mStick1, 1.f );
    this->addChild(disk1);
    disk2=Disk::create(this, "hanoi/disk_in_stick.png", mStick1, 0.9f);
    this->addChild(disk2);
    disk3=Disk::create(this, "hanoi/disk_in_stick.png", mStick1, 0.8f);
    this->addChild(disk3);
    disk4=Disk::create(this, "hanoi/disk_in_stick.png", mStick1, 0.7f);
    this->addChild(disk4);
    disk5=Disk::create(this, "hanoi/disk_in_stick.png", mStick1, 0.6f);
    this->addChild(disk5);
    
    mStick1->addDisk(disk1);
    mStick1->addDisk(disk2);
    mStick1->addDisk(disk3);
    mStick1->addDisk(disk4);
    mStick1->addDisk(disk5);
    
}


void DisksScene::checkFinish(){
    
    if(mMovements==0){
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("HANOI_GAMEOVER");
        mGameover=true;
    }
    if(mStick3->mArrayOfDisks->count()==5){
        mSuccess=true;
        mStars=3;
    }
    
    
    
    
    BaseScene::checkFinish(NULL);
}



void DisksScene::crearPanel(){
    
 
    mCounter= Label::createWithTTF("44", CCGetFont(), 60);
    mCounter->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    //mCounter->setColor(Color3B(174,66,23));
    mCounter->setPosition(Point(mSize.width/2,mSize.height*0.9f));
    mCounter->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mCounter->setTextColor(Color4B(174,66,23,255));
    mCounter->enableOutline(Color4B(255,255,255,255),4);
    this->addChild(mCounter,4);
    
}

void DisksScene::descenderCounter(){
    mMovements--;
    char temp[200];
    sprintf(temp, "%i",mMovements);
    if(mMovements<5){
        mCounter->setColor(Color3B(255.f,0.f,0.f));
    }
    if(mMovements<10 && mMovements>=5){
        mCounter->setColor(Color3B(245.f,93.f,10.f));
    }
    mCounter->setString(temp);
    checkFinish();
}

void DisksScene::Retry(Ref* sender){
    Scene* scene=DisksScene::scene();
    Director *pDirector = Director::getInstance();
     pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
}

void DisksScene::HintStartTemp(float dt)
{
    HintStart(NULL);
}

void DisksScene::HintStart(Ref* sender){
    CCLOG("%i",mHintStep);
    switch(mHintStep){
        case 1:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 2:
            disk4->removeFromStick();
            disk4->setStick(mStick2);
            break;
        case 3:
            disk5->removeFromStick();
            disk5->setStick(mStick2);
            break;
        case 4:
            disk3->removeFromStick();
            disk3->setStick(mStick3);
            break;
            
        case 5:
            disk5->removeFromStick();
            disk5->setStick(mStick1);
            break;
        case 6:
            disk4->removeFromStick();
            disk4->setStick(mStick3);
            break;
        case 7:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 8:
            disk2->removeFromStick();
            disk2->setStick(mStick2);
            break;
        case 9:
            disk5->removeFromStick();
            disk5->setStick(mStick2);

            break;
        case 10:
            disk4->removeFromStick();
            disk4->setStick(mStick1);
            break;
        case 11:
            disk5->removeFromStick();
            disk5->setStick(mStick1);
            break;
        case 12:
            disk3->removeFromStick();
            disk3->setStick(mStick2);
        break;
        case 13:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 14:
            disk4->removeFromStick();
            disk4->setStick(mStick2);
            break;
        case 15:
            disk5->removeFromStick();
            disk5->setStick(mStick2);
            break;
        case 16:
            disk1->removeFromStick();
            disk1->setStick(mStick3);
            break;
        case 17:
            disk5->removeFromStick();
            disk5->setStick(mStick1);
            break;
        case 18:
            disk4->removeFromStick();
            disk4->setStick(mStick3);
            break;
        case 19:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 20:
            disk3->removeFromStick();
            disk3->setStick(mStick1);
            break;
        case 21:
            disk5->removeFromStick();
            disk5->setStick(mStick2);
            break;
        case 22:
            disk4->removeFromStick();
            disk4->setStick(mStick1);
            break;
        case 23:
            disk5->removeFromStick();
            disk5->setStick(mStick1);
            break;
        case 24:
            disk2->removeFromStick();
            disk2->setStick(mStick3);
            break;
       case 25:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 26:
            disk4->removeFromStick();
            disk4->setStick(mStick2);
            break;
        case 27:
            disk5->removeFromStick();
            disk5->setStick(mStick2);
            break;
        case 28:
            disk3->removeFromStick();
            disk3->setStick(mStick3);
            break;
        case 29:
            disk5->removeFromStick();
            disk5->setStick(mStick1);
            break;
        case 30:
            disk4->removeFromStick();
            disk4->setStick(mStick3);
            break;
        case 31:
            disk5->removeFromStick();
            disk5->setStick(mStick3);
            break;
        case 36:
            HintStop(NULL);
            break;
    }
    mHintStep++;
   
}



void DisksScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void DisksScene::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        CCLOG("MOSTRAR");
        colocarDisks();
        mHintStep=1;
        mMovements=44;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        
        mScheudleHint=CC_SCHEDULE_SELECTOR(DisksScene::HintStartTemp);
        this->schedule(mScheudleHint,2.f);
        
    }
}

void DisksScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/hanoi/grillos.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}


