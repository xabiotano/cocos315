//
//  Disk.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Disk__
#define __logicmaster2__Disk__

#include <iostream>
#include "../widgets/Boton.h"
#include "Stick.fwd.h"


class Disk: public Boton
{
    
public:
   
    
    Disk(Stick* stick_origen,float scale);
    virtual ~Disk(void);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    
    static Disk* create(Node* parent,std::string imagen,Stick* stick_origen,float scale);
 
    void clickDisk(Ref* sender);
   
    bool onTouchBegan(Touch* touch, Event* event) override;
    void onTouchMoved(Touch* touch, Event* event) override;
    void onTouchEnded(Touch* touch, Event* event) override;
    Stick* mStickOrigen;
   float mScale;
    void removeFromStick();
    void setStick(Stick* stick);
    void setFinalStick(Stick* stick);

private:
    bool mEsVerde;
    int mPosicion;
    Texture2D* mImagenInStick, *mImagenOutStick;
  
    Stick* getTouchingStick(Touch* touch);
    
    
};


#endif /* defined(__logicmaster2__Disk__) */
