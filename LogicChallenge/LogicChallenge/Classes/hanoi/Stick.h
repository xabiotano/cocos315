//
//  Stick.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Stick__
#define __logicmaster2__Stick__

#include <iostream>
#include "../widgets/Boton.h"
#include "Disk.h"

class Stick: public Sprite
{
    
public:
    
    Stick(int posicion);
    virtual ~Stick(void);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    
    static Stick* create(Node* parent,std::string imagen,int posicion);
 
    void clickStick(Ref* sender);
    
    bool addDisk(Disk* disk);
    void removeDisk(Disk* disk);
    Disk* getLastDisk();
    Array* mArrayOfDisks;
    void removeAllDisks();
    
private:
   
   
    int mPosicion;
   
    float getYOfDisk(Disk* disk);
  

    
    
};


#endif /* defined(__logicmaster2__Stick__) */
