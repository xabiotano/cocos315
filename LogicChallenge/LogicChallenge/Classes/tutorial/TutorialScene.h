#ifndef __TutorialScene_SCENE_H__
#define __TutorialScene_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../widgets/imports.h"


USING_NS_CC;
USING_NS_CC_EXT;

class TutorialScene : public cocos2d::Layer
{
    typedef enum TutorialPasoType
    {
        kPaso1,
        kPaso2,
        kPaso3,
        kPaso4,
        kPaso5,
        kPaso6,
        kPaso7,
        kPaso8,
        kPaso9,
        kPaso10,
        kPaso11
        
    } TutorialPasoType;
    

    
    public:
        template <typename T> std::string tostr(const T& t);
        // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
        virtual bool init();

        // there's no 'id' in cpp, so we recommend returning the class instance pointer
        static cocos2d::Scene* scene();
    
        // a selector callback
        void irAInicio();
   
        // implement the "static node()" method manually
        CREATE_FUNC(TutorialScene);
        void onEnterTransitionDidFinish();
    
    static const int TAG_LABEL=2942;
    static const int TAG_EXPLORER=2943;
    static const int TAG_BRUJO= 2944;
    
    
private:
    float scaley;
    
    void aparecerExplorer();
    void mostrarComic1();
    void mostrarComic2();
    void mostrarComic3();
  
    
    void mostrarBrujo();
    void mostrarLogicWorld();
    void mostrarElixirWorld();
    void nada();
   
    void mostrarExplicacionLogicWorld();
    void mostrarExplicacionLogicWorld2();
    void ocultarExploradorYMostrarBrujo();
    void mostrarElixirNecesario();
    
    
    void mostrarComicBrujo1();
    void mostrarComicBrujo2();
    void mostrarComicBrujo3();
    
    void actualizarComicPorLogicWorld();

    void actualizarComicPorDefecto();
    void actualizarComicPorDefectoBrujo();
    
    void playSonido();
    CallFunc* getActionPlaySonido();
    
    
    Size mVisibleSize;
    Sprite* mFondo;
        
    void onKeyBackClicked();
    void animarLogo();
    Sprite* mLogo;
    Sprite* mLeft,*mRight;
    Boton* mContinuar;
    
    TutorialPasoType mPasoTutorial;
    
    void click(Ref* sender);
    
    
    Sprite* mComic;
    
    bool mActivoNext;
    void desactivarNext();
    void activarNext();
    
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    
    virtual void onEnter();
    virtual void onExit();

};

#endif // __TutorialScene_SCENE_H__
