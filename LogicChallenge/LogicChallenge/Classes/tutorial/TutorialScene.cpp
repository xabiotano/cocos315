#include "TutorialScene.h"
#include "../InicioScene.h"
#include "SimpleAudioEngine.h"
#include "../helpers/LogicSQLHelper.h"

USING_NS_CC;



template <typename T> std::string TutorialScene::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }


Scene* TutorialScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    TutorialScene *layer = TutorialScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool TutorialScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    mVisibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    if(!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/hawaii.mp3",true);
        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.2f);
    }
  
   
    Sprite* mFondo = Sprite::create("tutorial/fondo.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
   
    
  
    
    
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
    
  
    if(!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/hawaii.mp3",true);
        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.2f);
    }
    
    mActivoNext=false;
    
    

    
    
    
    
    
   
    
    
    std::string logoImagen;
    logoImagen="inicio/logo.png";
    
    
    
    LanguageType curLanguage = Application::getInstance()->getCurrentLanguage();
    switch (curLanguage) {
        case LanguageType::RUSSIAN:
        logoImagen = "inicio/simplelogo_ru.png";
        break;
        case LanguageType::KOREAN:
        logoImagen = "inicio/simplelogo_ko.png";
        break;
        case LanguageType::GERMAN:
        logoImagen = "inicio/simplelogo_de.png";
        break;
        default:
        logoImagen = "inicio/simplelogo.png";
        break;
        
    }

    
     mLogo=Sprite::create(logoImagen.c_str());
    
    
    
    mLogo->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height+ mLogo->getContentSize().height/2));
    this->addChild(mLogo);
    
    mContinuar=Boton::createBoton(this, menu_selector(TutorialScene::click), "ui/skip.png");
    mContinuar->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.2f));
    addChild(mContinuar,60);
    mContinuar->setOpacity(0.f);
    mContinuar->setSound("sounds/serieslogic/insert_letter.mp3");
    
    
    mPasoTutorial=kPaso1;
    
    this->setKeypadEnabled(true);
    
    
    return true;
}


void TutorialScene::animarLogo(){
    
    mLogo->runAction(EaseInOut::create(JumpTo::create(0.5f, Point(mVisibleSize.width/2,mVisibleSize.height - mLogo->getContentSize().height*1.5f/2), -mLogo->getContentSize().height, 1),3));
    

}

void TutorialScene::irAInicio(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/elegirnivel.mp3");
    Scene* scene=Inicio::createScene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
}



void TutorialScene::onKeyBackClicked()
{
    Director::getInstance()->end();
}


void TutorialScene::onEnterTransitionDidFinish(){
   
    runAction(Sequence::create(DelayTime::create(0.5f),
                                 CallFunc::create([this]() { this->animarLogo();}),
                                 DelayTime::create(1.f),
                                CallFunc::create([this]() { this->aparecerExplorer();}),
                                 NULL));
}


void TutorialScene::aparecerExplorer(){
    LayerColor* layer=LayerColor::create(Color4B(0,0,0,100), mVisibleSize.width, mVisibleSize.height);
    layer->setOpacity(0.f);
    addChild(layer);
    layer->runAction(FadeTo::create(1.f, 150.f));
    Sprite* explorer=Sprite::create("levelunlocked/explorer_xxl_without_bulb.png");
    explorer->setPosition(Point(-explorer->getContentSize().width/2, explorer->getContentSize().height/2 ));
    this->addChild(explorer,3);
    explorer->setTag(TAG_EXPLORER);
    explorer->runAction(Sequence::create(
                                           MoveTo::create(0.5f, Point(explorer->getContentSize().width/2,explorer->getContentSize().height/2)),
                                           DelayTime::create(0.5f),
                                           CallFunc::create([this]() { this->mostrarComic1(); }),
                                           getActionPlaySonido(),
                                           CallFunc::create([this]() { this->activarNext(); }),
                                           NULL
                                            ));
    
    
    
}


void TutorialScene::mostrarComic1(){
    mComic=Sprite::create("tutorial/comic.png");
    mComic->setPosition(Point(mVisibleSize.width*0.6f,mVisibleSize.height*0.6f));
    this->addChild(mComic,4);
    mComic->setScale(0.f);
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                         CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                        ));
    

    
    
    Label* textoComic= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_1", "TUTORIAL_PASO_1"), CCGetFont(), 45);
    textoComic->setWidth(mComic->getContentSize().width*0.8f);
    textoComic->setHeight(mComic->getContentSize().height*0.7f);
    textoComic->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    textoComic->setPosition(Point(mComic->getContentSize().width*0.5f,mComic->getContentSize().height*0.6f));
    textoComic->setColor(Color3B( 110,153,61));
    mComic->addChild(textoComic);
    textoComic->setTag(TutorialScene::TAG_LABEL);
    
    

}


void TutorialScene::mostrarComic2(){
    
    desactivarNext();
    if(mComic==NULL){
        return;
    }
    Label* textoComic=(Label*)mComic->getChildByTag(TutorialScene::TAG_LABEL);
    textoComic->setSystemFontName(CCGetFont());
     textoComic->setSystemFontSize(45);
    textoComic->setOverflow(Label::Overflow::SHRINK);
    if(textoComic==NULL){
        return;
    }
    textoComic->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_2", "TUTORIAL_PASO_2"));
   
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                        CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));
   
}


void TutorialScene::mostrarComic3(){
    
    
    Sprite* explorer=(Sprite*) getChildByTag(TAG_EXPLORER);
    explorer->runAction(Sequence::create(
                                           MoveTo::create(0.5f, Point(explorer->getContentSize().width/4,explorer->getContentSize().height*0.4f)),
                                           //CCCallFunc::create(this, callfunc_selector(TutorialScene::mostrarBrujo))
                                           NULL
                                            ));
    
    mComic->runAction(Sequence::create(
                                         MoveTo::create(0.5f, Point(mVisibleSize.width*0.4f,mVisibleSize.height*0.6f)),
                                        CallFunc::create([this]() { this->mostrarLogicWorld(); }),
                                         DelayTime::create(0.6f),
                                         CallFunc::create([this]() { this->mostrarExplicacionLogicWorld(); }),
                                        CallFunc::create([this]() { this->activarNext(); }),
                                        NULL
                                         ));

    
}



void TutorialScene::mostrarExplicacionLogicWorld(){
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    if(label==NULL){
        return;
    }
    label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_2_EXPLICARLOGIC", "TUTORIAL_PASO_2_EXPLICARLOGIC"));
     label->setSystemFontName(CCGetFont());
     label->setSystemFontSize(45);
    label->setOverflow(Label::Overflow::SHRINK);
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                         NULL
                                         ));
    
}
void TutorialScene::mostrarExplicacionLogicWorld2(){
    
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    if(label==NULL){
        return;
    }
   
    label->setSystemFontName(CCGetFont());
    label->setOverflow(Label::Overflow::SHRINK);
    label->setSystemFontSize(45);
     label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_2_EXPLICARLOGIC2", "TUTORIAL_PASO_2_EXPLICARLOGIC2"));
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                        CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));

}

void TutorialScene::actualizarComicPorDefecto(){
   
    mComic->initWithTexture(Director::getInstance()->getTextureCache()->addImage("tutorial/comic.png"));
    mComic->removeAllChildrenWithCleanup(true);
    
    
    Label* textoComic= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_1", "TUTORIAL_PASO_1"), CCGetFont(), 45);
    textoComic->setWidth(mComic->getContentSize().width*0.8f);
    textoComic->setHeight(mComic->getContentSize().height*0.7f);
    textoComic->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    textoComic->setPosition(Point(mComic->getContentSize().width*0.5f,mComic->getContentSize().height*0.6f));
    textoComic->setColor(Color3B( 110,153,61));
    textoComic->setOverflow(Label::Overflow::SHRINK);
    mComic->addChild(textoComic);
    textoComic->setTag(TutorialScene::TAG_LABEL);
    
    
    
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                        CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));
  
    
}


void TutorialScene::actualizarComicPorDefectoBrujo(){
   
    mComic->initWithTexture(Director::getInstance()->getTextureCache()->addImage("tutorial/comic_reverse.png"));
    mComic->removeAllChildrenWithCleanup(true);
    mComic->setPosition(Point(mVisibleSize.width*0.5f,mVisibleSize.height*0.6f));
    
    /*CCLabelTTF* textoComic=CCLabelTTF::create(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_1", "TUTORIAL_PASO_1"), CCGetFont(), 45,Size(mComic->getContentSize().width*0.8f, mComic->getContentSize().height*0.7f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentCenter );
    textoComic->setPosition(Point(mComic->getContentSize().width*0.5f,mComic->getContentSize().height*0.6f));
    textoComic->setColor(Color3B( 110,153,61));
    mComic->addChild(textoComic);
    textoComic->setTag(TutorialScene::TAG_LABEL);*/
    Label* textoComic= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_1", "TUTORIAL_PASO_1"), CCGetFont(), 45);
    textoComic->setWidth(mComic->getContentSize().width*0.8f);
    textoComic->setHeight(mComic->getContentSize().height*0.7f);
    textoComic->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    textoComic->setPosition(Point(mComic->getContentSize().width*0.5f,mComic->getContentSize().height*0.6f));
    textoComic->setColor(Color3B( 110,153,61));
    mComic->addChild(textoComic);
    textoComic->setTag(TutorialScene::TAG_LABEL);
    
    
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                         NULL
                                         ));
    
    
}


void TutorialScene::actualizarComicPorLogicWorld(){
    
    mComic->initWithTexture(Director::getInstance()->getTextureCache()->addImage("tutorial/comic_logic.png"));
    mComic->removeAllChildren();
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                         CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));
    
}





void TutorialScene::nada(){
    
}


void TutorialScene::mostrarBrujo(){
    Sprite* brujo=Sprite::create("tutorial/brujo_xxl.png");
    brujo->setPosition(Point(mVisibleSize.width+brujo->getContentSize().width/2, brujo->getContentSize().height/2 ));
    this->addChild(brujo,3);
    brujo->setTag(TAG_BRUJO);
    brujo->runAction(Sequence::create(
                                           MoveTo::create(0.5f, Point(mVisibleSize.width-brujo->getContentSize().width/2,brujo->getContentSize().height/2)),
                                           DelayTime::create(0.5f),
                                           CallFunc::create([this]() { this->mostrarComicBrujo1(); }),
                                           getActionPlaySonido(),
                                           NULL
                                           ));
    mComic->runAction(Sequence::create(
                                         MoveTo::create(0.5f, Point(mVisibleSize.width*0.6f,mVisibleSize.height*0.5f)),
                                         CallFunc::create([this]() { this->mostrarElixirWorld(); }),
                                         DelayTime::create(0.6f),
                                         
                                         NULL
                                         ));

    
}


void TutorialScene::mostrarElixirWorld(){
    mLeft=Sprite::create("inicio/izquierda.png");
    mLeft->setPosition(Point(-mLeft->getContentSize().width/2, mVisibleSize.height/2 ));
    mLeft->setScale(scaley);
    addChild(mLeft);
    mLeft->runAction(EaseInOut::create(MoveTo::create(0.6f, Point(mVisibleSize.width/2- mLeft->getContentSize().width*0.68f/2 ,mVisibleSize.height/2)), 3));
    
}

void TutorialScene::mostrarComicBrujo1(){
   
    actualizarComicPorDefectoBrujo();
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_1_BRUJO", "TUTORIAL_PASO_1_BRUJO"));
    label->setSystemFontName(CCGetFont());
    label->setSystemFontSize(45);
    
    label->setOverflow(Label::Overflow::SHRINK);

    
    
}

void TutorialScene::mostrarComicBrujo2(){
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_2_BRUJO", "TUTORIAL_PASO_2_BRUJO"));
    label->setSystemFontName(CCGetFont());
    label->setOverflow(Label::Overflow::SHRINK);
    label->setSystemFontSize(35.f);
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                          CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));
}

void TutorialScene::mostrarComicBrujo3(){
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_3_BRUJO", "TUTORIAL_PASO_3_BRUJO"));
    label->setSystemFontName(CCGetFont());
    label->setOverflow(Label::Overflow::SHRINK);
     label->setSystemFontSize(45);
    mComic->runAction(Sequence::create(
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.1f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 1.f), 3),
                                         getActionPlaySonido(),
                                         CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
                                         ));

    
}


void TutorialScene::mostrarLogicWorld(){
    mRight=Sprite::create("inicio/derecha.png");
    mRight->setPosition(Point(mVisibleSize.width+ mRight->getContentSize().width/2, mVisibleSize.height/2 ));
    mRight->setScale(scaley);
    addChild(mRight);
    mRight->runAction(EaseInOut::create(MoveTo::create(0.6f, Point(mVisibleSize.width/2+ mRight->getContentSize().width*0.68f/2,mVisibleSize.height/2)), 3));

  
}

void TutorialScene::mostrarElixirNecesario(){
    actualizarComicPorDefecto();
    Label* label=(Label*) mComic->getChildByTag(TAG_LABEL);
    label->setSystemFontName(CCGetFont());
    label->setOverflow(Label::Overflow::SHRINK);
    label->setString(LanguageManager::getInstance()->getStringForKey("TUTORIAL_PASO_3_ELIXIR_NECESARIO"));
    label->setSystemFontSize(35);
    Sprite* elixir=Sprite::create("elixircomun/elixir_m_relieve.png");
    elixir->setPosition(Point(mComic->getContentSize().width/2,elixir->getContentSize().height*0.82f));
    mComic->addChild(elixir);
    elixir->runAction(Sequence::create(
                                         DelayTime::create(1.5f),
                                         EaseInOut::create(ScaleTo::create(0.3f, 0.7f), 3),
                                         EaseInOut::create(ScaleTo::create(0.3f, 0.6f), 3),
                                         getActionPlaySonido(),
                                          CallFunc::create([this]() { this->activarNext(); }),
                                         NULL
      ));
    elixir->setScale(0.6f);

    
}



void TutorialScene::ocultarExploradorYMostrarBrujo(){
    Sprite* explorer=(Sprite*) getChildByTag(TAG_EXPLORER);
    mComic->runAction(ScaleTo::create(0.5f, 0));
    mRight->runAction(EaseInOut::create(MoveTo::create(0.6f, Point(mVisibleSize.width+ mRight->getContentSize().width/2,mVisibleSize.height/2)), 3));
    
    explorer->runAction(Sequence::create(
                                     MoveTo::create(0.5f, Point(explorer->getContentSize().width/4,explorer->getContentSize().height*0.4f)),
                                     CallFunc::create([explorer]() { explorer->removeFromParent(); }),NULL));
    this->runAction(Sequence::create(DelayTime::create(0.5f),
                                           CallFunc::create([this]() { this->mostrarBrujo(); }),
                                           DelayTime::create(0.5f),
                                           CallFunc::create([this]() { this->mostrarComicBrujo1(); }),
                                           DelayTime::create(1.f),
                                           CallFunc::create([this]() { this->activarNext(); }),
                                          NULL
                                           ));
   
}





void TutorialScene::click(Ref* sender){
    
   
    if(mPasoTutorial==TutorialPasoType(kPaso1)){
        mPasoTutorial=kPaso2;
        desactivarNext();
        mostrarComic2();
    }else if(mPasoTutorial==TutorialPasoType(kPaso2)){
        desactivarNext();
        mPasoTutorial=kPaso3;
        mostrarComic3();
    }else if(mPasoTutorial==TutorialPasoType(kPaso3)){
        desactivarNext();
        mostrarExplicacionLogicWorld2();
        mPasoTutorial=kPaso4;
    }else if(mPasoTutorial==TutorialPasoType(kPaso4)){
        desactivarNext();
        actualizarComicPorLogicWorld();
        mPasoTutorial=kPaso5;
    }else if(mPasoTutorial==TutorialPasoType(kPaso5)){
        desactivarNext();
        mPasoTutorial=kPaso6;
        mostrarElixirNecesario();
    }else if(mPasoTutorial==TutorialPasoType(kPaso6)){
        desactivarNext();
        mPasoTutorial=kPaso7;
        ocultarExploradorYMostrarBrujo();
    }else if(mPasoTutorial==TutorialPasoType(kPaso7)){
        desactivarNext();
        mPasoTutorial=kPaso8;
        mostrarComicBrujo2();
        
    }else if(mPasoTutorial==TutorialPasoType(kPaso8)){
        desactivarNext();
        mPasoTutorial=kPaso9;
        mostrarComicBrujo3();
    }else if(mPasoTutorial==TutorialPasoType(kPaso9)){
        desactivarNext();
        mPasoTutorial=kPaso10;
        LogicSQLHelper::getInstance().setTutorialSuperado();
        irAInicio();
    }


}


void TutorialScene::playSonido(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/serieslogic/insert_letter1.mp3",false);
}

CallFunc* TutorialScene::getActionPlaySonido(){
    
    return CallFunc::create([this]() { this->playSonido(); });
    //return CCCallFunc::create(this, callfunc_selector(TutorialScene::playSonido));
}

void TutorialScene::desactivarNext(){
    mContinuar->runAction(FadeOut::create(0.1f));
    mActivoNext=false;
}
void TutorialScene::activarNext(){
    mContinuar->runAction(FadeIn::create(0.1f));
    mActivoNext=true;
}



bool TutorialScene::onTouchBegan(Touch* touch, Event* event)
{
    TutorialScene::activarNext();
    return false;
}


void TutorialScene::onEnter(){
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(TutorialScene::onTouchBegan, this);
    Layer::onEnter();
}
void TutorialScene::onExit(){
     this->getEventDispatcher()->removeEventListener(mListener);
    Layer::onExit();
}


