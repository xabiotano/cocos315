#include "Levelsv2.h"
#include "SimpleAudioEngine.h"
#include "Variables.h"
#include "layers/LayerHelp.h"
#include "layers/LayerShop.h"
#include "InicioScene.h"
#include "layers/LayerHintOk.h"
#include "Levels.h"
#include "sql/SQLiteHelper.h"
#include "widgets/imports.h"

#include "widgets/LevelButton.h"
#include "helpers/LogicSQLHelper.h"
#include "layers/LayerDesbloqueadoNivel.h"

#include "series/SeriesScene.h"
#include "psico/PsicoScene.h"
#include "frutas/FrutasScene.h"

using namespace cocos2d;
using namespace CocosDenshion;



Levelsv2::~Levelsv2(void){
    CC_SAFE_RELEASE_NULL(mCol1);
    CC_SAFE_RELEASE_NULL(mCol2);
    CC_SAFE_RELEASE_NULL(mCol3);
    CC_SAFE_RELEASE_NULL(mNivelesJugables);
    mSlidingLayer->release();
  
}


//Levelsv2*  InstanciaLevelsv2;

/*Levelsv2* Levelsv2::GetInstance(){
    return InstanciaLevelsv2;
}*/

Scene* Levelsv2::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    Levelsv2 *layer = Levelsv2::create();
    
    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Levelsv2::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LevelsBase::initWithColor(Color4B(229,209,170,255)) )
    {
        return false;
    }

    mCol1 = Dictionary::create();
    mCol1->retain();
    mCol2 = Dictionary::create();
    mCol2->retain();
    mCol3 = Dictionary::create();
    mCol3->retain();
    mNivelesJugables = Dictionary::create();
    mNivelesJugables->retain();
   
    
    mVisibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    fondo = Sprite::create("levelsv2/background.png");
    
    // position the sprite on the center of the screen
    fondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    fondo->setTag(1021);
    this->addChild(fondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/fondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/fondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    fondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    mOffsetYInicial=0.f;
    mNumeroElixiresUsuario= LogicSQLHelper::getInstance().getElixiresGanados();
    mElixiresParaSiguienteNivel=LogicSQLHelper::getInstance().getElixiresParaSiguienteNivel();
  
    
    mSlidingLayer=ScrollView::create();
    mSlidingLayer->retain();
   
 
    
    item_madera_tex_bacth=SpriteBatchNode::create("levelsv2/item_madera.png", 150);
    item_elixir_cerebro_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_cerebro.png", 150);
    item_elixir_fresa_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_fresa.png", 150);
    item_elixir_energia_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_energia.png", 150);
    item_elixir_locked_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_locked.png", 150);
    item_elixir_item_wood_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_item_small_inside_wood.png", 150);
    item_elixir_item_wood_hole_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_item_small_inside_wood_hole.png", 150);
    item_elixir_item_tex_bacth=SpriteBatchNode::create("levelsv2/elixir_item_small.png", 150);
    
    item_madera_tex_bacth->setPosition(Point::ZERO);
    item_elixir_cerebro_tex_bacth->setPosition(Point::ZERO);
    item_elixir_fresa_tex_bacth->setPosition(Point::ZERO);
    item_elixir_energia_tex_bacth->setPosition(Point::ZERO);
    item_elixir_locked_tex_bacth->setPosition(Point::ZERO);
    item_elixir_item_wood_tex_bacth->setPosition(Point::ZERO);
    item_elixir_item_wood_hole_tex_bacth->setPosition(Point::ZERO);
    item_elixir_item_tex_bacth->setPosition(Point::ZERO);
    
    
    this->addChild(item_madera_tex_bacth);
    this->addChild(item_elixir_cerebro_tex_bacth);
    this->addChild(item_elixir_fresa_tex_bacth);
    this->addChild(item_elixir_energia_tex_bacth);
    this->addChild(item_elixir_locked_tex_bacth);
    this->addChild(item_elixir_item_wood_tex_bacth);
    this->addChild(item_elixir_item_wood_hole_tex_bacth);
    this->addChild(item_elixir_item_tex_bacth);

    
    
   
    
    //sdkbox::PluginChartboost::cache("Level Complete");
    pintarContenido();
    this->setKeypadEnabled(true);
    
    return true;
}


void Levelsv2::pintarContenido(){
    
    
    mMadero=Boton::createBoton(menu_selector(Levelsv2::nothing), "levelsv2/madero_superior.png");
    mMadero->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height- mMadero->getContentSize().height/2));
    this->addChild(mMadero);
    
    
    Sprite* elixir_mode=Sprite::create("levelsv2/elixir_mode.png");
    elixir_mode->setPosition(Point(mMadero->getContentSize().width/2,mMadero->getContentSize().height*0.6f));
    mMadero->addChild(elixir_mode);
    
    
    mPanel_izquierdo=Sprite::create("levelsv2/panel_izquierdo.png");
    mPanel_izquierdo->setScale((mVisibleSize.height-mMadero->getContentSize().height *0.9f ) / mPanel_izquierdo->getContentSize().height  );
    mPanel_izquierdo->setPosition(Point(mPanel_izquierdo->getBoundingBox().size.width/2,mPanel_izquierdo->getBoundingBox().size.height/2));
    this->addChild(mPanel_izquierdo);
    
    
    
    Sprite* explorer=Sprite::create("levelsv2/explorer_small.png");
    explorer->setPosition(Point(mPanel_izquierdo->getContentSize().width- explorer->getContentSize().width/2, explorer->getContentSize().height/2));
    mPanel_izquierdo->addChild(explorer);
    
    
     Label* label=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("LEVELS_V2_YOU_NEED"), CCGetFont(), 30);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label->setWidth(mPanel_izquierdo->getContentSize().width*0.95f);
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    label->setVerticalAlignment(TextVAlignment::TOP);
    label->enableOutline(Color4B(111,74,32,255),2);
     label->setPosition(Point(mPanel_izquierdo->getContentSize().width/2,mPanel_izquierdo->getContentSize().height*0.9f));
    mPanel_izquierdo->addChild(label,2);
    
  
    
    Sprite* botellita=Sprite::create("levelsv2/elixir_m.png");
    botellita->setPosition(Point(mPanel_izquierdo->getContentSize().width/2,mPanel_izquierdo->getContentSize().height*0.75f));
    mPanel_izquierdo->addChild(botellita,2);
    
    

    mNumeroElixiresParaSiguienteNivel=Label::createWithTTF(tostr(mElixiresParaSiguienteNivel).c_str(), CCGetFont(), 35);
    mNumeroElixiresParaSiguienteNivel->setWidth(botellita->getContentSize().width*0.9f);
    mNumeroElixiresParaSiguienteNivel->setHorizontalAlignment(TextHAlignment::CENTER);
    mNumeroElixiresParaSiguienteNivel->setVerticalAlignment(TextVAlignment::CENTER);
    mNumeroElixiresParaSiguienteNivel->setPosition(Point(botellita->getContentSize().width/2,botellita->getContentSize().height*0.35f));
    mNumeroElixiresParaSiguienteNivel->setTextColor(Color4B(255,255,255,255));
    mNumeroElixiresParaSiguienteNivel->enableOutline(Color4B(193,39,37,255),3);
    botellita->addChild(mNumeroElixiresParaSiguienteNivel,3);
    

    Label* label_unlock=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("LEVELS_V2_TO_UNLOCK","LEVELS_V2_TO_UNLOCK"), CCGetFont(), 25);
    label_unlock->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label_unlock->setWidth(mPanel_izquierdo->getContentSize().width*0.9f);
    //series_mode->setHeight(getContentSize().height);
    label_unlock->setHorizontalAlignment(TextHAlignment::CENTER);
    label_unlock->setVerticalAlignment(TextVAlignment::TOP);
    label_unlock->enableOutline(Color4B(111,74,32,255),2);
    label_unlock->setPosition(Point(mPanel_izquierdo->getContentSize().width/2,mPanel_izquierdo->getContentSize().height*0.5f));
     mPanel_izquierdo->addChild(label_unlock,2);
    
    
    
    Label* series_mode=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("LOGIC_MODE","LOGIC_MODE"), CCGetFont(), 30);
    series_mode->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    series_mode->setWidth(mPanel_izquierdo->getContentSize().width*0.87f);
    series_mode->setHorizontalAlignment(TextHAlignment::CENTER);
    series_mode->setVerticalAlignment(TextVAlignment::TOP);
     series_mode->setColor(Color3B(131,213,59));
    series_mode->enableOutline(Color4B(0,0,0,255),2);
    series_mode->setPosition(Point(mPanel_izquierdo->getContentSize().width/2,label_unlock->getPositionY()- label_unlock->getContentSize().height*0.85f));
    mPanel_izquierdo->addChild(series_mode,2);
    

    
    
    
    
    
    
    
    Sprite* hojas= Sprite::create("levelsv2/hojas_izquierda.png");
    hojas->setPosition(Point(hojas->getContentSize().width/2,mVisibleSize.height- hojas->getContentSize().height/2));
    this->addChild(hojas);
    
    Sprite* hojasD= Sprite::create("levelsv2/hojas_izquierda.png");
    hojasD->setPosition(Point(mVisibleSize.width-hojas->getContentSize().width/2,mVisibleSize.height- hojas->getContentSize().height/2));
    hojasD->setScaleX(-1);
    this->addChild(hojasD);
    
    
    

    
   this->setNumeroNiveles(150);//MUCHO MAS RAPIDO QUE CONULTARLO JEJE
  
    
}



void Levelsv2::crearScroll(){
    
    mPaddingY=item_madera_tex_bacth->getTexture()->getContentSize().height*0.1f;
    
    int widthFila=mVisibleSize.width*0.95f-mPanel_izquierdo->getBoundingBox().size.width;
    mHeightFila=item_madera_tex_bacth->getTexture()->getContentSize().height;
    
   
    float numeroItemsPorFila=3.f;
    float numeroFilasFloat= mNumeroItems/numeroItemsPorFila;
    
    int numeroFilas= (int) floor(numeroFilasFloat);

    float heightLayer= numeroFilas *(mHeightFila);
    CCLOG("NumeroFilas:%i , heightLayer %f",numeroFilas,heightLayer);
    
    mFondo= LayerColor::create(Color4B(0,0,0,0), widthFila, heightLayer);
    mFondo->setIgnoreAnchorPointForPosition(false);
    mFondo->setAnchorPoint(Point(0.5,0.5));
    
    
    mSlidingLayer->setContentSize(mFondo->getContentSize());
    mSlidingLayer->setDirection(ScrollView::Direction::VERTICAL);
    mSlidingLayer->setPosition(Point(mPanel_izquierdo->getBoundingBox().size.width,mVisibleSize.height*0.f ));
    fondo->addChild(mSlidingLayer,1);

    mSlidingLayer->setBounceable(true);
    
    mSlidingLayer->setClippingToBounds(true);
    mSlidingLayer->setContainer(mFondo);
    
    mSlidingLayer->setViewSize(Size(mVisibleSize.width-mPanel_izquierdo->getContentSize().width ,mVisibleSize.height*0.9f));
    //mSlidingLayer->setContentOffset(Point(5.f, (mVisibleSize.height * 0.75-mFondo->getContentSize().height)), true);
   
    
    
   
    mNumeroItem=1;
    mFila=0;
    mColumna=0;
    pintarNiveles();
    
}



int Levelsv2::RandomInt2(int min, int max)
{
    return min + (rand() % (int)(max - min + 1));
}



void Levelsv2::contarNiveles()
{
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    sprintf (sqlgenerado, "SELECT count(idnivel) as numero from niveles");
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, cargarNumeroNiveles,this, &SQLiteHelper::zErrMsg);
    
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
    
    
}


int Levelsv2::cargarNumeroNiveles(void *NotUsed, int argc, char **argv, char **azColName)
{
    Levelsv2* seriesScene= (Levelsv2*) NotUsed;
    int numeroniveles;
    for(int i=0; i<argc; i++)
    {
        if(strcmp( azColName[i],"numero")==0){
            numeroniveles=atoi(argv[i]);
            CCLOG("Numero niveles %i",numeroniveles);
        }
      
    }
    seriesScene->setNumeroNiveles(numeroniveles);
    return 0;

}

void Levelsv2::setNumeroNiveles(int numero){
    mNumeroItems=numero;
    //una vez contados el numero de niveles
    crearScroll();
}


void Levelsv2::pintarNiveles()
{
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    
    //como mucho muestro 20
    sprintf (sqlgenerado, "SELECT  idnivel,tiponivel,superado,locked,estrellas from niveles ORDER BY idnivel");
    
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, cargarNiveltexto,this, &SQLiteHelper::zErrMsg);
   
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }
  
}



// This is the callback function to display the select data in the table
int Levelsv2::cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName)
{
   
    Levelsv2* levelv2= (Levelsv2*) NotUsed;
    int idnivel;
    int tiponivel;
    int superado;
    int locked;
     int estrellas;
    for(int i=0; i<argc; i++)
    {
        if(strcmp( azColName[i],"idnivel")==0){
            idnivel=atoi(argv[i]);
        }
        else if(strcmp( azColName[i],"tiponivel")==0){
           tiponivel=atoi(argv[i]);
        } else if(strcmp( azColName[i],"superado")==0){
            superado=atoi(argv[i]);
        }else if(strcmp( azColName[i],"locked")==0){
            locked=atoi(argv[i]);
        }else if(strcmp( azColName[i],"estrellas")==0){
            estrellas=atoi(argv[i]);
        }
    }
   
    levelv2->creaItemNivel(idnivel,tiponivel,(superado==1),(locked==1),estrellas);
    return 0;
}

void Levelsv2::creaItemNivel(int idnivel,int tiponivel,bool superado,bool locked,int numeroestrellas){
   
    CCLOG("InfoNivel:[%i =>superado:%i] ",idnivel,superado);
    mNivelesJugables->setObject(Bool::create(!locked), idnivel);
    if(mColumna==0){
        mCol1->setObject(Bool::create(superado), idnivel);
    }else if(mColumna==1){
        mCol2->setObject(Bool::create(superado), idnivel);
    }else if(mColumna==2){
        mCol3->setObject(Bool::create(superado), idnivel);
    }
    CCLOG("[idnivel:%i,tiponivel:%i,superado:%i ;locked:%i; numeroestrellas:%i]",idnivel,tiponivel,superado,locked,numeroestrellas);
    
   LevelButton* boton=LevelButton::createLevelButtonWithTexture(menu_selector(Levelsv2::onClickNivel),item_madera_tex_bacth->getTexture(),tiponivel,idnivel);

  

    Point punto;
    if(mColumna==0){
        punto=Point(mFondo->getContentSize().width * 0.165f,mFondo->getContentSize().height- (mHeightFila *mFila +item_madera_tex_bacth->getTexture()->getContentSize().height/2 ));
    }else if(mColumna==1){
        punto=Point(mFondo->getContentSize().width * 0.5f,mFondo->getContentSize().height- (mHeightFila* mFila +item_madera_tex_bacth->getTexture()->getContentSize().height/2));
        
    }else if(mColumna==2){
        punto=Point(mFondo->getContentSize().width * 0.835f,mFondo->getContentSize().height- (mHeightFila* mFila+item_madera_tex_bacth->getTexture()->getContentSize().height/2));
        
    }
    boton->setPosition(punto);
    boton->setScale((mFondo->getContentSize().width/3.f)/boton->getContentSize().width);
    
    LabelBMFont* label=LabelBMFont::create(tostr(mNumeroItem).c_str(), "font.fnt",-1, TextHAlignment::LEFT,Point(0,0));
   
    label->setScale(0.6f);
    label->setPosition(Point(boton->getContentSize().width/2,boton->getContentSize().height*0.22f));
    boton->addChild(label,2);
    
    
    
    mFondo->addChild(boton,3);
    boton->setTag(mNumeroItem);
    pintarElixir(boton,idnivel, tiponivel, superado, locked,numeroestrellas);
   
    
 

    //ajusto el scroll al ultimo sin superar
    if((superadoAnterior && !superado )|| (!superado && mFila==0)){
        if(mOffsetYInicial==0.f){
             mOffsetYInicial= -boton->getPositionY()+mHeightFila/2;
           
        }
       
        
       
    }
   
   
    
    superadoAnterior=superado;
   
    
    mColumna++;
    if(mColumna>2){
        mColumna=0;
        mFila++;
    }
    mNumeroItem++;
    if(mNumeroItem==mNumeroItems){
        this->scheduleOnce(schedule_selector(Levelsv2::cargarAnimacionNivelSuperado), 0.5f);
        this->scheduleOnce(schedule_selector(Levelsv2::ponerEnSuSitioOffset),0.2f);
        SQLiteHelper::closeDB();
    }
}

void Levelsv2::ponerEnSuSitioOffset(float dt){
   
    float Offset=mOffsetYInicial + mHeightFila/2;
    mSlidingLayer->setContentOffset(Point(0.f, Offset), true);
    CCLOG("ajusto el scroll al ultimo sin superar :%f",Offset);

}



void Levelsv2::pintarElixir(LevelButton* item,int idnivel,int tipo,bool superado,bool locked,int numeroestrellas){
    if(superado){
        item->pintarElixiresGanados(this,numeroestrellas);
    }else{
        item->pintarTipo(this,superado, locked, tipo);
    }
}

void Levelsv2::animarContadoresElixirRecienSuperado(int numEstrellas,int idnivel){
    
    mDestinoUnlock=(Sprite*)mFondo->getChildByTag(idnivel);
    mSlidingLayer->setContentOffset(Point(0.f, -mDestinoUnlock->getPositionY() + mHeightFila), true);
    
    CallFunc* sumarContadorCallback =CallFunc::create([this]() { this->sumarElixirContadores(); });
    
    Sequence* secuencia=Sequence::create(DelayTime::create(0.35f),
                                             sumarContadorCallback,NULL);
    if(numEstrellas>1){
         secuencia=Sequence::create(DelayTime::create(0.35f),
                                      sumarContadorCallback,
                                      DelayTime::create(0.35f),
                                      sumarContadorCallback,NULL);
    }if(numEstrellas>2){
        secuencia=Sequence::create(DelayTime::create(0.35f),
                                     sumarContadorCallback,
                                     DelayTime::create(0.35f),
                                     sumarContadorCallback,
                                     DelayTime::create(0.35f),
                                     sumarContadorCallback,
                                     NULL);
    }
    this->runAction(secuencia);
}









void Levelsv2::cargarAnimacionNivelSuperado(float dt){
    
    CCLOG("cargarAnimacionNivelSuperado");
   // LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(true,12,3);
    if(LogicSQLHelper::getInstance().getAnimacionDesbloqueoPendiente()){
        
        int idnivel=LogicSQLHelper::getInstance().getAnimacionDesbloqueoPendienteIdNivel();
        int numEstrellas=LogicSQLHelper::getInstance().getAnimacionDesbloqueoPendienteEstrellas();
        CCLOG("cargarAnimacionNivelSuperado.getAnimacionDesbloqueoPendiente idnivel:%i numEstrellas:%i",idnivel,numEstrellas );
        //hago una explosion en dicho boton
        LevelButton* itemAAnimar=(LevelButton*) mFondo->getChildByTag(idnivel);
        itemAAnimar->animarElixirRecienSuperado(this,numEstrellas);
        animarContadoresElixirRecienSuperado(numEstrellas,idnivel);
        
        bool col1Superado=false;
        bool col2Superado=false;
        bool col3Superado=false;
        
        int columna=-1;
        if(mCol1->objectForKey(idnivel)!=NULL){
            columna=1;
            col1Superado=true;
        }else if(mCol2->objectForKey(idnivel)!=NULL){
            columna=2;
            col2Superado=true;
        }else if(mCol3->objectForKey(idnivel)!=NULL){
            columna=3;
            col3Superado=true;
        }
        CCLOG("columna %i idnivel%i",columna,idnivel);
        int idnivelPrimeraFila=-1;
        
        if(columna==1){
            idnivelPrimeraFila=idnivel;
            col2Superado= ((Bool*)mCol2->objectForKey(idnivel+1))->getValue();
            col3Superado= ((Bool*)mCol3->objectForKey(idnivel+2))->getValue();
        }else if(columna==2){
             idnivelPrimeraFila=idnivel-1;
            col1Superado= ((Bool*)mCol1->objectForKey(idnivel-1))->getValue();
            col3Superado= ((Bool*)mCol3->objectForKey(idnivel+1))->getValue();
        }else if(columna==3){
            idnivelPrimeraFila=idnivel-2;
            col1Superado= ((Bool*)mCol1->objectForKey(idnivel-2))->getValue();
            col2Superado= ((Bool*)mCol2->objectForKey(idnivel-1))->getValue();
        }
        CCLOG("superadofila[ %i ; %i ;%i]primero:%i",col1Superado,col2Superado,col3Superado,idnivelPrimeraFila);
        if(col1Superado && col2Superado && col3Superado){
            CCLOG("FILA SUPERADA!!");
            LogicSQLHelper::getInstance().setNivelDesbloqueado(idnivelPrimeraFila+3);
            LogicSQLHelper::getInstance().setNivelDesbloqueado(idnivelPrimeraFila+4);
            LogicSQLHelper::getInstance().setNivelDesbloqueado(idnivelPrimeraFila+5);
            
            //actualizo el array
            mNivelesJugables->setObject(Bool::create(true), idnivelPrimeraFila+3);
            mNivelesJugables->setObject(Bool::create(true), idnivelPrimeraFila+4);
            mNivelesJugables->setObject(Bool::create(true), idnivelPrimeraFila+5);

       
            mDestinoUnlock=(Sprite*)mFondo->getChildByTag(idnivelPrimeraFila+5);
            CallFunc* hacerScrollYDesbloquear=CallFunc::create([this]() { this->hacerScrollUnlock(); });
            this->runAction(Sequence::create(DelayTime::create(1.f),hacerScrollYDesbloquear,NULL));
       }
      
        
        LogicSQLHelper::getInstance().setNivelSuperadoElixir(idnivel, numEstrellas);
        LogicSQLHelper::getInstance().setNivelSuperadoElixirenMemoria(idnivel);
        LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(false, 0, 0);
        LogicSQLHelper::getInstance().addElixiresUsuario(numEstrellas);
        
        
    }
}

void Levelsv2::hacerScrollUnlock(){
    //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/reveal.mp3",false);
    
    mSlidingLayer->setContentOffset(Point(0.f, -mDestinoUnlock->getPositionY() + mHeightFila), true);
     CCLOG("hacerScrollUnlock :%i [%f]", mDestinoUnlock->getTag(),-mDestinoUnlock->getPositionY());
    
   /* CCParticleSystem* emitter = ParticleSystemQuad::create("particles/unlock.plist");
    mFondo->addChild( emitter, 5 );
    emitter->setPosition(Point(0, mDestinoUnlock->getPositionY()) );
    emitter->runAction(MoveTo::create(1.f, Point(mFondo->getContentSize().width + mDestinoUnlock->getContentSize().width ,mDestinoUnlock->getPositionY()   )));
    
    CCCallFunc* moveCallback = CCCallFunc::create(emitter,callfunc_selector(CCParticleSystem::removeFromParent));
    DelayTime* delayAction = DelayTime::create(2.f);
    emitter->runAction(Sequence::create(delayAction, moveCallback, NULL));
    
    //hago sonidos cada 1.seg /3
    CCCallFunc* sonido=CCCallFunc::create(this,callfunc_selector(Levelsv2::playSonidoUnlock));
    
    this->runAction(Sequence::create(DelayTime::create(0.25f),sonido,DelayTime::create(0.25f),sonido,DelayTime::create(0.25f),sonido,NULL));
    
    
 
    
    //animo los items
    LevelButton* itemCol1=(LevelButton*)mFondo->getChildByTag(mDestinoUnlock->getTag()-2);
    LevelButton* itemCol2=(LevelButton*)mFondo->getChildByTag(mDestinoUnlock->getTag()-1);
    LevelButton* itemCol3=(LevelButton*)mDestinoUnlock;
    
    CCCallFunc* unlockFuncion1=CCCallFunc::create(itemCol1, callfunc_selector(LevelButton::unlock));
    itemCol1->runAction(Sequence::create(DelayTime::create(0.25f),unlockFuncion1,NULL));
    CCCallFunc* unlockFuncion2=CCCallFunc::create(itemCol2, callfunc_selector(LevelButton::unlock));
    itemCol2->runAction(Sequence::create(DelayTime::create(0.25f),unlockFuncion2,NULL));
    CCCallFunc* unlockFuncion3=CCCallFunc::create(itemCol3, callfunc_selector(LevelButton::unlock));
    itemCol3->runAction(Sequence::create(DelayTime::create(0.25f),unlockFuncion3,NULL));
    */
    
    
    
}

void Levelsv2::playSonidoUnlock(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/item_unlock.mp3",false);
}
                    
void Levelsv2::sumarElixirContadores(){
    
    mNumeroElixiresUsuario++;
    
    mLabelNumeroItems->setString(tostr(mNumeroElixiresUsuario).c_str());
    mLabelNumeroItems->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.1f, 1.1f), 3),EaseInOut::create(ScaleTo::create(0.1f, 1.f), 3),NULL ));
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/itemplus.wav",false);
    mElixiresParaSiguienteNivel--;
    if(mElixiresParaSiguienteNivel>=0){
       
        mNumeroElixiresParaSiguienteNivel->setString(tostr(mElixiresParaSiguienteNivel).c_str());
        mNumeroElixiresParaSiguienteNivel->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.1f, 1.1f), 3),EaseInOut::create(ScaleTo::create(0.1f, 1.f), 3),NULL ));
    }
    if(mElixiresParaSiguienteNivel<=0){
        if(Variables::ES_DEBUG || LogicSQLHelper::getInstance().estaElJuegoCompleto()){
            return;
        }
        //muestro panel de LevelSeries Unlocked;
        //muestro una layer de lock
        LayerBlock* layerBlock=LayerBlock::create();
        this->addChild(layerBlock,50);
        layerBlock->setPosition(Point(0,0));
        layerBlock->setTag(LayerBlock::TAG_LAYER_BLOCK);
        CallFunc* call=CallFunc::create([this]() { this->mostrarDesbloqueoNivel(); });
        this->runAction(Sequence::createWithTwoActions(DelayTime::create(1.5f),call));
    }
   

    
}


void Levelsv2::mostrarDesbloqueoNivel(){
    getChildByTag(LayerBlock::TAG_LAYER_BLOCK)->removeFromParent();
    LayerDesbloqueadoNivel::mostrar();
    //actualizo lo que necesitas para el siguiente
    mElixiresParaSiguienteNivel=LogicSQLHelper::getInstance().getElixiresParaSiguienteNivel();
    mNumeroElixiresParaSiguienteNivel->setString(tostr(mElixiresParaSiguienteNivel).c_str());
}




void Levelsv2::onClickNivel(Ref* sender)
{
    CCLOG("niveles jugables %i", mNivelesJugables->count());
    bool jugable= ((Bool*)mNivelesJugables->objectForKey(Variables::LEVELS_ELIXIR_CLICKADO))->getValue();
    if(jugable ||Variables::ES_DEBUG){
        LevelButton* levelbutton=(LevelButton*)mFondo->getChildByTag(Variables::LEVELS_ELIXIR_CLICKADO);
        Scene* escena;
        if(levelbutton->mTipo==1){
            escena=SeriesScene::scene();
            Variables::CURRENT_SCENE=Variables::SERIES_LEVEL;
        }else if(levelbutton->mTipo==2){
              escena=FrutasScene::scene();
            Variables::CURRENT_SCENE=Variables::FRUTAS_LEVEL;
        }else if(levelbutton->mTipo==3){
            escena=PsicoScene::scene();
            Variables::CURRENT_SCENE=Variables::PSICO_LEVEL;
        }
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/select.mp3",false);
        CCLOG("levelbutton->mTipo= %i",levelbutton->mTipo);
        Director *pDirector = Director::getInstance();
        pDirector->replaceScene(TransitionCrossFade::create(0.5f, escena));
    }else{
        mFondo->getChildByTag(Variables::LEVELS_ELIXIR_CLICKADO)->runAction(Sequence::create(RotateTo::create(0.25f, 10), RotateTo::create(0.25f, -10.f),RotateTo::create(0.25f, 0),NULL));
    }
}








void Levelsv2::compradoItem(const sdkbox::Product &p){
    int numero=LogicSQLHelper::getInstance().getElixiresGanados();
    mLabelNumeroItems->setString(tostr(numero).c_str());
}

void Levelsv2::onKeyBackClicked()
{
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP)){
        LayerShop::ocultar(NULL);
    }else{
        Back(NULL);
    }
}


void Levelsv2::nothing(Ref* sender){

}

void Levelsv2::onExit()
{
    setKeypadEnabled(false);
    mFondo->setKeypadEnabled(false);
    mSlidingLayer->setKeypadEnabled(false);
    LevelsBase::onExit();
}



template <typename T> std::string Levelsv2::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

