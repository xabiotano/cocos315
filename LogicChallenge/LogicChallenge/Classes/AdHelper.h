//PrintMessage.h
#ifndef __AdHelper_H__
#define __AdHelper_H__

#include <stddef.h>
#include "cocos2d.h"

USING_NS_CC;

class AdHelper:public cocos2d::Node
{
public:
    AdHelper();
    static AdHelper* getInstance();
    static void showIntersitial();
    void showIntersitialWithMaxDelay(float maxDelay);
   
    static void showBanner();
    static void hideBanner();
   
    
    void setIntersitialCached(bool cacheado);
    bool getIntersitialCached();
    void setIntersitialAdWhenLoaded(bool value);
    bool getIntersitialAdWhenLoaded();

    void interstitialDidAppear();
    
private:
    static void cacheIntersitial();
    bool mShowIntersitialAdWhenLoaded=false;
    bool mInteristialCached;
    static AdHelper* _adHelperinstancia;
    
   
    void showIntersitialPrivate(Node* sender);
    
    

    
};

#endif





