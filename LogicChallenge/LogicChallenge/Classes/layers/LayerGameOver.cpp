#include "LayerGameOver.h"
#include "../widgets/Boton.h"
#include "SimpleAudioEngine.h"



LayerGameOver:: ~LayerGameOver(void){}

LayerGameOver* LayerGameOver::initLayer(std::string textoGameOver, bool smallGameOver,TipoNivel tipoNivel)
{
    LayerGameOver *layer = LayerGameOver::create();
    layer->mTipoNivel=tipoNivel;
    layer->setGameOverText(textoGameOver);
    if(smallGameOver){
        layer->setMinVersion();
    }
    layer->setBotones();
    layer->mPriority=-100;
    return layer;
}



// on "init" you need to initialize your instance
bool LayerGameOver::init()
{
    // 1. super init first
    mSize = Director::getInstance()->getVisibleSize();
    if (!LayerBase::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
    
    
    fondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    fondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.5f) );
    fondo_panel->setPosition(Point(mSize.width*0.5f,mSize.height*0.5f));
    fondo_panel->setTag(100);
    this->addChild(fondo_panel);
    fondo_panel->setColor(Color3B(255,220,220));
    
    Sprite* ribbon=Sprite::create("ui/levels/ribbon_red.png");
    ribbon->setPosition(Point(fondo_panel->getContentSize().width/2,fondo_panel->getContentSize().height*0.9f));
    fondo_panel->addChild(ribbon);
    ribbon->setTag(101);
    //ribbon->setColor(Color3B(255,200,200));
    
    /*CCLabelStroke* label=CCLabelStroke::create(ribbon, mTextoGameOver.c_str(), CCGetFont(), 40, ribbon->getContentSize(), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    label->setPosition(Point(ribbon->getContentSize().width/2,ribbon->getContentSize().height*0.67f));
    ribbon->addChild(label,4);

    label->setString(LanguageManager::getInstance()->getStringForKey("GAMEOVER","GAME OVER"),Color3B(0,0,0));
    */
    Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("GAMEOVER", "GAME OVER"), CCGetFont(), 40);
    label->setColor(Color3B(255,255,255));
    label->setWidth(ribbon->getContentSize().width);
    label->setHeight(ribbon->getContentSize().height);
    label->setPosition(Point(ribbon->getContentSize().width/2,ribbon->getContentSize().height*0.67f));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255),2);
    ribbon->addChild(label,4);
    
    /*
    CCLabelStroke* explicacion=CCLabelStroke::create(fondo_panel, "", CCGetFont(), 20, Size(fondo_panel->getContentSize().width*0.8f, fondo_panel->getContentSize().height*0.7f), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    explicacion->setPosition(Point(fondo_panel->getContentSize().width/2,fondo_panel->getContentSize().height*0.51f));
    fondo_panel->addChild(explicacion,4);
    explicacion->setTag(333);
    */
    
    Label* explicacion= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("GAMEOVER", "GAME OVER"), CCGetFont(), 20);
    explicacion->setColor(Color3B(255,255,255));
    explicacion->setWidth(fondo_panel->getContentSize().width*0.8f);
    explicacion->setHeight( fondo_panel->getContentSize().height*0.7f);
    explicacion->setPosition(Point(fondo_panel->getContentSize().width/2,fondo_panel->getContentSize().height*0.51f));
    explicacion->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    explicacion->enableOutline(Color4B(0,0,0,255),2);
    fondo_panel->addChild(explicacion,4);
    explicacion->setTag(333);

    
   
    
    
    
    this->setKeypadEnabled(true);

    
    return true;
}


void LayerGameOver::setBotones(){
    Boton* menu=Boton::createBotonWithPriority(this, menu_selector(LayerBase::Menu), "ui/levels/menu.png",-102);
    fondo_panel->addChild(menu);
    menu->setSound("sounds/ui/click1.mp3");
    
    
    Boton* back=Boton::createBotonWithPriority(this, menu_selector(LayerGameOver::Hint), "ui/hint.png",-102);
        fondo_panel->addChild(back);
    back->setSound("sounds/ui/click1.mp3");

    
    Boton* retry=Boton::createBotonWithPriority(this, menu_selector(LayerBase::Reload), "ui/reload.png",-102);
        fondo_panel->addChild(retry);
    retry->setSound("sounds/ui/click1.mp3");

    
    if(mTipoNivel==TipoNivel(kNivelLogica)){
        menu->setPosition(Point(fondo_panel->getContentSize().width*0.25f, fondo_panel->getContentSize().height*0.22f));
        back->setPosition(Point(fondo_panel->getContentSize().width*0.5f, fondo_panel->getContentSize().height*0.22f));
        retry->setPosition(Point(fondo_panel->getContentSize().width*0.75f, fondo_panel->getContentSize().height*0.22f));
    }else{
        menu->setPosition(Point(fondo_panel->getContentSize().width*0.33f, fondo_panel->getContentSize().height*0.22f));
        back->removeFromParent();
        retry->setPosition(Point(fondo_panel->getContentSize().width*0.66f, fondo_panel->getContentSize().height*0.22f));
    }
}


void LayerGameOver::setMinVersion(){
    Scale9Sprite* fondo=(Scale9Sprite*) this->getChildByTag(100);
    Sprite* ribbon=(Sprite*) fondo->getChildByTag(101);
    fondo->setOpacity(50.5f);
    ribbon->setOpacity(50.5f);
    this->setOpacity(100.f);
}












void LayerGameOver::setGameOverText(std::string textoGameOver){
    Label* label=(Label*)fondo_panel->getChildByTag(333);
    label->setString(textoGameOver.c_str());
}



void LayerGameOver::close()
{
    this->getParent()->removeChild(this);
}



void LayerGameOver::Hint(Ref* sender){
    LayerGameOver::close();
    BaseScene::getInstance()->Hint(NULL);
};


void LayerGameOver::onEnter()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/gameover1.mp3");
    LayerBase::onEnter();
}





