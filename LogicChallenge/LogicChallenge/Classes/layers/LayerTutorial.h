//
//  LayerTutorial.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerTutorial__
#define __trivial__LayerTutorial__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerTutorial:  public LayerBase
{
    public:
    
    static LayerTutorial* initLayer(std::string texto);
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerTutorial);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerTutorial* mostrar(std::string texto);
 
  
       
  
    
    static int TAG_TUTORIAL;
    void setTexto(std::string texto);
    Node* getCanvas();
    Label* crearTexto(std::string texto);
    void setNoOcultable();
    void setSprite(Sprite* imagen);
    void setBoton(Boton* boton1);
    void set2Boton(Boton* boton1,Boton *boton2);
private:
    Label* mTexto;
    Boton* mClose;
    ScrollView* mScroll;
    std::string mTextoHelp;
    Sprite* mFondo;
    bool mOcultable;
    
    


};

#endif 
