//
//  LayerGameOver.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerGameOver__
#define __trivial__LayerGameOver__

#include <iostream>
#include "cocos2d.h"
#include "../BaseScene.h"
#include "LayerBase.h"


USING_NS_CC;


class LayerGameOver:  public LayerBase
{
    public:
    
        virtual ~LayerGameOver(void);
        static LayerGameOver* get_instance();
        static LayerGameOver* initLayer(std::string textoGameOver,bool smallGameOver,TipoNivel tipoNivel);
        Rect rect();
    
        virtual bool init() override;
    
        void onEnter() override;
        CREATE_FUNC(LayerGameOver);
        
       
    
       
        std::string mTextoGameOver;
        void setGameOverText(std::string textoGameOver);
        void setMinVersion();
    void setBotones();
    private:
        void close();
        Scale9Sprite* fondo_panel;
    
    protected:

        virtual void Hint(Ref* sender);

    
          
    
        
    
};

#endif /* defined(__trivial__LayerGameOver__) */
