//
//  LayerGanarItem.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerGanarItem__
#define __trivial__LayerGanarItem__

#include <iostream>
#include "cocos2d.h"

#include "../BaseScene.h"
#include "LayerBase.h"


USING_NS_CC;


class LayerGanarItem: public LayerBase
{
    public:
    
        virtual ~LayerGanarItem(void);
        static LayerGanarItem* get_instance();
        static LayerGanarItem* initLayer(int numItems);
        Rect rect();
        // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
        virtual bool init();
        // 'layer' is an autorelease object
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        CREATE_FUNC(LayerGanarItem);
    
    

        bool onTouchBegan(Touch* touch, Event* event) override;
    
    

       
        Size mVisibleSize;
        void dibujarItems(int numGranos);
        static void mostrar(int numItems);
        int mNumeroItems;
        template <typename T> std::string tostr(const T& t);
    
    private:
    
    bool primerUpdate;
        void close(Ref* sender);
        Sprite* mFlecha;
        void iniciarSorteo(float dt);
        static const int TAG_GANAR_ITEM=259313;
        bool mIniciada;
        int mNumeroVueltas;
        int mAngle;
        void finRotacion();
        void ganarItem(Ref* sender);
        int mGanados;
      
        int mPaso;
        void update( float dt );
        float mIndiceRotacion;
        Sprite* mItemganadoSimbolo;
        void irANiveles();
        int itemsGanadosRestantes;
        void animarPanelClose(Ref* sender);
    
        bool mDesbloqueo;
        SEL_SCHEDULE mSelectorGanarItem;
        unsigned int mSonidoRuleta;
    
        Sprite* mRuleta;
        int tag1,tag2_1,tag2_2,tag3_1,tag3_2,tag3_3;
    
};

#endif /* defined(__trivial__LayerGanarItem__) */
