//
//  LayerSuccess.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerSuccess__
#define __trivial__LayerSuccess__

#include <iostream>
#include "cocos2d.h"

#include "../BaseScene.h"
#include "LayerBase.h"

USING_NS_CC;


class LayerSuccess: public LayerBase
{
    public:
        const int TAG_FONDO=10;
        const int TAG_ESTRELLA1=20;
        const int TAG_ESTRELLA2=21;
        const int TAG_ESTRELLA3=22;
        static const int TAG_LAYER_SUCCESS=294061;
    
        virtual ~LayerSuccess(void);
        static LayerSuccess* get_instance();
        static LayerSuccess* initLayer(BaseScene* base,int numStars,TipoNivel tipoNivel);
        static LayerSuccess* mostrar(BaseScene* base,int numStars, TipoNivel tipoNivel);
        Rect rect();
        // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
        virtual bool init() override;
        // 'layer' is an autorelease object
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        CREATE_FUNC(LayerSuccess);
    
    
    
        
        Size mSize;
        void dibujarEstrellas(int numStars);
        void dibujarElixires(int numEnergys);
        void setTexto(std::string texto);
        Scale9Sprite* mFondo_panel;
        Boton* mNextButton;
        Boton* mMenuButton;
        Boton* mReloadButton;

        void onEnter() override;
    private:
        void close(Ref* sender);
        BaseScene* mBase;
        void Next(Ref* sender);
    
    
    
    
    
        
    
};

#endif /* defined(__trivial__LayerSuccess__) */
