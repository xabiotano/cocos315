#include "LayerBlock.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"





LayerBlock* LayerBlock::create()
{
    Size  mVisibleSize = Director::getInstance()->getVisibleSize();
   
    LayerBlock * pLayer = new LayerBlock();
    if( pLayer && pLayer->initWithColor(Color4B(0,0,0,0),mVisibleSize.width,mVisibleSize.height))
    {
        pLayer->autorelease();
        return pLayer;
    }
    CC_SAFE_DELETE(pLayer);
    return NULL;
    
}



void LayerBlock::onEnter()
{
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(LayerBlock::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(LayerBlock::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(LayerBlock::onTouchEnded, this);
    
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener, this);
    Layer::onEnter();
}

void LayerBlock::onExit()
{
     this->getEventDispatcher()->removeEventListener(mListener);
   
    
    Layer::onExit();
}


bool LayerBlock::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}

void LayerBlock::onTouchEnded(Touch* touch, Event* event)
{
    
}
void LayerBlock::onTouchMoved(Touch* touch, Event* event)
{
}

