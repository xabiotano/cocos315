//
//  LayerBlock.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerBlock__
#define __trivial__LayerBlock__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerBlock:  public LayerColor
{
    public:
    virtual void onEnter();
    virtual void onExit();

    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);

    
    static LayerBlock* create();
    
    static const int TAG_LAYER_BLOCK=1928;


};

#endif 
