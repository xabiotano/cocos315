//
//  LayerExplicarItems.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerExplicarItems.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"




int LayerExplicarItems::TAG_EXPLICACION_ITEM=12115;


LayerExplicarItems* LayerExplicarItems::initLayer(){
    LayerExplicarItems *layer = LayerExplicarItems::create();
    layer->dibujarContent();
    layer->mSwallowTouches=true;
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerExplicarItems::init()
{
    if (!LayerBase::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
    
   
    this->setKeypadEnabled(true);
    return true;
}

void LayerExplicarItems::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/info_panel_xxl.png");
    
 
    
    
    
    
    Sprite* coffeItem= Sprite::create("elixircomun/elixir_m.png");
    coffeItem->setPosition(Point(mFondo->getContentSize().width*0.12f,mFondo->getContentSize().height*0.7f));
    mFondo->addChild(coffeItem);
    coffeItem->setScale(1.5f);
    
    /*CCLabelTTF* label=CCLabelTTF::create(LanguageManager::getInstance()->getStringForKey("EXPLICACION_CAFE", "EXPLICACION_CAFE"), "fonts/KGBlankSpaceSketch.ttf", 40,Size(mFondo->getContentSize().width*0.7, mFondo->getContentSize().height*0.4), kCCTextAlignmentLeft, kCCVerticalTextAlignmentCenter);*/
    Label* label=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("EXPLICACION_CAFE", "EXPLICACION_CAFE"), "fonts/KGBlankSpaceSketch.ttf",40);
    label->setWidth(mFondo->getContentSize().width*0.7);
    label->setHeight(mFondo->getContentSize().height*0.4);
    label->setHorizontalAlignment(TextHAlignment::LEFT);
    label->setVerticalAlignment(TextVAlignment::CENTER);
    label->setPosition(Point(mFondo->getContentSize().width*0.56f,mFondo->getContentSize().height*0.7));
    mFondo->addChild(label,4);
    label->setColor(Color3B(107,128,140));
    
   
    
    
    
    Sprite* energyItem= Sprite::create("elixircomun/elixir_m.png");
    energyItem->setPosition(Point(mFondo->getContentSize().width*0.12f,mFondo->getContentSize().height*0.3f));
    mFondo->addChild(energyItem);
    energyItem->setScale(1.5f);

    
    
    
    
    
    
    
    /*CCLabelTTF* labelEnergy=CCLabelTTF::create(LanguageManager::getInstance()->getStringForKey("EXPLICACION_ENERGY", "EXPLICACION_ENERGY"), "fonts/KGBlankSpaceSketch.ttf", 40,Size(mFondo->getContentSize().width*0.7, mFondo->getContentSize().height*0.4), kCCTextAlignmentLeft, kCCVerticalTextAlignmentCenter);
   */
    Label* labelEnergy=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("EXPLICACION_ENERGY", "EXPLICACION_ENERGY"), "fonts/KGBlankSpaceSketch.ttf", 40);
    labelEnergy->setWidth(mFondo->getContentSize().width*0.7);
    labelEnergy->setHeight(mFondo->getContentSize().height*0.4);
    labelEnergy->setHorizontalAlignment(TextHAlignment::LEFT);
    labelEnergy->setVerticalAlignment(TextVAlignment::CENTER);
    
     labelEnergy->setPosition(Point(mFondo->getContentSize().width*0.56f,mFondo->getContentSize().height*0.3));
    mFondo->addChild(labelEnergy,4);
    labelEnergy->setColor(Color3B(107,128,140));

    
    
    
    
    
    
    
    
    
    mFondo->setPosition(Point(mSize.width/2,mSize.height/2));
    this->addChild(mFondo);

}


void LayerExplicarItems::close(Ref* sender){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
     CCLOG("close");
    ocultar(NULL);
}
LayerExplicarItems* LayerExplicarItems::mostrar()
{
    LayerExplicarItems* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM)==NULL){
        LayerExplicarItems* layer= LayerExplicarItems::initLayer();
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerExplicarItems::TAG_EXPLICACION_ITEM);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
    }
    else{
        devolver=(LayerExplicarItems*)Director::getInstance()->getRunningScene()->getChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM);
        CCLOG("Layer loading previamente mostrada");
    }
    return devolver;
}

void LayerExplicarItems::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerExplicarItems::TAG_EXPLICACION_ITEM,true);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
    }
  
}



void LayerExplicarItems::onTouchEnded(Touch* touch, Event* event)
{
     ocultar(this);
   
}


void LayerExplicarItems::onKeyBackClicked()
{
    this->close(NULL);
    
}




