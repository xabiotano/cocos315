#include "LayerNecesitasSuperar.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../series/SeriesScene.h"
#include "../psico/PsicoScene.h"
#include "../Variables.h"
#include "../widgets/NivelItem.h"
#include "../BaseScene.h"
#include "../psico/PsicoScene.h"
#include "../series/SeriesScene.h"
#include "../Levelsv2.h"
#include "../TipoNivelTypes.h"
#include "SimpleAudioEngine.h"
#include "../helpers/LogicSQLHelper.h"



LayerNecesitasSuperar:: ~LayerNecesitasSuperar(void){}

template <typename T> std::string LayerNecesitasSuperar::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

LayerNecesitasSuperar* LayerNecesitasSuperar::mostrar()
{
    LayerNecesitasSuperar* devolver=NULL;
    if(!Director::getInstance()->getRunningScene()){
        return NULL;
    }
    devolver= (LayerNecesitasSuperar*)Director::getInstance()->getRunningScene()->getChildByTag(LayerNecesitasSuperar::TAG_LAYER_NECESITASSUPERAR);
    if(devolver==NULL){
        LayerNecesitasSuperar* layer= LayerNecesitasSuperar::initLayer();
        layer->dibujarItems();
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerNecesitasSuperar::TAG_LAYER_NECESITASSUPERAR);
        Director::getInstance()->getRunningScene()->addChild(layer);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/info.mp3");
        
        devolver=layer;
    }
    else{
        CCLOG("LayerNecesitasSuperar previamente mostrada");
    }
    
    return devolver;
}

LayerNecesitasSuperar* LayerNecesitasSuperar::initLayer()
{
    LayerNecesitasSuperar *layer = LayerNecesitasSuperar::create();
  
    return layer;
}





void LayerNecesitasSuperar::dibujarItems(){
    
    
    auto close= ui::Button::create("ui/close.png", "ui/close.png", "ui/close.png");
    
    close->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
        switch (type)
        {
            case ui::Widget::TouchEventType::BEGAN:
                break;
            case ui::Widget::TouchEventType::ENDED:
                LayerNecesitasSuperar::close();
                break;
            default:
                break;
        }
    });
    //Boton* close=Boton::createBoton(this, menu_selector(LayerNecesitasSuperar::close), "ui/close.png");
    close->setPosition(Point(mFondo_panel->getContentSize().width*0.95f, mFondo_panel->getContentSize().height*0.95f));
    mFondo_panel->addChild(close);
    
    
    
    
 

    
    
    
 
    

    
}


// on "init" you need to initialize your instance
bool LayerNecesitasSuperar::init()
{
    mSize = Director::getInstance()->getWinSize();
    if ( !LayerColor::initWithColor((Color4B){0,0,0,200}, mSize.width, mSize.height )){
        return false;
    }
    
    mFondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    mFondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.7f) );
    mFondo_panel->setPosition(Point(-mSize.width*0.5f,mSize.height*0.5f));
    this->addChild(mFondo_panel);
    mFondo_panel->setTag(TAG_FONDO);
    
 
    Sprite* mono=Sprite::create("ui/necesitas_superar.png");
    mono->setPosition(Point(mono->getContentSize().width/2,mono->getContentSize().height/2 ));
    mFondo_panel->addChild(mono);
    
   
    Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("NEED_TO_PASS_PREVIOUS", "NEED_TO_PASS_PREVIOUS"), CCGetFont(), 40);
    label->setWidth(mFondo_panel->getContentSize().width*0.5f);
    label->setPosition(Point(mFondo_panel->getContentSize().width*0.65f,mFondo_panel->getContentSize().height*0.5f));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setColor(Color3B(217,38,92));
    label->enableOutline(Color4B(0,0,0,255),2);
    mFondo_panel->addChild(label,3);
    this->setKeypadEnabled(true);

    
    mFondo_panel->runAction(EaseInOut::create(MoveTo::create(0.35f, Point(mSize.width*0.5f,mSize.height*0.5f)),3));
    
    return true;
}




void LayerNecesitasSuperar::onEnter()
{
    ////Director* pDirector = Director::getInstance();
    ////pDirector->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
    Layer::onEnter();
}

void LayerNecesitasSuperar::onExit()
{
    ////Director* pDirector = Director::getInstance();
    //pDirector->getTouchDispatcher()->removeDelegate(this);
     Layer::onExit();
}




void LayerNecesitasSuperar::close()
{
    ocultar(NULL);
}








void LayerNecesitasSuperar::onKeyBackClicked()
{
    close();
    
}

void LayerNecesitasSuperar::ocultar(Ref* pSender){
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerNecesitasSuperar::TAG_LAYER_NECESITASSUPERAR)){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerNecesitasSuperar::TAG_LAYER_NECESITASSUPERAR,true);
    }

}


