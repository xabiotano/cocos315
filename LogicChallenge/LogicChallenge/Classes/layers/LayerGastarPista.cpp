
#include "LayerGastarPista.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"
#include "../helpers/LogicSQLHelper.h"






void LayerGastarPista::initLayer(){
    dibujarContent();
   
}


// on "init" you need to initialize your instance
bool LayerGastarPista::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerBase::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
    
  
    
    return true;
}

void LayerGastarPista::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/info_panel.png");
    mFondo->setPosition(Point(mSize.width/2,mSize.height/2));
    this->addChild(mFondo);

    std::string texto=std::string(LanguageManager::getInstance()->getStringForKey("LAYERGANARPISTA_DESEAS_GASTAR","LAYERGANARPISTA_DESEAS_GASTAR"));
    /*mTexto= CCLabelStroke::create(mFondo,texto.c_str(), CCGetFont(), 30,Size(mFondo->getContentSize().width*0.8f,mFondo->getContentSize().height*0.9f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentCenter);
    mTexto->setColor(Color3B(255,255,255));
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f,mFondo->getContentSize().height*0.5f));
    mFondo->addChild(mTexto,10);
    mTexto->setString(texto.c_str(), Color3B(0,0,0));*/
    mTexto= Label::createWithTTF(texto.c_str(), CCGetFont(), 30);
    mTexto->setColor(Color3B(255,255,255));
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f,mFondo->getContentSize().height*0.5f));
    mTexto->setWidth(mFondo->getContentSize().width*0.8f);
    mTexto->setHeight(mFondo->getContentSize().height*0.9f);
    mTexto->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mTexto->enableOutline(Color4B(0,0,0,255),2);
    mFondo->addChild(mTexto,10);
    
    
    mClose=Boton::createBoton(menu_selector(LayerGastarPista::close), "ui/close.png");
    mFondo->addChild(mClose,1);
    mClose->setPosition(Point(mFondo->getContentSize().width*0.25f,0));
 
    
    
    
    mOkButton=Boton::createBoton(menu_selector(LayerGastarPista::ok), "ui/ok.png");
    mFondo->addChild(mOkButton,1);
    mOkButton->setPosition(Point(mFondo->getContentSize().width*0.75f,0));
    
   
}


void LayerGastarPista::close(Ref* sender){
    BaseScene::getInstance()->HintCallback(false);
    ocultar(NULL);
}

void LayerGastarPista::ok(Ref* sender){
    BaseScene::getInstance()->HintCallback(true);
    ocultar(NULL);
}
LayerGastarPista* LayerGastarPista::mostrar()
{
    LayerGastarPista* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerGastarPista::TAG_GASTARPISTA)==NULL){
        LayerGastarPista *layer = LayerGastarPista::create();
        layer->initLayer();
         layer->mSwallowTouches=true;
        layer->setTag(LayerGastarPista::TAG_GASTARPISTA);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
    }
    else{
        devolver=(LayerGastarPista*)Director::getInstance()->getRunningScene()->getChildByTag(LayerGastarPista::TAG_GASTARPISTA);
        CCLOG("Layer loading previamente mostrada");
    }
    return devolver;
}

void LayerGastarPista::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerGastarPista::TAG_GASTARPISTA)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerGastarPista::TAG_GASTARPISTA,true);
    }
  
}








Node* LayerGastarPista::getCanvas()
{
    return mFondo;
}




void LayerGastarPista::setNoOcultable(){
    mOcultable=false;
}



