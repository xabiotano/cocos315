//
//  LayerHelp.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerHelp.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"



int LayerHelp::TAG_HELP=1;

LayerHelp::~LayerHelp(void)
{
    CC_SAFE_RELEASE(mScroll);
}

LayerHelp* LayerHelp::initLayer(std::string texto){
    CCLOG("LayerHelp %s",texto.c_str());
    LayerHelp *layer = LayerHelp::create();
    layer->setTexto(texto);
    layer->dibujarContent();
    layer->mSwallowTouches=true;
    return layer;
}


// on "init" you need to initialize your instance
bool LayerHelp::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerBase::init()){
        return false;
    }
    this->setKeypadEnabled(true);

    return true;
}

void LayerHelp::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    Scale9Sprite* fondo=Scale9Sprite::create("ui/pizarra.png");
    fondo->setContentSize( Size(mSize.width*0.95f,mSize.height*0.95f) );
    fondo->setPosition(Point(mSize.width*0.2,mSize.height*0.9f));
    fondo->setScale(0.f);
    fondo->runAction(ScaleTo::create(0.2, 1.f));
    fondo->runAction(MoveTo::create(0.2f, Point(mSize.width/2,mSize.height/2)));
    this->addChild(fondo);
    
    mTexto=Label::createWithTTF(mTextoHelp.c_str(),CCGetFont(), 30);
    //mTexto->setWidth(mSize.width*0.8f);
    mTexto->setHeight(100.f);
    mTexto->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    mTexto->setDimensions(mSize.width*0.8f, 0.f);
    mTexto->setHorizontalAlignment(TextHAlignment::CENTER);
    mTexto->setVerticalAlignment(TextVAlignment::TOP);
    mTexto->setColor(Color3B(255,255,255));
    mTexto->setPosition(Point(mSize.width*0.5f,mSize.height*0.3f));
   
    mScroll=ScrollView::create();

    mScroll->setColor(Color3B(255.f,0,0));
    mScroll->setPosition(Point(50,30));
    mScroll->setBounceable(true);
    mScroll->retain();
    mScroll->setClippingToBounds(true);
    mScroll->setDirection(ScrollView::Direction::VERTICAL);
    mScroll->setContainer(mTexto);
    mScroll->setViewSize(Size(mSize.width*0.85,mSize.height*0.85));
    mScroll->setContentOffset(Point(0.f, (mSize.height*0.85-mTexto->getContentSize().height)), false);
    fondo->addChild(mScroll,1);
    
    mClose=Boton::createBoton(menu_selector(LayerHelp::close), "ui/close.png");
    mClose->setPosition(Point(mSize.width*0.93,mSize.height*0.93));
    mClose->setScale(0.85f);
    fondo->addChild(mClose,1);
    
   

}


void LayerHelp::close(Ref* sender){
    CCLOG("close");
    ocultar(NULL);
}
void LayerHelp::mostrar(std::string texto,Boton* mbotonHelp)
{
  
    if(!Director::getInstance()->getRunningScene()){
        return;
    }
    if(!Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP)){
        LayerHelp* layer= LayerHelp::initLayer(texto);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerHelp::TAG_HELP);
        layer->mbotonHelp=mbotonHelp;
        Director::getInstance()->getRunningScene()->addChild(layer,LayerBase::ZINDEX_MEDIO);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/help.mp3",false);
        
    }
    else{
        CCLOG("Layer loading previamente mostrada");
    }
}

void LayerHelp::ocultar(Ref* pSender)
{
    LayerHelp* layerHelp=(LayerHelp*)Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP);
    if(layerHelp!=NULL ){
        if(BaseScene::getInstance()!=NULL){
            BaseScene::getInstance()->HelpClose(NULL);
        }
        Size size = Director::getInstance()->getVisibleSize();
        layerHelp->runAction(MoveTo::create(0.2f, Point(size.width*0.2f- layerHelp->getContentSize().width/2,size.height*0.5f)));//misma posicion que help
        //CCCallFunc* eliminar=CCCallFunc::create(layerHelp, callfunc_selector(LayerHelp::eliminar));
        
        CallFuncN* eliminar= CallFuncN::create(CC_CALLBACK_1(LayerHelp::eliminar,layerHelp));
        layerHelp->runAction(Sequence::create(ScaleTo::create(0.2f, 0.f),eliminar,NULL));
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
        layerHelp->mbotonHelp->runAction(Sequence::create(DelayTime::create(0.2f), ScaleTo::create(0.1f, 1.3f),ScaleTo::create(0.1f, 1.f),NULL));
        
    }
  
}


void LayerHelp::eliminar(Ref* pSender){
    LayerHelp* layerHelp=(LayerHelp*)Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP);
    if(layerHelp!=NULL ){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerHelp::TAG_HELP,true);
        
    }
}








void LayerHelp::setTexto(std::string texto){
      this->mTextoHelp=texto;
}



void LayerHelp::onKeyBackClicked()
{
    this->close(NULL);
    
}


