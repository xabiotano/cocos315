#include "LayerNecesitasItems.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../series/SeriesScene.h"
#include "../psico/PsicoScene.h"
#include "../Variables.h"
#include "../widgets/NivelItem.h"
#include "../BaseScene.h"
#include "../psico/PsicoScene.h"
#include "../series/SeriesScene.h"
#include "../Levelsv2.h"
#include "../TipoNivelTypes.h"
#include "SimpleAudioEngine.h"
#include "../helpers/LogicSQLHelper.h"



LayerNecesitasItems:: ~LayerNecesitasItems(void){}

template <typename T> std::string LayerNecesitasItems::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

LayerNecesitasItems* LayerNecesitasItems::mostrar(int numNivel)
{
    LayerNecesitasItems* devolver=NULL;
    if(!Director::getInstance()->getRunningScene()){
        return NULL;
    }
    devolver= (LayerNecesitasItems*)Director::getInstance()->getRunningScene()->getChildByTag(LayerNecesitasItems::TAG_LAYER_NECESITASITEMS);
    if(devolver==NULL){
        LayerNecesitasItems* layer= LayerNecesitasItems::initLayer(numNivel);
        layer->dibujarItems();
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerNecesitasItems::TAG_LAYER_NECESITASITEMS);
        Director::getInstance()->getRunningScene()->addChild(layer);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/info.mp3");
        
        devolver=layer;
    }
    else{
        CCLOG("LayerNecesitasItems previamente mostrada");
    }
    
    return devolver;
}

LayerNecesitasItems* LayerNecesitasItems::initLayer(int numNivel)
{
    LayerNecesitasItems *layer = LayerNecesitasItems::create();
    layer->mNumeroNivel=numNivel;
    return layer;
}





void LayerNecesitasItems::dibujarElixires(int elixiresReq ){
    
    
    Sprite* ribbon=Sprite::create("ui/layernecesitasitems/ribbon.png");
    ribbon->setPosition(Point(mFondo_panel->getContentSize().width/2,mFondo_panel->getContentSize().height));
    mFondo_panel->addChild(ribbon);
    // ribbon->setColor(Color3B(165,226,77));
    
    
    /*CCLabelStroke* label=CCLabelStroke::create(ribbon, "", CCGetFont(), 40, Size(0, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    label->setPosition(Point(ribbon->getContentSize().width*0.5f,ribbon->getContentSize().height*0.7f));*/
    //ribbon->addChild(label,4);
    std::string numeroNivel=tostr(mNumeroNivel);
    std::stringstream ss;
    ss << LanguageManager::getInstance()->getStringForKey("LAYER_NECESITAS_ITEM_LEVEL", "Level ") << numeroNivel;
    //label->setColor(Color3B(227,80,94));
   // label->setString(ss.str().c_str() ,Color3B(0,0,0));
    
    Label* label= Label::createWithTTF(ss.str().c_str(), CCGetFont(), 45);
    label->setColor(Color3B(255,255,255));
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label->setPosition(Point(ribbon->getContentSize().width*0.5f,ribbon->getContentSize().height*0.7f));
    label->setWidth(ribbon->getContentSize().width*0.5f);
    label->setHeight(ribbon->getContentSize().height*0.7f);
    label->setColor(Color3B(227,80,94));
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255.f),4);
    ribbon->addChild(label,4);
    
    
    
    
    Boton* close=Boton::createBoton(this, menu_selector(LayerNecesitasItems::close), "ui/close.png");
    close->setPosition(Point(mFondo_panel->getContentSize().width*0.95f, mFondo_panel->getContentSize().height*0.95f));
    mFondo_panel->addChild(close);
    
    
    
    Sprite* botonera=Sprite::create("ui/background_ribbon_button.png");
    botonera->setPosition(Point(mFondo_panel->getContentSize().width*0.5f,mFondo_panel->getContentSize().height*0.15f));
    mFondo_panel->addChild(botonera);
    
    
    
    Boton* buyItems=Boton::createBoton(this, menu_selector(LayerBase::Comprar), "ui/left_button.png");
    buyItems->setPosition(Point(botonera->getContentSize().width*0.25f, botonera->getContentSize().height*0.5f));
    botonera->addChild(buyItems);
    
    Boton* unLockAll=Boton::createBoton(this, menu_selector(LayerBase::Comprar), "ui/right_button.png");
    unLockAll->setPosition(Point(botonera->getContentSize().width*0.75f, botonera->getContentSize().height*0.5f));
    botonera->addChild(unLockAll);
    
    
   /* CCLabelStroke*  BuyItemsTxt=CCLabelStroke::create(buyItems, "OK", CCGetFont(), 25, Size(0, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    BuyItemsTxt->setPosition(Point(buyItems->getContentSize().width*0.5f,buyItems->getContentSize().height*0.5f));*/
    
    Label* BuyItemsTxt= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("BUY_ITEM", "Buy Item"), CCGetFont(), 25);
    BuyItemsTxt->setColor(Color3B(255,255,255));
    //BuyItemsTxt->setWidth(fondo_panel->getContentSize().width*0.8f);
    //BuyItemsTxt->setHeight( fondo_panel->getContentSize().height*0.7f);
    BuyItemsTxt->setPosition(Point(buyItems->getContentSize().width*0.5f,buyItems->getContentSize().height*0.5f));
    BuyItemsTxt->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    BuyItemsTxt->enableOutline(Color4B(0,0,0,255),3);
    buyItems->addChild(BuyItemsTxt,4);

    // BuyItemsTxt->setString(LanguageManager::getInstance()->getStringForKey("BUY_ITEM", "Buy Item") ,Color3B(0,0,0));
    
    
    
    
   /* CCLabelStroke*  UnLockAllTxt=CCLabelStroke::create(unLockAll, "OK", CCGetFont(), 22, Size(0, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    UnLockAllTxt->setPosition(Point(unLockAll->getContentSize().width*0.5f,unLockAll->getContentSize().height*0.5f));
    unLockAll->addChild(UnLockAllTxt,4);
    UnLockAllTxt->setString(LanguageManager::getInstance()->getStringForKey("UNLOCK_ALL","Unlock All") ,Color3B(0,0,0));*/
    
    Label* UnLockAllTxt= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("UNLOCK_ALL","Unlock All"), CCGetFont(), 22);
    UnLockAllTxt->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    UnLockAllTxt->setColor(Color3B(255,255,255));
    UnLockAllTxt->setPosition(Point(unLockAll->getContentSize().width*0.5f,unLockAll->getContentSize().height*0.5f));
    UnLockAllTxt->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    UnLockAllTxt->enableOutline(Color4B(0,0,0,255),3);
    unLockAll->addChild(UnLockAllTxt,4);
    
   
    
    
    
   
    int numeroElixiresUsuario=LogicSQLHelper::getInstance().getElixiresGanados();
    
    float percentage=(float)numeroElixiresUsuario/(float)elixiresReq;
    if(percentage>1.f){
        percentage=100.f;
    }else{
        percentage=percentage*100.f;
    }
    
    Sprite* fondo_barra_progreso=Sprite::create("ui/layernecesitasitems/fondo_barra_progreso.png");
    fondo_barra_progreso->setPosition(Point(mFondo_panel->getContentSize().width*0.4f,mFondo_panel->getContentSize().height*0.45f));
    mFondo_panel->addChild(fondo_barra_progreso);
    
    
    /*CCLabelStroke*  CafesRequiredTxt=CCLabelStroke::create(fondo_barra_progreso, "OK", CCGetFont(), 25, Size(fondo_barra_progreso->getContentSize().width, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    CafesRequiredTxt->setPosition(Point(fondo_barra_progreso->getContentSize().width* 0.5f,fondo_barra_progreso->getContentSize().height* 2.f));
    fondo_barra_progreso->addChild(CafesRequiredTxt,4);
    CafesRequiredTxt->setString(LanguageManager::getInstance()->getStringForKey("ELIXIR_REQUIRED","ELIXIR REQUIRED") ,Color3B(0,0,0));
    CafesRequiredTxt->setColor(Color3B(252,237,192));*/
    
    Label* CafesRequiredTxt= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("ELIXIR_REQUIRED","ELIXIR REQUIRED"), CCGetFont(), 25);
    CafesRequiredTxt->setWidth(fondo_barra_progreso->getContentSize().width);
    CafesRequiredTxt->setColor(Color3B(255,255,255));
    CafesRequiredTxt->setPosition(Point(fondo_barra_progreso->getContentSize().width* 0.5f,fondo_barra_progreso->getContentSize().height* 2.f));
    CafesRequiredTxt->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    CafesRequiredTxt->enableOutline(Color4B(0,0,0,255),2);
    fondo_barra_progreso->addChild(CafesRequiredTxt,4);
    
    
    
    
    ProgressTimer* mProgresoEnergyDrink = ProgressTimer::create(Sprite::create("ui/layernecesitasitems/barra_rosa.png"));
    mProgresoEnergyDrink->setType(ProgressTimer::Type::BAR);
    mProgresoEnergyDrink->setMidpoint(Point(0,1));
     mProgresoEnergyDrink->setBarChangeRate(Point(1,0));
    mProgresoEnergyDrink->setPercentage(percentage);
    mProgresoEnergyDrink->setPosition(Point(fondo_barra_progreso->getContentSize().width*0.6f, fondo_barra_progreso->getContentSize().height*0.5f));
   
    fondo_barra_progreso->addChild(mProgresoEnergyDrink,2);

    
    
    
    std::string numeroItemsReq=tostr(elixiresReq);
    std::stringstream ss1;
    ss1 << numeroElixiresUsuario << "/" << numeroItemsReq;
    if(percentage==100.f){
        ss1<<LanguageManager::getInstance()->getStringForKey("Completed"," Completed!");
    }else{
      //  ss<< LanguageManager::getInstance()->getStringForKey("Pending"," Pending");
    }
    
    
    /*CCLabelTTF* label1=CCLabelTTF::create(ss1.str().c_str(), CCGetFont(),30, fondo_barra_progreso->getContentSize(),kCCTextAlignmentCenter,kCCVerticalTextAlignmentCenter);
    */
    Label* label1=Label::createWithTTF(ss1.str().c_str(), CCGetFont(),30);
    label1->setWidth(fondo_barra_progreso->getContentSize().width);
    label1->setHeight(fondo_barra_progreso->getContentSize().height);
    label1->setHorizontalAlignment(TextHAlignment::CENTER);
    label1->setVerticalAlignment(TextVAlignment::CENTER);
    
   
     label1->setPosition(Point(mProgresoEnergyDrink->getContentSize().width*0.5f,mProgresoEnergyDrink->getContentSize().height*0.5f));
    mProgresoEnergyDrink->addChild(label1,4);
    
    
    Sprite* elixirBotella;
    Texture2D * elixirTex=Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_m.png");
    elixirBotella=Sprite::createWithTexture(elixirTex);
    elixirBotella->setPosition(Point(fondo_barra_progreso->getContentSize().width*0.1f,fondo_barra_progreso->getContentSize().height*0.5f));
    fondo_barra_progreso->addChild(elixirBotella);
    elixirBotella->setScale(0.7f);
    
    if(numeroElixiresUsuario<elixiresReq){
        Sprite* lock=Sprite::create("ui/lock.png");
        lock->setPosition(Point(elixirBotella->getContentSize().width*0.3f,elixirBotella->getContentSize().height*0.3f));
        elixirBotella->addChild(lock);
        lock->setRotation(-10.f);
    }
    
    Boton* playCafe=Boton::createBoton(this, menu_selector(LayerNecesitasItems::goToLevelsV2), "ui/play_button.png");
    playCafe->setPosition(Point(mFondo_panel->getContentSize().width*0.8f, mFondo_panel->getContentSize().height*0.45f));
    mFondo_panel->addChild(playCafe);
    playCafe->setSound("sounds/serieslogic/insert_letter.mp3");
    playCafe->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateTo::create(0.3f, 10), RotateTo::create(0.3f, -10))));
    playCafe->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.7f, 200,200, 200),TintTo::create(0.7f, 255,255, 255))));
}


// on "init" you need to initialize your instance
bool LayerNecesitasItems::init()
{
    mSize = Director::getInstance()->getWinSize();
    if ( !LayerBase::initWithColor(Color4B(0,0,0,200 ))){
        return false;
    }
    
    mFondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    mFondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.7f) );
    mFondo_panel->setPosition(Point(-mSize.width*0.5f,mSize.height*0.5f));
    this->addChild(mFondo_panel);
    mFondo_panel->setTag(TAG_FONDO);
    
 
    this->setKeypadEnabled(true);
    
    
    mFondo_panel->runAction(EaseInOut::create(MoveTo::create(0.35f, Point(mSize.width*0.5f,mSize.height*0.5f)),3));
    
    return true;
}




void LayerNecesitasItems::onEnter()
{
    LayerBase::onEnter();
}

void LayerNecesitasItems::onExit()
{
     LayerBase::onExit();
}




void LayerNecesitasItems::close(Ref* sender)
{
    ocultar(NULL);
}




void LayerNecesitasItems::goToLevelsV2(Ref* sender){
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f,Levelsv2::scene()));
    CCLOG("NEXT");
}


void LayerNecesitasItems::dibujarItems(){
    int elixiresReq=LogicSQLHelper::getElixiresRequeridosNivelLogica(mNumeroNivel);
    dibujarElixires(elixiresReq);

}


void LayerNecesitasItems::onKeyBackClicked()
{
    close(NULL);
    
}

void LayerNecesitasItems::ocultar(Ref* pSender){
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerNecesitasItems::TAG_LAYER_NECESITASITEMS)){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerNecesitasItems::TAG_LAYER_NECESITASITEMS,true);
    }

}


