//
//  LayerHint.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerHint.h"
#include "../Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"



int LayerHint::TAG_HINT=1640;


LayerHint* LayerHint::initLayer(std::string texto){
    LayerHint *layer = LayerHint::create();
    layer->setTexto(texto);
    layer->dibujarContent();
    layer->mSwallowTouches=true;
    return layer;
}


// on "init" you need to initialize your instance
bool LayerHint::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
    
    
    return true;
}

void LayerHint::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    Scale9Sprite* fondo=Scale9Sprite::create("ui/layerhint.png");
    fondo->setContentSize( Size(mSize.width*0.95f,mSize.height*0.95f) );
    
    //mTexto=CCLabelTTF::create(mTextoHelp.c_str(), CCGetFont(), 30,Size(mSize.width*0.8f,0),kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
    

    
    
    mTexto=Label::createWithTTF(mTextoHelp.c_str(),CCGetFont(), 30);
    mTexto->setWidth(mSize.width*0.8f);
    //mTexto->setHeight(mSize.height*0.95f);
    mTexto->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    mTexto->setHorizontalAlignment(TextHAlignment::CENTER);
    mTexto->setVerticalAlignment(TextVAlignment::TOP);
    mTexto->setColor(Color3B(60,53,102));
    mTexto->setPosition(Point(mSize.width*0.5f,mSize.height*0.3f));

    
    
    
    mScroll=ScrollView::create();
    mScroll->setPosition(Point(50,30));
    mScroll->setBounceable(true);
    mScroll->retain();
    mScroll->setClippingToBounds(true);/*kCCScrollViewDirectionVertical*/
    mScroll->setDirection(ScrollView::Direction::VERTICAL);
    mScroll->setContainer(mTexto);

    mScroll->setViewSize(Size(mSize.width*0.85,mSize.height*0.85));
    mScroll->setContentOffset(Point(0.f, (mSize.height*0.85-mTexto->getContentSize().height)), false);
    mScroll->setDelegate(this);
   // mScroll->setPosition(Point(size.width/2,size.height/2));
    fondo->addChild(mScroll,1);
    
  
    //this->addChild(mTexto,1);
    
    mClose=Boton::createBoton(menu_selector(LayerHint::close), "ui/close.png");
    fondo->addChild(mClose,1);
    mClose->setPosition(Point(mSize.width*0.93,mSize.height*0.93));
    mClose->setScale(0.85f);
    
  
    mScrollIndicator=Sprite::create("ui/scroll_indicator.png");
    mScrollIndicator->setPosition(Point(fondo->getContentSize().width*0.9f,fondo->getContentSize().height/2));
    fondo->addChild(mScrollIndicator);
    
    mScrollIndicatorBullet=Sprite::create("ui/scroll_indicator_bullet.png");
    mScrollIndicator->addChild(mScrollIndicatorBullet);
    scrollViewDidScroll(mScroll);
  
    
    fondo->setPosition(Point(-mSize.width/2,mSize.height/2));

    fondo->runAction(EaseBounceIn::create(MoveTo::create(0.25f, Point(mSize.width/2,mSize.height/2))));
    this->addChild(fondo);

}


void LayerHint::close(Ref* sender){
     CCLOG("close");
    ocultar(NULL);
}
void LayerHint::mostrar(std::string texto)
{
  
    if(!Director::getInstance()->getRunningScene()){
        return;
    }
    if(!Director::getInstance()->getRunningScene()->getChildByTag(LayerHint::TAG_HINT)){
        LayerHint* layer= LayerHint::initLayer(texto);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerHint::TAG_HINT);
        Director::getInstance()->getRunningScene()->addChild(layer);
    }
    else{
        CCLOG("Layer loading previamente mostrada");
    }
}

void LayerHint::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerHint::TAG_HINT)){
        BaseScene::getInstance()->HintStop(NULL);
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerHint::TAG_HINT,true);
    }
  
}







void LayerHint::setTexto(std::string texto){
      this->mTextoHelp=texto;
}


void LayerHint::scrollViewDidScroll(ScrollView* view){
    
   
   
    
    float y= (-view->getContentOffset().y)/view->getContainer()->getContentSize().height;
    CCLOG("%f / %f => %f",-view->getContentOffset().y,view->getContainer()->getContentSize().height,y);
    //0.10   0.3
    y=y/0.8f;
    CCLOG("%f",y);
    if(y<0.f ){
        y=0;
    }
    if(y>1.f){
        y=1.f;
    }

    mScrollIndicatorBullet->setPosition(Point(mScrollIndicatorBullet->getParent()->getContentSize().width*0.5f,mScrollIndicatorBullet->getParent()->getContentSize().height*y));
}

