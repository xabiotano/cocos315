//
//  LayerHint.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerHint__
#define __trivial__LayerHint__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;
USING_NS_CC_EXT;

class LayerHint:  public LayerBase,public ScrollViewDelegate
{
    public:
    
    static LayerHint* initLayer(std::string texto);
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerHint);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static void mostrar(std::string texto);
    
   
    static int TAG_HINT;
    void setTexto(std::string texto);
    
    
    virtual void scrollViewDidScroll(ScrollView* view) ;
    virtual void scrollViewDidZoom(ScrollView* view){};
    
private:
    void close(Ref* sender);
    Label* mTexto;
    Boton* mClose;
    ScrollView* mScroll;
    std::string mTextoHelp;
    Sprite* mScrollIndicator;
    Sprite* mScrollIndicatorBullet;



};

#endif /* defined(__trivial__LayerHint__) */
