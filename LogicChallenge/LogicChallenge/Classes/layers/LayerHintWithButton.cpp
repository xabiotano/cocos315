//
//  LayerHintWithButton.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerHintWithButton.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"



int LayerHintWithButton::TAG_TUTORIAL=123713;


LayerHintWithButton* LayerHintWithButton::initLayer(std::string texto){
    LayerHintWithButton *layer = LayerHintWithButton::create();
    layer->setTexto(texto);
    layer->dibujarContent();
    layer->mSwallowTouches=false;
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerHintWithButton::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(100,100,100,0))){
        return false;
    }
    //mOcultable=true;
    
    return true;
}

void LayerHintWithButton::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/info_panel.png");
    mFondo->setPosition(Point(mSize.width/2,mSize.height*0.8f));
    this->addChild(mFondo);
    
    mTexto=Label::createWithTTF(mTextoHelp.c_str(), CCGetFont(), 20);
    mTexto->setWidth(mFondo->getContentSize().width*0.8f);
    mTexto->setHeight(mFondo->getContentSize().height*0.9f);
    mTexto->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    mTexto->setColor(Color3B(116,90,62));
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f,mFondo->getContentSize().height*0.5f));
    mFondo->addChild(mTexto,2);
    
 
    Boton* mNext=Boton::createBoton(BaseScene::getInstance(),menu_selector(BaseScene::HintNext), "ui/skip.png");
    this->addChild(mNext,50);
    mNext->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    
    Boton* mSkip=Boton::createBoton(BaseScene::getInstance(),menu_selector(BaseScene::HintStop), "ui/stop.png");
    //this->addChild(mSkip,2);
    mSkip->setPosition(Point(mSize.width*0.1f,mSize.height*0.9f));
    
 
}



LayerHintWithButton* LayerHintWithButton::mostrar(std::string texto)
{
    LayerHintWithButton* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerHintWithButton::TAG_TUTORIAL)==NULL){
        LayerHintWithButton* layer= LayerHintWithButton::initLayer(texto);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerHintWithButton::TAG_TUTORIAL);
        Director::getInstance()->getRunningScene()->addChild(layer,101);
        devolver=layer;
    }
    else{
        devolver=(LayerHintWithButton*)Director::getInstance()->getRunningScene()->getChildByTag(LayerHintWithButton::TAG_TUTORIAL);
        CCLOG("Layer loading previamente mostrada");
    }
    return devolver;
}

void LayerHintWithButton::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerHintWithButton::TAG_TUTORIAL)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerHintWithButton::TAG_TUTORIAL,true);
    }
  
}





void LayerHintWithButton::setTexto(std::string texto){
      this->mTextoHelp=texto;
}

Node* LayerHintWithButton::getCanvas()
{
    return mFondo;
}


Label* LayerHintWithButton::crearTexto(std::string texto){
    /*CCLabelTTF* Texto=CCLabelTTF::create(texto.c_str(), CCGetFont(), 20,Size(mFondo->getContentSize().width*0.8f,mFondo->getContentSize().height*0.3f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);*/
    Label* Texto=Label::createWithTTF(texto.c_str(), CCGetFont(), 20);
    Texto->setWidth(mFondo->getContentSize().width*0.8f);
    Texto->setHeight(mFondo->getContentSize().height*0.3f);
    Texto->setAlignment(TextHAlignment::CENTER,TextVAlignment::TOP);
    Texto->setColor(Color3B(116,90,62));
    return Texto;
}

/*
void LayerHintWithButton::setNoOcultable(){
    mOcultable=false;
}*/


void LayerHintWithButton::setSprite(Sprite* imagen){
    mFondo->addChild(imagen);
    imagen->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.4f));
    float tamanoXImagen=imagen->getContentSize().width;
    float tamanoYImagen=imagen->getContentSize().height;
    
    float scaleX=tamanoXImagen/mFondo->getContentSize().width*0.35f;
    float scaleY=tamanoYImagen/mFondo->getContentSize().height*0.5f;
    
    if(scaleX<=scaleY){
        imagen->setScale(scaleX);
        CCLOG("scaleX %f",scaleX);
    }else{
         imagen->setScale(scaleY);
        CCLOG("scaleY %f",scaleY);
    }
}

void LayerHintWithButton::setBoton(Boton* boton){
    mFondo->addChild(boton);
    boton->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.4f));
}


void LayerHintWithButton::set2Boton(Boton* boton1,Boton *boton2){
    mFondo->addChild(boton1);
    mFondo->addChild(boton2);

    boton1->setPosition(Point(mFondo->getContentSize().width*0.25f,mFondo->getContentSize().height*0.4f));
    boton2->setPosition(Point(mFondo->getContentSize().width*0.75f,mFondo->getContentSize().height*0.4f));
}

