//
//  LayerHintWithButton.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerHintWithButton__
#define __trivial__LayerHintWithButton__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerHintWithButton:  public LayerBase
{
    public:
    
    static LayerHintWithButton* initLayer(std::string texto);
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerHintWithButton);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerHintWithButton* mostrar(std::string texto);
    EventListenerTouchOneByOne* mListener;
       
    static int TAG_TUTORIAL;
    void setTexto(std::string texto);
    Node* getCanvas();
    Label* crearTexto(std::string texto);
   // void setNoOcultable();
    void setSprite(Sprite* imagen);
    void setBoton(Boton* boton1);
    void set2Boton(Boton* boton1,Boton *boton2);
private:
    Label* mTexto;
    Boton* mClose;
    ScrollView* mScroll;
    std::string mTextoHelp;
    Sprite* mFondo;

    
    


};

#endif 
