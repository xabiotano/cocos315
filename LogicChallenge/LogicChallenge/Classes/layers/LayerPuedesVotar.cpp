#include "LayerPuedesVotar.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../series/SeriesScene.h"
#include "../psico/PsicoScene.h"
#include "../Variables.h"
#include "../widgets/NivelItem.h"
#include "../BaseScene.h"
#include "../psico/PsicoScene.h"
#include "../series/SeriesScene.h"
#include "../Levelsv2.h"
#include "../TipoNivelTypes.h"
#include "SimpleAudioEngine.h"
#include "../helpers/LogicSQLHelper.h"




LayerPuedesVotar:: ~LayerPuedesVotar(void){}

template <typename T> std::string LayerPuedesVotar::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

LayerPuedesVotar* LayerPuedesVotar::mostrar(int numeroElixirReq)
{
    LayerPuedesVotar* devolver=NULL;
    if(!Director::getInstance()->getRunningScene()){
        return NULL;
    }
    devolver= (LayerPuedesVotar*)Director::getInstance()->getRunningScene()->getChildByTag(LayerPuedesVotar::TAG_LAYER_PUEDESVOTAR);
    if(devolver==NULL){
        LayerPuedesVotar* layer= LayerPuedesVotar::initLayer(numeroElixirReq);
        layer->dibujarItems();
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerPuedesVotar::TAG_LAYER_PUEDESVOTAR);
        Director::getInstance()->getRunningScene()->addChild(layer);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/info.mp3");
        
        devolver=layer;
    }
    else{
        CCLOG("LayerPuedesVotar previamente mostrada");
    }
    
    return devolver;
}

LayerPuedesVotar* LayerPuedesVotar::initLayer(int numeroElixirReq)
{
    LayerPuedesVotar *layer = LayerPuedesVotar::create();
    layer->mNumeroElixirReq=numeroElixirReq;
    return layer;
}





void LayerPuedesVotar::dibujarItems(){
    
    Boton* close=Boton::createBoton(this, menu_selector(LayerPuedesVotar::close), "ui/close.png");
    close->setPosition(Point(mFondo_panel->getContentSize().width*0.95f, mFondo_panel->getContentSize().height*0.95f));
    mFondo_panel->addChild(close);

    
    /*CCLabelStroke* labelPuedesVotar=CCLabelStroke::create(NULL,LanguageManager::getInstance()->getStringForKey("LAYER_PUEDES_VOTAR_TEXTO", "LAYER_PUEDES_VOTAR_TEXTO"), CCGetFont(), 30, Size(mFondo_panel->getContentSize().width*0.85f, 0),kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    
    labelPuedesVotar->setPosition(Point(mFondo_panel->getContentSize().width*0.5f,mFondo_panel->getContentSize().height*0.8f));
    labelPuedesVotar->setColor(Color3B(141,21,32));
    mFondo_panel->addChild(labelPuedesVotar,3);
    labelPuedesVotar->setString(LanguageManager::getInstance()->getStringForKey("LAYER_PUEDES_VOTAR_TEXTO", "LAYER_PUEDES_VOTAR_TEXTO"), Color3B((255,255,255));*/
    Label* labelPuedesVotar= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("LAYER_PUEDES_VOTAR_TEXTO", "LAYER_PUEDES_VOTAR_TEXTO"), CCGetFont(), 30);
    labelPuedesVotar->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    labelPuedesVotar->setWidth(mFondo_panel->getContentSize().width*0.85f);
    //label->setHeight(mFondo_panel->getContentSize().width*0.5f);
    labelPuedesVotar->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    labelPuedesVotar->setPosition(Point(mFondo_panel->getContentSize().width*0.5f,mFondo_panel->getContentSize().height*0.8f));
    labelPuedesVotar->enableOutline(Color4B(255,255,255,255),2);
    labelPuedesVotar->setTextColor(Color4B(141,21,32,255));
    mFondo_panel->addChild(labelPuedesVotar,3);
    
    
    
    
    
    
    
    
    
    Sprite* regalo=Sprite::create("ui/layerpuedesvotar/regalo_elixir.png");
    regalo->setPosition(Point(mFondo_panel->getContentSize().width/2,mFondo_panel->getContentSize().height*0.5f ));
    mFondo_panel->addChild(regalo);
    
    
    Sprite* elixirNumero;//=Sprite::create("ui/layerpuedesvotar/regalo_elixir.png");
    if(mNumeroElixirReq<=9){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/10.png");
    }else if(mNumeroElixirReq>=10 && mNumeroElixirReq<=19){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/20.png");
    }else if(mNumeroElixirReq>=20 && mNumeroElixirReq<=29){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/30.png");
    }else if(mNumeroElixirReq>=30 && mNumeroElixirReq<=39){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/40.png");
    }else if(mNumeroElixirReq>=40 && mNumeroElixirReq<=49){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/50.png");
    }else if(mNumeroElixirReq>=50 && mNumeroElixirReq<=60){
        elixirNumero=Sprite::create("ui/layerpuedesvotar/60.png");
    }else{
        elixirNumero=Sprite::create("ui/layerpuedesvotar/150.png");
    }
    
    if(mNumeroElixirReq<=9){
        mNumeroElixirReq=10;
    }else if(mNumeroElixirReq>=10 && mNumeroElixirReq<=19){
        mNumeroElixirReq=20;
    }else if(mNumeroElixirReq>=20 && mNumeroElixirReq<=29){
        mNumeroElixirReq=30;
    }else if(mNumeroElixirReq>=30 && mNumeroElixirReq<=39){
        mNumeroElixirReq=40;
    }else if(mNumeroElixirReq>=40 && mNumeroElixirReq<=49){
         mNumeroElixirReq=50;
    }else if(mNumeroElixirReq>=50 && mNumeroElixirReq<=60){
        mNumeroElixirReq=60;
    }
    else{
        mNumeroElixirReq=150;
    }
    
    
    
    elixirNumero->setPosition(Point(regalo->getContentSize().width*0.5f,regalo->getContentSize().height*0.4f ));
    regalo->addChild(elixirNumero);
    
    
    Boton* no=Boton::createBoton(NULL, CC_MENU_SELECTOR(LayerPuedesVotar::close), "ui/layerpuedesvotar/no_button.png");
    no->setPosition(Point(mFondo_panel->getContentSize().width*0.3f,mFondo_panel->getContentSize().height*0.15f));
    mFondo_panel->addChild(no);
    
    
    Boton* ok=Boton::createBoton(NULL, CC_MENU_SELECTOR(LayerPuedesVotar::ok), "ui/layerpuedesvotar/ok_button.png");

    
    
    
    ok->setPosition(Point(mFondo_panel->getContentSize().width*0.65f,mFondo_panel->getContentSize().height*0.15f));
    mFondo_panel->addChild(ok);

    std::string texto;
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    texto=LanguageManager::getInstance()->getStringForKey("OK_REDIRECT_ME_GOOGLE", "OK_REDIRECT_ME_GOOGLE");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    texto=LanguageManager::getInstance()->getStringForKey("OK_REDIRECT_ME_IOS", "OK_REDIRECT_ME_IOS");
#endif
    
    
    /*CCLabelStroke* ok_redirect=CCLabelStroke::create(NULL,texto, CCGetFont(), 25, Size(ok->getContentSize().width*0.85f, 0),kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    
    ok_redirect->setPosition(Point(ok->getContentSize().width*0.5f,ok->getContentSize().height*0.5f));
    ok_redirect->setColor(Color3B(255,255,255));
    ok->addChild(ok_redirect,3);
    ok_redirect->setString(texto, Color3B((0,0,0));
    
    CCLabelStroke* no_text=CCLabelStroke::create(NULL,LanguageManager::getInstance()->getStringForKey("NO_THANKS", "NO_THANKS"), CCGetFont(), 20, Size(no->getContentSize().width*0.85f, 0),kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    
    no_text->setPosition(Point(no->getContentSize().width*0.5f,no->getContentSize().height*0.5f));
    no_text->setColor(Color3B(255,255,255));
    no->addChild(no_text,3);
    no_text->setString(LanguageManager::getInstance()->getStringForKey("NO_THANKS", "NO_THANKS"), Color3B((0,0,0));

*/
    
    Label* ok_redirect= Label::createWithTTF(texto.c_str(), CCGetFont(), 25);
    ok_redirect->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    ok_redirect->setWidth(ok->getContentSize().width*0.85f);
    //label->setHeight(mFondo_panel->getContentSize().width*0.5f);
    ok_redirect->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    ok_redirect->setPosition(Point(ok->getContentSize().width*0.5f,ok->getContentSize().height*0.5f));
    ok_redirect->setColor(Color3B(255,255,255));
    ok_redirect->enableOutline(Color4B(0,0,0,255),2);
    ok->addChild(ok_redirect,3);
    
    
    Label* no_text= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("NO_THANKS", "NO_THANKS"), CCGetFont(), 20);
    no_text->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    no_text->setWidth(no->getContentSize().width*0.85f);
    //label->setHeight(mFondo_panel->getContentSize().width*0.5f);
    no_text->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    no_text->setPosition(Point(no->getContentSize().width*0.5f,no->getContentSize().height*0.5f));
    no_text->setColor(Color3B(255,255,255));
    no_text->enableOutline(Color4B(0,0,0,255),2);
    no->addChild(no_text,3);
    
    
}


// on "init" you need to initialize your instance
bool LayerPuedesVotar::init()
{
    mSize = Director::getInstance()->getWinSize();
    if ( !LayerColor::initWithColor((Color4B){0,0,0,200}, mSize.width, mSize.height )){
        return false;
    }
    
    mFondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    mFondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.7f) );
    mFondo_panel->setPosition(Point(-mSize.width*0.5f,mSize.height*0.5f));
    this->addChild(mFondo_panel);
    mFondo_panel->setTag(TAG_FONDO);
    
 
    

    
    mFondo_panel->runAction(EaseInOut::create(MoveTo::create(0.35f, Point(mSize.width*0.5f,mSize.height*0.5f)),3));
    
    
    sdkbox::PluginReview::setListener(this);
    
    this->setKeypadEnabled(true);
   

    
    return true;
}







void LayerPuedesVotar::close(Ref* sender)
{
    ocultar(NULL);
}



void LayerPuedesVotar::ok(Ref* sender)
{
    sdkbox::PluginReview::setTitle(LanguageManager::getInstance()->getStringForKey("RATE_PREMIO_TITLE", "RATE_PREMIO_TITLE"));
    sdkbox::PluginReview::setMessage(LanguageManager::getInstance()->getStringForKey("RATE_PREMIO_CONTENT", "RATE_PREMIO_CONTENT"));
    sdkbox::PluginReview::setRateButtonTitle(LanguageManager::getInstance()->getStringForKey("RATE_PREMIO_IR", "RATE_PREMIO_IR"));
    sdkbox::PluginReview::setRateLaterButtonTitle("");
    sdkbox::PluginReview::setCancelButtonTitle("Kangaroo Games ™");
    sdkbox::PluginReview::show(true);
    
}







void LayerPuedesVotar::onKeyBackClicked()
{
    
    close(NULL);
    
}

void LayerPuedesVotar::ocultar(Ref* pSender){
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerPuedesVotar::TAG_LAYER_PUEDESVOTAR)){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerPuedesVotar::TAG_LAYER_PUEDESVOTAR,true);
    }

}


void LayerPuedesVotar::onDisplayAlert(){
    
}
void LayerPuedesVotar::onDeclineToRate(){
      LayerPuedesVotar::ok(NULL);
    //NO LE DEJO CHAPARLO!
}
void LayerPuedesVotar::onRate(){
    CCLOG("RATE!");
    LogicSQLHelper::getInstance().setRegaloVoto(true,mNumeroElixirReq);
}
void LayerPuedesVotar::onRemindLater(){
    LayerPuedesVotar::ok(NULL);
    //NO LE DEJO CHAPARLO!
}


