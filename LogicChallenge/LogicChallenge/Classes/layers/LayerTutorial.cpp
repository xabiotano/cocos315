//
//  LayerTutorial.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerTutorial.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../BaseScene.h"



int LayerTutorial::TAG_TUTORIAL=12613;


LayerTutorial* LayerTutorial::initLayer(std::string texto){
    LayerTutorial *layer = LayerTutorial::create();
    layer->setTexto(texto);
    layer->mSwallowTouches=true;
    layer->dibujarContent();
    return layer;
    
}


// on "init" you need to initialize your instance
bool LayerTutorial::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(100,100,100,0))){
        return false;
    }
    mOcultable=true;
    
    return true;
}

void LayerTutorial::dibujarContent(){
    
    mSize = Director::getInstance()->getWinSize();
    mFondo=Sprite::create("ui/info_panel.png");
    mFondo->setPosition(Point(mSize.width/2,mSize.height*0.8f));
    this->addChild(mFondo);
    
    
    /*mTexto=CCLabelTTF::create(mTextoHelp.c_str(), CCGetFont(), 20,Size(mFondo->getContentSize().width*0.8f,mFondo->getContentSize().height*0.9f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
    mTexto->setColor(Color3B(116,90,62));
   
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f,mFondo->getContentSize().height*0.5f));
    */
    
    
    mTexto= Label::createWithTTF(mTextoHelp.c_str(), CCGetFont(), 20);
    mTexto->setWidth(mFondo->getContentSize().width*0.8f);
    mTexto->setHeight(mFondo->getContentSize().height*0.9f);
    mTexto->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
    mTexto->setColor(Color3B(116,90,62));
    mTexto->setPosition(Point(mFondo->getContentSize().width*0.5f,mFondo->getContentSize().height*0.5f));
    mFondo->addChild(mTexto,2);
    
 
    Boton* mNext=Boton::createBoton(BaseScene::getInstance(),menu_selector(BaseScene::TutorialNext), "ui/skip.png");
    addChild(mNext,1);
    mNext->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    
 
    Boton* mSkip=Boton::createBoton(BaseScene::getInstance(),menu_selector(BaseScene::TutorialStop), "ui/stop.png");
    addChild(mSkip,1);
    mSkip->setPosition(Point(mSize.width*0.1f,mSize.height*0.9f));
    
}



LayerTutorial* LayerTutorial::mostrar(std::string texto)
{
    LayerTutorial* devolver=NULL;
    
    if(!Director::getInstance()->getRunningScene()){
        CCLOG("no hay running scene");
        return devolver;
    }
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerTutorial::TAG_TUTORIAL)==NULL){
        LayerTutorial* layer= LayerTutorial::initLayer(texto);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerTutorial::TAG_TUTORIAL);
        Director::getInstance()->getRunningScene()->addChild(layer,10);
        devolver=layer;
    }
    else{
        devolver=(LayerTutorial*)Director::getInstance()->getRunningScene()->getChildByTag(LayerTutorial::TAG_TUTORIAL);
        CCLOG("Layer loading previamente mostrada");
    }
    return devolver;
}

void LayerTutorial::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerTutorial::TAG_TUTORIAL)){
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerTutorial::TAG_TUTORIAL,true);
    }
  
}











void LayerTutorial::setTexto(std::string texto){
      this->mTextoHelp=texto;
}

Node* LayerTutorial::getCanvas()
{
    return mFondo;
}


Label* LayerTutorial::crearTexto(std::string texto){
    /*CCLabelTTF* Texto=CCLabelTTF::create(texto.c_str(), CCGetFont(), 20,Size(mFondo->getContentSize().width*0.8f,mFondo->getContentSize().height*0.3f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
    Texto->setColor(Color3B(116,90,62));*/
    //CCLabelTTF* Texto=CCLabelTTF::create(texto.c_str(), CCGetFont(), 20,Size(mFondo->getContentSize().width*0.8f,mFondo->getContentSize().height*0.3f),kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
    
    Label* Texto=Label::createWithTTF(texto.c_str(),CCGetFont(), 20);
    Texto->setWidth(mFondo->getContentSize().width*0.8f);
    Texto->setHeight(mFondo->getContentSize().height*0.3f);
    Texto->setHorizontalAlignment(TextHAlignment::CENTER);
    Texto->setVerticalAlignment(TextVAlignment::TOP);
    Texto->setColor(Color3B(116,90,62));
    
    return Texto;
}


void LayerTutorial::setNoOcultable(){
    mOcultable=false;
}


void LayerTutorial::setSprite(Sprite* imagen){
    mFondo->addChild(imagen);
    imagen->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.3f));
    float tamanoXImagen=imagen->getContentSize().width;
    float tamanoYImagen=imagen->getContentSize().height;
    
    float scaleX=tamanoXImagen/mFondo->getContentSize().width*0.35f;
    float scaleY=tamanoYImagen/mFondo->getContentSize().height*0.35f;
    
    if(scaleX<=scaleY){
        imagen->setScale(scaleX);
        CCLOG("scaleX %f",scaleX);
    }else{
         imagen->setScale(scaleY);
        CCLOG("scaleY %f",scaleY);
    }
}

void LayerTutorial::setBoton(Boton* boton){
    mFondo->addChild(boton);
    boton->setPosition(Point(mFondo->getContentSize().width/2,mFondo->getContentSize().height*0.4f));
}


void LayerTutorial::set2Boton(Boton* boton1,Boton *boton2){
    mFondo->addChild(boton1);
    mFondo->addChild(boton2);

    boton1->setPosition(Point(mFondo->getContentSize().width*0.25f,mFondo->getContentSize().height*0.4f));
    boton2->setPosition(Point(mFondo->getContentSize().width*0.75f,mFondo->getContentSize().height*0.4f));
}

