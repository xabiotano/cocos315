#include "LayerPuedesUsarPista.h"
#include "../widgets/Boton.h"
#include "../Levels.h"
#include "../series/SeriesScene.h"
#include "../psico/PsicoScene.h"
#include "../Variables.h"
#include "../widgets/NivelItem.h"
#include "../BaseScene.h"
#include "../psico/PsicoScene.h"
#include "../series/SeriesScene.h"
#include "../Levelsv2.h"
#include "../TipoNivelTypes.h"
#include "SimpleAudioEngine.h"
#include "../helpers/LogicSQLHelper.h"



LayerPuedesUsarPista:: ~LayerPuedesUsarPista(void){}

template <typename T> std::string LayerPuedesUsarPista::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }

LayerPuedesUsarPista* LayerPuedesUsarPista::mostrar(Ref* sender)
{
    LayerPuedesUsarPista* devolver=NULL;
    if(!Director::getInstance()->getRunningScene()){
        return NULL;
    }
    devolver= (LayerPuedesUsarPista*)Director::getInstance()->getRunningScene()->getChildByTag(LayerPuedesUsarPista::TAG_LAYER_NECESITASPISTA);
    if(devolver==NULL){
        LayerPuedesUsarPista* layer= LayerPuedesUsarPista::initLayer();
        layer->dibujarItems();
        layer->mSwallowTouches=true;
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(LayerPuedesUsarPista::TAG_LAYER_NECESITASPISTA);
        Director::getInstance()->getRunningScene()->addChild(layer);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/info.mp3");
        
        devolver=layer;
    }
    else{
        CCLOG("LayerPuedesUsarPista previamente mostrada");
    }
    return devolver;
}

LayerPuedesUsarPista* LayerPuedesUsarPista::initLayer()
{
    LayerPuedesUsarPista *layer = LayerPuedesUsarPista::create();
    return layer;
}

void LayerPuedesUsarPista::dibujarItems(){
    Boton* close=Boton::createBoton(this, menu_selector(LayerPuedesUsarPista::close), "ui/close.png");
    close->setPosition(Point(mFondo_panel->getContentSize().width*0.95f, mFondo_panel->getContentSize().height*0.95f));
    mFondo_panel->addChild(close);
}


// on "init" you need to initialize your instance
bool LayerPuedesUsarPista::init()
{
    mSize = Director::getInstance()->getWinSize();
    if ( !LayerColor::initWithColor((Color4B){0,0,0,200}, mSize.width, mSize.height )){
        return false;
    }
    
    mFondo_panel=Scale9Sprite::create("ui/levels/panel.png");
    mFondo_panel->setContentSize( Size(mSize.width*0.7f,mSize.height*0.7f) );
    mFondo_panel->setPosition(Point(-mSize.width*0.5f,mSize.height*0.5f));
    this->addChild(mFondo_panel);
    mFondo_panel->setTag(TAG_FONDO);
    
 
    Sprite* mono=Sprite::create("ui/puedes_usar_pista.png");
    mono->setPosition(Point(mono->getContentSize().width/2,mono->getContentSize().height/2 ));
    mFondo_panel->addChild(mono);
   
    Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("YOU_CAN_USE_HINT", "YOU_CAN_USE_HINT"), CCGetFont(), 40);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label->setWidth(mFondo_panel->getContentSize().width*0.7f);
    //label->setHeight(mFondo_panel->getContentSize().width*0.5f);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->setPosition(Point(mFondo_panel->getContentSize().width*0.5f,mFondo_panel->getContentSize().height*0.65f));
    label->enableOutline(Color4B(0,0,0,255),2);
    label->setColor(Color3B(166,44,55));
    mFondo_panel->addChild(label,3);

    Boton* pista=Boton::createBoton(this, menu_selector(LayerPuedesUsarPista::Hint), "ui/hint.png");
    pista->setPosition(Point(mFondo_panel->getContentSize().width*0.5f, label->getPositionY() - label->getContentSize().height*0.9f ));
    mFondo_panel->addChild(pista);
    
    
    this->setKeypadEnabled(true);
    mFondo_panel->runAction(EaseInOut::create(MoveTo::create(0.35f, Point(mSize.width*0.5f,mSize.height*0.5f)),3));
    return true;
}

void LayerPuedesUsarPista::Hint(Ref* sender){
    ocultar(NULL);
    BaseScene::getInstance()->Hint(NULL);
}


void LayerPuedesUsarPista::close(Ref* sender)
{
    ocultar(NULL);
}


void LayerPuedesUsarPista::onKeyBackClicked()
{
    close(NULL);
}

void LayerPuedesUsarPista::ocultar(Ref* pSender){
    if( Director::getInstance()->getRunningScene()->getChildByTag(LayerPuedesUsarPista::TAG_LAYER_NECESITASPISTA)){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/ui/popup_close.mp3");
        Director::getInstance()->getRunningScene()->removeChildByTag(LayerPuedesUsarPista::TAG_LAYER_NECESITASPISTA,true);
    }
}


