#ifndef ___ILayerShop__
#define ___ILayerShop__

#include <iostream>


class ILayerShop
{
public:

    virtual void onElixirComprado(int numero) {};

    virtual void onHintsComprados(int numero)  {};
    
    virtual void onCompleteGameComprado() {};
    
    virtual void onRemoveAdsComprado() {};
    
};

#endif
