//
//  LayerExplicarItems.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerExplicarItems__
#define __trivial__LayerExplicarItems__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"
#include "../widgets/CCLocalizedString/CCLocalizedString.h"
#include "../widgets/LanguageManager.h"

USING_NS_CC;

USING_NS_CC_EXT;

class LayerExplicarItems:  public LayerBase
{
    public:
    
    static LayerExplicarItems* initLayer();
    Rect rect();
   
    virtual bool init() override;
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerExplicarItems);
    
    void dibujarContent();
    
    static void ocultar(Ref* pSender);
    static LayerExplicarItems* mostrar();
    EventListenerTouchOneByOne* mListener;
    void onTouchEnded(Touch* touch, Event* event) override;

    
    static int TAG_EXPLICACION_ITEM;
    void onKeyBackClicked();
       
  
private:
    void close(Ref* sender);
   
    Boton* mClose;
    Sprite* mFondo;
  
    
    


};

#endif 
