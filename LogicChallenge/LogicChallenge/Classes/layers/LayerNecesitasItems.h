//
//  LayerNecesitasItems.h
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#ifndef __trivial__LayerNecesitasItems__
#define __trivial__LayerNecesitasItems__

#include <iostream>
#include "cocos2d.h"
#include "LayerBase.h"
#include "cocos-ext.h"



USING_NS_CC_EXT;
USING_NS_CC;


class LayerNecesitasItems: public LayerBase
{
    public:
       template <typename T> std::string tostr(const T& t);
        const int TAG_FONDO=10;
        static const int TAG_LAYER_NECESITASITEMS=2942041;
    
        virtual ~LayerNecesitasItems(void);
        static LayerNecesitasItems* get_instance();
        static LayerNecesitasItems* initLayer(int numNivel);
        static LayerNecesitasItems* mostrar(int numNivel);
        Rect rect();
        // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
        virtual bool init();
        // 'layer' is an autorelease object
        // preprocessor macro for "static create()" constructor ( node() deprecated )
        CREATE_FUNC(LayerNecesitasItems);
        virtual void onEnter();
        virtual void onExit();
        Size mSize;
    
        void dibujarItems();
        void onKeyBackClicked();
        static void ocultar(Ref* pSender);

    private:
        void close(Ref* sender);
        void goToLevelsV2(Ref* sender);
        void dibujarElixires( int elixires);
        int mNumeroNivel;
       // void Next();
        Scale9Sprite* mFondo_panel;
    
    
    
    
    
        
    
};

#endif /* defined(__trivial__LayerNecesitasItems__) */
