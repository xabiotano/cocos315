

#ifndef __trivial__LayerBase__
#define __trivial__LayerBase__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../widgets/Boton.h"
#include "../TipoNivelTypes.h"

USING_NS_CC;

USING_NS_CC_EXT;

class LayerBase:  public LayerColor
{
public:
    TipoNivel mTipoNivel;
    bool init() override;
    bool initWithColor(Color4B color);
    bool initWithPriority(Color4B color, int priority);
    bool initWithPriority(int priority);
    int mSubTipoNivel;
    virtual void Menu(Ref* sender);
    virtual void Comprar(Ref* sender);
    virtual void Reload(Ref* sender);
    bool containsTouchLocation(Touch* touch);
    EventListenerTouchOneByOne* mListener;
    bool onTouchBegan(Touch* touch, Event* event) override;
    
    void onExit() override;
    bool mSwallowTouches=true;
    int mPriority=-1;
    void addEventListener();
    const static int ZINDEX_MEDIO=400;
    const static int ZINDEX_ALTO=500;
    
protected:
    Size mSize;
    virtual void hint(){};
    //int PRIORIDAD_MEDIA=-300;
    //int PRIORIDAD_ALTA=-400;
    
 

   
     
    
   
    
};

#endif
