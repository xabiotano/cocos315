#ifndef __trivial__LayerHintSteps__
#define __trivial__LayerHintSteps__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "LayerBase.h"


USING_NS_CC;

USING_NS_CC_EXT;

class LayerHintSteps:  public LayerBase,public ScrollViewDelegate
{
    public:
    
    static LayerHintSteps* initLayer(CCArray* listaPasos);
    Rect rect();
   
    virtual bool init();
    // 'layer' is an autorelease object
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LayerHintSteps);
    
    void dibujarContent(CCArray* listaPasos);
    
    static void ocultar(Ref* pSender);
    static LayerHintSteps* mostrar(CCArray* listaPasos);
  
    
    static const  int TAG_HINTSTEPS=19214;
    static bool EstaMostrado();
    
    virtual void scrollViewDidScroll(ScrollView* view) ;
    virtual void scrollViewDidZoom(ScrollView* view){};
    
private:
    void close(Ref* sender);
    Boton* mClose;
    ScrollView* mScroll;
    Sprite* mFondo;
    LayerColor* mContenido;
    
    

    Label* mNumeroPistas;
    
    Sprite* mScrollIndicator;
    Sprite* mScrollIndicatorBullet;

};

#endif 
