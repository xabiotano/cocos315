//
//  SQLiteHelper.h
//  logicmaster2
//
//  Created by Xabier on 14/11/13.
//
//

#ifndef __logicmaster2__SQLiteHelper__
#define __logicmaster2__SQLiteHelper__
#include "cocos2d.h"
#include "sqlite3.h"
//#include "../BaseScene.h"
#include <iostream>

USING_NS_CC;

class SQLiteHelper{
public:

    //SQL
    static sqlite3 *_db;
    static int rc;
    static char* zErrMsg;

    static void initDB();
    static void openDB();
    static void closeDB();
  
   
    
    
};


#endif /* defined(__logicmaster2__SQLiteHelper__) */
