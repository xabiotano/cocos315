//
//  Frog.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Frog.h"
#include "SimpleAudioEngine.h"

Frog::Frog(bool esVerde,int posicion)
{
    mEsVerde=esVerde;
    mPosicion=posicion;
    mIsMoving=false;
}
Frog::~Frog(void)
{
    
}





Frog* Frog::create(Node* parent,std::string imagen,bool esVerde,int posicion)
{
    Frog *devolver= new Frog(esVerde,posicion);
    devolver->autorelease();
    devolver->setPosition(posicionPiedra(posicion));
    devolver->initBoton(devolver, menu_selector(Frog::moveToStone), imagen);
    devolver->setSound("sounds/fivefrogs/jump_water.mp3");
    return devolver;
}


void Frog::moveToStone(Ref* sender)
{
    if(mIsMoving){
        return;
    }
    
    int indexDestino=0;
    int signoDestino=1;
    if(mEsVerde)
    {
        indexDestino=mPosicion+1;
        signoDestino=1;
        
       
    }
    else{
        indexDestino=mPosicion-1;
        signoDestino=-1;
        
        
    }
    FiveFrogs* ffrogs=FiveFrogs::getInstance();
    
    //si la siguiente y la 2 mas lejos estan llenas no muevo
    if(ffrogs->mArrayOfFrogs->objectForKey(indexDestino)!=NULL && ffrogs->mArrayOfFrogs->objectForKey(indexDestino+(signoDestino))!=NULL )
    {
        //no se puede mover,siguiente y 2+ ocupadas
        return ;
    }
    
    ffrogs->mArrayOfFrogs->removeObjectForKey(mPosicion);
    if(ffrogs->mArrayOfFrogs->objectForKey(indexDestino)!=NULL &&  ffrogs->mArrayOfFrogs->objectForKey(indexDestino+(signoDestino))==NULL)
    {
        //mov de +2
        indexDestino+=signoDestino;
    }
    //compruebo la escala
    if(mEsVerde){
        if(indexDestino>8){
            return;
        }
        if(indexDestino>3){
            this->setScaleX(-1);
        }
        
        
    }
    else{
        //es marron
        if(indexDestino<0){
            return;
        }
        if(indexDestino<4)
        {
            this->setScaleX(-1);
        }
        
    }
    ffrogs->mArrayOfFrogs->setObject(this, indexDestino);
    this->mPosicion=indexDestino;
    Point destino=posicionPiedra(indexDestino);
    CallFunc* finMove=CallFunc::create([this]() { this->finMovement(); });
    mIsMoving=true;
    this->runAction(Sequence::create(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1), finMove,NULL));
    
    FiveFrogs::getInstance()->checkFinish();
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/fivefrogs/water_noise.mp3",false);
}

Point Frog::posicionPiedra(int index){
    Size mVisibleSize=Director::getInstance()->getVisibleSize();
    
    switch(index)
    {
        case 0:
            return Point((840.f/960.f) * mVisibleSize.width,(170.f/640.f)*mVisibleSize.height);
            break;
        case 1:
            return Point((680.f/960.f) * mVisibleSize.width,(190.f/640.f)*mVisibleSize.height);
             break;
        case 2:
            return Point((510.f/960.f) * mVisibleSize.width,(210.f/640.f)*mVisibleSize.height);
             break;
        case 3:
            return Point((330.f/960.f )* mVisibleSize.width,(230.f/640.f)*mVisibleSize.height);
             break;
        case 4:
            //VACIA
            return Point((180.f/960.f) * mVisibleSize.width,(260.f/640.f)*mVisibleSize.height);
             break;
        case 5:
            return Point((300.f/960.f) * mVisibleSize.width,(330.f/640.f)*mVisibleSize.height);
             break;
        case 6:
            return Point((480.f/960.f) * mVisibleSize.width,(340.f/640.f)*mVisibleSize.height);
             break;
        case 7:
            return Point((650.f/960.f) * mVisibleSize.width,(360.f/640.f)*mVisibleSize.height);
             break;
        case 8:
            return Point((820.f/960.f) * mVisibleSize.width,(375.f/640.f)*mVisibleSize.height);
             break;
        default:
            return Point(0,0);
    }
}

bool Frog::esverde(){
    return mEsVerde;
}




void Frog::finMovement(){
    mIsMoving=false;
}






