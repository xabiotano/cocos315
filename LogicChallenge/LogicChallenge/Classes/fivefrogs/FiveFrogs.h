#ifndef __FiveFrogs_SCENE_H__
#define __FiveFrogs_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "Frog.h"
#include "../widgets/imports.h"

USING_NS_CC;

class FiveFrogs : public BaseScene
{
public:
    
    virtual ~FiveFrogs(void);
    virtual void Retry(Ref* sender) override;
    void Hint(Ref* sender) override;
    void HintCallback(bool accepted) override;
    void HintStart(Ref* sender) override;
    void HintStop(Ref* sender) override;
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(FiveFrogs);
    static FiveFrogs* getInstance();
    Dictionary* mArrayOfFrogs;
    virtual void checkFinish();
    void onEnterTransitionDidFinish() override;
    bool someFrogMoving;
    void HintStartTemp(float dt);
    
private:
    Frog* rana1,*rana2,*rana3,*rana4,*rana5,*rana6,*rana7,*rana8,*rana9;
    void colocarRanas();
    
   
};

#endif // __FiveFrogs_SCENE_H__
