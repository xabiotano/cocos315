 #include "BaseScene.h"
#include "SimpleAudioEngine.h"
#include "layers/LayerGameOver.h"
#include "layers/LayerSuccess.h"
#include "layers/LayerShop.h"
#include "layers/LayerGastarPista.h"
#include "layers/LayerHint.h"
#include "layers/LayerTutorial.h"
#include "layers/LayerHintOk.h"
#include "layers/LayerHintSteps.h"
#include "layers/LayerHintWithButton.h"
#include "Variables.h"
#include "SandClock/SandClockScene.h"
#include "fivefrogs/FiveFrogs.h"
#include "goatlettucewolf/GoatLettuceWolfScene.h"
#include "hanoi/DisksScene.h"
#include "canibals/CanibalsScene.h"
#include "volcan/VolcanScene.h"
#include "bridge/BridgeScene.h"
#include "botellas/BotellasScene.h"
#include "sacks/SacksScene.h"
#include "series/SeriesScene.h"
#include "psico/PsicoScene.h"
#include "elevators/ElevatorsScene.h"
#include "hardbridge/HardBridgeScene.h"
#include "lightsout/LightsOutScene.h"
#include "Levels.h"
#include "PluginIAP/PluginIAP.h"
#include "widgets/imports.h"
//#include "PluginAdMob/PluginAdMob.h"
#include "helpers/LogicSQLHelper.h"
//#include "PluginAdColony/PluginAdColony.h"
#include "Levelsv2.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #include "objc/ObjCCalls.h"
#else
    #include "jni/InterfaceJNI.h"
#endif
#include <time.h>
#include "AdHelper.h"


using namespace cocos2d;
using namespace CocosDenshion;
using namespace sdkbox;

BaseScene* instancia_base;




std::string BaseScene::IAP_INIT= "IAP_INIT";
int BaseScene::TAG_HAND=9394;


BaseScene::~BaseScene(void){
   // this->unscheduleAllCallbacks();

}

template <typename T> std::string BaseScene::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }



bool BaseScene::init()
{
    
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    mNombrePantalla="BaseScene";
    
    
    mTextoGameOver=std::string("");
    mSmallGameOver=false;
    initGameScene();
    mShowed=false;
    mStars=3;
    mMotrarHint=false;
    instancia_base=this;
    mSituacionHint=false;
    mSituacionTutorial=false;
    mHintNodeToTouch=NULL;
    mHintStep=0;
    mRetryVisibleDuringGame=false;
   
    
    mEsPosibleCompartirEnWhatssap=false;
    
    this->setKeypadEnabled(true);
    return true;
}
BaseScene* BaseScene::getInstance(){
    return instancia_base;
}




int BaseScene::randomInt(int min, int max) //range : [min, max)
{

    return min +  arc4random() % (max - min);
}


float BaseScene::getFloat(float a, float b) {
    
   
    
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}


void BaseScene::GameOver()
{
    if(mShowed){
        return;
    }
    mShowed=true;
    LayerGameOver* gameOver=LayerGameOver::initLayer(mTextoGameOver,mSmallGameOver,mTipoNivel);
    this->addChild(gameOver,40);
    //por si acaso estra mostrada
    LayerInfo::ocultar(this);
    if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
        AdHelper::getInstance()->setIntersitialAdWhenLoaded(true);
        if(mTipoNivel!=kNivelLogica){
            AdHelper::showIntersitial();
        }else{
            /*
            CallFunc* anuncio= CallFunc::create([this]() { AdHelper::showIntersitial(); });
            float ran=CCRANDOM_0_1()* 2.f;
                this->runAction(Sequence::createWithTwoActions(DelayTime::create(ran),anuncio));
                
            }*/
            AdHelper::getInstance()->showIntersitialWithMaxDelay(2.f);
        }
    }
    
}



void BaseScene::Success(){
    
    if(mShowed){
        return;
    }
    mShowed=true;
    LayerSuccess* layer=LayerSuccess::mostrar(this, mStars, mTipoNivel);
    if(!mTextoSuccess.empty()){
        layer->setTexto(mTextoSuccess);
    }
    int numero_nivel;
    if(mTipoNivel==TipoNivel(kNivelLogica)){
        numero_nivel=Variables::CURRENT_SCENE;
    }
    if(!LogicSQLHelper::getInstance().getNivelSuperadoLogica(numero_nivel))
    {
        LogicSQLHelper::getInstance().setNivelSuperadoLogica(numero_nivel,mStars);
    }
}


bool BaseScene::getNivelJugable(int nivel,TipoNivel tipoNivel)
{
    if(tipoNivel!=TipoNivel(kNivelLogica)){
        CCAssert(true, "No mires si es jugable uno que no sea de logica");
    }
    if(LogicSQLHelper::getInstance().estaElJuegoCompleto())
    {
        return true;
    }
    if(Variables::ES_DEBUG)
    {
        return true;
    }
    if(nivel==1){
        return true;
    }
    else{
        if(LogicSQLHelper::getInstance().getNivelSuperadoLogica((nivel-1))){
            return true;
        }else{
            return false;
        }
    }
    
}

void BaseScene::setStars(int mGanados){
    mStars=mGanados;
}

















int BaseScene::getPrimerNivelNoSuperadoLogica()
{
    bool superado=true;
    int devolver=0;
    while(superado){
        devolver++;
        superado=LogicSQLHelper::getInstance().getNivelSuperadoLogica(devolver);
    }
    
    return devolver;
}



void BaseScene::Back(Ref* sender)
{
    Scene* scene;
    if(mTipoNivel==TipoNivel(kNivelLogica)){
        scene=Levels::createScene();
    }else if(mTipoNivel==TipoNivel(kNivelElixir)){
         scene=Levelsv2::scene();
    }
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
    
}

void BaseScene::Retry(Ref* sender)
{
    CCLOG("Unimplemented");
    
}





void BaseScene::ShowToast(std::string texto){
    
    mSize=Director::getInstance()->getWinSize();
    mVisibleSize=Director::getInstance()->getVisibleSize();
    mPanel=Sprite::create("ui/toast_panel.png");
    mPanel->setPosition(Point(mSize.width/2,0-mPanel->getContentSize().height));
    this->addChild(mPanel,20);
     CallFunc* finShowToast= CallFunc::create([this]() { this->finShowToast(); });
  
    mPanel->runAction(Sequence::createWithTwoActions(MoveTo::create(0.5f,(Point(mSize.width/2,mPanel->getContentSize().height/2 ))),finShowToast));
    //CCLabelTTF* textoPanel=CCLabelTTF::create(texto.c_str(), "Arial", 20);
      //textoPanel->setPosition(Point(mPanel->getContentSize().width/2,mPanel->getContentSize().height/2));
    Label* textoPanel=Label::createWithTTF(texto.c_str(),"Arial", 20);
    textoPanel->setPosition(Point(mPanel->getContentSize().width/2,mPanel->getContentSize().height/2));
    //Texto->setWidth(mFondo->getContentSize().width*0.8f);
    //Texto->setHeight(mFondo->getContentSize().height*0.3f);
    //Texto->setHorizontalAlignment(TextHAlignment::CENTER);
    //Texto->setVerticalAlignment(TextVAlignment::TOP);
    //Texto->setColor(Color3B(116,90,62));
    
    mPanel->addChild(textoPanel);
  
}

void BaseScene::finShowToast(){
    CallFunc* eliminarToast= CallFunc::create([this]() { this->eliminarToast(); });
    mPanel->runAction(Sequence::createWithTwoActions(DelayTime::create(2.f),eliminarToast));
}
void BaseScene::eliminarToast(){
    if(mPanel){
        mPanel->removeFromParent();
        mPanel=NULL;
    }
}


void BaseScene::initGameScene()
{
    mSize=Director::getInstance()->getWinSize();
    mVisibleSize=Director::getInstance()->getVisibleSize();
    
    mBack=Boton::createBoton(this, menu_selector(BaseScene::Back), "ui/back.png");
    
    mBack->setPosition(Point(mSize.width*0.10f,mSize.height*0.9f));
    this->addChild(mBack,31);
    mBack->setSound("sounds/ui/click1.mp3");
    
    
    //boton de ayuda
    mHelp=Boton::createBoton(this, menu_selector(BaseScene::Help), "ui/help.png");
    mHelp->setPosition(Point(mSize.width*0.20f,mSize.height*0.9f));
    this->addChild(mHelp,31);
    mHelp->setSound("sounds/ui/popup_open.mp3");
    
    std::string spritePista= "ui/hint.png";
    if(LogicSQLHelper::getInstance().userOwnsHint(Variables::CURRENT_SCENE)){
        spritePista="ui/hint_eye.png";
    }
    mHint=Boton::createBoton(this, menu_selector(BaseScene::Hint), spritePista);
    mHint->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    this->addChild(mHint,31);
    
    
    mContadorPistas=Sprite::create("ui/shop/hints_counter.png");
    mHint->addChild(mContadorPistas,-1);
    mContadorPistas->setPosition(Point(mContadorPistas->getContentSize().width*0.95f,mContadorPistas->getContentSize().width/2));
    updateHintCounter();
    
}

void BaseScene::updateHintCounter(){
    mContadorPistas->removeAllChildrenWithCleanup(true);
    
    if(LogicSQLHelper::getInstance().userOwnsHint(Variables::CURRENT_SCENE)){
        mHint->initWithFile("ui/hint_eye.png");
    }
    
    
    Label* numPistas= Label::createWithTTF("", CCGetFont(), 25);
    numPistas->setColor(Color3B(255,255,255));
    numPistas->setPosition(Point(mContadorPistas->getContentSize().width*0.65f,mContadorPistas->getContentSize().height/2));
    numPistas->setWidth(mContadorPistas->getContentSize().width);
    numPistas->setHeight(mContadorPistas->getContentSize().height);
    numPistas->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    numPistas->enableOutline(Color4B(0,0,0,255),2);
    mContadorPistas->addChild(numPistas,5);
    
    int num_pistas=LogicSQLHelper::getInstance().getHintsUsuario();
    std::string numero= tostr(num_pistas);
    if(LogicSQLHelper::getInstance().estaElJuegoCompleto()){
        numero="";
        numPistas->setSystemFontSize(17);
    }
    numPistas->setString(numero.c_str());
    
}




bool BaseScene::estaEnHint()
{
    return mSituacionHint;
}

void BaseScene::Help(Ref* sender)
{
    CCLOG("Help %s",mTextoHelp.c_str());
    LayerHelp::mostrar(this->mTextoHelp,mHelp);
}
void BaseScene::Shop(Ref* sender){
    LayerShop* layerShop=LayerShop::mostrar(this);
}






void BaseScene::Hint(Ref* sender)
{
    CCLOG("Hint");
    if(mSituacionHint){
        CCLOG("Ya esta en hint");
        return;
    }
    if(LogicSQLHelper::getInstance().userOwnsHint(Variables::CURRENT_SCENE)){
        HintCallback(true);
    }else{
        int num_pistas=LogicSQLHelper::getInstance().getHintsUsuario();
       
       
        CCLOG("Pistas %i",num_pistas);
        if(num_pistas>0){
             LayerGastarPista::mostrar();
        }else{
            LayerShop::mostrar(this);
        }
    }
    sdkbox::PluginGoogleAnalytics::logEvent("INTERACCION", "BOTONES", "HINT",1);
}





void BaseScene::checkFinish(Ref* sender)
{
    int intentos=0;
    if(mTipoNivel!=kNivelLogica){
        LogicSQLHelper::getInstance().sumarIntento(Variables::CURRENT_SCENE,mTipoNivel);
    }else{
        intentos=LogicSQLHelper::getInstance().getIntentos(Variables::CURRENT_SCENE,mTipoNivel);
    }
    if(mGameover){
        GameOver();
    }
    if(mSuccess){
        sdkbox::PluginGoogleAnalytics::logEvent("PROGRESO", "EXITO", mNombrePantalla,intentos);
        Success();
    }
    
    sdkbox::PluginGoogleAnalytics::logEvent("PROGRESO", "CHECK", mNombrePantalla,intentos);
    
   
}


LayerInfo* BaseScene::Info(std::string texto)
{
    return LayerInfo::mostrar(texto);
}

Point BaseScene::convertirAVisible(Point point){
    return Point((point.x/960.f) *mVisibleSize.width,(point.y/640.f)*mVisibleSize.height);
}



void BaseScene::HintCallback(bool accepted){
   if(accepted){
       if(!LogicSQLHelper::getInstance().userOwnsHint(Variables::CURRENT_SCENE)){
           LogicSQLHelper::gastarPista(Variables::CURRENT_SCENE);
           updateHintCounter();
           
       }
       CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/hint.mp3",false);
       mHint->setPosition(Point(-500,-500));
       mHelp->setPosition(Point(-500,-500));
       mBack->setPosition(Point(-500,-500));
       if(mRetry!=NULL){
         mRetry->setPosition(Point(-500,-500));
       }
    }
}
void BaseScene::HintStop(Ref* sender){
    LayerInfo::ocultar(NULL);//POR SI ACASO HA PREGUNTADO SI DESEA CERRARLA
    mHint->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    mHelp->setPosition(Point(mSize.width*0.20f,mSize.height*0.9f));
    mBack->setPosition(Point(mSize.width*0.10f,mSize.height*0.9f));
    if(mRetry!=NULL){
       mRetry->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.9f));
    }
    mSituacionHint=false;
    if(mScheudleHint!=NULL){
        this->unschedule(mScheudleHint);
        mHintStep=100;
    }
    LayerHintOk::ocultar(NULL);
    LayerHintSteps::ocultar(NULL);
    LayerHintWithButton::ocultar(NULL);
};


void BaseScene::TutorialStart(Ref* sender){
    if(mSkip!=NULL){
        mSkip->removeFromParent();
        mSkip=NULL;
    }
    mSkip=Boton::createBoton(this,menu_selector(BaseScene::TutorialNext), "ui/skip.png");
    addChild(mSkip,1);
    mSkip->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    mHint->setPosition(Point(-500,-500));
    mBack->setPosition(Point(-500,-500));
    mHelp->setPosition(Point(-500,-500));
}

void BaseScene::TutorialStop(Ref* sender){
    mSituacionTutorial=false;
    mStepTutorial=0;
    mHint->setPosition(Point(mSize.width*0.9f,mSize.height*0.9f));
    mBack->setPosition(Point(mSize.width*0.10f,mSize.height*0.9f));
    mHelp->setPosition(Point(mSize.width*0.20f,mSize.height*0.9f));
    mSkip->setPosition(Point(-500,-500));
    LayerTutorial::ocultar(NULL);
    if(mHand!=NULL){
        mHand->removeFromParent();
    }
}














Sprite* BaseScene::getHand(){
    Sprite* hand=(Sprite*) getChildByTag(28710);
    if(hand==NULL){
        hand=Sprite::create("ui/hand.png");
        hand->runAction(RepeatForever::create(JumpBy::create(0.5f, Point(0,0), hand->getContentSize().height/2, 1)));
        hand->setTag(BaseScene::TAG_HAND);
        hand->setScale(0.8f);
            }
    return hand;
   
}


void BaseScene::setHand(Sprite* item){
    mAccionTintado=item->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.5f, 100, 50, 50), TintTo::create(0.5f, 255, 255, 255))));
    
}





void BaseScene::onKeyBackClicked()
{
    if(Director::getInstance()->getRunningScene()->getChildByTag(LayerHelp::TAG_HELP)){
        LayerHelp::ocultar(NULL);
    }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerInfo::TAG_INFO)){
        LayerInfo::ocultar(NULL);
    }else if(Director::getInstance()->getRunningScene()->getChildByTag(LayerShop::TAG_SHOP)){
        LayerShop::ocultar(NULL);
    }else{
         Back(NULL);
    }

     
   
   
}



void BaseScene::onEnter()
{
    Layer::onEnter();
}

void BaseScene::onExit()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    Layer::onExit();
}


bool BaseScene::onTouchBegan(Touch* touch, Event* event){
    return false;
}


void BaseScene::onEnterTransitionDidFinish(){
    CCLOG("onEnterTransitionDidFinish");
     if(mTipoNivel==kNivelLogica){
          LogicSQLHelper::getInstance().sumarIntento(Variables::CURRENT_SCENE,mTipoNivel);
     }
  
    if(mTipoNivel==kNivelLogica){
       runAction(Sequence::create(DelayTime::create(0.5f),CallFunc::create([this]() { this->gestionarNivelJugado(); }),NULL));
    }
    
    if(mRetryVisibleDuringGame){
        mostrarRetryEnNivel();
    }
    if(mEsPosibleCompartirEnWhatssap){
        //si es posible añado el boton
        mostrarCompartirEnWhatssap();
    }

    if(mTipoNivel==kNivelLogica){
        int intentos=LogicSQLHelper::getInstance().getIntentos(Variables::CURRENT_SCENE,mTipoNivel);
        int numPistas=LogicSQLHelper::getInstance().getHintsUsuario();
        bool superado=LogicSQLHelper::getInstance().getNivelSuperadoLogica(Variables::CURRENT_SCENE);
        CCLOG("superado:%i ,intentos %i ,pistas %i",superado,intentos, numPistas);
        if(mTipoNivel==kNivelLogica && (intentos==6 || intentos==10 || intentos==15 || intentos ==20) && numPistas>0 && !superado){
            //muestro que use la pista
            mScheduleMostrarPanelPista=schedule_selector(BaseScene::mostrarPanelUsarPista);
            this->scheduleOnce(mScheduleMostrarPanelPista, 0.5f);
        }
        if(intentos==0){
            sdkbox::PluginGoogleAnalytics::logEvent("PROGRESO", "UNLOCK", mNombrePantalla,0);
        }
    }
    
   
    if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
        //sdkbox::PluginAdColony::setListener(this);
       // sdkbox::PluginAdMob::setListener(this);
        //sdkbox::PluginChartboost::setListener(this);
        AdHelper::showBanner();
        //showAdSEL_SCHEDULE=schedule_selector(BaseScene::showAd);
        //this->schedule(showAdSEL_SCHEDULE, 2.f);
        //mIntentosAd=0;
        
    }

  
}



void BaseScene::mostrarPanelUsarPista(float dt){
    LayerPuedesUsarPista::mostrar(NULL);
}

void BaseScene::gestionarNivelJugado(){
    CCAssert(mTipoNivel==kNivelLogica, "Solo se puede aplicar en niveles de logica");
    
    mNivelJugadoAlgunaVezLogica=LogicSQLHelper::getInstance().getNivelJugadoAlgunaVezLogica(Variables::LEVELS_NIVEL_CLICKADO);
    LogicSQLHelper::getInstance().setNivelJugadoAlgunaVezLogica(Variables::LEVELS_NIVEL_CLICKADO);
    if(!mNivelJugadoAlgunaVezLogica){
        Help(NULL);
    }
    
}

void BaseScene::mostrarRetryEnNivel(){
    mRetry=Boton::createBoton(this, menu_selector(BaseScene::ReloadWithAd), "ui/reload2.png");
    this->addChild(mRetry,30);
    //retry->setOpacity(f);
    mRetry->setPosition(Point(mVisibleSize.width*0.8f,mVisibleSize.height*0.9f));
    mRetry->setSound("sounds/ui/click1.mp3");
}

void BaseScene::mostrarCompartirEnWhatssap(){
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    mShare=Boton::createBoton(this, menu_selector(BaseScene::shareButton), "ui/share_ios.png");
#else
    mShare=Boton::createBoton(this, menu_selector(BaseScene::shareButton), "ui/share_android.png");
#endif
    mShare->setSwallowTouches(false);
    this->addChild(mShare,28);
    
    //retry->setOpacity(f);
    mShare->setPosition(Point(mVisibleSize.width*0.9f,mVisibleSize.height*0.75f));
    mShare->setSound("sounds/ui/click1.mp3");

}


void BaseScene::shareButton(Ref* sender){
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        //ObjCCalls::shareAmigos();
#else
   // InterfaceJNI::shareAmigos();
#endif
    
    

}



void BaseScene::ReloadWithAd(Ref* sender){
    Retry(NULL);
    sdkbox::PluginGoogleAnalytics::logEvent("PROGRESO", "RELOAD", mNombrePantalla,1);
    //muestro el anuncio
    if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
        AdHelper::showIntersitial();
     
    }
   
}






/*ILayerShop*/
void BaseScene::onElixirComprado(int numero)
{
    //NADA
}
void BaseScene::onHintsComprados(int numero)
{
    updateHintCounter();
}
void BaseScene::onCompleteGameComprado()
{
    
    updateHintCounter();
    AdHelper::hideBanner();
}
void BaseScene::onRemoveAdsComprado()
{
    AdHelper::hideBanner();
    Retry(NULL);
    
}
void BaseScene::onExitTransitionDidStart()
{
    stopAllActions();
    //this->unscheduleAllCallbacks();
    Layer::onExitTransitionDidStart();
    
}



