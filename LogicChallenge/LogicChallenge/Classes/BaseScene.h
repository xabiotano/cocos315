#ifndef __BaseScene_SCENE_H__
#define __BaseScene_SCENE_H__

#include "Prioridades.h"
#include "cocos2d.h"
#include "layers/LayerHelp.h"
#include "widgets/Boton.h"
#include "widgets/StarLocked.h"
#include "layers/LayerBlock.h"
#include "layers/LayerInfo.h"
#include "layers/LayerHint.h"
#include "layers/LayerTutorial.h"
#include "layers/LayerHintOk.h"
#include "layers/LayerHintSteps.h"
#include "layers/LayerHintWithButton.h"
#include "layers/LayerGanarItem.h"
#include "layers/LayerDesbloqueadoNivel.h"
#include "PluginIAP/PluginIAP.h"
#include "TipoNivelTypes.h"
#include "widgets/imports.h"

#include "helpers/LogicSQLHelper.h"
#include "layers/LayerPuedesUsarPista.h"
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#include "layers/ILayerShop.h"
#include "AdHelper.h"


USING_NS_CC;




class BaseScene : public cocos2d::Layer , public ILayerShop


{
public:
    virtual ~BaseScene(void);
    virtual void Back(Ref* sender);
    virtual void Retry(Ref* sender);
    virtual void Help(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void Shop(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void Success();
    virtual void checkFinish(Ref* sender);
    virtual void GameOver();
    //opcionales
    virtual void HintStart(Ref* sender){};
    virtual void HintStop(Ref* sender);
    virtual void HintNext(Ref* sender){};
    
    virtual void TutorialStart(Ref* sender);
    virtual void TutorialStop(Ref* sender);
    virtual void TutorialNext(Ref* sender){};
    
    virtual void HelpClose(Ref* sender){}
    
    void onKeyBackClicked();
    
    virtual LayerInfo*  Info(std::string texto);

    void ReloadWithAd(Ref* sender);
    static BaseScene* getInstance();
    void updateHintCounter();
   

  
 

    
    
   
    
    static std::string IAP_INIT;
    static int TAG_HAND;
    
    bool estaEnHint();
    bool mSituacionHint;
    template <typename T> std::string tostr(const T& t);
    
    
    static bool getNivelJugable(int nivel,TipoNivel tipoNivel);
    void setStars(int mGanados);

    static int getPrimerNivelNoSuperadoLogica();
    
    
    void  onEnterTransitionDidFinish() override;
    void  onEnter() override;
    void  onExit() override;
    void onExitTransitionDidStart() override;
    bool onTouchBegan(Touch* touch, Event* event) override;
    
    
   // void showIntersitial();
   // void cargarAnuncio();
    
protected:
    std::string mNombrePantalla;
    TipoNivel mTipoNivel;
    virtual bool init() override;
    Size mSize;
    Size mVisibleSize;
  
   
    virtual void ShowToast(std::string texto);
    void initGameScene();
   
    bool mMotrarHint;
    bool mSuccess;
    bool mGameover;
    bool mSmallGameOver;
    
  
    bool mRetryVisibleDuringGame;
   
    //Label* mHelpPanel;
    std::string mTextoHelp;
    std::string mTextoGameOver;
    std::string mTextoSuccess;
    int mStars;
    Point convertirAVisible(Point point);
  
    std::vector<sdkbox::Product> _products;
    //Label* mHintCounter;
    int mHintStep;
    void setHand(Sprite* item);
    
    Sprite* mHintNodeToTouch;
    Action* mAccionTintado;
    SEL_SCHEDULE mScheudleHint;
 
    int mStepTutorial;
    Sprite* mHand;
    bool mSituacionTutorial;
    bool mShowed;
    bool mNivelJugadoAlgunaVezLogica;
    void gestionarNivelJugado();
    
    bool mEsPosibleCompartirEnWhatssap;
    
    
    Boton* mShare;
    void mostrarPanelUsarPista(float dt);
    
private:
    SEL_SCHEDULE mScheduleMostrarPanelPista;
    //SEL_SCHEDULE showAdSEL_SCHEDULE;
    
    void mostrarRetryEnNivel();
    void mostrarCompartirEnWhatssap();
    void recargarShop();
    Boton* mHint,*mHelp,*mBack,*mSkip,*mRetry;
    Sprite* getHand();
    void finShowToast();
    void eliminarToast();
    Sprite* mPanel;
   
    Sprite* mContadorPistas;
  
    //CHARTBOOST
    /*
    void onChartboostCached(const std::string& name) override;
    bool onChartboostShouldDisplay(const std::string& name) override;
    void onChartboostDisplay(const std::string& name) override;
    void onChartboostDismiss(const std::string& name) override;
    void onChartboostClose(const std::string& name) override;
    void onChartboostClick(const std::string& name) override;
    void onChartboostReward(const std::string& name, int reward) override;
    void onChartboostFailedToLoad(const std::string& name, sdkbox::CB_LoadError e) override;
    void onChartboostFailToRecordClick(const std::string& name, sdkbox::CB_ClickError e) override;
    void onChartboostConfirmation() override;
    void onChartboostCompleteStore() override;
     */
    //ADMOB
    /*
    void adViewDidReceiveAd(const std::string &name) override;
    void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) override;
    void adViewWillPresentScreen(const std::string &name) override;
    void adViewDidDismissScreen(const std::string &name) override;
    void adViewWillDismissScreen(const std::string &name) override;
    void adViewWillLeaveApplication(const std::string &name) override;
    */
    
    int mIntentosAd;
    //void showAd(float dt);
   
    //void showIntersitialVideoTambien(bool videoSi);
    void shareButton(Ref* sender);
    float getFloat(float a, float b);
    int randomInt(int min, int max) ;//range : [min, max)
    
    EventListenerTouchOneByOne* mListener;
    
   
    
    
    //ADCOLONY
  /*  void onAdColonyChange(const sdkbox::AdColonyAdInfo& info, bool available) override;
    void onAdColonyReward(const sdkbox::AdColonyAdInfo& info,
                          const std::string& currencyName, int amount, bool success) override;
    void onAdColonyStarted(const sdkbox::AdColonyAdInfo& info) override;
    void onAdColonyFinished(const sdkbox::AdColonyAdInfo& info) override;
*/
    
    
    
    //ILayerShop
    void onElixirComprado(int numero) override;
    void onHintsComprados(int numero) override;
    void onCompleteGameComprado() override;
    void onRemoveAdsComprado() override;

    
    
    
};

#endif // __BaseScene_SCENE_H__
