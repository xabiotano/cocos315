#ifndef __INICIO_SCENE_H__
#define __INICIO_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "widgets/imports.h"


USING_NS_CC;
//USING_NS_CC_EXT;

class Inicio : public cocos2d::Layer
{
    public:
    static cocos2d::Scene* createScene();
    
     template <typename T> std::string tostr(const T& t);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* scene();
    
    // a selector callback
    void irALogic(Ref* sender);
    void irALevelsV2(Ref* sender);
    //void irAPsico();
    
    // implement the "static node()" method manually
    CREATE_FUNC(Inicio);
    
    private:
    Size mVisibleSize;
    Sprite* mFondo;
    //void cargarPorcentajes();
    Label*  mPorcentajeEnergyLabel,*mPorcentajeLogicLabel,*mPorcentajeCafeLabel;

    void onEnterTransitionDidFinish();
    void onKeyBackClicked();
    void animarLogo(float dt);
    void cobrarRegalos(float dt);
    Sprite* mLogo;
    Boton* mLeft,*mRight;
    
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event);

    

};

#endif // __INICIO_SCENE_H__
