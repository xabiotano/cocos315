//
//  Elevator.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Elevator__
#define __logicmaster2__Elevator__

#include <iostream>
#include "../widgets/Boton.h"
#include "Elevator.fwd.h"
//#include "../widgets/CCLabelStroke.h"





class Elevator: public Boton
{
    
    
    public:
    
        Elevator(int piso);
        virtual ~Elevator(void);
        static cocos2d::Scene* scene();
        void initElavator();
        static Elevator* create(Node* parent,int z_index);
        void click(Ref* sender);
        void setMoving(bool moving);
        bool isMoving();
        bool isSelected();
        void setPiso(int piso);
        int getPiso();
        int mPiso;
        void up();
        void down();
    
    private:
        void finMoving();
        bool mIsMoving;
        unsigned int mSoundEffect;
        Sprite* mContadorBackground;
        Label* mLabelPiso;
        bool mSelected;
        int tempPiso;
        void pisoUpIteration(float dt);
        void pisoDownIteration(float dt);
        SEL_SCHEDULE mScheduleAscensor;
      };


#endif /* defined(__logicmaster2__Elevator__) */
