#include "ElevatorsScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Elevator.h"


using namespace cocos2d;
using namespace CocosDenshion;


ElevatorsScene* instancia_elevator;
ElevatorsScene::~ElevatorsScene(void){
    CC_SAFE_RELEASE(mElevatorsSeleccionados);
}


Scene* ElevatorsScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    ElevatorsScene *layer = ElevatorsScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool ElevatorsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_elevator=this;
    mNombrePantalla="ElevatorsScene";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    mVisibleSize=Director::getInstance()->getVisibleSize();
    
  
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    
    
    Sprite* mFondo = Sprite::create("elevators/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height - mFondo->getContentSize().height/2 + origin.y ));
    this->addChild(mFondo, 0);
    
    
    
    isMoving=false;
  
    
    
    float scalex=Director::getInstance()->getVisibleSize().width/960;
    float scaley=Director::getInstance()->getVisibleSize().height/460;
    if(scalex>scaley){
        scaley=scalex;
    }
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);

    
  

    
    pintarElevators();
    
    //panel botones
    mPanelBotones=Sprite::create("elevators/panel_botones.png");
    mPanelBotones->setPosition(Point(mPanelBotones->getContentSize().width/2 ,mVisibleSize.height*0.48f));
    this->addChild(mPanelBotones,1);

    mPanelAvisos=Sprite::create("elevators/fondo_texto.png");
    mPanelAvisos->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height*0.9f));
    this->addChild(mPanelAvisos);

    pintarAvisoSeleccionarElevator();
    pinarBotonesLateral();
    
    
    
    
  
    
    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_ELEVATORS");
    mRetryVisibleDuringGame=true;
    
    
    
    return true;
}


void ElevatorsScene::pinarBotonesLateral(){
    
    mPanelBotones->removeAllChildrenWithCleanup(true);
    
    botonUp=Boton::createBoton(this, menu_selector(ElevatorsScene::clickUp), "elevators/button_up.png");
    botonUp->setPosition(Point(mPanelBotones->getContentSize().width*0.5f,mPanelBotones->getContentSize().height*0.65f));
    mPanelBotones->addChild(botonUp,1);
    botonUp->setSound("sounds/elevators/elevatorup.mp3");
    
    botonDown=Boton::createBoton(this, menu_selector(ElevatorsScene::clickDown), "elevators/button_down.png");
    botonDown->setPosition(Point(mPanelBotones->getContentSize().width*0.5f,mPanelBotones->getContentSize().height*0.22f));
    mPanelBotones->addChild(botonDown,1);
    botonDown->setSound("sounds/elevators/elevatorup.mp3");
    
    desactivarBotones();

}

void ElevatorsScene::pintarElevators()
{
    if(mElevatorsSeleccionados!=NULL){
        CC_SAFE_RELEASE(mElevatorsSeleccionados);
    }
    mElevatorsSeleccionados=CCArray::create();
    mElevatorsSeleccionados->retain();
    
   
    
    isMoving=false;
    if(mElevator0!=NULL){
        mElevator0->removeFromParent();
        mElevator0=NULL;
    }
    if(mElevator1!=NULL){
        mElevator1->removeFromParent();
        mElevator1=NULL;
    }
    if(mElevator2!=NULL){
        mElevator2->removeFromParent();
        mElevator2=NULL;
    }
    if(mElevator3!=NULL){
        mElevator3->removeFromParent();
        mElevator3=NULL;
    }
    if(mElevator4!=NULL){
        mElevator4->removeFromParent();
        mElevator4=NULL;
    }
    mElevator0=Elevator::create(this, 17);
    mElevator1=Elevator::create(this, 26);
    mElevator2=Elevator::create(this, 20);
    mElevator3=Elevator::create(this, 19);
    mElevator4=Elevator::create(this, 31);
    
    
    
    
    
    
    
    Size tamanoElevators=mVisibleSize;
    tamanoElevators.width=tamanoElevators.width*0.8f;
    float extraWidth=mVisibleSize.width*0.2f;
    
    mElevator0->setPosition(Point(tamanoElevators.width*0.1f+extraWidth ,mVisibleSize.height*0.5f));
    mElevator1->setPosition(Point(tamanoElevators.width*0.3f+extraWidth,mVisibleSize.height*0.5f));
    mElevator2->setPosition(Point(tamanoElevators.width*0.5f+extraWidth,mVisibleSize.height*0.5f));
    mElevator3->setPosition(Point(tamanoElevators.width*0.7f+extraWidth,mVisibleSize.height*0.5f));
    mElevator4->setPosition(Point(tamanoElevators.width*0.9f+extraWidth,mVisibleSize.height*0.5f));
    
    Sprite* fondo_texto=Sprite::create("elevators/fondo_texto.png");
    fondo_texto->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height*0.9f));
    this->addChild(fondo_texto);
    
    
    this->addChild(mElevator0,1);
    this->addChild(mElevator1,1);
    this->addChild(mElevator2,1);
    this->addChild(mElevator3,1);
    this->addChild(mElevator4,1);
    
    
    mNumeroMovimientos=0;
    
    Sprite* suelo= Sprite::create("elevators/suelo.png");
    suelo->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height/2- mElevator0->getContentSize().height/2- suelo->getContentSize().height/2 ));
    this->addChild(suelo);
    

}



void ElevatorsScene::clickUp(Ref* sender){
   
    if(!isValidMove(true)){
        return;
    }
    
    Object* it = NULL;
    CCARRAY_FOREACH(mElevatorsSeleccionados, it)
    {
        Elevator *elevator =(Elevator*) it;
        elevator->up();
       
    }
    mElevatorsSeleccionados->removeAllObjects();
    isMoving=true;
    desactivarBotones();
    subirContador();
    ElevatorsScene::getInstance()->pintarAvisoSeleccionarElevator();
    pintarAvisoWait();

}

bool ElevatorsScene::isValidMove(bool esParaArriba){
    if(isMoving){
        return false;
    }
    if(mElevatorsSeleccionados->count()!=2){
        return false;
    }
    bool valido=true;
    Object* it = NULL;
    CCARRAY_FOREACH(mElevatorsSeleccionados, it)
    {
        Elevator *elevator =(Elevator*) it;
        if(!esParaArriba){
            if(elevator->getPiso()<13){
                MessageBox("Oups!", LanguageManager::getInstance()->getStringForKey("ELEVATORS_UNDER_0","There are not floors under 0").c_str());
                valido=false;
            }
        }else{
            if(elevator->getPiso()>=42){
                MessageBox("Oups!", LanguageManager::getInstance()->getStringForKey("ELEVATORS_UP_49","There are not floors up 49").c_str());
                valido=false;
            }
        }
    }
    return valido;
}




void ElevatorsScene::clickDown(Ref* sender){
   
    
    if(!isValidMove(false)){
        return;
    }
    Object* it = NULL;
    CCARRAY_FOREACH(mElevatorsSeleccionados, it)
    {
        Elevator *elevator =(Elevator*) it;
         elevator->down();
    }
    mElevatorsSeleccionados->removeAllObjects();
    isMoving=true;
    desactivarBotones();
    subirContador();
    ElevatorsScene::getInstance()->pintarAvisoSeleccionarElevator();
    pintarAvisoWait();
}

void ElevatorsScene::subirContador(){
    mNumeroMovimientos++;
   
    char memory[200];
    sprintf(memory,"#%i",mNumeroMovimientos);
    //mCounter->setString(memory);
    
}

ElevatorsScene* ElevatorsScene::getInstance(){
    return instancia_elevator;
};

void ElevatorsScene::checkFinish(){
    int min_piso=21;
    int max_pixo=25;
    int THREE_STARS_RANGE=8;
    int TWO_STARS_RANGE=12;
    
    
    
    if(mElevator0->getPiso()>=min_piso && mElevator0->getPiso()<=max_pixo
       &&   mElevator1->getPiso()>= min_piso&& mElevator1->getPiso()<=max_pixo
       &&   mElevator2->getPiso()>=min_piso && mElevator2->getPiso()<=max_pixo
       &&   mElevator3->getPiso()>= min_piso&& mElevator3->getPiso()<=max_pixo
       &&   mElevator4->getPiso()>=min_piso && mElevator4->getPiso()<=max_pixo)
    {
        mSuccess=true;
        if(mNumeroMovimientos<=THREE_STARS_RANGE){
            mStars=3;
        }else if(mNumeroMovimientos<=TWO_STARS_RANGE)
        {
            mStars=3;
        }
        else{
            mStars=3;
        }
        botonDown->setEnabled(false);
        botonUp->setEnabled(false);
        BaseScene::checkFinish(NULL);
    }
}
void ElevatorsScene::pintarAvisoSeleccionarElevator()
{
    if(mAviso!=NULL){
        mPanelAvisos->removeAllChildrenWithCleanup(true);
    }
    this->removeChildByTag(999);
    int numElevators=getNumElevatorsSelecccionados();
   
    /*mAviso=CCLabelStroke::create(mPanelAvisos, "", CCGetFont(), 20, Size(mPanelAvisos->getContentSize().width, mPanelAvisos->getContentSize().height), kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
     mAviso->setPosition(Point(mPanelAvisos->getContentSize().width/2,mPanelAvisos->getContentSize().height*0.3f));
    mPanelAvisos->addChild(mAviso,4);
    mAviso->setTag(999);*/
    
    
    mAviso= Label::createWithTTF("", CCGetFont(), 20);
    mAviso->setColor(Color3B(255,255,255));
    mAviso->setWidth(mPanelAvisos->getContentSize().width);
    mAviso->setHeight(mPanelAvisos->getContentSize().height);
        mAviso->setPosition(Point(mPanelAvisos->getContentSize().width/2,mPanelAvisos->getContentSize().height*0.3f));
    mAviso->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
    mAviso->enableOutline(Color4B(0,0,0,255),2);
     mAviso->setTag(999);
    mPanelAvisos->addChild(mAviso,4);
    
    
   
    if(numElevators==1){
        mAviso->setString(LanguageManager::getInstance()->getStringForKey("ELEVATORS_PICK_ANOTHER","Pick another one to move!"));
    }else if(numElevators==2){
        mAviso->setString(LanguageManager::getInstance()->getStringForKey("ELEVATORS_CLICK_LEFT","Click up/down on the left panel!"));
    }
    else if(numElevators==0){
        mAviso->setString(LanguageManager::getInstance()->getStringForKey("ELEVATORS_PICK_2_ELEVATORS","Pick 2 elevators to move!"));
    }
   
   
}




void ElevatorsScene::pintarAvisoWait(){
    if(mAviso!=NULL){
        mPanelAvisos->removeAllChildrenWithCleanup(true);
    }
    this->removeChildByTag(999);
    int numElevators=getNumElevatorsSelecccionados();
    /*mAviso=CCLabelStroke::create(mPanelAvisos, "", CCGetFont(), 28, Size(mPanelAvisos->getContentSize().width, mPanelAvisos->getContentSize().height), kCCTextAlignmentCenter,kCCVerticalTextAlignmentTop);
    mAviso->setPosition(Point(mPanelAvisos->getContentSize().width/2,mPanelAvisos->getContentSize().height*0.3f));
    mPanelAvisos->addChild(mAviso,4);
    mAviso->setTag(999);
    mAviso->setString(LanguageManager::getInstance()->getStringForKey("ELEVATORS_WAIT", "Wait..."), Color3B(0,0,0));*/
    
    
    
    mAviso= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("ELEVATORS_WAIT", "Wait..."), CCGetFont(), 28);
    mAviso->setColor(Color3B(255,255,255));
    mAviso->setWidth(mPanelAvisos->getContentSize().width);
    mAviso->setHeight(mPanelAvisos->getContentSize().height);
    mAviso->setPosition(Point(mPanelAvisos->getContentSize().width/2,mPanelAvisos->getContentSize().height*0.3f));
    mAviso->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
    mAviso->enableOutline(Color4B(0,0,0,255),2);
    mAviso->setTag(999);
    mPanelAvisos->addChild(mAviso,4);
    
    

}


int ElevatorsScene::getNumElevatorsSelecccionados()
{
    return mElevatorsSeleccionados->count();
}


bool ElevatorsScene::addElevator(Elevator* elevator){
    
    if(mElevatorsSeleccionados->count()==2){
        return false;
        
    }else{
        mElevatorsSeleccionados->addObject(elevator);
        if(mElevatorsSeleccionados->count()==2){
           
            activarBotones();
        }
        return true;
    }
}



void ElevatorsScene::removeElevator(Elevator*  elevator){
   
    mElevatorsSeleccionados->removeObject(elevator);
     pintarAvisoSeleccionarElevator();
    if(mElevatorsSeleccionados->count()!=2){
        desactivarBotones();
    }
   
}

void ElevatorsScene::movimientoFinish(){
    isMoving=false;
    pintarAvisoSeleccionarElevator();
    checkFinish();
}


void ElevatorsScene::activarBotones(){
    botonUp->setOpacity(255.f);
    botonDown->setOpacity(255.f);
}
void ElevatorsScene::desactivarBotones(){
    botonUp->setOpacity(120.f);
    botonDown->setOpacity(120.f);
}


void ElevatorsScene::Retry(Ref* sender){
    Scene* scene=ElevatorsScene::scene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));    
}



void ElevatorsScene::HintStart(Ref* sender){
    CCLOG("%i",mHintStep);
    if(mElevator0->isMoving()||
       mElevator1->isMoving()||
       mElevator2->isMoving()||
       mElevator3->isMoving()||
       mElevator4->isMoving()){
        this->runAction(Sequence::create(DelayTime::create(2.f), CallFuncN::create(CC_CALLBACK_1(ElevatorsScene::HintStart,this)),NULL));
        CCLOG("ElevatorsScene::HintStart Moving");
        return;
    }
    /*
     0 2 up
     3 4 up
     3 4 down
     1 4 up
     1 4 down
     2 4 up
     2 4 down
     3 4 up
     */
   
    
    switch(mHintStep){
        case 1:
            mElevator0->click(NULL);
            mElevator2->click(NULL);
            break;
        case 2:
            botonUp->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickUp(NULL);
            break;
        case 3:
            mElevator3->click(NULL);
            mElevator4->click(NULL);
            break;
        case 4:
            botonUp->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickUp(NULL);
            break;
        case 5:
            mElevator3->click(NULL);
            mElevator4->click(NULL);
            break;
        case 6:
            botonDown->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickDown(NULL);
            break;
            
          
        case 7:
            mElevator1->click(NULL);
            mElevator4->click(NULL);
            break;
        case 8:
            botonUp->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickUp(NULL);
            break;
       case 9:
            mElevator1->click(NULL);
            mElevator4->click(NULL);
            break;
        case 10:
            botonDown->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickDown(NULL);
            break;
        case 11:
            mElevator2->click(NULL);
            mElevator4->click(NULL);
            break;
        case 12:
            botonUp->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickUp(NULL);
            break;
        case 13:
            mElevator2->click(NULL);
            mElevator4->click(NULL);
            break;
        case 14:
            botonDown->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickDown(NULL);
            break;
        case 15:
            mElevator3->click(NULL);
            mElevator4->click(NULL);
            break;
        case 16:
            botonUp->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.3f), ScaleTo::create(0.5f, 1.f)));
            clickUp(NULL);
            break;
        
   }
    if(mHintStep<=16){
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(ElevatorsScene::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
    }
    
    mHintStep++;
}

void ElevatorsScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void ElevatorsScene::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        CCLOG("MOSTRAR");
        pintarElevators();
        pintarAvisoSeleccionarElevator();
        pinarBotonesLateral();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(ElevatorsScene::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
        //mScheudleHint=schedule_selector(ElevatorsScene::HintStart);
        //this->schedule(mScheudleHint,2.f);
    }else{
        CCLOG("CANCELADO");
    }
}

void ElevatorsScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}

