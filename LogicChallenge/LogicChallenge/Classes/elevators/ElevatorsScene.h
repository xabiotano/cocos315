#ifndef __ElevatorsScene_SCENE_H__
#define __ElevatorsScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Elevator.fwd.h"
USING_NS_CC;


class ElevatorsScene : public BaseScene
{
public:
    
    ~ElevatorsScene(void);
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(ElevatorsScene);
    static ElevatorsScene* getInstance();
    
    virtual void checkFinish();
    
   
    
    void clickUp(Ref* sender);
    void clickDown(Ref* sender);
    bool addElevator(Elevator* elevator);
    void removeElevator(Elevator* elevator);
    void movimientoFinish();
    bool isMoving;
    void activarBotones();
    void desactivarBotones();
    int getNumElevatorsSelecccionados();
    void pintarAvisoSeleccionarElevator();
    void onEnterTransitionDidFinish();
private:
  
    Sprite* mPanelAvisos;
    Label* mAviso;
   
    int mNumeroMovimientos;
    Elevator* mElevator0,*mElevator1,*mElevator2,*mElevator3,*mElevator4;
    CCArray* mElevatorsSeleccionados;
    bool isValidMove(bool esParaArriba);
    Boton *botonUp,*botonDown;
    void subirContador();
    void pintarAvisoWait();
    void pinarBotonesLateral();
    Sprite* mPanelBotones;
    void pintarElevators();
    
protected:
    virtual void Retry(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
    virtual void TutorialStart(){};
    
    
};


#endif // __ElevatorsScene_SCENE_H__
