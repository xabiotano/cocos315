//
//  Elevator.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Elevator.h"
#include "SimpleAudioEngine.h"
#include "ElevatorsScene.h"
#include <stdio.h>
#include "ElevatorsScene.h"

Elevator::Elevator(int piso){
    mIsMoving=false;
    mPiso=piso;
}

Elevator::~Elevator(void){}


Elevator* Elevator::create(Node* parent,int piso)
{
    Elevator *devolver= new Elevator(piso);
  
    devolver->initElavator();
    devolver->autorelease();
    return devolver;
}


void Elevator::initElavator()
{
    this->initBoton(this, menu_selector(Elevator::click), std::string("elevators/elevator_closed.png"));
    mContadorBackground=Sprite::create("elevators/panel_contador.png");
    Size size=this->getContentSize();
    mContadorBackground->setPosition(Point(size.width/2,size.height*1.1f));
    this->addChild(mContadorBackground);
    //mContadorBackground->setColor(Color3B(0.f,0.f,0.f));
    mLabelPiso=Label::createWithTTF("", CCGetFont(), 30);
    mLabelPiso->enableOutline(Color4B(0,0,0,255),2);
    mContadorBackground->addChild(mLabelPiso);
    
    Size tamano=mContadorBackground->getContentSize();
    mLabelPiso->setPosition(Point(tamano.width/2.f,tamano.height/2.f));
    setPiso(mPiso);
    mSelected=false;
   
    
}

void Elevator::click(Ref* sender)
{
    
    if(mIsMoving){
        return;
    }
    if(ElevatorsScene::getInstance()->isMoving){
        CCLOG("Algun elevator isMoving");
        return;
    }
   
    
    
    if(this->isSelected()){
        this->stopAllActions();
        this->setColor(Color3B(255.f,255.f,255.f));
        mSelected=false;
        ElevatorsScene::getInstance()->removeElevator(this);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false); 
        return;
    }
    //compruebo que no haya mas de dos
    if(ElevatorsScene::getInstance()->addElevator(this)){
        this->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(0.75f, 255.f, 200.f, 255.f), TintTo::create(0.75f, 255.f, 170.f, 255.f))));
        mSelected=true;
         this->runAction(Sequence::create(EaseInOut::create(ScaleTo::create(0.2f, 1.2f), 3),EaseInOut::create(ScaleTo::create(0.2f, 1.f), 3),NULL ));
       CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elavator_select.mp3",false);
       
        
    }else{
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);   
    }
    
    
    /*
    CCCallFunc *finMoving = CCCallFunc::create(this,callfunc_selector(Elevator::finMoving));
    
    this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finMoving));
        this->runAction(ScaleTo::create(0.5f, 1.f));
        return;
    }*/
    ElevatorsScene::getInstance()->pintarAvisoSeleccionarElevator();
    
}



                    


void Elevator::setMoving(bool moving){
    this->mIsMoving=moving;
}



bool Elevator::isMoving(){
    return mIsMoving;
}


bool Elevator::isSelected(){
    return mSelected;
}


void Elevator::setPiso(int piso){
    mPiso=piso;
    char memory[200];
    sprintf(memory,"%i",piso);
    mLabelPiso->setString(memory);
    
}
int Elevator::getPiso(){
    return mPiso;
}


void Elevator::up(){
    CCLOG("up");
    stopAllActions();
    this->setColorBoton(Color3B(255.f,255.f,255.f));
    tempPiso=8;
    mScheduleAscensor=schedule_selector(Elevator::pisoUpIteration);
    this->schedule(mScheduleAscensor, 0.5f);
    mSelected=false;
    mContadorBackground->setColor(Color3B(69.f,131.f,67.f));
    mIsMoving=true;
    
}
void Elevator::down(){
    CCLOG("down");
    stopAllActions();
    this->setColorBoton(Color3B(255.f,255.f,255.f));
    tempPiso=13;
    mScheduleAscensor=schedule_selector(Elevator::pisoDownIteration);
    this->schedule(mScheduleAscensor, 0.5f);
     mSelected=false;
    mContadorBackground->setColor(Color3B(255.f,0.f,0.f));
    //mDownIndicador->setOpacity(255.f);
    mIsMoving=true;

}

void Elevator::pisoUpIteration(float dt){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevatormove.mp3",false);
 
    tempPiso--;
    this->mPiso++;
    setPiso(mPiso);
    if(tempPiso<=0){
        this->unschedule(mScheduleAscensor);
        mContadorBackground->setColor(Color3B(255.f,255.f,255.f));
        ElevatorsScene::getInstance()->movimientoFinish();
        mIsMoving=false;
    }
}

void Elevator::pisoDownIteration(float dt){
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevatormove.mp3",false);
    tempPiso--;
    this->mPiso--;
    setPiso(mPiso);
    if(tempPiso<=0){
        this->unschedule(mScheduleAscensor);
        mContadorBackground->setColor(Color3B(255.f,255.f,255.f));
        ElevatorsScene::getInstance()->movimientoFinish();
        mIsMoving=false;
    }
}




