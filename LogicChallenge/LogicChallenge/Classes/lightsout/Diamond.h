//
//  Diamond.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Diamond__
#define __logicmaster2__Diamond__

#include <iostream>
#include "Diamond.fwd.h"
#include "../widgets/Boton.h"





class Diamond: public Boton
{
    
    
    public:
    
        static Diamond* create(Node* parent,int x,int y, bool active);
        void initDiamond(Node* parent,int x,int y, bool active);
        virtual ~Diamond(void);
        static cocos2d::Scene* scene();
        void click(Ref* sender);
        bool getLuzActive();
        void swapLuz();
        static Diamond* create(int numero,int peso);
        void reset();
        void establecerEstadoInicial();
        int getX();
        int getY();
    private:
        int mX,mY;
        bool mLuzActive;
        bool mInitialLuzActive;
        Node* mParent;
        void timeout();
    
       };


#endif /* defined(__logicmaster2__Diamond__) */
