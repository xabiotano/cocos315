#include "LightsOutScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "Variables.h"
#include "../Levels.h"
#include "Diamond.h"


using namespace cocos2d;
using namespace CocosDenshion;


LightsOut* empty_instancia_lights;

LightsOut::~LightsOut(void)
{
    CC_SAFE_RELEASE(mLuces);
    CC_SAFE_RELEASE(mTableros);
    CC_SAFE_RELEASE(mSoluciones);
}

Scene* LightsOut::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    LightsOut *layer = LightsOut::create();
    // add layer as a child to scene
    scene->addChild(layer);
    // return the scene
    return scene;
}

bool LightsOut::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    empty_instancia_lights=this;
    mNombrePantalla="LightsOut";
    mTipoNivel=kNivelLogica;
    Point origin = Director::getInstance()->getVisibleOrigin();
    
   /* LayerColor* mFondo = LayerColor::create(Color4B(239,238,235,255.f), mVisibleSize.width, mVisibleSize.height);
     //position the sprite on the center of the screen
    mFondo->setPosition(Point(0,0));*/

    Sprite* mFondo = Sprite::create("lightsout/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(mVisibleSize.width/2 + origin.x, mVisibleSize.height/2 + origin.y));
    
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/mVisibleSize.width);

    NUMERO_CASILLAS=5;
    
    mTableros=CCArray::create();
    mTableros->retain();
    CCString* string1=CCString::create("0110010001010110111101001");/**/
    CCString* string2=CCString::create("0101000011110111100011100");/**/
    
    CCString* string3=CCString::create("1111111001101001011111110");/**/
    CCString* string4=CCString::create("0101111110001101110000011");/**/
    CCString* string5=CCString::create("0100010010101110100111011");/**/
    
    mTableros->addObject(string1);
    mTableros->addObject(string2);
    mTableros->addObject(string3);
    mTableros->addObject(string4);
    mTableros->addObject(string5);
    
    
    mSoluciones=Dictionary::create();
    mSoluciones->retain();
    mSoluciones->setObject(CCString::create("0110000001000011011000000"), 0);//7
    mSoluciones->setObject(CCString::create("1000001101111010000011010"), 1);//11
    mSoluciones->setObject(CCString::create("0100011100001001100101011"), 2);//10
    mSoluciones->setObject(CCString::create("0000010100101111111111010"), 3);//14
    mSoluciones->setObject(CCString::create("0100101000110000010000000"), 4);
    
    mLuces=Dictionary::create();
    mLuces->retain();
    
    //añadimos de nuevo el mapa
    
    srand(time(0));
    mNumeroTablero= rand() % mTableros->count();
    CCLOG("randomTablero %i",mNumeroTablero);
    
    std::string tablero=((CCString*)mTableros->getObjectAtIndex(mNumeroTablero))->getCString();
    
    
    std::stringstream out1;
    out1 << mNumeroTablero;
    std::string s1 = "tablero:"+ out1.str();
   /* CCLabelTTF* label=CCLabelTTF::create(s1.c_str(), "", 30);
    this->addChild(label);
    label->setPosition(Point(100,100));*/
    
    float porcentajeRestanteY=0.8f;
    float porcentajeRestanteX=0.5f;
    
    float widthCasilla=mVisibleSize.width*porcentajeRestanteX/NUMERO_CASILLAS;
    float heightCasilla=mVisibleSize.height *porcentajeRestanteY/NUMERO_CASILLAS;
    
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("lightsout/light.png");
    
    float scaleX=widthCasilla/texture->getContentSize().width;
    float scaleY=heightCasilla/texture->getContentSize().height;
    
    
    if(scaleX<scaleY){
        mScaleBase=scaleX;
    }else{
        mScaleBase=scaleY;
    }
    CCLOG("SCALE %f",mScaleBase);
    
    
    float originWidth=mVisibleSize.width *(1.f-porcentajeRestanteX)/2.f;
    originWidth+=widthCasilla/2;
    
    float originHeight=mVisibleSize.height*(1.f-porcentajeRestanteY)/2;
    originHeight+=heightCasilla;
    int index=0;
    for(int i=0;i<NUMERO_CASILLAS;i++){
        for(int j=0;j<NUMERO_CASILLAS;j++){
            
            std::string activoStr=tablero.substr(index,1);
            bool activo=StringToBool(activoStr);
            
            Diamond* luz= Diamond::create(this, i, j, activo);
            luz->setSound("sounds/elixircomun/select.mp3");
            std::string key=getStringFromInt(i,j);
            
            CCLOG("%s",key.c_str());
            mLuces->setObject(luz,key.c_str());
            
            luz->setPosition(Point(originWidth + (j*widthCasilla),originHeight + (heightCasilla*i)));
            this->addChild(luz);
            luz->setScale(mScaleBase);
            index++;
        }
    }

    mNumeroMovimientos=15;
    
    Sprite* counter=Sprite::create("lightsout/counter.png");
    this->addChild(counter);
    counter->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.5f));
    
   
    /*mLabelCounter=CCLabelStroke::create(counter,"15", CCGetFont(),70,counter->getContentSize(),kCCTextAlignmentCenter,kCCVerticalTextAlignmentCenter);
    mLabelCounter->setPosition(Point(counter->getContentSize().width/2, counter->getContentSize().height/2));
    counter->addChild(mLabelCounter,2);
    mLabelCounter->setString("15", Color3B(0,0,0));*/
    mLabelCounter=Label::createWithTTF("15", CCGetFont(), 70);
    mLabelCounter->setWidth(counter->getContentSize().width);
    mLabelCounter->setHeight(counter->getContentSize().height);
    mLabelCounter->setHorizontalAlignment(TextHAlignment::CENTER);
    mLabelCounter->setVerticalAlignment(TextVAlignment::CENTER);
    mLabelCounter->enableOutline(Color4B(0,0,0,255),2);
    mLabelCounter->setPosition(Point(counter->getContentSize().width/2, counter->getContentSize().height/2));
    counter->addChild(mLabelCounter,2);
    
    updateCounter();
    
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_LIGHTSOUT","HELP_LIGHTSOUT"));
    mRetryVisibleDuringGame=true;
    

    return true;
}

void LightsOut::updateCounter(){
    char temp[10];
    sprintf(temp,"%i",mNumeroMovimientos);
    mLabelCounter->setString(temp);
}

bool LightsOut::StringToBool(std::string cadena){
    //activo?
  
    int numero;
    std::istringstream ss(cadena);
    ss >> numero;
    
   
    return (numero==1);
}

std::string LightsOut::getStringFromInt(int uno,int dos){
    

    std::string s1;
    std::stringstream out1;
    out1 << uno;
    s1 = out1.str();
    
    std::string s2;
    std::stringstream out2;
    out2 << dos;
    s2 = out2.str();

    std::string devolver=s1+ s2;
    return devolver;
}


void LightsOut::clickDiamond(int x,int y){
    
    
    
    Diamond* diamanate= (Diamond*) mLuces->objectForKey(getStringFromInt(x,y));
    
    if(mSituacionHint){
        if(!mSerieSolucion.empty()){
        
            CCLOG("Diamante clickado %i %i , Sugerido: %i %i", diamanate->getX(), diamanate->getY(), mDiamanteParpadeando->getX(), mDiamanteParpadeando->getY());
            if(mDiamanteParpadeando->getX()!=diamanate->getX() || mDiamanteParpadeando->getY()!= diamanate->getY()){
                CCLOG("Has clickado otro que no era el del hint");
                return;
            }
        }else{
             CCLOG("Pila vacia");
        }
        if(mDiamanteParpadeando!=NULL){
            mDiamanteParpadeando->stopAllActions();
            mDiamanteParpadeando->setScale(mScaleBase);
            
        }
    }
    mNumeroMovimientos--;
    if(mNumeroMovimientos<0){
        updateCounter();
        mGameover=true;
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("LIGHTSOUT_NO_MORE_MOVES","You don't have more moves");
        BaseScene::checkFinish(NULL);
        return;
    }
    updateCounter();
   
    
    diamanate->swapLuz();
    
    if(x+1<NUMERO_CASILLAS){
        Diamond* diamanate= (Diamond*) mLuces->objectForKey(getStringFromInt(x+1,y));
        diamanate->swapLuz();
    }
    if(x-1>=0){
        Diamond* diamanate= (Diamond*) mLuces->objectForKey(getStringFromInt(x-1,y));
          diamanate->swapLuz();
    }
    if(y+1<NUMERO_CASILLAS){
        Diamond* diamanate= (Diamond*) mLuces->objectForKey(getStringFromInt(x,y+1));
          diamanate->swapLuz();
    }
    if(y-1>=0){
        Diamond* diamanate= (Diamond*) mLuces->objectForKey(getStringFromInt(x,y-1));
          diamanate->swapLuz();
    }
    
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(LightsOut::checkFinish,this));
    this->runAction(Sequence::create(DelayTime::create(1.f), funcion,NULL));

    //this->scheduleOnce(schedule_selector(LightsOut::checkFinish), 1.f);
    
    //si estamos en el proceso de hint
    if(mSituacionHint){
        HintStart(NULL);
    }
    
    
}


LightsOut* LightsOut::getInstance(){
    return empty_instancia_lights;
};

void LightsOut::checkFinish(Ref* sender){
    
    bool solucionado=true;
    
    for(int i=0;i<NUMERO_CASILLAS;i++){
        for(int j=0;j<NUMERO_CASILLAS;j++){
          
            
            std::string key=getStringFromInt(i,j);
            
            
            Diamond* diamante=(Diamond*)mLuces->objectForKey(key.c_str());
            
            if(!diamante->getLuzActive()){
                solucionado=false;
            }
        }
    }
    if(solucionado) {
        LayerHintWithButton::ocultar(NULL);//por si estuviese mostrada
        mSuccess=true;
        mTextoSuccess=std::string(LanguageManager::getInstance()->getStringForKey("LIGHTSOUT_SUCESS","Yes!!you got it!"));
        if(mNumeroMovimientos>3){
            mStars=3;
        }else if(mNumeroMovimientos<3 && mNumeroMovimientos>1 ){
            mStars=3;
        }else{
            mStars=3;
        }
        BaseScene::checkFinish(NULL);
    }
    
};

void LightsOut::HintStart(Ref* sender){
    if(mSerieSolucion.empty()){
        CCLOG("WTF!! no deberia estar vacia");
        return;
    }
    Diamond* sugerido=(Diamond*) mSerieSolucion.top();
    mDiamanteParpadeando=sugerido;
    sugerido->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.2f, 1.1f),ScaleTo::create(0.2f, mScaleBase) )));
    mSerieSolucion.pop();
   
   
}

void LightsOut::HintStop(Ref* sender)
{
    mSituacionHint=false;
    BaseScene::HintStop(sender);
    for(int i=0;i<NUMERO_CASILLAS;i++){
        for(int j=0;j<NUMERO_CASILLAS;j++){
            std::string key=getStringFromInt(i,j);
            Diamond* diamante=(Diamond*)mLuces->objectForKey(key.c_str());
            diamante->stopAllActions();
            diamante->setScale(mScaleBase);
        }
    }
}



void LightsOut::resetNivel(){
    //resetamos la pantalla
    for(int i=0;i<NUMERO_CASILLAS;i++){
        for(int j=0;j<NUMERO_CASILLAS;j++){
            std::string key=getStringFromInt(i,j);
            Diamond* diamante=(Diamond*)mLuces->objectForKey(key.c_str());
            diamante->reset();
        }
    }
}


void LightsOut::Retry(Ref* sender)
{
    Scene* scene=LightsOut::scene();
    Director *pDirector = Director::getInstance();
   pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}

void LightsOut::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}



void LightsOut::HintCallback(bool accepted){
    if(!accepted){
        return;
    }
    if(mSituacionHint){
        return;
    }
    BaseScene::HintCallback(accepted);
    mNumeroMovimientos=15;
    updateCounter();
    LayerHintOk::mostrar(false);
    mSituacionHint=true;
    resetNivel();
    Sprite* sprite=Sprite::create("lightsout/light.png");
    sprite->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleBy::create(0.5f,0.9f), ScaleBy::create(0.5f,1.1f))));
    LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("LIGHTSOUT_TOUCH_BLINKING","Touch the blinking diamonds"))->setSprite(sprite);
    while ( !mSerieSolucion.empty() )
    {
            mSerieSolucion.pop();
    }
    CCString*  solucionObj=(CCString*) mSoluciones->objectForKey(mNumeroTablero);
    // CCLOG("Solucion");
    // CCLOG(solucionObj->getCString());
    std::string solucion=std::string(solucionObj->getCString());
    int index=0;
    for(int i=0; i<NUMERO_CASILLAS;i++){
        for(int j=0;j<NUMERO_CASILLAS;j++){
            std::string key=getStringFromInt(i,j);
            Diamond* diamante=(Diamond*)mLuces->objectForKey(key);
            std::string subs=solucion.substr (index,1);
            CCLOG("%s",subs.c_str());
            if(subs==std::string("1")){
                //  CCLOG("index %i", index);
                mSerieSolucion.push(diamante);
            }
            index++;
        }
    }
    //CCLOG("tamano pila %d", mSerieSolucion.size());
    
    this->scheduleOnce(schedule_selector(LightsOut::HintStartTemp),1.f);
}

void LightsOut::HintStartTemp(float dt){
    HintStart(NULL);
}

void LightsOut::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}


