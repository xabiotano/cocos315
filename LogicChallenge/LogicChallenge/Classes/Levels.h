#ifndef __Levels_SCENE_H__
#define __Levels_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "BaseScene.h"
#include "widgets/CCSlidingLayer.h"
#include "LevelsBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Levels : public LevelsBase
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* createScene();
    
    static cocos2d::Scene* scene();
   

    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(Levels);
     static bool GoToScene(int NUM_SCENE);
    //void onKeyBackClicked();
    void onEnterTransitionDidFinish() override;
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) override;
    
    void onExitTransitionDidStart() override;
private:
    SEL_SCHEDULE mMostrarPremio;
    template <typename T> std::string tostr(const T& t);
    void pintarNiveles();
    Sprite* mFondo;
    void onClickNivel(Ref* sender);
    Size mVisibleSize;
    CCSlidingLayer* mSlidingLayer;
    Boton* mBack;
    void Back(Ref* sender) override;
    Label* mLabelNumeroItems;
    void mostrarPremio(float dt);


};

#endif // __Levels_SCENE_H__
