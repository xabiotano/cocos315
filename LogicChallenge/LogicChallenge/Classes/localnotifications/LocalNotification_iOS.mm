#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "LocalNotification.h"
#import <UserNotifications/UNNotificationContent.h>


using namespace std;

void LocalNotification::show(std::string message, int interval, int tag)
{
  UILocalNotification *notification = [[UILocalNotification alloc] init];
  
  notification.fireDate = [[NSDate date] dateByAddingTimeInterval:interval];
  notification.timeZone = [NSTimeZone defaultTimeZone];
    NSString* prueba=@(message.c_str());
    NSLog(@"%@", prueba);
    
    
  notification.alertBody = prueba;
    
   
   
    
    
   
    NSString *emoji;
    //anado el tigre
    uint32_t bytes = htonl(0x01F42F); // Convert the character to a known ordering
    emoji=  [[NSString alloc] initWithBytes:&bytes length:sizeof(uint32_t) encoding:NSUTF32StringEncoding];
    notification.alertBody=  [NSString stringWithFormat:@"%@ %@", emoji, notification.alertBody];
    
    //anado la linterna
    if(tag==NOTIFICATION_GIFT){
        uint32_t bytes = htonl(0x01F4A1); // Convert the character to a known ordering
        emoji=  [[NSString alloc] initWithBytes:&bytes length:sizeof(uint32_t) encoding:NSUTF32StringEncoding];
        notification.alertBody=  [NSString stringWithFormat:@"%@ %@",notification.alertBody, emoji];

    }else{
        //anado las huellas
        uint32_t bytes = htonl(0x01F43E); // Convert the character to a known ordering
        emoji=  [[NSString alloc] initWithBytes:&bytes length:sizeof(uint32_t) encoding:NSUTF32StringEncoding];
        notification.alertBody=  [NSString stringWithFormat:@"%@ %@", notification.alertBody,emoji];

    }

    
    NSString *sentence = notification.alertBody;
    NSMutableString *buffer = [sentence mutableCopy];
    CFMutableStringRef bufferRef = (__bridge CFMutableStringRef)buffer;
    CFStringTransform(bufferRef, NULL, kCFStringTransformToLatin, false);
    CFStringTransform(bufferRef, NULL, kCFStringTransformStripDiacritics, false);
    NSArray *arr = [buffer componentsSeparatedByString:@" "];
    NSLog(@"%@", arr);
    
    
  notification.alertAction = @"Open";
  notification.soundName = UILocalNotificationDefaultSoundName;
  
  NSNumber* tag1 = [NSNumber numberWithInteger:tag];
  NSDictionary *infoDict = [NSDictionary dictionaryWithObject:tag1 forKey:@"ID"];
  notification.userInfo = infoDict;


   /*
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = [NSString stringWithCString:message.c_str()];
    content.body = [NSString stringWithCString:message.c_str()];
    content.sound = [UNNotificationSound defaultSound];
    
    */
    
  [[UIApplication sharedApplication] scheduleLocalNotification:notification];
  
  [notification release];
}

NSString *MyStringFromUnicodeCharacter(uint32_t character) {
    uint32_t bytes = htonl(character); // Convert the character to a known ordering
    return [[NSString alloc] initWithBytes:&bytes length:sizeof(uint32_t) encoding:NSUTF32StringEncoding];
}


void LocalNotification::cancel(int tag)
{
  for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
    if([[notification.userInfo objectForKey:@"ID"] integerValue] == tag) {
      [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
  }
}












#endif // CC_TARGET_PLATFORM == CC_PLATFORM_IOS
