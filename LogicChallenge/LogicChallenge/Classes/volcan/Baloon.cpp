//
//  Baloon.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Baloon.h"
#include "SimpleAudioEngine.h"
#include "Bear.h"

Baloon::Baloon(Point izquierda,Point derecha)
{
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=izquierda;
    mPosicionDerecha=derecha;
    mState=BaloonState(kBaloonIzquierda);
    bearsMontados.empty();
    numero_osos=0;
   
    
}
Baloon::~Baloon(void){}


Baloon* Baloon::create(Node* parent,Point izquierda,Point derecha){
    Baloon *devolver= new Baloon(izquierda,derecha);
    devolver->autorelease();
    devolver->setPosition(devolver->mPosicionIzquierda);
    if(devolver->bearsMontados.size()>0){
        devolver->bearsMontados.empty();
    }
    devolver->initBoton(devolver, menu_selector(Baloon::move), std::string("volcan/baloon.png"));
    devolver->mSeat1=Point(devolver->getContentSize().width*0.25f,devolver->getContentSize().height*0.15f);
    devolver->mSeat2=Point(devolver->getContentSize().width*0.5f,devolver->getContentSize().height*0.15f);
    devolver->dibujarTexto();
    return devolver;
}


void Baloon::dibujarTexto(){
    Label* label= Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("VOLCAN_TOUCH"), CCGetFont(), 20);
    label->setOverflow(Label::Overflow::RESIZE_HEIGHT);
    label->setWidth(this->getContentSize().width*0.5f);
    //label->setHeight(this->getContentSize().height*0.5f);
    label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    label->enableOutline(Color4B(0,0,0,255),2);
    label->setPosition(Point(this->getContentSize().width*0.5f, this->getContentSize().height*0.5f));
    this->addChild(label,2);
   
    
   
}
void Baloon::move(Ref* sender){
    if(mIsMoving){
        CCLOG("is moving!");
        return;
    }
    if(getPassengers()==0)
    {
        Sprite* balloon=Sprite::create("volcan/baloon.png");
        balloon->setScale(0.5f);
        LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("VOLCAN_BALLOON_EMPTY"))->setSprite(balloon);
        return;
    }
    Point destino;
    if(estaEnIzquierda()){
        destino=this->mPosicionDerecha;
    }else{
         destino=this->mPosicionIzquierda;
    }
    CallFunc *finMove = CallFunc::create([this]() { this->finMove(); });
    this->runAction(Sequence::createWithTwoActions(MoveTo::create(1.f,destino),finMove));
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/volcan/baloon.mp3",false);
    mEstaIzquierda=!mEstaIzquierda;
    mIsMoving=true;
}

void Baloon::finMove(){
   if(!mEstaIzquierda){
        mState=BaloonState(kBaloonDerecha);
    }else{
         mState=BaloonState(kBaloonIzquierda);
    }
    mIsMoving=false;
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(this->mSoundEffect);
    if(bearsMontados[0]!=NULL){
        bearsMontados[0]->mEstaIzquierda=mEstaIzquierda;
        bearsMontados[0]->click(NULL);
    }
    if(bearsMontados[1]!=NULL){
        bearsMontados[1]->mEstaIzquierda=mEstaIzquierda;
         bearsMontados[1]->click(NULL);
    }
    
    
    
    VolcanScene::getInstance()->checkFinish(NULL);
   
}
                    
bool Baloon::estaEnIzquierda(){
    return mEstaIzquierda;
}


void Baloon::mountBearLogic(Bear* bear){
    
    CCLOG("numero Bears %i",numero_osos);
    if(numero_osos==0){
        bearsMontados[numero_osos]=bear;
        
    }else{
        if(bearsMontados[0]==NULL){
            bearsMontados[0]=bear;
        }else{
            bearsMontados[1]=bear;
        }
    }
    numero_osos++;
}

bool Baloon::mountBear(Bear* bear,Point position){
    bear->removeFromParent();
    this->addChild(bear,-1);
    this->setTouchPriority(-8);
    bear->setTouchPriority(-10);
    bear->setPosition(convertToNodeSpace(position));
    return true;
}

bool Baloon::unMountBear(Bear* bear){
    
    if(bear){
        int Bearpos=(int)bear->getPositionX();
        int posBaloon=(int)mSeat1.x;
        if(Bearpos==posBaloon){
            //estaba en la pos1
            CCLOG("bearsMontados[0]=NULL");
            bearsMontados[0]=NULL;
        }else{
            CCLOG("bearsMontados[1]=NULL");
            bearsMontados[1]=NULL;
        }
        numero_osos--;
        //lo añadimos a la esecena
        Point posactual=convertToWorldSpace(bear->getPosition());
        bear->removeFromParent();
        this->getParent()->addChild(bear,bear->mZIndex);
        bear->setPosition(posactual);

        return true;
    }
    return false;
    
}



int Baloon::getPassengers(){
    return numero_osos;
}

Point Baloon::getSeat(){
    Point devolver;
    Point p1=mSeat1;
    Point p2=mSeat2;
    CCLOG("numero Osos %i",numero_osos);
    if(numero_osos==0){
        CCLOG("pos1");
        devolver =p1;
    }else{
        if(bearsMontados[0]==NULL){
            CCLOG("pos1");
            devolver= p1;
        }else{
            CCLOG("pos2");
            devolver= p2;
        }
    }
    devolver=convertToWorldSpace(devolver);
    return devolver;
}

Point Baloon::getSeatBaloon(){
    Point p=getSeat();
    p=convertToNodeSpace(p);
    return p;
}




bool Baloon::isMoving(){
    return mIsMoving;
}


bool Baloon::containsTouchLocation(Touch* touch)
{
    Size s = getTexture()->getContentSize();
    return  Rect(-s.width / 2, -s.height *0.2f, s.width, s.height).containsPoint(convertTouchToNodeSpaceAR(touch));
}






