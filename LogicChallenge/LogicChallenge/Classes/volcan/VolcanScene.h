#ifndef __VolcanScene_SCENE_H__
#define __VolcanScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Baloon.fwd.h"
#include "Bear.fwd.h"

USING_NS_CC;


class VolcanScene : public BaseScene
{
public:
    void Retry(Ref* sender) override;
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;
    virtual ~VolcanScene(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(VolcanScene);
    static VolcanScene* getInstance();
    virtual void checkFinish(Ref* sender) override;
    virtual void Hint(Ref* sender) override;
    virtual void HintCallback(bool accepted) override;
    virtual void HintStart(float dt);
    virtual void TutorialStart(Ref* sender) override {};
    Baloon* getBaloon();
    
    
    const static int Z_INDEX_OSOS=10;
    const static int Z_INDEX_BUBUS=11;
    const static int Z_INDEX_GLOBO=12;

    void onEnterTransitionDidFinish() override;
  /*  virtual bool TouchBegan(Touch* touch, Event* event);*/
    
private:
    Size mVisibleSize ;
    Sprite* mFondo;
    Label* mCounter;
    Baloon* mBaloon;
    Bear* mBear1,*mBear2,*mBear3;
    Bear* mBaby1,*mBaby2,*mBaby3;
    void iniciarEscena();
  
    
};


#endif // __VolcanScene_SCENE_H__
