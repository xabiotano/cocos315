#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "InterfaceJNI.h"
#include "Variables.h"
#include "../AppDelegate.h"
#include "../AdHelper.h"

#include "../helpers/LogicSQLHelper.h"

    #include "platform/android/jni/JniHelper.h"
    #include <jni.h>
    #include <android/log.h>






using namespace cocos2d;

//static JavaVM *gJavaVM;
//static jmethodID mid;
//static jclass mClass;

void InterfaceJNI::shareAmigos()
{
    Size size=Director::getInstance()->getVisibleSize();
    
    RenderTexture* texture = RenderTexture::create((int)size.width, (int)size.height);
    texture->setPosition(Point(size.width/2, size.height/2));
    texture->begin();
    Director::getInstance()->getRunningScene()->visit();
    texture->end();
    
    
    std::string path = FileUtils::getInstance()->getWritablePath() + "screenshot.png";
    texture->saveToFile(path.c_str());
    
    CCLOG("path screenshot %s",path.c_str());
    
    
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    
	JavaVM* jvm = JniHelper::getJavaVM();
	int status;
	JNIEnv *env;
	jmethodID mid;

	bool isAttached = false;

	CCLog("Static shareAmigos");

	// Get Status
	status = jvm->GetEnv((void **) &env, JNI_VERSION_1_6);



	if(status < 0)
	{
		//LOGE("callback_handler: failed to get JNI environment, " // "assuming native thread");
		status = jvm->AttachCurrentThread(&env, NULL);
		CCLog("Status 2: %d", status);
		if(status < 0)
		{
			// LOGE("callback_handler: failed to attach " // "current thread");
			return;
		}
		isAttached = true;
		CCLog("Status isAttached: %d", isAttached);
	}
	//-----------------------------------------------------------

	CCLog("Status: %d", status);

	jclass mClass = env->FindClass("com/kangaroo/logicchallenge/LogicChallenge");

	CCLog("jClass Located?");

	mid = env->GetStaticMethodID(mClass, "shareAmigos", "(Ljava/lang/String;)V");

	CCLog("mID: %d", mid);

    if (mid!=0)
    {
        jstring StringArg1 = env->NewStringUTF(path.c_str());
        env->CallStaticVoidMethod(mClass, mid,StringArg1);
    }
			
	CCLog("Finish");
	if(isAttached)
		jvm->DetachCurrentThread();
    #endif
	return;
}







void InterfaceJNI::showIntersitial()
{
    CCLOG("AdHelper InterfaceJNI::showIntersitial");
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    JavaVM* jvm = JniHelper::getJavaVM();
    int status;
    JNIEnv *env;
    jmethodID mid;
    
    bool isAttached = false;
    
    
    
    // Get Status
    status = jvm->GetEnv((void **) &env, JNI_VERSION_1_6);
    
    
    
    if(status < 0)
    {
        //LOGE("callback_handler: failed to get JNI environment, " // "assuming native thread");
        status = jvm->AttachCurrentThread(&env, NULL);
        CCLOG("Status 2: %d", status);
        if(status < 0)
        {
            // LOGE("callback_handler: failed to attach " // "current thread");
            return;
        }
        isAttached = true;
        CCLOG("Status isAttached: %d", isAttached);
    }
    //-----------------------------------------------------------
    
    CCLOG("Status: %d", status);
    
    jclass mClass = env->FindClass("com/kangaroo/tools/AdsActvity");
    
    CCLOG("jClass Located?");
    
    mid = env->GetStaticMethodID(mClass, "showIntersitial", "()V");
    
    CCLOG("mID: %d", mid);
    
    if (mid!=0)
        env->CallStaticVoidMethod(mClass, mid);
    //-----------------------------------------------------------
    CCLOG("Finish");
    if(isAttached)
        jvm->DetachCurrentThread();
    #endif
    return;
}
    




void InterfaceJNI::showBanner()
{
    CCLog("AdHelper InterfaceJNI::showBanner");
}
void InterfaceJNI::cacheIntersitial()
{
    CCLog("AdHelper InterfaceJNI::cacheIntersitial");

    JavaVM* jvm = JniHelper::getJavaVM();
    int status;
    JNIEnv *env;
    jmethodID mid;
    
    bool isAttached = false;
    
    
    
    // Get Status
    status = jvm->GetEnv((void **) &env, JNI_VERSION_1_6);
    
    
    
    if(status < 0)
    {
        //LOGE("callback_handler: failed to get JNI environment, " // "assuming native thread");
        status = jvm->AttachCurrentThread(&env, NULL);
        CCLOG("Status 2: %d", status);
        if(status < 0)
        {
            // LOGE("callback_handler: failed to attach " // "current thread");
            return;
        }
        isAttached = true;
        CCLOG("Status isAttached: %d", isAttached);
    }
    //-----------------------------------------------------------
    
    CCLOG("Status: %d", status);
    
    jclass mClass = env->FindClass("com/kangaroo/tools/AdsActvity");
    
    CCLOG("jClass Located?");
    
    mid = env->GetStaticMethodID(mClass, "cacheIntersitial", "()V");
    
    CCLOG("mID: %d", mid);
    
    if (mid!=0)
        env->CallStaticVoidMethod(mClass, mid);
    //-----------------------------------------------------------
    CCLOG("Finish");
    if(isAttached)
        jvm->DetachCurrentThread();

    return;
    
}
void InterfaceJNI::hideBanner()
{
     CCLOG("AdHelper InterfaceJNI::hideBanner");
}


#ifdef __cplusplus
extern "C"
{
#endif
  
    
    JNIEXPORT void JNICALL Java_com_kangaroo_tools_AdsActvity_setIntersitialCached(JNIEnv *env, jobject thiz, jboolean cacheado);
    
    JNIEXPORT void JNICALL Java_com_kangaroo_tools_AdsActvity_interstitialDidAppear(JNIEnv *env, jobject thiz, jstring device_token);
    
    JNIEXPORT jboolean JNICALL Java_com_kangaroo_tools_AdsActvity_getRemoveAdsPurchased(JNIEnv *env, jobject thiz);
    
    
     JNIEXPORT void JNICALL Java_com_kangaroo_tools_notifications_NotificationHelper_showGiftNotification(JNIEnv *env, jobject thiz, jboolean mostrado);
    
 
    
    

#ifdef __cplusplus
}
#endif



JNIEXPORT void JNICALL Java_com_kangaroo_tools_AdsActvity_setIntersitialCached(JNIEnv *env, jobject thiz, jboolean cacheado){
   
    CCLOG("AdHelper InterfaceJNI::AdHelper cacheado?:%i",cacheado);
    AdHelper::getInstance()->setIntersitialCached(cacheado);
    
}

JNIEXPORT void JNICALL Java_com_kangaroo_tools_AdsActvity_interstitialDidAppear(JNIEnv *env, jobject thiz, jstring device_token)
{
     CCLOG("AdHelper InterfaceJNI::AdHelper->interstitialDidAppear");
    AdHelper::getInstance()->interstitialDidAppear();
}


JNIEXPORT jboolean JNICALL Java_com_kangaroo_tools_AdsActvity_getRemoveAdsPurchased(JNIEnv *env, jobject thiz)
{
    CCLOG("AdHelper InterfaceJNI::LogicSQLHelper::getInstance()->estaSinPublicidadComprado()");
    return LogicSQLHelper::getInstance().estaSinPublicidadComprado();
}

JNIEXPORT void JNICALL Java_com_kangaroo_tools_notifications_NotificationHelper_showGiftNotification(JNIEnv *env, jobject thiz, jboolean mostrado)
{
    CCLOG("InterfaceJNI AppDelegate::showGifNotification");
    AppDelegate::showGifNotification();
}




#endif









