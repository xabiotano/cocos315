#ifndef __GoatLettuceWolf_SCENE_H__
#define __GoatLettuceWolf_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "Boat.h"
#include "../widgets/Boton.h"
#include "Animal.h"
USING_NS_CC;

class GoatLettuceWolf : public BaseScene
{
public:
    

    virtual void Retry(Ref* sender) override;
    virtual void Hint(Ref* sender) override;
    virtual void HintCallback(bool accepted) override;
    virtual void HintStart(Ref* sender) override;
    void HintStop(Ref* sender) override;
     virtual void TutorialStart(){};
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init() override;

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(GoatLettuceWolf);
    static GoatLettuceWolf* getInstance();
    Boat* mBoat;
    virtual void checkFinish();
    Boat* getBoat();
    void swapBoatButton();
    void BoatClick(Ref* sender);
    Animal* mSheep,*mWolf,*mLettuce;
    void clickBoat(Ref* sender);
    void onEnterTransitionDidFinish() override;   
    void TutorialStart(Ref* sender) override{};

    
 
private:
  //  Boton* mBoatButton;
    
    void crearPersonajes();
    
    
    
   
};

#endif // __GoatLettuceWolf_SCENE_H__
