#include "GoatLettuceWolfScene.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

GoatLettuceWolf* instancia_goat;

GoatLettuceWolf* GoatLettuceWolf::getInstance(){
    return instancia_goat;
}






Scene* GoatLettuceWolf::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    GoatLettuceWolf *layer = GoatLettuceWolf::create();

    // add layer as a child to scene
    scene->addChild(layer);

    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GoatLettuceWolf::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_goat=this;
    mNombrePantalla="GoatLettuceWolf";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("lobocabralechuga/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
  
    
    
    Sprite* islands=Sprite::create("lobocabralechuga/islands.png");
    islands->setPosition(Point(mVisibleSize.width/2,mVisibleSize.height*0.3f));
    this->addChild(islands,3);
    
    crearPersonajes();
   
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_GOAT", "HELP_GOAT"));
   // 
    return true;
}



void GoatLettuceWolf::crearPersonajes(){
    
    if(mBoat!=NULL){
        mBoat->removeFromParent();
    }
    if(mSheep!=NULL){
        mSheep->removeFromParent();
    }
    if(mWolf!=NULL){
        mWolf->removeFromParent();
    }
    if(mLettuce!=NULL){
        mLettuce->removeFromParent();
    }
    
    mBoat= Boat::create();
    this->addChild(mBoat,2);
    
    mSheep= Animal::create(this, std::string("sheep"), Point((65.f/960)*mVisibleSize.width,(300.f/640)*mVisibleSize.height),  Point((895.f/960)*mVisibleSize.width,(300.f/640)*mVisibleSize.height),"lobocabralechuga/sheep.png");
    this->addChild(mSheep,3);
    
    mWolf=Animal::create(this,std::string("wolf"),Point((70.f/960)*mVisibleSize.width,(170.f/640)*mVisibleSize.height), Point((890.f/960)*mVisibleSize.width,(170.f/640)*mVisibleSize.height),"lobocabralechuga/wolf.png");
    this->addChild(mWolf,10);
    
    mLettuce=Animal::create(this,std::string("lettuce"),Point((245.f/960)*mVisibleSize.width,(180.f/640)*mVisibleSize.height), Point((715.f/960)*mVisibleSize.width,(180.f/640)*mVisibleSize.height),"lobocabralechuga/lettuce.png");
    this->addChild(mLettuce,6);
}

void GoatLettuceWolf::clickBoat(Ref* sender){
    mBoat->move();
}

void GoatLettuceWolf::BoatClick(Ref* sender){
    CCLOG("CLICK");
  //  mBoatButton->runAction(Sequence::createWithTwoActions(CCEaseBounce::create(ScaleTo::create(0.5f, 1.1f)), ScaleTo::create(0.5f, 0.9f)));
    
}

void GoatLettuceWolf::swapBoatButton(){
   // this->removeChild(mBoatButton,true);
    if(mBoat->estaEnIzquierda()){
      //  mBoatButton=Boton::createBoton(this, menu_selector(GoatLettuceWolf::clickBoat), "lobocabralechuga/boat_d.png");

    }else{
     //   mBoatButton=Boton::createBoton(this, menu_selector(GoatLettuceWolf::clickBoat), "lobocabralechuga/boat_i.png");
    }
   // mBoatButton->setPosition(Point(mSize.width/2,mSize.height*0.9f));
   // this->addChild(mBoatButton);

}




void GoatLettuceWolf::checkFinish(){
    if(mWolf->mEstado==AnimalState(kIslaDerecha) &&
       mSheep->mEstado==AnimalState(kIslaDerecha) &&
       mLettuce->mEstado==AnimalState(kIslaDerecha)){
        mStars=3;
        mSuccess=true;
    }
    else{
        if(mWolf->mEstado==AnimalState(kIslaDerecha) &&
           mSheep->mEstado==AnimalState(kIslaDerecha)&&
           mBoat->estaEnIzquierda()){
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_W_SHEEP");
            mGameover=true;
        }
        else if(mWolf->mEstado==AnimalState(kIslaIzquierda) &&
           mSheep->mEstado==AnimalState(kIslaIzquierda)&&
           !mBoat->estaEnIzquierda()){
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_W_SHEEP");
            mGameover=true;
        }
        else if(mWolf->mEstado==AnimalState(kIslaDerecha) &&
                mSheep->mEstado==AnimalState(kIslaDerecha)&&
                mBoat->estaEnIzquierda()){
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_W_SHEEP");
            mGameover=true;
        }
        else if(mLettuce->mEstado==AnimalState(kIslaIzquierda) &&
                mSheep->mEstado==AnimalState(kIslaIzquierda)&&
                !mBoat->estaEnIzquierda()){
            mTextoGameOver= LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_SH_LETTUCE");
            mGameover=true;
        }
        else if(mLettuce->mEstado==AnimalState(kIslaIzquierda) &&
                mSheep->mEstado==AnimalState(kIslaIzquierda)&&
                !mBoat->estaEnIzquierda()){
            mTextoGameOver=LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_SH_LETTUCE");
            mGameover=true;
        }
        else if(mLettuce->mEstado==AnimalState(kIslaDerecha) &&
                mSheep->mEstado==AnimalState(kIslaDerecha)&&
                mBoat->estaEnIzquierda()){
            mTextoGameOver= LanguageManager::getInstance()->getStringForKey("GOAT_GAMEOVER_SH_LETTUCE");
            mGameover=true;
        }
    }
   

    BaseScene::checkFinish(NULL);
}





Boat* GoatLettuceWolf::getBoat(){
    return mBoat;
}

void GoatLettuceWolf::Retry(Ref* sender){
    Scene* scene=GoatLettuceWolf::scene();
    Director *pDirector = Director::getInstance();
   pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}



void GoatLettuceWolf::HintStart(Ref* sender){
    CCLOG("%i",mHintStep);
    switch(mHintStep){
        case 1:
            mSheep->click(NULL);
            break;
        case 2:
            clickBoat(NULL);
            break;
        case 3:
            clickBoat(NULL);
            break;
        case 4:
            mLettuce->click(NULL);
            break;
        case 5:
            clickBoat(NULL);
            break;
        case 6:
            mSheep->click(NULL);
            break;
        case 7:
            clickBoat(NULL);
            break;
        case 8:
            mWolf->click(NULL);
             break;
        case 9:
           clickBoat(NULL);
             break;
        case 10:
            clickBoat(NULL);
             break;
        case 11:
            mSheep->click(NULL);
             break;
        case 12:
            clickBoat(NULL);
             break;
    

    }
   
    if(mHintStep<=12){
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(GoatLettuceWolf::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
    }
    mHintStep++;
}

void GoatLettuceWolf::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void GoatLettuceWolf::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        CCLOG("MOSTRAR");
        crearPersonajes();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(GoatLettuceWolf::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
    }else{
        CCLOG("CANCELADO");
    }
}

void GoatLettuceWolf::HintStop(Ref* sender)
{
    mHintStep=20;
    BaseScene::HintStop(sender);
}

void GoatLettuceWolf::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/cabralechuga/beach.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}
