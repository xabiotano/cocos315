//
//  Animal.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Animal__
#define __logicmaster2__Animal__

#include <iostream>
#include "../widgets/Boton.h"
#include "Animal.fwd.h"
#include "GoatLettuceWolfScene.h"


typedef enum animalState
{
    kIslaIzquierda,
    kMontado,
    kIslaDerecha
    
} AnimalState;

class Animal: public Boton
{
    
    
    public:
    
        Animal(Point mPointIzq,Point mPointDch,std::string name);
        virtual ~Animal(void);
        // there's no 'id' in cpp, so we recommend to return the class instance pointer
        static cocos2d::Scene* scene();
        // preprocessor macro for "static create()" constructor ( node() deprecated )
   
        static Animal* create(Node* parent,std::string imagen,Point mPointIzq,Point mPointDch,std::string name);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        std::string mName;
        std::string getName();
        AnimalState mEstado;
protected:
     

    private:
        bool mEstaIzquierda;
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMove();
        void finUnmount();
        bool mIsMoving;
        unsigned int mSoundEffect;
        ParticleSystemQuad* mParticle;
    
};


#endif /* defined(__logicmaster2__Animal__) */
