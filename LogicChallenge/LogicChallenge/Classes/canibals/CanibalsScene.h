#ifndef __CanibalsScene_SCENE_H__
#define __CanibalsScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "Human.fwd.h"
#include "Canoe.fwd.h"
#include "HumanTypes.h"


USING_NS_CC;


class CanibalsScene : public BaseScene
{
public:
    virtual void Retry(Ref* sender);
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    ~CanibalsScene(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(CanibalsScene);
    static CanibalsScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintStart(Ref* sender);
    virtual void HintCallback(bool accepted);
     virtual void TutorialStart(Ref* sender){};
    
    Canoe* getCanoe();
    bool shouldCheckFinish();
    void onEnterTransitionDidFinish();
private:
  
    Label* mCounter;
    void crearHumanos();
    Human* mHumano1,*mHumano2,*mHumano3,*mCanibal1,*mCanibal2,*mCanibal3;
    Canoe* mCanoa;
    void HintStartTemp(float dt);
    
};


#endif // __CanibalsScene_SCENE_H__
