//
//  Human.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Human__
#define __logicmaster2__Human__

#include <iostream>
#include "../widgets/Boton.h"
#include "Human.fwd.h"
#include "CanibalsScene.h"
#include "HumanTypes.h"


typedef enum HumanState
{
    kLadoIzquierdo,
    kEmbarcado,
    kLadoDerecho
    
} HumanState;





class Human: public Boton
{
    
    
    public:
    
        Human(Point mPointIzq,Point mPointDch,HumanType tipo,int z_index);
        virtual ~Human(void);
        static cocos2d::Scene* scene();
    
   
        static Human* create(Node* parent,Point mPointIzq,Point mPointDch,HumanType tipo,int z_index);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        HumanType mTipo;
        HumanType getTipo();
        HumanState mEstado;
        int mZIndex;
        bool esExplorer();
    private:
        bool mEstaIzquierda;
        Point mPosicionIzquierda,mPosicionDerecha;
        void finMount();
        void finUnmount();
        bool mIsMoving;
        unsigned int mSoundEffect;
    
        
    
};


#endif /* defined(__logicmaster2__Human__) */
