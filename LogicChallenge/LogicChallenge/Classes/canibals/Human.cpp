#include "Human.h"
#include "SimpleAudioEngine.h"
#include "Canoe.h"

Human::Human(Point mPointIzq,Point mPointDch,HumanType tipo,int z_index){
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=kLadoIzquierdo;
    mTipo=tipo;
    setPosition(mPointIzq);
    mZIndex=z_index;
    mSwallowTouches=true;
}

Human::~Human(void){}


Human* Human::create(Node* parent,Point mPointIzq,Point mPointDch,HumanType tipo,int z_index)
{
    Human *devolver= new Human(mPointIzq,mPointDch,tipo,z_index);
    devolver->autorelease();
   
    
    std::string imagen;
    if(tipo==HumanType(kExplorer)){
        imagen="canibals/explorer.png";
    }else{
         imagen="canibals/canibal.png";
    }
    devolver->initBoton(devolver, menu_selector(Human::click), imagen);
    devolver->setPosition(devolver->mPosicionIzquierda);
    
    return devolver;
}


void Human::click(Ref* sender)
{
    if(mIsMoving){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(CanibalsScene::getInstance()->getCanoe()->isMoving()){
        CCLOG("CANOA MOVING");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    
     CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/canibals/jump.mp3");
   
    if(mEstado==HumanState(kEmbarcado)){
        if(!CanibalsScene::getInstance()->getCanoe()-> unMountHuman(this)){
            CCLOG("SIN SENTIDO");
            return;
        }
        mIsMoving=true;
        Point destino;
        if(CanibalsScene::getInstance()->getCanoe()->estaEnIzquierda()){
            destino=mPosicionIzquierda;
            this->mEstado=HumanState(kLadoIzquierdo);
            this->mEstaIzquierda=true;
        }
        else{
            destino=mPosicionDerecha;
            this->mEstado=HumanState(kLadoDerecho);
             this->mEstaIzquierda=false;
        }
       
        CallFunc *finUnmount = CallFunc::create([this]() { this->finUnmount(); });
        this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1),finUnmount));
        this->runAction(ScaleTo::create(0.5f, 1.f));
        return;
    }
    else{
       
        if(CanibalsScene::getInstance()->getCanoe()->getPassengers()==2){
            CCLOG("BARCA LLENA");
             CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
            return;
        }else{
           
            if(CanibalsScene::getInstance()->getCanoe()->estaEnIzquierda())
            {
                if(this->mEstado==HumanState(kLadoIzquierdo)){
                    Point destino=CanibalsScene::getInstance()->getCanoe()->getSeat();
                    CanibalsScene::getInstance()->getCanoe()->mountHumanLogic(this);
                    CallFunc *finMove = CallFunc::create([this]() { this->finMount(); });
                    this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f,destino , 100, 1),finMove));
                    this->mEstado=HumanState(kEmbarcado);
                    mIsMoving=true;
                    
                }
                
            }
            else{
                if(this->mEstado==HumanState(kLadoDerecho)){
                    Point destino=CanibalsScene::getInstance()->getCanoe()->getSeat();
                    CanibalsScene::getInstance()->getCanoe()->mountHumanLogic(this);
                    CallFunc *finMove =CallFunc::create([this]() { this->finMount(); });
                    this->runAction(Sequence::createWithTwoActions(JumpTo::create(0.5f,destino , 100, 1),finMove));
                    this->mEstado=HumanState(kEmbarcado);
                    mIsMoving=true;
                    
                }
            }
        }
    }
    
}

void Human::finUnmount(){
    mIsMoving=false;
    if(CanibalsScene::getInstance()->shouldCheckFinish()){
        CanibalsScene::getInstance()->checkFinish(NULL);
    }
    
}
void Human::finMount(){
    CanibalsScene::getInstance()->getCanoe()->mountHuman(this);
    mIsMoving=false;
}
                    
bool Human::estaEnIzquierda(){
    return mEstaIzquierda;
}

void Human::setMoving(bool moving){
    this->mIsMoving=moving;
}


HumanType Human::getTipo(){
    return mTipo;
}


bool Human::esExplorer(){

    return mTipo==HumanType(kExplorer);
}







