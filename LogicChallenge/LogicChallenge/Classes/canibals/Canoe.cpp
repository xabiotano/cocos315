//
//  Canoe.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Canoe.h"
#include "SimpleAudioEngine.h"
#include "Human.h"

Canoe::Canoe(bool estaIzquierda)
{
    mEstaIzquierda=estaIzquierda;
    mIsMoving=false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    mPosicionIzquierda=Point((370.f/960.f)*visibleSize.width,(150.f/640.f)*visibleSize.height);
    mPosicionDerecha=Point((600.f/960.f)*visibleSize.width,(150.f/640.f)*visibleSize.height);
    mState=CanoaState(kCanoaIzquierda);
    
   
       
}
Canoe::~Canoe(void){
}


Canoe* Canoe::create(Node* parent,std::string imagen,bool estaIzquierda){
    Canoe *devolver= new Canoe(estaIzquierda);
    devolver->autorelease();
    devolver->setPosition(devolver->mPosicionIzquierda);
    devolver->initBoton(devolver, menu_selector(Canoe::move), imagen);
    devolver->mSeat1=Point(devolver->getContentSize().width*0.33f,100.f);
    return devolver;
}
void Canoe::move(Ref* sender){
    if(mIsMoving){
        CCLOG("is moving!");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(mHumanosMontados.size()==0){
        CCLOG("size =0");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("CANIBALS_BOAT_EMPTY"))->setSprite(Sprite::create("canibals/boat.png"));
        return;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/canibals/boat.mp3");

    Point destino;
    if(estaEnIzquierda()){
        destino=this->mPosicionDerecha;
    }else{
         destino=this->mPosicionIzquierda;
    }
    CallFunc *finMove = CallFunc::create([this]() { this->finMove(); });
    this->runAction(Sequence::createWithTwoActions(MoveTo::create(1.f,destino),finMove));
    
    mEstaIzquierda=!mEstaIzquierda;
    mIsMoving=true;
}

void Canoe::finMove(){

    if(!mEstaIzquierda){
        mState=CanoaState(kCanoaDerecha);
    }else{
         mState=CanoaState(kCanoaIzquierda);
    }
    mIsMoving=false;
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(this->mSoundEffect);
    if(mHumanosMontados.size()>=0){
        Human* humanoTemp=(Human*) mHumanosMontados.at(0);
        if(humanoTemp!=NULL){
            humanoTemp->click(NULL);
        }
        humanoTemp=(Human*)mHumanosMontados.at(1);
        if(humanoTemp!=NULL){
             humanoTemp->click(NULL);
        }
    }
    CanibalsScene::getInstance()->checkFinish(NULL);
   
}
                    
bool Canoe::estaEnIzquierda(){
    return mEstaIzquierda;
}

void Canoe::mountHumanLogic(Human* human){
    if(mHumanosMontados.size()==0){
        mHumanosMontados.insert(0,human);
        CCLOG("humanosMontados[0]=human");
        
    }else{
        if(mHumanosMontados.at(0)==NULL){
            mHumanosMontados.insert(0,human);
            CCLOG("humanosMontados[0]=human");

        }else{
             mHumanosMontados.insert(1,human);
            CCLOG("humanosMontados[1]=human");
        }
    }
    //humanosMontados[mHumanosMontados->count()]=human;
    CCLOG("numero humanos %i", (int)mHumanosMontados.size());
}

bool Canoe::mountHuman(Human* human){
    Point position=this->convertToNodeSpace(human->getPosition());
    human->retain();
    human->removeFromParent();
    human->autorelease();
    this->addChild(human,-1);
    this->setTouchPriority(-10);
    human->setTouchPriority(-8);
    human->setPosition(position);
    return true;
}

bool Canoe::unMountHuman(Human* human){
    
    if(human){
        int humanpos=(int) human->getPositionX();
        int poscanoe=(int)mSeat1.x;
        CCLOG("humanpos:%i,poscanoe:%i",humanpos,poscanoe);
        if(humanpos==poscanoe){
            //estaba en la pos1
            CCLOG("humanosMontados[0]=NULL");
            mHumanosMontados.erase(0);
        }else{
            CCLOG("humanosMontados[1]=NULL");
            mHumanosMontados.erase(1);
        }
       
       
        //lo añadimos a la esecena
        Point posactual=convertToWorldSpace(human->getPosition());
        human->retain();
        human->removeFromParent();
        this->getParent()->addChild(human,human->mZIndex);
        human->autorelease();
        human->setPosition(posactual);
         CCLOG("numero humanos %u", (int)mHumanosMontados.size() );
        return true;
        
    }
    return false;
    
}



int Canoe::getPassengers(){
    return (int)mHumanosMontados.size();
}

Point Canoe::getSeat(){
    Point devolver;
    Point p1=mSeat1;
    Point p2=Point(this->getContentSize().width*0.66f,100.f);

    if(mHumanosMontados.size()==0){
        CCLOG("pos1");
        devolver =p1;
    }else if (mHumanosMontados.size()==1){
        if( mHumanosMontados.at(0)==NULL){
            CCLOG("pos1");
            devolver= p1;
        }else{
            CCLOG("pos2");
            devolver= p2;
        }
    }
    devolver=convertToWorldSpace(devolver);
    return devolver;
}

Point Canoe::getSeatCanoe(){
    Point p=getSeat();
    p=convertToNodeSpace(p);
    return p;
}




bool Canoe::isMoving(){
    return mIsMoving;
}


Human* Canoe::getHuman(int index){
    return (Human*)mHumanosMontados.at(index);
}





