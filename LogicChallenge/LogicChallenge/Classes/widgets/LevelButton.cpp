#include "LevelButton.h"
#include "SimpleAudioEngine.h"
#include "cocos-ext.h"
#include "../Variables.h"
#include "../Levelsv2.h"


LevelButton::LevelButton(void)
{
    _noActionLaunch=false;
}

LevelButton::~LevelButton(void)
{
   
}

LevelButton* LevelButton::create(Texture2D* pobTexture)
{
    LevelButton *pSprite = new LevelButton();
    if (pSprite && pSprite->initWithTexture(pobTexture))
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}






LevelButton* LevelButton::createLevelButtonWithTexture( SEL_MenuHandler accion,Texture2D* pobTexture,int tipo,int idnivel){
    LevelButton *devolver= LevelButton::create(pobTexture);
    devolver->_accion=accion;
    devolver->_colorfijado=false;
    devolver->_movido=false;
    devolver->mTipo=tipo;
    devolver->mIdNivel=idnivel;
    return devolver;
}

void LevelButton::onEnter()
{
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(false);
    mListener->onTouchBegan = CC_CALLBACK_2(LevelButton::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(LevelButton::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(LevelButton::onTouchEnded, this);
    
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener, this);
    Sprite::onEnter();
}

void LevelButton::onExit()
{
    _accion=NULL;
     this->getEventDispatcher()->removeEventListener(mListener);
   
    
    Sprite::onExit();
}


bool LevelButton::onTouchBegan(Touch* touch, Event* event)
{
    
    if ( !containsTouchLocation(touch) ){
        return false;
    }
    else{
        _puntotoque=Director::getInstance()->convertToGL(touch->getLocationInView());
        _movido=false;
        activo();
       return true;
    }
}

void LevelButton::onTouchMoved(Touch* touch, Event* event)
{
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
   // CCLOG("Toque movido in view:%f,%f", puntonuevo.x,puntonuevo.y);
    if(GetApproxDistance(_puntotoque, puntonuevo)>50)
    {
        //CCLOG("distancia superada");
        _movido=true;
        inactivo();
    }
}

void LevelButton::onTouchEnded(Touch* touch, Event* event)
{
    
    if(!_movido)
    {
        
        if(!this->_sound.empty())
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(_sound.c_str());
        }
       
        Variables::LEVELS_ELIXIR_CLICKADO=mIdNivel;
        
        if(getParent()!=NULL)
        {
            if(!_noActionLaunch){
                Node* escena=(Node*)Director::getInstance()->getRunningScene()->getChildren().at(1);
                (escena->*_accion)(escena);
            }
        }
        else
        {
            //sino hay parent es porque yo mismo lanzo la orden
            if(!_noActionLaunch){
                
                (this->*_accion)(this);
            }
        }
        _movido=false;
       
    }
    inactivo();
    if(_colorfijado)
    {
        this->setColor(_color);
    }
        
}

bool LevelButton::containsTouchLocation(Touch* touch)
{
    Size s = getTexture()->getContentSize();
    return  Rect(-s.width / 2, -s.height / 2, s.width, s.height).containsPoint(convertTouchToNodeSpaceAR(touch));
}

void LevelButton::activo()
{
    this->setColor((Color3B) {230,230,230});
    
    
}
void LevelButton::inactivo()
{
    this->setColor((Color3B) {255,255,255});
}


int LevelButton::GetApproxDistance(Point pt1, Point pt2) {
    
    int dx = (int)pt1.x - (int)pt2.x;
    int dy = (int)pt1.y - (int)pt2.y;
    dx = abs(dx);
    dy = abs(dy);
    if ( dx < dy ) {
        return dx + dy - (dx >> 1);
    } else {
        return dx + dy - (dy >> 1);
    }
}
void LevelButton::setSound(std::string soundname)
{
    this->_sound=soundname;
}

void LevelButton::setColorLevelButton(Color3B color)
{
    _colorfijado=true;
    this->_color=color;
    this->setColor(color);
}



void LevelButton::setTouchPriority(int priority){
    this->getEventDispatcher()->removeEventListener(mListener);
    this->getEventDispatcher()->addEventListenerWithFixedPriority(mListener, priority);
}

void LevelButton::setEnabled(bool activo)
{
    if(activo){
        
        this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener, this);
    }
    else{
        this->getEventDispatcher()->removeEventListener(mListener);
    }
}

void LevelButton::noActionLaunch(){
    _noActionLaunch=true;
}


void LevelButton::setTexto(std::string texto){
    mLabel=Label::createWithTTF(texto.c_str(), "fonts/KGBlankSpaceSketch.ttf", 32);
    mLabel->setWidth(this->getContentSize().width);
    mLabel->setHeight(this->getContentSize().height/2);
    mLabel->setHorizontalAlignment(TextHAlignment::CENTER);
    mLabel->setVerticalAlignment(TextVAlignment::CENTER);
    mLabel->enableOutline(Color4B(0,0,0,255),2);
    mLabel->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height/2));
    this->addChild(mLabel);
    
    
}






void  LevelButton::unlock(){
  //  Levelsv2* escena=(Levelsv2*)Director::getInstance()->getRunningScene()->getChildren()->getObjectAtIndex(0);
    Levelsv2* escena=(Levelsv2*)Director::getInstance()->getRunningScene()->getChildren().at(0);
    pintarTipo(escena,false,false,mTipo);
}


void LevelButton::pintarSuperado(){
    
}
void LevelButton::pintarTipo(Levelsv2* parent,bool superado,bool locked,int tipo){
    Sprite* itemElixir;
    
    if(!superado && locked){
        itemElixir= Sprite::createWithTexture(parent->item_elixir_locked_tex_bacth->getTexture());
    }else if(!superado && !locked){
        if(tipo==1){
            itemElixir= Sprite::createWithTexture(parent->item_elixir_cerebro_tex_bacth->getTexture());
        }else if(tipo==2){
            itemElixir= Sprite::createWithTexture(parent->item_elixir_fresa_tex_bacth->getTexture());
        }else if(tipo==3){
            itemElixir= Sprite::createWithTexture(parent->item_elixir_energia_tex_bacth->getTexture());
        }
    }
    itemElixir->setTag(666);
    itemElixir->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height*0.6f));
    this->addChild(itemElixir);
    
    if(!locked){
        itemElixir->runAction(RepeatForever::create(Sequence::createWithTwoActions(EaseInOut::create(ScaleTo::create(0.5f,1.05f),3),ScaleTo::create(0.5f, 1.f))));
    }

}



void LevelButton::pintarElixiresGanados(Levelsv2* parent, int numeroestrellas){
    Point p1=Point(this->getContentSize().width*0.33f,this->getContentSize().height*0.6f);
    Point p2=Point(this->getContentSize().width*0.5f,this->getContentSize().height*0.6f);
    Point p3=Point(this->getContentSize().width*0.67f,this->getContentSize().height*0.6f);
    
    Sprite* botella1,*botella2,*botella3;
    CCLOG("pintarElixiresGanados %i", numeroestrellas);

    if(numeroestrellas==1){
        botella1=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
        botella2=  Sprite::createWithTexture(parent->item_elixir_item_wood_hole_tex_bacth->getTexture());
        botella3=  Sprite::createWithTexture(parent->item_elixir_item_wood_hole_tex_bacth->getTexture());
        
    }else if(numeroestrellas==2){
        botella1=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
        botella2=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
        botella3=  Sprite::createWithTexture(parent->item_elixir_item_wood_hole_tex_bacth->getTexture());
    }else if(numeroestrellas==3){
        botella1=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
        botella2=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
        botella3=  Sprite::createWithTexture(parent->item_elixir_item_tex_bacth->getTexture());
    }
    botella1->setRotation(-15.f);
    botella3->setRotation(+15.f);
    botella1->setPosition(p1);
    this->addChild(botella1,2);
    botella2->setPosition(p2);
    this->addChild(botella2,2);
    botella3->setPosition(p3);
    this->addChild(botella3,2);
    botella1->setTag(555);
    botella2->setTag(556);
    botella3->setTag(557);
    
    
}

void LevelButton::animarElixirRecienSuperado(Levelsv2* parent,int numeroBotellitasActualizar){
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elixircomun/level_clear.mp3",false);
    
    ParticleExplosion* explosion = ParticleExplosion::create();
    //explosion->retain();
    explosion->setTexture(Director::getInstance()->getTextureCache()->addImage("particles/particle-stars.png"));
    explosion->setAutoRemoveOnFinish(true);
    explosion->setStartSize(30.0f);
    explosion->setEndSize(1.0f);
    explosion->setDuration(0.3f);
    explosion->setLife(1.0f);
    explosion->setLifeVar(1.0f);
    explosion->setSpeed(300.0f);
    explosion->setStartColor((Color4F){1,1,0,1});
    explosion->setEndColor((Color4F){1,1,0,1});
    explosion->setAnchorPoint(Point(0.5f,0.5f));
    explosion->setPosition(Point(this->getContentSize().width/2, this->getContentSize().height/2));
    this->addChild(explosion,2);

    
    
    this->removeChildByTag(666);//elimino la botella gorda
    pintarElixiresGanados(parent,numeroBotellitasActualizar);
    
    Sprite* item1=(Sprite*)this->getChildByTag(555);
    Sprite* item2=(Sprite*)this->getChildByTag(556);
    Sprite* item3=(Sprite*)this->getChildByTag(557);
    
    item1->setOpacity(0.f);
    item2->setOpacity(0.f);
    item3->setOpacity(0.f);
    
    Sprite* elixir_item_small_inside_wood_hole1=Sprite::create("levelsv2/elixir_item_small_inside_wood_hole.png");
    Sprite* elixir_item_small_inside_wood_hole2=Sprite::create("levelsv2/elixir_item_small_inside_wood_hole.png");
    Sprite* elixir_item_small_inside_wood_hole3=Sprite::create("levelsv2/elixir_item_small_inside_wood_hole.png");
    elixir_item_small_inside_wood_hole1->setPosition(item1->getPosition());
    elixir_item_small_inside_wood_hole2->setPosition(item2->getPosition());
    elixir_item_small_inside_wood_hole3->setPosition(item3->getPosition());
    this->addChild(elixir_item_small_inside_wood_hole1);
    this->addChild(elixir_item_small_inside_wood_hole2);
    this->addChild(elixir_item_small_inside_wood_hole3);
    
   
    
    
    
    item1->setOpacity(255.f);
    item1->setScale(0.f);
    item1->runAction(Sequence::create(FadeIn::create(0.25f),EaseInOut::create(ScaleTo::create(0.15f, 1.15f),3),EaseInOut::create(ScaleTo::create(0.05f, 1.f),3),NULL));
    
    
    if(numeroBotellitasActualizar>1){
        item2->setOpacity(0.f);
        item2->setScale(0.f);
        item2->runAction( Sequence::create(DelayTime::create(0.5f),FadeIn::create(0.25f),EaseInOut::create(ScaleTo::create(0.15f, 1.15f),3),EaseInOut::create(ScaleTo::create(0.05f, 1.f),3),NULL));
    }
    if(numeroBotellitasActualizar>2){
        item3->setScale(0.f);
        item3->setOpacity(0.f);
        item3->runAction(Sequence::create(DelayTime::create(0.75f),FadeIn::create(0.25f),EaseInOut::create(ScaleTo::create(0.15f, 1.15f),3),EaseInOut::create(ScaleTo::create(0.05f, 1.f),3),NULL));
    }
    
    
    
    
    
}




