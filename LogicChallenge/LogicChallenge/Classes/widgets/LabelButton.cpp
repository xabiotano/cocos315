//
//  LabelButton.cpp
//  logicmaster2
//
//  Created by Xabier on 23/11/13.
//
//

#include "LabelButton.h"
#include "../Variables.h"


 LabelButton::LabelButton(void){};
 LabelButton::~LabelButton(void){};


 int LabelButton::CLICKADO;

LabelButton* LabelButton::create(Node* parent, SEL_MenuHandler accion,std::string imagenPath,std::string label,int numOpcion)
{
   
    LabelButton* labelButton= new LabelButton();
    labelButton->autorelease();
    labelButton->mNumNivel=numOpcion;
    labelButton->initBoton(parent, accion, imagenPath);
    labelButton->setTag(numOpcion);
    Label *texto=Label::createWithTTF(label.c_str(), "fonts/KGBlankSpaceSketch.ttf", 60);
    texto->setPosition(Point(labelButton->getContentSize().width/2,labelButton->getContentSize().height/2));
    labelButton->addChild(texto,4);
   
 
    
    
    return labelButton;
}




bool LabelButton::isLocked()
{
    return true;
}

bool LabelButton::onTouchBegan(Touch* touch, Event* event)
{
    CCLOG("LabelButton:TouchBegan");
    return Boton::onTouchBegan(touch, event);
}


void LabelButton::onTouchEnded(Touch* touch, Event* event)
{
    CCLOG("LabelButton:TouchEnded");
    LabelButton::CLICKADO=this->getTag();
    Boton::onTouchEnded(touch, event);
}
