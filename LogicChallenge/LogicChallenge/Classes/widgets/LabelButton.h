//
//  LabelButton.h
//  logicmaster2
//
//  Created by Xabier on 23/11/13.
//
//
#ifndef __logicmaster2__LabelButton__
#define __logicmaster2__LabelButton__

#include <iostream>
#include "cocos2d.h"
#include "Boton.h"




USING_NS_CC;
class LabelButton: public Boton
{
    
    public:
        static int CLICKADO;
    
        LabelButton(void);
        virtual ~LabelButton(void);
        static LabelButton* create(Node* parent, SEL_MenuHandler accion,std::string imagenPath,std::string label,int numOpcion);
        virtual bool onTouchBegan(Touch* touch, Event* event);
        virtual void onTouchEnded(Touch* touch, Event* event);
        void lock();

    private:
        bool isLocked();
        int mNumNivel;
};




#endif /* defined(__logicmaster2__LabelButton__) */
