//
//  Boton.h
//  trivial
//
//  Created by Xabier on 11/04/13.
//
//

#ifndef __trivial__Boton2__
#define __trivial__Boton2__

#include <iostream>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
using namespace cocos2d;

USING_NS_CC;

class Boton2 : public ui::Button
{
public:
    CREATE_FUNC(Boton2);
    
    void loadTextures(const std::string& normal);
    
private:
    bool init() override;
protected:
    virtual void onPressStateChangedToNormal() override;
    virtual void onPressStateChangedToPressed() override;
    virtual void onPressStateChangedToDisabled() override;
};




#endif /* defined(__trivial__Boton2__) */
