//
//  NivelItem.h
//  logicmaster2
//
//  Created by Xabier on 23/11/13.
//
//
#ifndef __logicmaster2__NivelItem__
#define __logicmaster2__NivelItem__

#include <iostream>
#include "cocos2d.h"
#include "Boton.h"
#include "../BaseScene.h"

USING_NS_CC;
class NivelItem: public Boton
{
public:
    template <typename T> std::string tostr(const T& t);
    NivelItem(int numNivel,TipoNivel tipoNivel);
    virtual ~NivelItem(void);
    void  initNivelItem();
    static NivelItem* create(Node* parent, SEL_MenuHandler accion,int numNivel,TipoNivel tipoNivel);
   //virtual bool TouchBegan(Touch* touch, Event* event);
    
    void onTouchEnded(Touch* touch, Event* event) override;
    
    int getNivel();
    virtual void activo() override;
    virtual void inactivo() override;
    
    
private:
    int getElixiresRequeridos(int nivel);
    TipoNivel mTipoNivel;
    bool isLocked(int numNivel);
    static std::string getImagenNivel(int numNivel);
    Point getPositionNivel(int i);
    float getHeightPositionTexto(int i);
    int mNumNivel;
    void drawStars(int stars);
    bool mJugable;
    void dibujarElixiresRequeridos();
 
   
};




#endif /* defined(__logicmaster2__NivelItem__) */
