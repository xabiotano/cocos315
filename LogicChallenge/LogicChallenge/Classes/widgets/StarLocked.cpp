//
//  StarLocked.cpp
//  logicmaster2
//
//  Created by Xabier on 23/11/13.
//
//

#include "StarLocked.h"
#include "../Variables.h"
#include "SimpleAudioEngine.h"

 StarLocked::StarLocked(void){};
 StarLocked::~StarLocked(void){};



StarLocked* StarLocked::create(bool locked)
{
    StarLocked *devolver= new StarLocked();
    devolver->autorelease();
    devolver->initWithTexture(Director::getInstance()->getTextureCache()->addImage("ui/star_locked.png"));
    
    if(!locked) {
        Sprite* unlocked=Sprite::create("ui/star_unlocked_inside.png");
        
        unlocked->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().height/2 ));
        devolver->addChild(unlocked,-1);
        
    }
    return devolver;
}



void StarLocked::unlock(){
    Sprite* unlocked=Sprite::create("ui/star_unlocked_inside.png");
    unlocked->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height/2 ));
    this->addChild(unlocked,-1);
    
    ParticleExplosion* explosion = ParticleExplosion::create();
    explosion->retain();
    explosion->setTexture(Director::getInstance()->getTextureCache()->addImage("ui/particle-stars.png"));
    explosion->setAutoRemoveOnFinish(true);
    explosion->setStartSize(30.0f);
    explosion->setEndSize(1.0f);
    explosion->setDuration(0.3f);
    explosion->setLife(1.0f);
    explosion->setLifeVar(1.0f);
    explosion->setSpeed(300.0f);
    explosion->setStartColor((Color4F){1,1,0,1});
    explosion->setEndColor((Color4F){1,1,0,1});
    explosion->setAnchorPoint(Point(0.5f,0.5f));
    explosion->setPosition(this->getPosition() );
    this->addChild(explosion);
    
    
    
    this->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.5f, 1.5f), ScaleTo::create(0.5f, 1.f)));
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/revelation.mp3",false);
    
    
    
}





