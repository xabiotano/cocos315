#include "Boton2.h"
#include "SimpleAudioEngine.h"
#include "cocos-ext.h"
#include "CCGetFont.h"



bool Boton2::init()
{
    if (ui::Button::init())
    {
        // HERE YOUR CUSTOM CODE
        return true;
    }
    else
        return false;
}





void Boton2::loadTextures(const std::string& normal)
{
    ui::Button::loadTextures(normal, normal,normal,TextureResType::LOCAL);
    
}

void Boton2::onPressStateChangedToNormal(){
    
    ui::Button::onPressStateChangedToNormal();
    this->setColor(Color3B(255,255,255));
}

void Boton2::onPressStateChangedToPressed(){
    
    
    ui::Button::onPressStateChangedToPressed();
    this->setColor(Color3B(200,200,200));
}
void Boton2::onPressStateChangedToDisabled(){
    
    ui::Button::onPressStateChangedToDisabled();
    this->setColor(Color3B(0,0,0));
};
