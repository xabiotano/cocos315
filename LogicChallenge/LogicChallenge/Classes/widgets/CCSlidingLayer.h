#ifndef CCSlidingLAYER_DEF
#define CCSlidingLAYER_DEF
#include "cocos2d.h"


using namespace cocos2d;

typedef enum {
    Vertically,
    Horizontally
} SlideDirection;

typedef enum{
    BounceDirectionGoingUp = 1,
    BounceDirectionStayingStill = 0,
    BounceDirectionGoingDown = -1,
    BounceDirectionGoingLeft = 2,
    BounceDirectionGoingRight = 3
} BounceDirection;

typedef enum{
    kAlignmentCenter = 0,
    kAlignmentLeft = 1,
    kAlignmentRight = 2,
    kAlignmentTop = 3,
    kAlignmentBottom = 4,
} Alignment;

class CCSlidingLayer : public LayerColor
{
public:		
	virtual ~CCSlidingLayer(){};
	CCSlidingLayer();

	// Constructors
    CREATE_FUNC(CCSlidingLayer);
	static CCSlidingLayer* create(SlideDirection slideDirection, Size contentSize, Rect contentRect, Color4B color);
	virtual bool initSlidingLayer(SlideDirection slideDirection, Size contentSize, Rect contentRect, Color4B color);
	
	// Update function
	void update(float time);

	// Adds items from Top to Bottom and also increment size of the layer (not contentRect_) if needed
	void addChildWithSize(Node* child, Size size, Alignment alignment);

	// Control parameters
	SlideDirection slideDirection_;
	Rect contentRect_;
	BounceDirection direction_;
    
	// Tells if the user is dragging or not
	bool isDragging_;
    
	// Scrolling parameters
    Point m_tBeginPos;
	float lasty;
	float xvel;

	// Child count for adding items
	int childCount;

	// Margins (if desired to use)
	float verticalMargins;
	float horizontalMargins;
	
	// Getters
	int getChildCount(){return childCount;}
	float getVerticalMargins(){return verticalMargins;}
	float getHorizontalMargins(){return horizontalMargins;}
	Rect getContentRect(){return contentRect_;}
	bool isDragging(){return isDragging_;}

	// Setters
	void setVerticalMargins(float vM){verticalMargins = vM;}
	void setHorizontalMargins(float hM){horizontalMargins=hM;}
	void setContentRect(Rect newRect){contentRect_=newRect;}

    /** override functions */	
    EventListenerTouchOneByOne* mListener;
    virtual bool onTouchBegan(Touch* touch, Event* event);
    virtual void onTouchMoved(Touch* touch, Event* event);
    virtual void onTouchEnded(Touch* touch, Event* event);
    
    //virtual void registerWithTouchDispatcher();
	virtual void onEnter();
	virtual void onEnterTransitionDidFinish();
	virtual void onExit();
private:
	float round(float r);
};

#endif
