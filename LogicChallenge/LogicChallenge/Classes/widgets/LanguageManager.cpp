#include "LanguageManager.h"

LanguageManager* LanguageManager::_instance = 0;

LanguageManager::LanguageManager()
{
    string fileName;
    // detect current language
    
    LanguageType curLanguage = Application::getInstance()->getCurrentLanguage();
    switch (curLanguage) {
        case LanguageType::ENGLISH:
            fileName = "locate/Localized_en.json";
            break;
        case LanguageType::SPANISH:
            fileName = "locate/Localized_sp.json";
            break;
        case LanguageType::FRENCH:
            fileName = "locate/Localized_fr.json";
            break;
        case LanguageType::RUSSIAN:
            fileName = "locate/Localized_ru.json";
            break;
        case LanguageType::KOREAN:
            fileName = "locate/Localized_ko.json";
            break;
        case LanguageType::GERMAN:
            fileName = "locate/Localized_de.json";
            break;
        default:
            fileName = "locate/Localized_en.json";
            break;
            
    }
    

    
    
    
    
    

    
    // below we open, read and parse language data file with rapidjson library
    ssize_t size;
    const char* buf = (const char*)FileUtils::getInstance()->getFileData(fileName.c_str(), "r", &size);
    string content(buf);
    string clearContent = content.substr(0, content.rfind('}') + 1);
    //rapidjson::Document d;
    //d.Parse<rapidjson::ParseFlag::kParseStopWhenDoneFlag>((char *)json);
    
    document.Parse<rapidjson::ParseFlag::kParseStopWhenDoneFlag>((char *)clearContent.c_str());
    
    
    //document.Parse<0>(clearContent.c_str());
    if(document.HasParseError())
    {
        CCLOG("Language file parsing error!");
        return;
    }
}

LanguageManager* LanguageManager::getInstance()
{
    if(!_instance)
        _instance = new LanguageManager();
    return _instance;
}

string LanguageManager::getStringForKey(string key)
{
    return document[key.c_str()].GetString();
}

string LanguageManager::getStringForKey(string key,string alternative)
{
    if(document[key.c_str()].GetString()==NULL){
        return alternative;
    }else{
         return document[key.c_str()].GetString();
    }
}


