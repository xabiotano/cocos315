#include "Boton.h"
#include "SimpleAudioEngine.h"
#include "cocos-ext.h"
//#include "CCLabelStroke.h"
#include "CCGetFont.h"


Boton::Boton(void)
{
    _noActionLaunch=false;
}

Boton::~Boton(void)
{
     CCLOG("DELETE Boton %s", this->getTexture()->getPath().c_str());
}




Boton* Boton::create(Texture2D *pTexture)
{
    Boton *pSprite = new Boton();
    if (pSprite && pSprite->initWithTexture(pTexture))
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}


void Boton::customInit(Node* _parent,SEL_MenuHandler accion){
    this->_parent=_parent;
    this->_accion=accion;
    this->_colorfijado=false;
    this->mColorStroke=Color3B(0,0,0);
    this->_movido=false;
    this->mSwallowTouches=true;
    this->_enabled=true;
    this->mPriority=-1;
}

Boton* Boton::createBoton(Node* _parent,SEL_MenuHandler accion,std::string url_imagen)
{
    Boton *devolver= Boton::create(Director::getInstance()->getTextureCache()->addImage(url_imagen.c_str()));
    devolver->customInit(_parent, accion);
    return devolver;
}



Boton* Boton::createBotonWithPriority(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority)
{
    Boton *devolver= Boton::create(Director::getInstance()->getTextureCache()->addImage(url_imagen.c_str()));
    devolver->customInit(_parent, accion);
    devolver->mPriority=priority;
   // devolver->addEventListener();
    return devolver;
}



Boton* Boton::createBoton(SEL_MenuHandler accion,std::string url_imagen)
{
    Boton *devolver= Boton::create(Director::getInstance()->getTextureCache()->addImage(url_imagen.c_str()));
    devolver->customInit(NULL, accion);
   // devolver->addEventListener();
    return devolver;
}

Boton* Boton::createBotonWithPriority(SEL_MenuHandler accion,std::string url_imagen,int priority)
{
    Boton *devolver= Boton::create(Director::getInstance()->getTextureCache()->addImage(url_imagen.c_str()));
    devolver->customInit(NULL, accion);
    devolver->mPriority=priority;
   // devolver->addEventListener();
    return devolver;
}

Boton* Boton::createBotonWithPriorityAndSwallow(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority,bool swallow)
{
    Boton *devolver= Boton::create(Director::getInstance()->getTextureCache()->addImage(url_imagen.c_str()));
    devolver->customInit(_parent, accion);
    devolver->mPriority=priority;
    devolver->mSwallowTouches=swallow;
   // devolver->addEventListener();
    return devolver;

}


bool Boton::initBoton(Node* _parent,SEL_MenuHandler accion,std::string url_imagen)
{
    this->initWithFile(url_imagen);
    this->customInit(_parent, accion);
    return true;
}
bool Boton::initBotonWithPriority(Node* _parent,SEL_MenuHandler accion,std::string url_imagen,int priority)
{
    
    initBoton(_parent,accion,url_imagen);
    mPriority=priority;
    return true;
}


void Boton::addEventListener(){
    if(mListener!=NULL)
    {
        this->getEventDispatcher()->removeEventListener(mListener);
    }
    
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(mSwallowTouches);
    mListener->onTouchBegan = CC_CALLBACK_2(Boton::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(Boton::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(Boton::onTouchEnded, this);
    if(mPriority!=-1){
        this->getEventDispatcher()->addEventListenerWithFixedPriority(mListener, mPriority);
    }else{
        this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener,this);

    }
    
   
}
void Boton::addEventListenerSelfPriority(){
    
    
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(mSwallowTouches);
    mListener->onTouchBegan = CC_CALLBACK_2(Boton::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(Boton::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(Boton::onTouchEnded, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener,this);
    
    
}




void Boton::onEnter()
{
    addEventListener();
    Sprite::onEnter();
}

void Boton::onExit()
{
    
    CCLOG("exit %s", this->getTexture()->getPath().c_str());
    Sprite::onExit();
}

 
bool Boton::onTouchBegan(Touch* touch, Event* event)
{
    if(!_enabled){
        return false;
    }
    
    if ( !containsTouchLocation(touch) ){
        
        return false;
    }
    else{
        _puntotoque=Director::getInstance()->convertToGL(touch->getLocationInView());
        _movido=false;
        activo();
        return true;
    }
}

void Boton::onTouchMoved(Touch* touch, Event* event)
{
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
   // CCLOG("Toque movido in view:%f,%f", puntonuevo.x,puntonuevo.y);
    if(GetApproxDistance(_puntotoque, puntonuevo)>50)
    {
        //CCLOG("distancia superada");
        _movido=true;
        inactivo();
    }
}

void Boton::onTouchEnded(Touch* touch, Event* event)
{
    inactivo();
    if(_colorfijado)
    {
        this->setColor(_color);
    }
    
    if(!_movido)
    {
        
        if(!this->_sound.empty())
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(_sound.c_str());
        }
       
        
        
        if(_parent!=NULL)
        {
            if(!_noActionLaunch){
                (_parent->*_accion)(_parent);
            }
        }
        else
        {
            //sino hay parent es porque yo mismo lanzo la orden
            if(!_noActionLaunch){
                if(_accion!=NULL){
                    (this->*_accion)(this);
                }
                
            }
        }
        _movido=false;
       
    }
    
        
}

bool Boton::containsTouchLocation(Touch* touch)
{
    Size s = getTexture()->getContentSize();
    return  Rect(-s.width / 2, -s.height / 2, s.width, s.height).containsPoint(convertTouchToNodeSpaceAR(touch));
}

void Boton::activo()
{
    this->setColor((Color3B) {230,230,230});
    
    
}
void Boton::inactivo()
{
    this->setColor((Color3B) {255,255,255});
}

void Boton::setSwallowTouches(bool swallowTouches){
    this->mSwallowTouches=swallowTouches;
}


int Boton::GetApproxDistance(Point pt1, Point pt2) {
    
    int dx = (int)pt1.x - (int)pt2.x;
    int dy = (int)pt1.y - (int)pt2.y;
    dx = abs(dx);
    dy = abs(dy);
    if ( dx < dy ) {
        return dx + dy - (dx >> 1);
    } else {
        return dx + dy - (dy >> 1);
    }
}
void Boton::setSound(std::string soundname)
{
    this->_sound=soundname;
}

void Boton::setColorBoton(Color3B color)
{
    _colorfijado=true;
    this->_color=color;
    this->setColor(color);
}



void Boton::setTouchPriority(int priority){
    mPriority=priority;
    addEventListener();
}


void Boton::setEnabled(bool activo)
{
    
    _enabled=activo;
    
}

void Boton::noActionLaunch(){
    _noActionLaunch=true;
}


void Boton::setTexto(std::string texto){
    mLabel= Label::createWithTTF(texto, CCGetFont(), 50);
    mLabel->setWidth(this->getContentSize().width);
    mLabel->setHeight(this->getContentSize().height);
    mLabel->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mLabel->enableOutline(Color4B(0,0,0,255),2);
    mLabel->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height/2));
    this->addChild(mLabel,3);
    this->setCascadeOpacityEnabled(true);
    
    
}


void Boton::setColorSroke(Color3B color){
    mColorStroke=color;
}



void Boton::onExitTransitionDidStart()
{
    this->getEventDispatcher()->removeEventListener(mListener);
    mListener=NULL;
    stopAllActions();
    //this->unscheduleAllCallbacks();
    Sprite::onExitTransitionDidStart();
    
}



