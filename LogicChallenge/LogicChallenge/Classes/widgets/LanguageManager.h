#ifndef LanguageManager_h
#define LanguageManager_h

#include <string>
using std::string;

#include "cocos2d.h"
USING_NS_CC;

#include "cocos-ext.h"
using namespace rapidjson; // library that we use for json parsing

class LanguageManager
{
    Document document; // current document with language data
    LanguageManager(); // constructor is private
    static LanguageManager* _instance;
public:
    static LanguageManager* getInstance();
    string getStringForKey(string key);
    string getStringForKey(string key,string alternative);
};
#endif
