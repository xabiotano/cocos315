//
//  Balanza.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Balanza.h"
#include "SimpleAudioEngine.h"
#include "BalanzasScene.h"
#include <stdio.h>

Balanza::Balanza(int sitiosBalanza,std::string descripcion){
    mSitiosBalanza=sitiosBalanza;
    mIsMoving=false;
   /* mSitios=Dictionary::create();
    mSitios->retain();
    mSitios->setObject(Bool::create(false), 0);
    mSitios->setObject(Bool::create(false), 1);
    mSitios->setObject(Bool::create(false), 2);
    mSitios->setObject(Bool::create(false), 3);
    mSitios->setObject(Bool::create(false), 4);*/
    mSitios[0]=false;
    mSitios[1]=false;
    mSitios[2]=false;
    mSitios[3]=false;
    mSitios[4]=false;
    mDescripcion=descripcion;
    
}

Balanza::~Balanza(void){
    //CC_SAFE_RELEASE(mSitios);
}


Balanza* Balanza::create(int sitiosBalanza,std::string descripcion)
{
    Balanza *devolver= new Balanza(sitiosBalanza,descripcion);
    devolver->initBalanza();
    devolver->autorelease();
    return devolver;
}


void Balanza::initBalanza()
{
    this->initWithTexture(Director::getInstance()->getTextureCache()->addImage("balanzas/balanza_lateral.png"));
    
}


    


Point Balanza::getSitio(){
    Point devolver;
    float tamanoBalanzaWidth=this->getContentSize().width;
    float tamanoBalanzaheight=this->getContentSize().height;
    float percentHeight=1.4f;
    float origen= this->getPosition().x -tamanoBalanzaWidth/2;
    
  
    
    int sitio=getSitioIndex();
    if(sitio==0){
        devolver=Point(origen+  tamanoBalanzaWidth*0.07f,this->getPosition().y+ tamanoBalanzaheight*percentHeight);
    }else if(sitio==1){
        devolver=Point(origen+tamanoBalanzaWidth*0.29f,this->getPosition().y+tamanoBalanzaheight*percentHeight);
    }else if(sitio==2){
        devolver=Point(origen+tamanoBalanzaWidth*0.50f,this->getPosition().y+tamanoBalanzaheight*percentHeight);
    }else if(sitio==3){
        devolver=Point(origen+tamanoBalanzaWidth*0.71f,this->getPosition().y+tamanoBalanzaheight*percentHeight);
    }else if(sitio==4){
        devolver=Point(origen+ tamanoBalanzaWidth*0.93f,this->getPosition().y+tamanoBalanzaheight*percentHeight);
    }else if(sitio==5){
        CCLOG("getSitio() PROBLEMA!");
        return Point(0,0);
    }
   // CCLOG("position %f %f",this->getPosition().x, this->getPosition().y);
    devolver=this->getParent()->convertToWorldSpace(devolver);
    //CCLOG("getSitio %f %f",devolver.x, devolver.y);
    return devolver;

}

void Balanza::debug(){
    
   /* CCDictElement* pElement = NULL;
    CCDICT_FOREACH(mSitios, pElement)
    {
        Bool* sitioTemp = (Bool*)pElement->getObject();
        int sit=pElement->getIntKey();
        //CCLOG("Index: %i, Valor %i",sit,sitioTemp->getValue());
    }*/
    

}

int Balanza::getSitioIndex(){
    int sitio=6;
   /* CCDictElement* pElement = NULL;
    CCDICT_FOREACH(mSitios, pElement)
    {
        Bool* sitioTemp = (Bool*)pElement->getObject();
        if(!sitioTemp->getValue())
        {
            if(sitio>pElement->getIntKey()){
                sitio=pElement->getIntKey();
            }
            
        }
    }
    //CCLOG("getSitioIndex %i",sitio);*/
    
    for(auto& keyval : mSitios) {
        int key = keyval.first;
        cocos2d::Value val = keyval.second; /* could use Value& if need to modify dict  */
        if(val.getType() == cocos2d::Value::Type::BOOLEAN) {
            bool ocupado = val.asBool();
            if(!ocupado){
                if(sitio>key){
                    sitio=key;
                }
            }
        }
    }
    
    return sitio;
}


int  Balanza::aumentarSitio(){
    debug();
    int sitioLibre=getSitioIndex();
    CCLOG("aumentarSitio()=>sitio ocupado %i", sitioLibre);
    mSitios[sitioLibre]=true;
    //mSitios->setObject(Bool::create(true),sitioLibre );
    debug();
    return sitioLibre;
}

void Balanza::disminuirSitio(int sitio){
    debug();
    CCLOG("disminuirSitio()=> sitio liberado %i", sitio);
    mSitios[sitio]=false;
   // mSitios->setObject(Bool::create(false),sitio );
    debug();
}

int Balanza::getSitiosLibres(){
    int sitiosOcupados=0;
    /*CCDictElement* pElement = NULL;
    CCDICT_FOREACH(mSitios, pElement)
    {
        Bool* sitioTemp = (Bool*)pElement->getObject();
        if(sitioTemp->getValue() )
        {
            sitiosOcupados++;
        }
    }
    return (mSitiosBalanza-sitiosOcupados);*/
    for(auto& keyval : mSitios) {
        int key = keyval.first;
        cocos2d::Value val = keyval.second; /* could use Value& if need to modify dict  */
        if(val.getType() == cocos2d::Value::Type::BOOLEAN) {
            if(val.asBool()){
                sitiosOcupados++;
            }
        }
    }
    return (mSitiosBalanza-sitiosOcupados);
}

void Balanza::setMoving(bool moving){
    this->mIsMoving=moving;
}



bool Balanza::isMoving(){
    return mIsMoving;
}


bool Balanza::isSelected(){
    return mSelected;
}


std::string Balanza::getDescripcion(){
    return mDescripcion;
}









