//
//  Cake.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Cake__
#define __logicmaster2__Cake__

#include <iostream>
#include "Cake.fwd.h"
#include "BalanzasScene.h"
//#include "../widgets/CCLabelStroke.h"
#include "Balanza.fwd.h"




class Cake: public Sprite
{
    
    
    public:
    
        Cake(int numero,int peso);
        virtual ~Cake(void);
        static cocos2d::Scene* scene();
        void initCake(int numero);
        static Cake* create(int numero,int peso);
    
        EventListenerTouchOneByOne* mListener;
        virtual bool onTouchBegan(Touch* touch, Event* event);
        virtual void onTouchMoved(Touch* touch, Event* event);
        virtual void onTouchEnded(Touch* touch, Event* event);
    
  
    
        virtual void onEnter();
        virtual void onExit();
        virtual bool containsTouchLocation(Touch* touch);
        int getPeso();
        void setPeso(int peso);
        Balanza* getBalanza();
        bool getHaSidoPesada();
        void setHaSidoPesada();
        int  mNumero;
        void setImposibleSerLaMala();
        bool getImposibleSerLaMala();
    private:
        bool mIsMoving;
        bool mSelected;
        Sprite* mColorCake;
        Balanza* mBalanzaSobreLaQueEsta;
        int mSitioBalanza;
        int mPeso;
        bool mHaSidoPesada;
        bool mImposibleSerLaMala;
        template <typename T> std::string tostr(const T& t);
};


#endif /* defined(__logicmaster2__Cake__) */
