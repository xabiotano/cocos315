#include "BalanzasScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../Levels.h"
#include "Cake.h"
#include "Balanza.h"
#include <iostream>   // std::cout
#include <string>     // std::string, std::to_string

using namespace cocos2d;
using namespace CocosDenshion;


BalanzasScene* empty_instancia;



Scene* BalanzasScene::scene()
{
  
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    BalanzasScene *layer = BalanzasScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool BalanzasScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    empty_instancia=this; 
    
     mNombrePantalla="BalanzasScene";
    mTipoNivel=kNivelLogica;
    mIsMoving=false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("balanzas/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);

  //  mParteBaja=Sprite::create("balanzas/abajo.png");
   // this->addChild(mParteBaja);
   // mParteBaja->setPosition(Point(mVisibleSize.width/2,mParteBaja->getContentSize().height/2));
    
    
    float alturaCakeBase=0;// mParteBaja->getContentSize().height ;
    
    mBalanza=Sprite::create("balanzas/balanza_centro.png");
    this->addChild(mBalanza);
    
    
    float alturaRestante=(mVisibleSize.height-alturaCakeBase)*0.9f;
    float scale=alturaRestante/mBalanza->getContentSize().height;
    CCLOG("alturaRestante:%f mVisibleSize.height %f  scale %f",alturaRestante,mVisibleSize.height, scale);
    mBalanza->setScale(scale);
    
    mBalanza->setPosition(Point(mVisibleSize.width/2,alturaCakeBase+ mBalanza->getBoundingBox().size.height/2 ));
    
    
   
    
    
   /* Sprite* brazoIz=Sprite::create("balanzas/balanza_brazo_izquierda.png");
    brazoIz->setPosition(Point(0,mBalanza->getContentSize().height-brazoIz->getContentSize().height/2 ));
    mBalanza->addChild(brazoIz,-1);
    
    
    Sprite* brazoDch=Sprite::create("balanzas/balanza_brazo_dcha.png");
    brazoDch->setPosition(Point(mBalanza->getContentSize().width,mBalanza->getContentSize().height-brazoDch->getContentSize().height/2 ));
    mBalanza->addChild(brazoDch,-1);*/
    
    
    
  
    
    
    
    
    mCake1=Cake::create(0,100);
    this->addChild(mCake1,4);
    mCake1->setPosition(Point(mVisibleSize.width*0.10f, +  mVisibleSize.height*0.3f + mCake1->getContentSize().height/2));
    
    mCake2=Cake::create(1,100);
    this->addChild(mCake2,4);
    mCake2->setPosition(Point(mVisibleSize.width*0.30f, +  mVisibleSize.height*0.3f + mCake1->getContentSize().height/2));
    
    mCake3=Cake::create(2,100);
    this->addChild(mCake3,4);
    mCake3->setPosition(Point(mVisibleSize.width*0.50f, +  mVisibleSize.height*0.3f + mCake1->getContentSize().height/2));
    
    mCake4=Cake::create(3,100);
    this->addChild(mCake4,4);
    mCake4->setPosition(Point(mVisibleSize.width*0.70f, +  mVisibleSize.height*0.3f + mCake1->getContentSize().height/2));
    
    mCake5=Cake::create(4,100);
    this->addChild(mCake5,4);
    mCake5->setPosition(Point(mVisibleSize.width*0.90f, +  mVisibleSize.height*0.3f + mCake1->getContentSize().height/2));
    
    mCake6=Cake::create(5,100);
    this->addChild(mCake6,4);
    mCake6->setPosition(Point(mVisibleSize.width*0.2f, +  mVisibleSize.height*0.3f/2 + mCake1->getContentSize().height/2  ));
    
    mCake7=Cake::create(6,100);
    this->addChild(mCake7,4);
    mCake7->setPosition(Point(mVisibleSize.width*0.4f, +  mVisibleSize.height*0.3f/2 + mCake1->getContentSize().height/2));
    
    
    mCake8=Cake::create(7,100);
    this->addChild(mCake8,4);
    mCake8->setPosition(Point(mVisibleSize.width*0.6f, +  mVisibleSize.height*0.3f/2 + mCake1->getContentSize().height/2));
    
    
    mCake9=Cake::create(8,100);
    this->addChild(mCake9,4);
    mCake9->setPosition(Point(mVisibleSize.width*0.8f, +  mVisibleSize.height*0.3f/2 + mCake1->getContentSize().height/2));
    
    
    
    mCakes=CCArray::create();
    mCakes->retain();
    mCakes->addObject(mCake1);
    mCakes->addObject(mCake2);
    mCakes->addObject(mCake3);
    mCakes->addObject(mCake4);
    mCakes->addObject(mCake5);
    mCakes->addObject(mCake6);
    mCakes->addObject(mCake7);
    mCakes->addObject(mCake8);
    mCakes->addObject(mCake9);
    
    srand(time(0));
    int random= rand() % 9;
    int heavierOrLighter= rand() % 1;
    Cake* cake=(Cake*)mCakes->getObjectAtIndex(random);
    if(heavierOrLighter==1){
        mPesoDiferente=101;
        CCLOG("PESO HEAVIER");
    }else{
        CCLOG("PESO LIGHTER");
        mPesoDiferente=99;
    }
    cake->setPeso(mPesoDiferente);
    
    CCLOG("La random es la %i",random);
    
    
    
    mBalanzaIzq=Balanza::create(5,"izquierda");
    mBalanzaIzq->setPosition(Point(mVisibleSize.width*0.25f,mVisibleSize.height*0.6f));
    this->addChild(mBalanzaIzq,5);
    
    mBalanzaDch=Balanza::create(5,"derecha");
    mBalanzaDch->setPosition(Point(mVisibleSize.width*0.75f,mVisibleSize.height*0.6f));
    this->addChild(mBalanzaDch,5);
    
    
    mBoton=Boton::createBoton(this,menu_selector(BalanzasScene::pesar), "balanzas/boton_balanza.png");
    mBoton->setPosition(Point(mBalanza->getContentSize().width/2,mBalanza->getContentSize().height /*-mBoton->getContentSize().height/2*/));
    mBalanza->addChild(mBoton,6);
    mBoton->setSound("sounds/serieslogic/insert_letter.mp3");
    mBoton->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.8f, 10.f), RotateBy::create(0.8f, -10.f))));
    
    Sprite* numero_pesadas=Sprite::create("balanzas/numero_intentos.png");
    numero_pesadas->setPosition(Point(mBoton->getContentSize().width/2,0));

    mLabelNumPesadas= Label::createWithTTF("3", CCGetFont(), 50);
    mLabelNumPesadas->setColor(Color3B(255,54,97));
    numero_pesadas->addChild(mLabelNumPesadas);
    mLabelNumPesadas->setPosition(Point(numero_pesadas->getContentSize().width/2,numero_pesadas->getContentSize().height*0.4f));
    mLabelNumPesadas->enableOutline(Color4B(0,0,0,255),3);
    mBoton->addChild(numero_pesadas);
    
    
    mCakeSeleccionado=NULL;
    
    mNumeroPesadas=3;
    mFinPesadas=false;

    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_BALANZAS");
    
    return true;
}

bool BalanzasScene::isMoving(){
    return mIsMoving;
}



Balanza* BalanzasScene::checkIfIstouching(Cake* sender){
    
    Rect senderRect;
    
    senderRect = Rect(sender->getPosition().x - (sender->getBoundingBox().size.width*0.5f/2.f),
                            sender->getPosition().y - (sender->getBoundingBox().size.height*1.2f/2.f),
                            sender->getBoundingBox().size.width*0.5f,
                            sender->getBoundingBox().size.height*1.2f);
   Rect balanzaLeftRect = Rect(mBalanzaIzq->getPosition().x - (mBalanzaIzq->getBoundingBox().size.width/2),
                                mBalanzaIzq->getPosition().y - (mBalanzaIzq->getBoundingBox().size.height/2),
                                mBalanzaIzq->getBoundingBox().size.width,
                                mBalanzaIzq->getBoundingBox().size.height);
    
    Rect balanzaRightRect = Rect(mBalanzaDch->getPosition().x - (mBalanzaDch->getBoundingBox().size.width/2),
                                    mBalanzaDch->getPosition().y - (mBalanzaDch->getBoundingBox().size.height/2),
                                    mBalanzaDch->getBoundingBox().size.width,
                                    mBalanzaDch->getBoundingBox().size.height);
    
    
    if (balanzaLeftRect.intersectsRect(senderRect))
    {
        return mBalanzaIzq;
        
    }
    else if (balanzaRightRect.intersectsRect(senderRect))
    {
        return mBalanzaDch;
        
    }
    else{
        return NULL;
    }
}

void BalanzasScene::getCakesBalanza(int& izquierda,int& derecha)
{
    izquierda=0;
    derecha=0;
    Object *pElement;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
                if(cake->getBalanza()==mBalanzaIzq){
                    izquierda++;
                }else if(cake->getBalanza()==mBalanzaDch){
                    derecha++;
                }
            }
    }
    
}

void BalanzasScene::cambiarDiferenteAbajo(){
    
    int izquierda;
    int derecha;
    int base;
    
    getCakesBalanza(izquierda,derecha);
    base=mCakes->count() - izquierda-derecha;
    
    
    srand(time(0));
    int randomAbajo= rand() % base;
    int randomArriba=rand() % izquierda;
    
    int index=0;
    
    Object *pElement;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()==NULL ){
            if(index==randomAbajo){
                cake->setPeso(mPesoDiferente);
                CCLOG("La diferente pasa a ser=> %i",cake->mNumero);
            }else{
                cake->setPeso(100);
            }
            index++;
        }else{
            
            cake->setPeso(100);
        }
    }
}

int BalanzasScene::getCakesSinPesar(){
    Object *pElement;
    int devolver=0;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(!cake->getHaSidoPesada()){
            devolver++;
        }
    }
    return devolver;
}


void BalanzasScene::cambiarDiferenteASinPesar()
{
    bool cambiada=false;
    Object *pElement;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(!cambiada && !cake->getImposibleSerLaMala()){
            cambiada=true;
            cake->setPeso(mPesoDiferente);
        }else{
            cake->setPeso(100);
        }
    }
}

void BalanzasScene::getPesoBalanza(int& izquierda,int& derecha){
    izquierda=0;
    derecha=0;
    Object *pElement;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
            if(cake->getBalanza()==mBalanzaIzq){
                izquierda+=cake->getPeso();
            }else {
                derecha+=cake->getPeso();
            }
            cake->setHaSidoPesada();
        }
    }
}

int BalanzasScene::getNumeroImposibles(int& izquierda,int& derecha){
    izquierda=0;
    derecha=0;
    Object *pElement;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
            if(cake->getBalanza()==mBalanzaIzq){
                if(cake->getImposibleSerLaMala()){
                    izquierda++;
                }
            }else {
                if(cake->getImposibleSerLaMala()){
                    derecha++;
                }
            }
        }
    }
    
}

//cuando se pesan4 y 4 en la primera pesada y justo acieta a que la diferente esta abajo
void BalanzasScene::cambiarDiferenteArriba(){
    CCLOG("cambiarDiferenteArriba");
    bool cambiada=false;
    srand(time(0));
    int randomArriba=rand() % 8;
    
    Object *pElement;
    int index=0;
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
            if(randomArriba==index){
                cambiada=true;
                cake->setPeso(mPesoDiferente);
                CCLOG("cambiarDiferenteArriba pasa a ser la =>%i",cake->mNumero);
            }
           index++;
        }else{
            cake->setPeso(100);
        }
        
    }
}

void BalanzasScene::pesar(Ref* sender){

    CCLOG("Pesar");
    if(mIsMoving){
        CCLOG("Is moving");
        return;
    }
    if(mFinPesadas){
        CCLOG("mFinPesadas true");
        return;
    }
    Object *pElement;
    mIsMoving=true;
    int pesoBalanzaI=0;
    int pesoBalanzaD=0;
    int izquierdaCakes,derechaCakes;
    
    getCakesBalanza(izquierdaCakes,derechaCakes);
    getPesoBalanza(pesoBalanzaI, pesoBalanzaD);
    
    
   
    
    if(mNumeroPesadas==3){
        //Es la primera pesada
        //Si en las dos pesas hay el mismo numero de pasteles
        //Son mayores o iguales a 4
        //La diferente siempre esta abajo
        //pesan distinto
       
        if(izquierdaCakes ==derechaCakes && izquierdaCakes<3 && pesoBalanzaD!=pesoBalanzaI){
            cambiarDiferenteAbajo();
        }
        if(izquierdaCakes==derechaCakes && izquierdaCakes==4 && pesoBalanzaD==pesoBalanzaI){
            //si estan las 5 y pesan todas igual asignamos a cualquierda de las que estan siendo pesadas
             cambiarDiferenteArriba();
        }
        
        
    }
    if(mNumeroPesadas==2){
        int izquierdaImposibles;
        int derechaImposibles;
     
        getNumeroImposibles(izquierdaImposibles,derechaImposibles);
        //si a uno de los dos lados hay solo una no imposible
        //busco otro que sea posible para asignarle el peso dferente
        
        //caso de uno a derechas posible
        if(izquierdaCakes>0 && izquierdaCakes==izquierdaImposibles){
            if(derechaCakes - derechaImposibles==1){
                 cambiarDiferenteASinPesar();
            }
        }
        
        //caso de a izquierdas uno posible
        if(derechaCakes>0 && derechaCakes==derechaImposibles){
            if(izquierdaCakes - izquierdaImposibles==1){
                cambiarDiferenteASinPesar();
            }
        }
        
    }
    
    getPesoBalanza(pesoBalanzaI, pesoBalanzaD);
    /*if(mNumeroPesadas==1){
        //si queda alguna sin pensar abajo se le asigna el peso diferente
        if(getCakesSinPesar()>2){
            CCLOG("cambiarDiferenteASinPesar");
            cambiarDiferenteASinPesar();
        }
        
    }*/

    
    
    
    
    
    mBoton->setOpacity(0.f);
    LayerBlock* layer=LayerBlock::create();
    addChild(layer);
    layer->setTag(LayerBlock::TAG_LAYER_BLOCK);
    Sprite* pesaje=Sprite::create("balanzas/boton_balanza_pesaje.png");
    pesaje->setPosition(mBoton->getPosition());
    pesaje->setTag(666);
    mBalanza->addChild(pesaje);
    
    Sprite* aguja=Sprite::create("balanzas/boton_balanza_aguja.png");
    aguja->setPosition(mBoton->getPosition());
    pesaje->setTag(667);
    mBalanza->addChild(aguja);
   // aguja->setPosition(Point(mBoton->getContentSize().width*0.5f,0));
    
    CCLOG("peso balanza %i %i",pesoBalanzaI,pesoBalanzaD);
    DelayTime * delayM = DelayTime::create(1.5f);
    mBoton->stopAllActions();
    mBoton->setRotation(0.f);
    if(pesoBalanzaI==pesoBalanzaD){
        
        CallFunc* finmoveB=CallFunc::create([this]() { this->finMoveB(); });
        mBalanzaIzq->runAction(Sequence::create(MoveBy::create(0.5f, Point(0,-25)),delayM,finmoveB,NULL));
        mBalanzaDch->runAction(Sequence::create(MoveBy::create(0.5f, Point(0,-25)),delayM,NULL));
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            if(cake->getBalanza()!=NULL){
                    cake->runAction(MoveBy::create(0.5f, Point(0,-25)));
                    cake->setImposibleSerLaMala();
                }
        }
        aguja->runAction(RotateBy::create(0.5f, 0.f));
        
    }
    else if(pesoBalanzaI>pesoBalanzaD)
    {
        CallFunc* finmoveI=CallFunc::create([this]() { this->finMoveI(); });
        mBalanzaIzq->runAction(Sequence::create(MoveBy::create(0.5f, Point(0,-75)),delayM,finmoveI,NULL));
        int i=0;
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            if(cake->getBalanza()!=NULL){
                if(cake->getBalanza()==mBalanzaIzq){
                   cake->runAction(MoveBy::create(0.5f, Point(0,-75)));
                }
                else{
                    CCLOG("Balanza derecha");
                }
            }
            else{
                
                CCLOG("Balanza NULL:%i",i);
                i++;
            }

            
            
        }
        
        aguja->runAction(RotateBy::create(0.5f, -50));
    }else if(pesoBalanzaI<pesoBalanzaD)
    {
        CallFunc* finmoveD=CallFunc::create([this]() { this->finMoveD(); });
        mBalanzaDch->runAction(Sequence::create(MoveBy::create(0.5f, Point(0,-75)),delayM,finmoveD,NULL));
        
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            if(cake->getBalanza()!=NULL){
                if(cake->getBalanza()==mBalanzaDch){
                    cake->runAction(MoveBy::create(0.5f, Point(0,-75)));
                }
            }
        }
        aguja->runAction(RotateBy::create(0.5f, 50));
    }
    mNumeroPesadas--;
    char buff[100];
    snprintf(buff, sizeof(buff), "%i", mNumeroPesadas);
    std::string buffAsStdStr = buff;
    
    mLabelNumPesadas->setString(buffAsStdStr.c_str());
  
  
    
    
}

void BalanzasScene::nada(){
    
}

void BalanzasScene::finMove(){
    this->removeChildByTag(LayerBlock::TAG_LAYER_BLOCK);
    mBalanza->removeChildByTag(666);
    mBalanza->removeChildByTag(667);
    mBoton->setOpacity(255.f);
    CCLOG("finMove");
    mIsMoving=false;
    mBoton->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.8f, 10.f), RotateBy::create(0.8f, -10.f))));
    //mBoton->removeAllChildren();
    if(mNumeroPesadas==0){
        //iniciamos el proceso de elegir el cke
        CCLOG("CAKEE");
        mFinPesadas=true;
        mostrarPanelEleccionCake(true);
    }
}


void BalanzasScene::finMoveB(){
    CallFunc* finmove=CallFunc::create([this]() { this->finMove(); });
    Object *pElement;
    mBalanzaIzq->runAction(Sequence::createWithTwoActions(MoveBy::create(0.5f, Point(0,25)),finmove));
    mBalanzaDch->runAction(MoveBy::create(0.5f, Point(0,25)));
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
                cake->runAction(MoveBy::create(0.5f, Point(0,25)));
        }
    }
}

void BalanzasScene::finMoveI(){
    CallFunc* finmove=CallFunc::create([this]() { this->finMoveB(); });
    Object *pElement;
    mBalanzaIzq->runAction(Sequence::createWithTwoActions(MoveBy::create(0.5f, Point(0,75)),finmove));
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
            if(cake->getBalanza()==mBalanzaIzq){
                cake->runAction(MoveBy::create(0.5f, Point(0,75)));
            }
        }
    }
}
void BalanzasScene::finMoveD(){
    CallFunc* finmove=CallFunc::create([this]() { this->finMoveB(); });
   Object *pElement;
    mBalanzaDch->runAction(Sequence::createWithTwoActions(MoveBy::create(0.5f, Point(0,75)),finmove));
    CCARRAY_FOREACH(mCakes, pElement)
    {
        Cake* cake=(Cake*)pElement;
        if(cake->getBalanza()!=NULL){
            if(cake->getBalanza()==mBalanzaDch){
                cake->runAction(MoveBy::create(0.5f, Point(0,75)));
            }
        }
    }
}

bool BalanzasScene::getFinPesadas()
{
    return mFinPesadas;
}
bool BalanzasScene::getEleccionPeso(){
    return mFinEleccion;
}

void BalanzasScene::selectDifferentCake(Cake* cake){
    if(!mFinPesadas){
        return;
    }
    CCLOG("Peso de la seleccionada:%i",cake->getPeso());
    if(cake->getPeso()==mPesoDiferente){
        mFinEleccion=true;
        //mostrar panel de pesada /ligera
        Object* pElement;
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            cake->stopAllActions();
            
        }
        mCakeSeleccionado=cake;
        mostrarPanelEleccionPeso(true);
    }else{
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("BALANZAS_WRONG");
        mGameover=true;
        GameOver();
    }
}

void BalanzasScene::mostrarPanelEleccionCake(bool visible){
    if(visible){
        LayerColor* layerC=LayerColor::create(Color4B(185,68,96,78), mVisibleSize.width, mVisibleSize.height*0.20f);
        layerC->setPosition(Point(0 , mVisibleSize.height*0.8f ));
        this->addChild(layerC,7);
        layerC->setTag(572);
        Label* label=Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("BALANZAS_SELECT_DIFFERENT", "BALANZAS_SELECT_DIFFERENT"), CCGetFont(), 23);
        
        //CCLabelTTF::create(LanguageManager::getInstance()->getStringForKey("BALANZAS_SELECT_DIFFERENT", "BALANZAS_SELECT_DIFFERENT"), CCGetFont(), 23);
        label->enableOutline(Color4B(0,0,0,255),2);
        label->setColor(Color3B(255,255,255));
        layerC->addChild(label);
        label->setPosition(Point(layerC->getContentSize().width/2,layerC->getContentSize().height/2));
        Object *pElement;
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            cake->runAction(RepeatForever::create(JumpBy::create(0.5f, Point(0,0),20 , 1)));
        }
       
    }else{
        this->removeChildByTag(572);
        Object *pElement;
        CCARRAY_FOREACH(mCakes, pElement)
        {
            Cake* cake=(Cake*)pElement;
            cake->stopAllActions();
            
        }
        
        
    }
  
}
void BalanzasScene::heavier(Ref* sender){
    CCLOG("heavier");
    if(!mCakeSeleccionado->getHaSidoPesada())
    {
        CCLOG("No ha sido pesada");
        mGameover=true;
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("BALANZAS_LIGHTER");
    }
    LayerInfo::ocultar(this);
    if(mCakeSeleccionado->getPeso()==mPesoDiferente){
        mSuccess=true;
    }else{
        mGameover=true;
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("BALANZAS_LIGHTER");
    }
     checkFinish();
    
}
void BalanzasScene::lighter(Ref* sender){
    CCLOG("lighter");
    
    if(!mCakeSeleccionado->getHaSidoPesada())
    {
        CCLOG("No ha sido pesada");
        mGameover=true;
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("BALANZAS_HEAVIER", "BALANZAS_HEAVIER");
    }
    LayerInfo::ocultar(this);
    if(mCakeSeleccionado->getPeso()==mPesoDiferente){
        mSuccess=true;

    }else{
        mGameover=true;
        mTextoGameOver=LanguageManager::getInstance()->getStringForKey("BALANZAS_HEAVIER", "BALANZAS_HEAVIER");
        
    }
    checkFinish();
}


void BalanzasScene::mostrarPanelEleccionPeso(bool visible){
    LayerInfo* layer=LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("BALANZAS_HEAVIER_LIGHTER", "BALANZAS_HEAVIER_LIGHTER"));
    layer->setNoOcultable();
    Sprite* fondo=(Sprite*)layer->getCanvas();
    fondo->setScale(1.6f);
    Boton* heavier=Boton::createBoton(this, menu_selector(BalanzasScene::heavier), "balanzas/weighter.png");
    Boton* lighter=Boton::createBoton(this, menu_selector(BalanzasScene::lighter), "balanzas/lighter.png");
    fondo->addChild(heavier);
    fondo->addChild(lighter);
    heavier->setPosition(Point(fondo->getContentSize().width*0.3,fondo->getContentSize().height*0.45f));
    lighter->setPosition(Point(fondo->getContentSize().width*0.7,fondo->getContentSize().height*0.45f));
    heavier->setScale(0.5f);
    lighter->setScale(0.5f);

    fondo->setOpacity(180.f);
}



BalanzasScene* BalanzasScene::getInstance(){
    return empty_instancia;
};

void BalanzasScene::checkFinish(){
    BaseScene::checkFinish(NULL);
};


void BalanzasScene::Retry(Ref* sender){
    Scene* scene=BalanzasScene::scene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
}

void BalanzasScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void BalanzasScene::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        CCLOG("MOSTRAR");
        std::string texto=LanguageManager::getInstance()->getStringForKey("HINT_BALANZAS");
        LayerHint::mostrar(texto);
    }else{
        CCLOG("CANCELADO");
    }
}



void BalanzasScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/volcan/forest.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}

