//
//  Balanza.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Balanza__
#define __logicmaster2__Balanza__

#include <iostream>
#include "../widgets/Boton.h"
#include "BalanzasScene.h"





class Balanza: public Sprite
{
    
    
    public:
    
        Balanza(int sitiosBalanza,std::string descripcion);
        virtual ~Balanza(void);
        static cocos2d::Scene* scene();
        void initBalanza();
        static Balanza* create(int sitiosBalanza,std::string descripcion);
        void setMoving(bool moving);
        bool isMoving();
        bool isSelected();
        void up();
        void down();
        Point getSitio();
        int getSitioIndex();
        ValueMapIntKey mSitios;
        int aumentarSitio();
        void disminuirSitio(int sitio);
        int getSitiosLibres();
        void debug();
        std::string getDescripcion();
    private:
        void finMoving();
        bool mIsMoving;
        bool mSelected;
        int mSitiosBalanza;
        std::string mDescripcion;

};


#endif /* defined(__logicmaster2__Balanza__) */
