//
//  Cake.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Cake.h"
#include "SimpleAudioEngine.h"
#include "BalanzasScene.h"
#include <stdio.h>
#include "Balanza.h"


Cake::Cake(int numero,int peso){
    mIsMoving=false;
    mNumero=numero;
    mBalanzaSobreLaQueEsta=NULL;
    mSitioBalanza=-1;
    mPeso=peso;
    mHaSidoPesada=false;
    mImposibleSerLaMala=false;
}

Cake::~Cake(void){}


Cake* Cake::create(int numero,int peso)
{
    Cake *devolver= new Cake(numero,peso);
    devolver->initWithTexture(Director::getInstance()->getTextureCache()->addImage("balanzas/cake.png"));
    devolver->autorelease();
    devolver->initCake(numero);
    return devolver;
}


void Cake::initCake(int numero)
{

    Size size=this->getContentSize();
    Color3B color;
    if(numero==0){
        color=Color3B(255,0,0);
    }else if(numero==1){
        color=Color3B(255,128,0);
    }else if(numero==2){
        color=Color3B(255,255,0);
    }else if(numero==3){
        color=Color3B(128,255,200);
    }else if(numero==4){
        color=Color3B(0,255,255);
    }else if(numero==5){
        color=Color3B(0,0,255);
    }else if(numero==6){
        color=Color3B(127,0,255);
    }else if(numero==7){
        color=Color3B(255,0,255);
    }else if(numero==8){
        color=Color3B(0,0,0);
    }
    //mColorCake=Sprite::create("balanzas/cake_cover.png");
    //mColorCake->setColor(color);
    //mColorCake->setPosition(Point(size.width/2,size.height/2));
    //this->addChild(mColorCake,-1);
    
    Label* label=Label::createWithTTF(tostr(numero).c_str(), CCGetFont(), 25);
    label->setWidth(this->getContentSize().width);
    label->setWidth(this->getContentSize().height);
    label->setColor(Color3B(251,219,92));
    label->enableOutline(Color4B(85,65,25,255.f));
    label->setAlignment(TextHAlignment::CENTER,TextVAlignment::CENTER);
    label->enableOutline(Color4B(0.f,0.f,0.f,255.f),2);
    label->setPosition(Point(this->getContentSize().width*0.5f,this->getContentSize().height*0.75f));
    this->addChild(label,3);
  
    mSelected=false;
    
}





void Cake::onEnter()
{
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(Cake::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(Cake::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(Cake::onTouchEnded, this);
    
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mListener, this);
    Sprite::onEnter();
}

void Cake::onExit()
{
     this->getEventDispatcher()->removeEventListener(mListener);
   
    
    Sprite::onExit();
}



bool Cake::onTouchBegan(Touch* touch, Event* event)
{
    if(BalanzasScene::getInstance()->isMoving()){
        return false;
    }
    if ( !containsTouchLocation(touch) ){
        return false;
    }
    else{
         CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop.mp3",false);
        return true;
    }
}

void Cake::onTouchMoved(Touch* touch, Event* event)
{
    
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
    this->setPosition(puntonuevo);
}

void Cake::onTouchEnded(Touch* touch, Event* event)
{
    if(BalanzasScene::getInstance()->getFinPesadas()){
        BalanzasScene::getInstance()->selectDifferentCake(this);
        CCLOG("getFinPesadas");
        return;
        
    }
    if(BalanzasScene::getInstance()->getEleccionPeso()){
        CCLOG("getEleccionPeso");
        return ;
    }

    
    if(mBalanzaSobreLaQueEsta!=NULL)
    {
        mBalanzaSobreLaQueEsta->disminuirSitio(mSitioBalanza);
        mBalanzaSobreLaQueEsta=NULL;
        mSitioBalanza=-1;
    }
    Point puntonuevo=Director::getInstance()->convertToGL(touch->getLocationInView());
    Balanza* balanzaColision =BalanzasScene::getInstance()->checkIfIstouching(this);
    if(balanzaColision!=NULL){
        if(balanzaColision->getSitiosLibres()>0){
            this->setPosition(balanzaColision->getSitio());
            mSitioBalanza=balanzaColision->aumentarSitio();
            mBalanzaSobreLaQueEsta=balanzaColision;
            CCLOG("%s",mBalanzaSobreLaQueEsta->getDescripcion().c_str());
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/pop_grave.mp3",false);
            
        }else{
            CCLOG("NO hay sitio libre");
            this->runAction(MoveBy::create(0.2f, Point(0,-200)));
        }
      
    }
    
}

bool Cake::containsTouchLocation(Touch* touch)
{
    Size s = getTexture()->getContentSize();
    return  Rect(-s.width / 2, -s.height / 2, s.width, s.height).containsPoint(convertTouchToNodeSpaceAR(touch));
}

int Cake::getPeso(){
    //CCLOG("PESO:%i",mPeso);
    return mPeso;
}

void Cake::setPeso(int peso){
    mPeso=peso;
   // CCLOG("PESO:%i",mPeso);
}


Balanza* Cake::getBalanza(){
    return mBalanzaSobreLaQueEsta;
}


bool Cake::getHaSidoPesada(){
    return mHaSidoPesada;
}
void Cake::setHaSidoPesada(){
    mHaSidoPesada=true;
}

void Cake::setImposibleSerLaMala(){
    mImposibleSerLaMala=true;
}

bool Cake::getImposibleSerLaMala(){
    return mImposibleSerLaMala;
}


template <typename T> std::string Cake::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }



