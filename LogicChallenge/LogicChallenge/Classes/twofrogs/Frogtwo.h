//
//  Frogtwo.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Frogtwo__
#define __logicmaster2__Frogtwo__

#include <iostream>
#include "../widgets/Boton.h"
#include "TwoFrogs.h"

class Frogtwo: public Boton
{
    
public:
    //typedef std::map< int, Frogtwo*> PiedrasMap;
    
    Frogtwo(bool esVerde,int posicion);
    virtual ~Frogtwo(void);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    
    static Frogtwo* create(Node* parent,std::string imagen,bool esVerde,int posicion);
 
    void moveToStone(Ref* sender);
    
    static Point posicionPiedra(int index);
    bool esverde();
private:
    bool mIsMoving;
    bool mEsVerde;
    int mPosicion;
    void finMovement();
    
};


#endif /* defined(__logicmaster2__Frogtwo__) */
