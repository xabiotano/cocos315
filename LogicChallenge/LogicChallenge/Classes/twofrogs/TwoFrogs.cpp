#include "TwoFrogs.h"
#include "SimpleAudioEngine.h"
#include "Frogtwo.h"

using namespace cocos2d;
using namespace CocosDenshion;

TwoFrogs* instancia_frogs2;

TwoFrogs::~TwoFrogs(void){
    CC_SAFE_RELEASE(mArrayOfFrogs);
}


TwoFrogs* TwoFrogs::getInstance(){
    return instancia_frogs2;
}






Scene* TwoFrogs::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    TwoFrogs *layer = TwoFrogs::create();

    // add layer as a child to scene
    scene->addChild(layer);

    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool TwoFrogs::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_frogs2=this;
    mNombrePantalla="TwoFrogs";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("frogs/frogs_fondo2.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    mArrayOfFrogs= Dictionary::create();
    mArrayOfFrogs->retain();
    someFrogMoving=false;
    colocarRanas();
    mTextoSuccess=LanguageManager::getInstance()->getStringForKey("SUCCESS_MESSAGE", "SUCCESS_MESSAGE");
    
    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_TWO_FROGS");
    mRetryVisibleDuringGame=true;
   
    

    return true;
}



void TwoFrogs::colocarRanas()
{
    
    if(mArrayOfFrogs->count()>0){
        mArrayOfFrogs->removeAllObjects();
    }
    if(rana1!=NULL){
        rana1->removeFromParent();
    }
    if(rana2!=NULL){
        rana2->removeFromParent();
    }
    if(rana3!=NULL){
        rana3->removeFromParent();
    }
    if(rana4!=NULL){
        rana4->removeFromParent();
    }
    
    
    
    rana1=Frogtwo::create(this, "frogs/frog_green.png", true, 0);
    this->addChild(rana1);
    rana2=Frogtwo::create(this, "frogs/frog_green.png", true, 1);
    this->addChild(rana2);
    
    //2 vacia
    
    rana3=Frogtwo::create(this, "frogs/frog_brown.png", false, 3);
    this->addChild(rana3);
    rana4=Frogtwo::create(this, "frogs/frog_brown.png", false, 4);
    this->addChild(rana4);
   
    
  
  
    
    mArrayOfFrogs->setObject(rana1, 0);
    mArrayOfFrogs->setObject(rana2, 1);
    
    mArrayOfFrogs->setObject(rana3, 3);
    mArrayOfFrogs->setObject(rana4, 4);
   
    
    

}


void TwoFrogs::checkFinish(){
    if(mArrayOfFrogs->objectForKey(0)!=NULL &&
       mArrayOfFrogs->objectForKey(1)!=NULL &&
       mArrayOfFrogs->objectForKey(3)!=NULL &&
       mArrayOfFrogs->objectForKey(4)!=NULL)
    {
        
         Frogtwo* frog0=(Frogtwo*)mArrayOfFrogs->objectForKey(0);
         Frogtwo* frog1=(Frogtwo*)mArrayOfFrogs->objectForKey(1);
         Frogtwo* frog2=(Frogtwo*)mArrayOfFrogs->objectForKey(3);
         Frogtwo* frog3=(Frogtwo*)mArrayOfFrogs->objectForKey(4);
        if(!frog0->esverde() &&
           !frog1->esverde() &&
            frog2->esverde() &&
            frog3->esverde()
           )
        {
           
            mSuccess=true;
        }
        
        BaseScene::checkFinish(NULL);
    }

}



void TwoFrogs::Retry(Ref* sender){
    Scene* scene=TwoFrogs::scene();
    Director *pDirector = Director::getInstance();
     pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));
    
}


void TwoFrogs::HintStart(Ref* sender){
      // 5 4 3 5 6 7 4 3 2 1 5 6 7 8 4 3 2 1 6 7 8 2 1 8
    
    switch(mHintStep){
        case 1:
            rana2->moveToStone(NULL);
            break;
        case 2:
             rana3->moveToStone(NULL);
            break;
        case 3:
            rana4->moveToStone(NULL);
            break;
        case 4:
            rana1->moveToStone(NULL);
            break;
        case 5:
            rana2->moveToStone(NULL);
            break;
        case 6:
            rana1->moveToStone(NULL);
            break;
        case 7:
            rana3->moveToStone(NULL);
            break;
        case 8:
            rana4->moveToStone(NULL);
            break;
        case 9:
            rana1->moveToStone(NULL);
            break;
        case 10:
            rana1->moveToStone(NULL);
            break;
        }
    CCLOG("%i",mHintStep);
    mHintStep++;
    if(mHintStep<=11){
        CallFunc* funcion= CallFunc::create([this](){ this->HintStart(NULL);  });
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
    }
  
}




void TwoFrogs::HintStop(Ref* sender)
{
    mHintStep=20;
    BaseScene::HintStop(sender);
}


void TwoFrogs::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}



void TwoFrogs::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        colocarRanas();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
      
        CallFunc* funcion= CallFunc::create([this](){ this->HintStart(NULL);  });
        this->runAction(Sequence::create(DelayTime::create(2.f), funcion,NULL));
    }
}


void TwoFrogs::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/fivefrogs/jungle.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}
