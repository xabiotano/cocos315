//
//  Frogtwo.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "Frogtwo.h"
#include "SimpleAudioEngine.h"

Frogtwo::Frogtwo(bool esVerde,int posicion)
{
    mEsVerde=esVerde;
    mPosicion=posicion;
    mIsMoving=false;
    if(esVerde){
        setScaleX(-1);
    }
    
}
Frogtwo::~Frogtwo(void)
{
    
}





Frogtwo* Frogtwo::create(Node* parent,std::string imagen,bool esVerde,int posicion)
{
    Frogtwo *devolver= new Frogtwo(esVerde,posicion);
    devolver->autorelease();
    devolver->setPosition(posicionPiedra(posicion));
    devolver->initBoton(devolver, menu_selector(Frogtwo::moveToStone), imagen);
    devolver->setSound("sounds/fivefrogs/jump_water.mp3");
    return devolver;
}


void Frogtwo::moveToStone(Ref* sender)
{
    if(mIsMoving){
        return;
    }
    if(TwoFrogs::getInstance()->someFrogMoving){
        CCLOG("SOME Frogtwo MOVING");
      //  return;
    }
    
    int indexDestino=0;
    int signoDestino=1;
    if(mEsVerde)
    {
        indexDestino=mPosicion+1;
        signoDestino=1;
        
       
    }
    else{
        indexDestino=mPosicion-1;
        signoDestino=-1;
        
        
    }
   
    
    //si la siguiente y la 2 mas lejos estan llenas no muevo
    if(TwoFrogs::getInstance()->mArrayOfFrogs->objectForKey(indexDestino)!=NULL &&
       TwoFrogs::getInstance()->mArrayOfFrogs->objectForKey(indexDestino+(signoDestino))!=NULL)
       
    {
        //no se puede mover,siguiente y 2+ ocupadas
        return ;
    }
    
   
    if(TwoFrogs::getInstance()->mArrayOfFrogs->objectForKey(indexDestino)!=NULL &&  TwoFrogs::getInstance()->mArrayOfFrogs->objectForKey(indexDestino+(signoDestino))==NULL )
    {
        //mov de +2
        indexDestino+=signoDestino;
    }
    //compruebo la escala
    if(mEsVerde){
        if(indexDestino>4){
            return;
        }
        if(indexDestino<0){
            return;
        }
        
        
        
    }
    else{
        //es marron
        if(indexDestino<0){
            return;
        }
        
        
    }
    
    TwoFrogs::getInstance()->mArrayOfFrogs->removeObjectForKey(mPosicion);
    TwoFrogs::getInstance()->mArrayOfFrogs->setObject(this, indexDestino);
    this->mPosicion=indexDestino;
    Point destino=posicionPiedra(indexDestino);
    CallFunc* finMove=CallFunc::create([this]() { this->finMovement(); });
    mIsMoving=true;
    TwoFrogs::getInstance()->someFrogMoving=true;
    this->runAction(Sequence::create(JumpTo::create(0.5f, destino, Director::getInstance()->getWinSize().height*0.1f, 1), finMove,NULL));
    
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/TwoFrogs/water_noise.mp3",false);
}

Point Frogtwo::posicionPiedra(int index){
    Size mVisibleSize=Director::getInstance()->getVisibleSize();
    
    switch(index)
    {
        case 0:
            return Point((180.f /960.f) * mVisibleSize.width,0.35f*mVisibleSize.height);
            break;
        case 1:
            return Point(( 330.f /960.f) * mVisibleSize.width,0.35f*mVisibleSize.height);
             break;
        case 2:
            //VACIA
             return Point(0.5f* mVisibleSize.width,0.35f*mVisibleSize.height);
             break;
        case 3:
            return Point((680.f/960.f )* mVisibleSize.width,0.35f*mVisibleSize.height);
             break;
        case 4:
            return Point((840.f/960.f) * mVisibleSize.width,0.35f*mVisibleSize.height);
             break;
        default:
            return Point(0,0);
    }
}

bool Frogtwo::esverde(){
    return mEsVerde;
}




void Frogtwo::finMovement(){
    TwoFrogs::getInstance()->checkFinish();
    mIsMoving=false;
    TwoFrogs::getInstance()->someFrogMoving=false;
  
}






