#include "FrutasScene.h"
#include "SimpleAudioEngine.h"
#include "../layers/LayerGameOver.h"
#include "../layers/LayerSuccess.h"
#include "../Variables.h"
#include "../widgets/Boton.h"
#include "../sql/SQLiteHelper.h"
#include "../Levelsv2.h"
#include "../helpers/LogicSQLHelper.h"



using namespace cocos2d;
using namespace CocosDenshion;


FrutasScene* instancia_frutas;



Scene* FrutasScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    FrutasScene *layer = FrutasScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool FrutasScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    mNombrePantalla="FrutasScene";
    instancia_frutas=this;
    mLuchaPorItems=false;
    mTipoNivel=TipoNivel(kNivelElixir);
    mZoomActivado=false;
    mZoomActivandose=false;
    mEsPosibleCompartirEnWhatssap=true;
    
    mOpcionElegida=false;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();

    Sprite* mFondo = Sprite::create("frutas/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 1);
    
    
    mLayerOscura=LayerColor::create(Color4B(0,0,0,200.f), mVisibleSize.width, mVisibleSize.height);
    mLayerOscura->setPosition(Point(0,0));
    this->addChild(mLayerOscura, 2);
    mLayerOscura->setOpacity(0.f);
    
    
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
   
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    
    //mNumeroPrueba=Variables::LEVELS_ELIXIR_CLICKADO;
 
    
    
   
   
    
   srand(time(NULL));
    mTipoAnuncio= rand() % 10;
    CCLOG("mTipoAnuncio %i",mTipoAnuncio);
    
    return true;
}


void FrutasScene::cargarPrueba(){
    SQLiteHelper::openDB();
    char sqlgenerado [400];
    int idNivel=Variables::LEVELS_ELIXIR_CLICKADO;
    sprintf (sqlgenerado, "SELECT * from niveles where idnivel=%i",idNivel);
    SQLiteHelper::rc = sqlite3_exec(SQLiteHelper::_db, sqlgenerado, cargarNiveltexto,this, &SQLiteHelper::zErrMsg);
    CCLOG("%s",sqlgenerado);
    if( SQLiteHelper::rc!=SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", SQLiteHelper::zErrMsg);
        sqlite3_free(SQLiteHelper::zErrMsg);
    }

}
// This is the callback function to display the select data in the table
int FrutasScene::cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName)
{
    
    FrutasScene* frutasScene= (FrutasScene*) NotUsed;
    for(int i=0; i<argc; i++)
    {
        if(strcmp( azColName[i],"textonivel")==0){
            frutasScene->mNumeroPrueba=atoi(argv[i]);
        }else if(strcmp( azColName[i],"solucion")==0){
            frutasScene->mSolucionStr=argv[i];
        }else if(strcmp( azColName[i],"superado")==0){
            frutasScene->mSuperado=(atoi(argv[i])==1);
        }else if(strcmp( azColName[i],"estrellas")==0){
            frutasScene->mNumStars=atoi(argv[i]);
        }else if(strcmp( azColName[i],"intentos")==0){
            frutasScene->mIntentos=atoi(argv[i]);
        }else if(strcmp( azColName[i],"pattern")==0){
            frutasScene->mPattern=argv[i];
            CCLOG("pattern %s",frutasScene->mPattern.c_str());
        }
    }
    frutasScene->crearEscena();
   frutasScene->cargarFrutas();
    return 0;
}



void FrutasScene::crearEscena(){

    mPanel=Sprite::create("frutas/panel.png");
    mPanel->setPosition(Point(mVisibleSize.width*0.5f, mVisibleSize.height - mPanel->getContentSize().height/2));
    this->addChild(mPanel,30);
    
    
    mZoom=Boton::createBoton(this, menu_selector(FrutasScene::zoom), "elixircomun/zoom.png");
    mZoom->setPosition(Point(mPanel->getContentSize().width-mZoom->getContentSize().width/2,mPanel->getContentSize().height-mZoom->getContentSize().height/2));
    mPanel->addChild(mZoom);
    mZoom->setSound("sounds/serieslogic/zoom.mp3");
    
    mShowButtons=Boton::createBoton(this, menu_selector(FrutasScene::showButtons), "frutas/solve_button.png");
    mShowButtons->setPosition(Point(mVisibleSize.width*0.5f,mVisibleSize.height*0.25f));
    mShowButtons->setTexto(LanguageManager::getInstance()->getStringForKey("SOLVE_BUTTON", "SOLVE_BUTTON"));
    mShowButtons->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateTo::create(0.75f,2),RotateTo::create(0.75f,-2))));
    mShowButtons->setSound("sounds/serieslogic/insert_letter.mp3");
    this->addChild(mShowButtons,31);
    
    char memory[200];
    sprintf(memory,"frutas/pruebas/%i.png",mNumeroPrueba);
    
    Sprite* prueba=Sprite::create(memory);
    prueba->setPosition(Point(mPanel->getContentSize().width*0.5f,mPanel->getContentSize().height*0.6f));
    mPanel->addChild(prueba);
    float scale=1.f;
    
    float scalex=mPanel->getContentSize().width*0.9f/prueba->getContentSize().width;
    float scaley=mPanel->getContentSize().height*0.9f/prueba->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    prueba->setScale(scaley);
    
    CCLOG("Escalar prueba %f",scale);
    prueba->setScale(scale);
    
}




void FrutasScene::showButtons(Ref* sender){
    
    if(mLayerBotones!=NULL){
        mLayerBotones->removeFromParent();
        mLayerBotones=NULL;
    }
    mLayerOscura->runAction(FadeTo::create(0.5f, 200.f) );
    mLayerBotones=LayerColor::create(Color4B(0,0,0,0),  mVisibleSize.width, mVisibleSize.width*0.4f);
    mLayerBotones->setPosition(Point(0,-mLayerBotones->getContentSize().height/2));
 
    
    mBoton1=Boton::createBoton(this, menu_selector(FrutasScene::opcion1),"frutas/boton_madera.png" );
    mBoton2=Boton::createBoton(this, menu_selector(FrutasScene::opcion2),"frutas/boton_madera.png" );
    mBoton3=Boton::createBoton(this, menu_selector(FrutasScene::opcion3),"frutas/boton_madera.png" );
    mBoton4=Boton::createBoton(this, menu_selector(FrutasScene::opcion4),"frutas/boton_madera.png" );
   
    
    mBoton1->setPosition(Point(mLayerBotones->getContentSize().width*0.125f,mLayerBotones->getContentSize().height*0.5f));
    mBoton2->setPosition(Point(mLayerBotones->getContentSize().width*0.375f,mLayerBotones->getContentSize().height*0.5f));
    mBoton3->setPosition(Point(mLayerBotones->getContentSize().width*0.625f,mLayerBotones->getContentSize().height*0.5f));
    mBoton4->setPosition(Point(mLayerBotones->getContentSize().width*0.875f,mLayerBotones->getContentSize().height*0.5f));

    
    mBoton1->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton2->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton3->setSound("sounds/ui/elegirrespuesta.mp3");
    mBoton4->setSound("sounds/ui/elegirrespuesta.mp3");
    
    mBoton1->setOpacity(255);
    mBoton2->setOpacity(255);
    mBoton3->setOpacity(255);
    mBoton4->setOpacity(255);
    
    
    std::vector<std::string> soluciones = split(mPattern, ':');
    std::string solucion1=soluciones.at(0);
    std::string solucion2=soluciones.at(1);
    std::string solucion3=soluciones.at(2);
    std::string solucion4=soluciones.at(3);

    if(mSolucionStr==solucion1){
        mSolucion=1;
    }if(mSolucionStr==solucion2){
        mSolucion=2;
    }if(mSolucionStr==solucion3){
        mSolucion=3;
    }if(mSolucionStr==solucion4){
        mSolucion=4;
    }



    
   
    /*mOpcion1Text=CCLabelStroke::create(mBoton1, "", CCGetFont(), 60, mBoton1->getContentSize(), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mOpcion1Text->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mBoton1->addChild(mOpcion1Text,2);
    mOpcion1Text->setString(solucion1.c_str(), Color3B(0,0,0));*/
    
    mOpcion1Text= Label::createWithTTF(solucion1.c_str(), CCGetFont(), 60);
    mOpcion1Text->setColor(Color3B(255,255,255));
    mOpcion1Text->setWidth(mBoton1->getContentSize().width);
    mOpcion1Text->setHeight(mBoton1->getContentSize().height);
    mOpcion1Text->setPosition(Point(mBoton1->getContentSize().width/2,mBoton1->getContentSize().height/2));
    mOpcion1Text->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mOpcion1Text->enableOutline(Color4B(0,0,0,255),2);
    mBoton1->addChild(mOpcion1Text,2);

    
    
   /* mOpcion2Text=CCLabelStroke::create(mBoton2, "", CCGetFont(), 60, mBoton2->getContentSize(), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mOpcion2Text->setPosition(Point(mBoton2->getContentSize().width/2,mBoton2->getContentSize().height/2));
    mBoton2->addChild(mOpcion2Text,2);
    mOpcion2Text->setString(solucion2.c_str(), Color3B(0,0,0));*/
    mOpcion2Text= Label::createWithTTF(solucion2.c_str(), CCGetFont(), 60);
    mOpcion2Text->setColor(Color3B(255,255,255));
    mOpcion2Text->setWidth(mBoton2->getContentSize().width);
    mOpcion2Text->setHeight(mBoton2->getContentSize().height);
    mOpcion2Text->setPosition(Point(mBoton2->getContentSize().width/2,mBoton2->getContentSize().height/2));
    mOpcion2Text->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mOpcion2Text->enableOutline(Color4B(0,0,0,255),2);
    mBoton2->addChild(mOpcion2Text,2);
    
    
   /* mOpcion3Text=CCLabelStroke::create(mBoton3, "", CCGetFont(), 60, mBoton3->getContentSize(), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mOpcion3Text->setPosition(Point(mBoton3->getContentSize().width/2,mBoton3->getContentSize().height/2));
    mBoton3->addChild(mOpcion3Text,2);
    mOpcion3Text->setString(solucion3.c_str(), Color3B(0,0,0));*/
    mOpcion3Text= Label::createWithTTF(solucion3.c_str(), CCGetFont(), 60);
    mOpcion3Text->setColor(Color3B(255,255,255));
    mOpcion3Text->setWidth(mBoton3->getContentSize().width);
    mOpcion3Text->setHeight(mBoton3->getContentSize().height);
    mOpcion3Text->setPosition(Point(mBoton3->getContentSize().width/2,mBoton3->getContentSize().height/2));
    mOpcion3Text->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mOpcion3Text->enableOutline(Color4B(0,0,0,255),2);
    mBoton3->addChild(mOpcion3Text,2);
    
    
    /*mOpcion4Text=CCLabelStroke::create(mBoton4, "", CCGetFont(), 60, mBoton4->getContentSize(), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mOpcion4Text->setPosition(Point(mBoton4->getContentSize().width/2,mBoton4->getContentSize().height/2));
    mBoton4->addChild(mOpcion4Text,2);
    mOpcion4Text->setString(solucion4.c_str(), Color3B(0,0,0));*/
    mOpcion4Text= Label::createWithTTF(solucion4.c_str(), CCGetFont(), 60);
    mOpcion4Text->setColor(Color3B(255,255,255));
    mOpcion4Text->setWidth(mBoton4->getContentSize().width);
    mOpcion4Text->setHeight(mBoton4->getContentSize().height);
    mOpcion4Text->setPosition(Point(mBoton4->getContentSize().width/2,mBoton4->getContentSize().height/2));
    mOpcion4Text->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mOpcion4Text->enableOutline(Color4B(0,0,0,255),2);
    mBoton4->addChild(mOpcion4Text,2);
    
    Boton* close=Boton::createBoton(this, menu_selector(FrutasScene::closeButtons),"frutas/close_button.png");
    close->setPosition(Point(mLayerBotones->getContentSize().width*0.9f,mLayerBotones->getContentSize().height*0.95f));
    mLayerBotones->addChild(close,2);
    close->setSound("sounds/ui/popup_close.mp3");
    close->runAction(RepeatForever::create(Sequence::createWithTwoActions(RotateBy::create(0.5f, 15), RotateBy::create(0.5f, -15))));
    
    
    mLayerBotones->addChild(mBoton1);
    mLayerBotones->addChild(mBoton2);
    mLayerBotones->addChild(mBoton3);
    mLayerBotones->addChild(mBoton4);
    
    mLayerBotones->runAction(MoveTo::create(0.5f, Point(0,0)));
    mShowButtons->runAction(MoveTo::create(0.5f, Point(mShowButtons->getPositionX(),-mShowButtons->getContentSize().height/2)));
    
    this->addChild(mLayerBotones,32);
    
}

void FrutasScene::opcion1(Ref* sender){
    opcionClickada(1);
}
void FrutasScene::opcion2(Ref* sender){
     opcionClickada(2);
}
void FrutasScene::opcion3(Ref* sender){
     opcionClickada(3);
}
void FrutasScene::opcion4(Ref* sender){
     opcionClickada(4);
}
void FrutasScene::opcionClickada(int opcion){
    if(mOpcionElegida){
        return;
    }
    
    mOpcionElegida=true;
    CCLOG("%i",opcion);
    if(opcion==mSolucion){
        mSuccess=true;
    }else{
        
        mGameover=true;
        mTextoGameOver=std::string(LanguageManager::getInstance()->getStringForKey("FrutasScene_GAMEOVER","Sorry, you choosed a incorrect answer"));
    }
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(FrutasScene::checkFinish,this));
    this->runAction(Sequence::create(DelayTime::create(0.2f), funcion,NULL));
    //this->scheduleOnce(schedule_selector(FrutasScene::checkFinish),0.2f);
}


void FrutasScene::closeButtons(Ref* sender){
    mLayerBotones->runAction(MoveTo::create(0.5f, Point(mLayerBotones->getPositionX(),-mVisibleSize.height)));
    CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(FrutasScene::removePanel,this));
    this->runAction(Sequence::create(DelayTime::create(0.5f), funcion,NULL));
    

    //this->scheduleOnce(schedule_selector(FrutasScene::removePanel), 0.5f);
    mLayerOscura->runAction(FadeOut::create(0.5f));

}

void FrutasScene::removePanel(Ref* sender){
    mShowButtons->runAction(MoveTo::create(0.5f, Point(mShowButtons->getPositionX(),mVisibleSize.height*0.25f)));
};

FrutasScene* FrutasScene::getInstance(){
    return instancia_frutas;
};



void FrutasScene::Help(Ref* sender){
    mTextoHelp=std::string(LanguageManager::getInstance()->getStringForKey("HELP_FRUTAS","Click solve button and choose the right answer!"));
    BaseScene::Help(NULL);
};


void FrutasScene::Retry(Ref* sender){
    Director::getInstance()->replaceScene(FrutasScene::scene());
};

void FrutasScene::Hint(Ref* sender){
    if(mLayerBotones!=NULL){
        closeButtons(NULL);
    }
    BaseScene::Hint(NULL);
}


void FrutasScene::HintStart(Ref* sender){
    
    if(mHintStep==1){
        if(mSolucion!=1){
            mBoton1->runAction(FadeOut::create(0.5f));
            mOpcion1Text->runAction(FadeOut::create(0.5f));
            mBoton1->removeAllChildren();
        }
        if(mSolucion!=2){
            mBoton2->runAction(FadeOut::create(0.5f));
            mOpcion2Text->runAction(FadeOut::create(0.5f));
            mBoton2->removeAllChildren();
        }
        if(mSolucion!=3){
            mBoton3->runAction(FadeOut::create(0.5f));
            mOpcion3Text->runAction(FadeOut::create(0.5f));
            mBoton3->removeAllChildren();
        }
        if(mSolucion!=4){
            mBoton4->runAction(FadeOut::create(0.5f));
            mOpcion4Text->runAction(FadeOut::create(0.5f));
            mBoton4->removeAllChildren();
        }
    }
    else if(mHintStep==2){
        if(mSolucion!=1){
            mBoton1->removeFromParent();
        }
        if(mSolucion!=2){
             mBoton2->removeFromParent();
        }
        if(mSolucion!=3){
             mBoton3->removeFromParent();
        }
        if(mSolucion!=4){
             mBoton4->removeFromParent();
        }
    }else if(mHintStep==2){
        BaseScene::HintStop(NULL);
    }
    mHintStep++;
}



void FrutasScene::HintCallback(bool accepted){
    
    if(accepted){
        CCLOG("MOSTRAR");
        BaseScene::HintCallback(accepted);
        mOpcionElegida=false;
        mSituacionHint=true;
        mHintStep=1;
        
        CallFuncN* funcion= CallFuncN::create(CC_CALLBACK_1(FrutasScene::HintStart,this));
        this->runAction(Sequence::create(DelayTime::create(0.5), funcion,DelayTime::create(0.5), funcion,NULL));
        
        
         //runAction(Sequence::create(DelayTime::create(0.5f), CCCallFunc::create(this, callfunc_selector(FrutasScene::HintStart)),DelayTime::create(0.5f),CCCallFunc::create(this, callfunc_selector(FrutasScene::HintStart)),NULL));
        
        showButtons(NULL);
    }else{
        if(mOpcionElegida){
            //si habia elegido alguna opcion resetamos el nivel
            Retry(NULL);
        }
    }
}

void FrutasScene::Back(Ref* sender){
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, Levelsv2::scene()));
}




void FrutasScene::cargarFrutas()
{
   
    if(mIntentos==0)
    {
        mLuchaPorItems=true;
        //temporizador
        mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
        mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.08f,mVisibleSize.height*0.7f));
        this->addChild(mFondo_temporizador,2);
        mFondo_temporizador->setScale(0.7f);
        
      
        bool nivelIntentado=mIntentos>0;
        
        if(mSuperado){
            //los que gane en su dia
            mEnergyDrinkAspirables=mStars;
        }else{
            //los que pueda aspirar
            if(nivelIntentado){
                mEnergyDrinkAspirables=1;
            }else{
                mEnergyDrinkAspirables=3;
            }
        }
        mStars=mEnergyDrinkAspirables;
        dibujarEnergyDrink(mEnergyDrinkAspirables);
        
        NUMERO_SEGUNDOS_MAX=50.f;
        mNumeroSegundos=NUMERO_SEGUNDOS_MAX;
        mProgresoEnergyDrink = ProgressTimer::create(Sprite::create("elixircomun/temporizador_energy.png"));
        mProgresoEnergyDrink->setType(ProgressTimer::Type::RADIAL);
        mProgresoEnergyDrink->setPercentage(100.f);
        mProgresoEnergyDrink->setPosition(mFondo_temporizador->getPosition());
        addChild(mProgresoEnergyDrink,3);
        mProgresoEnergyDrink->setScale(0.7f);
        
        if(mEnergyDrinkAspirables>1 && !nivelIntentado){
            mSchedhuleCountdown=schedule_selector(FrutasScene::countDown);
            this->schedule(mSchedhuleCountdown, 1.f);
            
        }else{
            mProgresoEnergyDrink->setPercentage(0.f);
        }
    }else{
        
        
        float scale=0.5f;
        float rotateValue=20.f;
        float scaleCorona=0.8f;
        if(!mSuperado){
            
            mEnergyDrinkAspirables=1;
            mStars=mEnergyDrinkAspirables;
            mFondo_temporizador = Sprite::create("elixircomun/temporizador_back.png");
            mFondo_temporizador->setPosition(Point(mVisibleSize.width*0.08f,mVisibleSize.height*0.7f));
            addChild(mFondo_temporizador,2);
            mFondo_temporizador->setScale(0.7f);
            dibujarEnergyDrink(mEnergyDrinkAspirables);
        }else{
            int granosGanados=mNumStars ;
            mStars=granosGanados;
            /*if(granosGanados>=1){
                Sprite* granoganado1=Sprite::create("elixircomun/elixir_s.png");
                granoganado1->setScale(scale);
               // granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.10f,mVisibleSize.height*0.75f));
                addChild(granoganado1,2);
                Sprite* corona=Sprite::create("elixircomun/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona);
            }
            if(granosGanados>=2){
                Sprite* granoganado1=Sprite::create("elixircomun/elixir_s.png");
                granoganado1->setScale(scale);
                //  granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.15f,mVisibleSize.height*0.75f));
                addChild(granoganado1,2);
                 Sprite* corona=Sprite::create("elixircomun/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona,2);
            }
            if(granosGanados>=3){
                Sprite* granoganado1=Sprite::create("elixircomun/elixir_s.png");
                granoganado1->setScale(scale);
                 // granoganado1->setRotation(-rotateValue);
                granoganado1->setPosition(Point(mVisibleSize.width*0.20f,mVisibleSize.height*0.75f));
                addChild(granoganado1,2);
                 Sprite* corona=Sprite::create("elixircomun/king_dark.png");
                corona->setScale(scaleCorona);
                corona->setPosition(Point(granoganado1->getPositionX(),granoganado1->getPositionY()-granoganado1->getBoundingBox().size.height/3));
                this->addChild(corona,2);
            }*/
        }
    }
    
}



void FrutasScene::countDown(float dt){
    mNumeroSegundos--;
    float from=(mNumeroSegundos/NUMERO_SEGUNDOS_MAX)*100.f;
    float to=((mNumeroSegundos-1)/NUMERO_SEGUNDOS_MAX)*100.f;
    mProgresoEnergyDrink->runAction(ProgressFromTo::create(1.f, from,to));
    if(mNumeroSegundos<=NUMERO_SEGUNDOS_MAX/2 && mStars==3){
        Sprite* cafe2= (Sprite*) mFondo_temporizador->getChildByTag(FrutasScene::TAG_ENERGY_DRINK_2);
        cafe2->runAction(FadeTo::create(1.f,0.f));
        Sprite* x3=(Sprite*)mFondo_temporizador->getChildByTag(FrutasScene::TAG_X3);
        if(x3!=NULL){
            x3->runAction(FadeOut::create(0.5f));
            x3->runAction(ScaleTo::create(0.5f, 0.f));
        }
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(FrutasScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(Sequence::createWithTwoActions(DelayTime::create(0.7f), FadeIn::create(0.8f)));
        }
        
        mStars--;
        CCLOG("reduccion starts %i",mStars);
    }
    if(mNumeroSegundos<=0 && mStars==2){
        Sprite* cafe1=(Sprite*) mFondo_temporizador->getChildByTag(FrutasScene::TAG_ENERGY_DRINK_3);
        cafe1->runAction(FadeTo::create(1.f,0.f));
        Sprite* x2=(Sprite*)mFondo_temporizador->getChildByTag(FrutasScene::TAG_X2);
        if(x2!=NULL){
            x2->runAction(FadeOut::create(0.5f));
            x2->runAction(ScaleTo::create(0.5f, 0.f));
        }
        
        mStars--;
        this->unschedule(mSchedhuleCountdown);
        mSchedhuleCountdown=NULL;
        CCLOG("reduccion starts %i",mStars);
    }
    
    
}



void FrutasScene::dibujarEnergyDrink(int numero){
    Texture2D * elixirTex=Director::getInstance()->getTextureCache()->addImage("elixircomun/elixir_s.png");
    
    if(numero==1){
        Sprite* elixir1=Sprite::createWithTexture(elixirTex);
        elixir1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.5f));
        mFondo_temporizador->addChild(elixir1);
        elixir1->setScale(0.8f);
        elixir1->setRotation(-20);
        
    }else if(numero>1){
        Sprite* elixir1=Sprite::createWithTexture(elixirTex);
        elixir1->setPosition(Point(mFondo_temporizador->getContentSize().width*0.32f,mFondo_temporizador->getContentSize().height*0.38f));
        mFondo_temporizador->addChild(elixir1);
        elixir1->setScale(0.6f);
        elixir1->setRotation(40);
        
        
    }
    if(numero>=2){
        Sprite* elixir2=Sprite::createWithTexture(elixirTex);
        elixir2->setPosition(Point(mFondo_temporizador->getContentSize().width*0.7f,mFondo_temporizador->getContentSize().height*0.4f));
        mFondo_temporizador->addChild(elixir2);
        elixir2->setScale(0.6f);
        elixir2->setRotation(-40);
        elixir2->setTag(FrutasScene::TAG_ENERGY_DRINK_2);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x2.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setOpacity(0.f);
        multiplicador->setTag(FrutasScene::TAG_X2);
        multiplicador->setScale(1.5f);
        
    }
    if(numero==3){
        
        Sprite* elixir3=Sprite::createWithTexture(elixirTex);
        elixir3->setPosition(Point(mFondo_temporizador->getContentSize().width*0.5f,mFondo_temporizador->getContentSize().height*0.7f));
        mFondo_temporizador->addChild(elixir3);
        elixir3->setScale(0.6f);
        elixir3->setRotation(90);
        elixir3->setTag(FrutasScene::TAG_ENERGY_DRINK_3);
        
        Sprite* multiplicador=Sprite::create("elixircomun/x3.png");
        multiplicador->setPosition(Point(0,mFondo_temporizador->getContentSize().height*0.9f));
        mFondo_temporizador->addChild(multiplicador);
        multiplicador->setTag(FrutasScene::TAG_X3);
        multiplicador->setScale(1.5f);
        
    }
    
}

void FrutasScene::checkFinish(Ref* sender){
    if(mSuccess){
        if(!mSuperado){
            LogicSQLHelper::getInstance().setAnimacionDesbloqueoPendiente(true,Variables::LEVELS_ELIXIR_CLICKADO, mStars);
            LayerGanarItem::mostrar(mStars);
            return;
        }
    }
    BaseScene::checkFinish(NULL);
};

template <typename T> std::string FrutasScene::tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }




void FrutasScene::onEnterTransitionDidFinish(){
    cargarPrueba();
    LogicSQLHelper::getInstance().sumarIntentoSQL(Variables::LEVELS_ELIXIR_CLICKADO);
    BaseScene::onEnterTransitionDidFinish();
}




void FrutasScene::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}


std::vector<std::string> FrutasScene::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}


void FrutasScene::zoom(Ref* sender){
    if(mGameFinished){
        return;
    }
    if(mZoomActivandose){
        return;
    }
    CallFunc* callfunc= CallFunc::create([this]() { this->zoomStop(); });
    if(!mZoomActivado){
        mPanel->runAction(Sequence::create(ScaleTo::create(0.3f, mVisibleSize.width/mPanel->getContentSize().width ),callfunc,NULL));
        mPanel->runAction(MoveTo::create(0.3f, Point(mVisibleSize.width/2,mVisibleSize.height/2)));
        mZoom->setTexture(Director::getInstance()->getTextureCache()->addImage("elixircomun/zoom-less.png"));
      
        mZoom->runAction(MoveTo::create(0.3f,Point(mPanel->getContentSize().width-mZoom->getContentSize().width/2,mPanel->getContentSize().height/2) ));
        mZoom->runAction(ScaleTo::create(0.3, 0.5f));
        mShowButtons->runAction(MoveTo::create(0.1f, Point(mVisibleSize.width/2 ,-mShowButtons->getContentSize().height)));
        mZoomActivado=true;
        
    }else{
        mZoom->runAction(MoveTo::create(0.3f,Point(mPanel->getContentSize().width-mZoom->getContentSize().width/2,mPanel->getContentSize().height-mZoom->getContentSize().height/2)));
        mPanel->runAction(Sequence::create(ScaleTo::create(0.3f, 1),callfunc,NULL));
        mPanel->runAction(MoveTo::create(0.3f, Point(mVisibleSize.width/2,mVisibleSize.height-mPanel->getContentSize().height/2)));
        mZoom->setTexture(Director::getInstance()->getTextureCache()->addImage("elixircomun/zoom.png"));
        mZoom->runAction(ScaleTo::create(0.3, 1.f));
        mZoomActivado=false;
       mShowButtons->runAction(MoveTo::create(0.1f, Point(mVisibleSize.width/2 ,mVisibleSize.height*0.25f)));

    }
  
    mZoomActivandose=true;
    
    
    
}

void FrutasScene::zoomStop(){
    mZoomActivandose=false;
}


