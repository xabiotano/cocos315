#ifndef __FrutasScene_SCENE_H__
#define __FrutasScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include <string>
#include <sstream>
#include <vector>


USING_NS_CC;


class FrutasScene : public BaseScene
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(FrutasScene);
    static FrutasScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Back(Ref* sender);
    virtual void Help(Ref* sender);
    virtual void Retry(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
    virtual void TutorialStart(){};
    static int cargarNiveltexto(void *NotUsed, int argc, char **argv, char **azColName);
    int mNumeroPrueba;
    bool mSuperado;
    int mNumStars;
    int mIntentos;
    std::string mSolucionStr;
    std::string mPattern;
    
private:
    LayerColor* mLayerOscura;
    Boton* mBoton1,*mBoton2,*mBoton3,*mBoton4;
    Label* mOpcion1Text,*mOpcion2Text,*mOpcion3Text,*mOpcion4Text;
    Label* mCounter;
    Boton* mShowButtons;
    LayerColor* mLayerBotones;
    void closeButtons(Ref* sender);
    void removePanel(Ref* sender);
    void crearEscena();
    void showButtons(Ref* sender);
    void opcion1(Ref* sender);
    void opcion2(Ref* sender);
    void opcion3(Ref* sender);
    void opcion4(Ref* sender);
    void opcionClickada(int opcion);
    bool mOpcionElegida;
    void cargarPrueba();
    int mIdNivel;
    int mSolucion;
    
    
    void cargarFrutas();
    //contador energy drink
    Sprite* mFondo_serie;
    ProgressTimer* mProgresoEnergyDrink;
    
    bool mGameFinished;
    
    int mEnergyDrinkAspirables;
    Sprite *mFondo_temporizador;
    void dibujarEnergyDrink(int numero);
    static  std::string SERIES_NUMERO_ENERGY_DRINK_CONSEGUIDOS;
    float mNumeroSegundos;
    float NUMERO_SEGUNDOS_MAX;
    void countDown(float dt);
    SEL_SCHEDULE mSchedhuleCountdown;

    
    const int TAG_ENERGY_DRINK_2=840;
    const int TAG_ENERGY_DRINK_3=841;
    const int TAG_X2=8472;
    const int TAG_X3=29402;
    
    bool mLuchaPorItems;
    int mTipoAnuncio;
    template <typename T> std::string tostr(const T& t);


    void onEnterTransitionDidFinish();
    void zoom(Ref* sender);
    bool  mZoomActivandose;
    bool mZoomActivado;
    //float mScalePanel;
    Sprite* mPanel;
    Boton* mZoom;
    void zoomStop();
        
    std::vector<std::string> split(const std::string &s, char delim);
    void split(const std::string &s, char delim, std::vector<std::string> &elems) ;
};


#endif // __FrutasScene_SCENE_H__
