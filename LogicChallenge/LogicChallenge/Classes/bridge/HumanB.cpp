//
//  HumanB.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "HumanB.h"
#include "SimpleAudioEngine.h"


HumanB::HumanB(Point mPointIzq,Point mPointDch,std::string name,int z_index,int tiempo_move){
    mEstaIzquierda=true;
    mIsMoving=false;
    mPosicionIzquierda=mPointIzq;
    mPosicionDerecha=mPointDch;
    mEstado=kLadoIzquierdo;
    mName=name;
    setPosition(mPointIzq);
    mZIndex=z_index;
    mTiempoMove=tiempo_move;
    mSelected=false;
    mSwallowTouches=true;
}

HumanB::~HumanB(void){}


HumanB* HumanB::create(Node* parent,std::string name,Point mPointIzq,Point mPointDch,std::string imagen,int z_index,int tiempo_move)
{
    HumanB *devolver= new HumanB(mPointIzq,mPointDch,name,z_index,tiempo_move);
    devolver->autorelease();
    devolver->setPosition(devolver->mPosicionIzquierda);
    devolver->initBoton(devolver, menu_selector(HumanB::click), imagen);
    return devolver;
}


void HumanB::click(Ref* sender)
{
    if(mIsMoving){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(BridgeScene::getInstance()->isAntorchaMoving()){
        CCLOG("Antorcha MOVING");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    
    if(mSelected){
        this->setSelected(false);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    
    if(mEstado==HumanBState(kLadoIzquierdo)){
      if(BridgeScene::getInstance()->estaAntorchaEnIzquierda()){
          this->runAction(Sequence::createWithTwoActions(EaseBounceIn::create(ScaleTo::create(0.2f, 1.1f)), ScaleTo::create(0.2f, 1.f)));
           CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elavator_select.mp3",false);
          this->setSelected(true);
        
        }
        
    }
    else{
        
        if(!BridgeScene::getInstance()->estaAntorchaEnIzquierda()){
            this->runAction(Sequence::createWithTwoActions(EaseBounceIn::create(ScaleTo::create(0.2f, 1.1f)), ScaleTo::create(0.2f, 1.f)));
            
            this->setSelected(true);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elavator_select.mp3",false);
        }
        
        
        
        
       
        

    }
    
}


void HumanB::finMove(){
    mIsMoving=false;
    setSelected(false);
}

                    
bool HumanB::estaEnIzquierda(){
    return (mEstado==HumanBState(kLadoIzquierdo));
}

void HumanB::setMoving(bool moving){
    mIsMoving=moving;
}

bool HumanB::isMoving(){
    return mIsMoving;
}


bool HumanB::isSelected(){
    return mSelected;
}

std::string HumanB::getNameOfHuman(){
    return mName;
}



void HumanB::setSelected(bool selected){
   
    if(selected && this->mSelected){
        CCLOG("Ya se ha procesado");
        return;
    }
    this->mSelected=selected;
    if(selected){
        this->runAction(RepeatForever::create(Sequence::createWithTwoActions(TintTo::create(1.f, 100,250.f, 100.f), TintTo::create(1.f, 100, 150, 100.f))));
        BridgeScene::getInstance()->addHumanToSelected(this);
    }
    else{
        BridgeScene::getInstance()->removeHumanFromSelected(this);
        this->stopAllActions();
        this->setColor(Color3B(255.f,255.f,255.f));
    }
}

int HumanB::getTiempoMove(){
    return mTiempoMove;
}


void HumanB::move(){
    if(isSelected()){
        
        Point destino;
        if(BridgeScene::getInstance()->estaAntorchaEnIzquierda()){
            destino=mPosicionDerecha;
            mEstado=HumanBState(kLadoDerecho);
        }
        else{
            destino=mPosicionIzquierda;
            mEstado=HumanBState(kLadoIzquierdo);
        }
        CallFunc* finmove=CallFunc::create([this]() { this->finMove(); });
        runAction(Sequence::createWithTwoActions(MoveTo::create(BridgeScene::getInstance()->getMaxTimeOfMove(), destino),finmove));
        mIsMoving=true;
        
    }
}




