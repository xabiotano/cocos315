#include "BridgeScene.h"
#include "SimpleAudioEngine.h"
#include "../Layers/LayerGameOver.h"
#include "../Layers/LayerSuccess.h"
#include "../Layers/LayerInfo.h"
#include "../Variables.h"
#include "../Levels.h"
#include "HumanB.h"

using namespace cocos2d;
using namespace CocosDenshion;


BridgeScene* instancia_bridge;

BridgeScene::~BridgeScene(void){
   // CC_SAFE_RELEASE(mSelectedHumans);
}

Scene* BridgeScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    // 'layer' is an autorelease object
    BridgeScene *layer = BridgeScene::create();
    // add layer as a child to scene
    scene->addChild(layer);
     // return the scene
    return scene;
}

bool BridgeScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !BaseScene::init() )
    {
        return false;
    }
    instancia_bridge=this;
    mNombrePantalla="BridgeScene";
    mTipoNivel=kNivelLogica;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    Sprite* mFondo = Sprite::create("bridge/background.png");
    // position the sprite on the center of the screen
    mFondo->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    this->addChild(mFondo, 0);
    float scalex=Director::getInstance()->getVisibleSize().width/mFondo->getContentSize().width;
    float scaley=Director::getInstance()->getVisibleSize().height/mFondo->getContentSize().height;
    if(scalex>scaley){
        scaley=scalex;
    }
    mFondo->setScale(scaley);
    
    Director::getInstance()->setContentScaleFactor(960/visibleSize.width);
    mNumberOfSeconds=30;
    
    mPointAntorchaIz=convertirAVisible(Point(350,400));
    mPointAntorchaDc=convertirAVisible(Point(650,380));
    mAntorcha= Sprite::create("bridge/lamp.png");
    mAntorcha->setPosition(mPointAntorchaIz);
    this->addChild(mAntorcha,6);
    mAntorchaIzquierda=true;
    mAntorchaMoving=false;

    mBotonMove=Boton::createBoton(this, menu_selector(BridgeScene::Go), "bridge/go.png");
    mBotonMove->setAnchorPoint(convertirAVisible(Point(0.37,0.5f)));
    mBotonMove->setPosition(convertirAVisible(Point(mSize.width/2,mSize.height*0.3)));
    this->addChild(mBotonMove,10);
   
    
    /*CCLabelStroke* goText=CCLabelStroke::create(this, "GO!", CCGetFont(), 30, Size(100, 100), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    goText->setPosition(Point(mBotonMove->getPositionX(),mBotonMove->getPositionY()- 85));
    this->addChild(goText,12);
    goText->setString("GO!", Color3B(0,0,0));*/
    
    Label* goText = Label::createWithTTF("GO!", CCGetFont(), 30);
    goText->setWidth(100);
    goText->setHeight(100);
    goText->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    goText->enableOutline(Color4B(0,0,0,255),2);
    goText->setPosition(Point(mBotonMove->getPositionX(),mBotonMove->getPositionY()- 85));
    goText->setColor(Color3B(255,255,255));
     this->addChild(goText,12);
    

    
    
    //mSelectedHumans=Dictionary::create();
    //mSelectedHumans->retain();

    Sprite* clock_container=Sprite::create("bridge/clock_container.png");
    clock_container->setPosition(Point(mSize.width-clock_container->getContentSize().width/2,mSize.height*0.2f));
    this->addChild(clock_container);
   
    /* mCountDown=CCLabelStroke::create(clock_container, "00:30", CCGetFont(), 50, Size(clock_container->getContentSize().width*0.8, clock_container->getContentSize().height*0.8f), kCCTextAlignmentCenter, kCCVerticalTextAlignmentCenter);
    mCountDown->setPosition(Point(clock_container->getContentSize().width/2,clock_container->getContentSize().height/2));
    clock_container->addChild(mCountDown,2);
    mCountDown->setString("00:30", Color3B((0,0,0));
    mCountDown->setColor(Color3B(216,216,189));*/
    
    mCountDown = Label::createWithTTF("00:30", CCGetFont(), 50);
    mCountDown->setWidth(clock_container->getContentSize().width*0.8);
    mCountDown->setHeight(clock_container->getContentSize().height*0.8f);
    mCountDown->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    mCountDown->enableOutline(Color4B(0,0,0,255),2);
    mCountDown->setPosition(Point(clock_container->getContentSize().width/2,clock_container->getContentSize().height/2));
    mCountDown->setColor(Color3B(255,255,255));
    mCountDown->setColor(Color3B(216,216,189));
    clock_container->addChild(mCountDown,2);

    
    
    crearPersonajes();
    
    Sprite* hiena=Sprite::create("bridge/hiena.png");
    hiena->setPosition(Point(mVisibleSize.width*0.1f,hiena->getContentSize().height/2));
    this->addChild(hiena);
    hiena->runAction(RepeatForever::create(Sequence::create(MoveTo::create(6.f, Point(mVisibleSize.width*0.6f,hiena->getContentSize().height/2)),MoveTo::create(6.f, Point(-mVisibleSize.width*0.1f,hiena->getContentSize().height/2)),NULL)));
    
    mTextoHelp=LanguageManager::getInstance()->getStringForKey("HELP_BRIDGE");
    mRetryVisibleDuringGame=true;
    
    mTextoGameOver=LanguageManager::getInstance()->getStringForKey("GAME_OVER_BRIDGE");
    
    return true;
}


void BridgeScene::crearPersonajes(){
    
    mNumberOfSeconds=31;
    descenderSegundos(0.f);
    mPanel_selected=Sprite::create("bridge/panel_selected.png");
    mPanel_selected->setPosition(Point(mSize.width/2,mSize.height- mPanel_selected->getContentSize().height/2));
    this->addChild(mPanel_selected);
    
    
    
    mSelectedHumans.clear();
    //dibujo los personajes
    int padding=40;
    if(mBebe!=NULL){
        mBebe->removeFromParent();
    }
    if(mAtleta!=NULL){
        mAtleta->removeFromParent();
    }
    if(mPirata!=NULL){
        mPirata->removeFromParent();
    }
    if(mAbuelo!=NULL){
        mAbuelo->removeFromParent();
    }
    if(mChica!=NULL){
        mChica->removeFromParent();
    }
    
    mBebe =HumanB::create(this, std::string("bridge/baby.png"),convertirAVisible(Point(60,270)), convertirAVisible(Point(900+18,270)), std::string("bridge/baby.png"), 8,12);
    
    mAtleta=HumanB::create(this, std::string("bridge/atleta.png"), convertirAVisible(Point(235,320)), convertirAVisible(Point(725+padding,320)), std::string("bridge/atleta.png"),7, 1);
    
    mAbuelo=HumanB::create(this, std::string("bridge/boy.png"),convertirAVisible( Point(120,350)), convertirAVisible(Point(840+padding,350)), std::string("bridge/boy.png"), 6,3);
    mChica=HumanB::create(this, std::string("bridge/girl.png"), convertirAVisible(Point(270,450)), convertirAVisible(Point(690+padding+20,450)), std::string("bridge/girl.png"), 5,6);
    
    
    mPirata=HumanB::create(this, std::string("bridge/pirate.png"), convertirAVisible(Point(60,460)), convertirAVisible(Point(900+20,460)), std::string("bridge/pirate.png"), 4,8);

    this->addChild(mBebe,8);
    this->addChild(mAtleta,7);
    this->addChild(mAbuelo,6);
    this->addChild(mChica,5);
    this->addChild(mPirata,4);
}


void BridgeScene::Go(Ref* sender){
    CCLOG("GO");
    if(mSelectedHumans.size() ==0){
        CCLOG("Ninguno seleccionado");
        LayerInfo::mostrar(LanguageManager::getInstance()->getStringForKey("BRIDGE_CHOOSE_CHARS"))->setSprite(Sprite::create("ui/touch.png"));
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;
    }
    if(mAntorchaMoving){
        return;
    }
    mAntorchaMoving=true;
    int max_time=getMaxTimeOfMove();
   
    Point destino;
    if(mAntorchaIzquierda){
        destino=mPointAntorchaDc;
    }else{
        destino=mPointAntorchaIz;
    }
    
    this->schedule(schedule_selector(BridgeScene::descenderSegundos), 1.f, max_time-1, 0.f);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevatorup.mp3", false);
    CallFunc *finMove =CallFunc::create([this]() { this->finMoveAntorcha(); });
    mAntorcha->runAction(Sequence::createWithTwoActions(MoveTo::create(getMaxTimeOfMove(), destino),finMove));
  
    mBebe->move();
    mChica->move();
    mAbuelo->move();
    mPirata->move();
    mAtleta->move();
    mAntorchaIzquierda=!mAntorchaIzquierda;
    
}
void BridgeScene::finMoveAntorcha(){
    mBotonMove->runAction(RotateBy::create(0.5f,180.f ));
    mAntorchaMoving=false;
    checkFinish(NULL);
}

BridgeScene* BridgeScene::getInstance(){
    return instancia_bridge;
};

void BridgeScene::checkFinish(Ref* sender){
    if(!mChica->estaEnIzquierda()&&
       !mBebe->estaEnIzquierda()&&
       !mPirata->estaEnIzquierda()&&
       !mAbuelo->estaEnIzquierda()&&
       !mAtleta->estaEnIzquierda()){
        mSuccess=true;
        BaseScene::Success();
    }
      

       
       
       
       
    
};


bool BridgeScene::estaAntorchaEnIzquierda(){
    return mAntorchaIzquierda;
}
bool BridgeScene::isAntorchaMoving(){
    return mAntorchaMoving;
}

int BridgeScene::getMaxTimeOfMove(){
    int max_time=0.f;
    if(mBebe->isSelected() && max_time<mBebe->getTiempoMove()){
            max_time=mBebe->getTiempoMove();
        
    }
    if(mAtleta->isSelected() && max_time<mAtleta->getTiempoMove()){
        max_time=mAtleta->getTiempoMove();
        
    }
    if(mAbuelo->isSelected() && max_time<mAbuelo->getTiempoMove()){
        max_time=mAbuelo->getTiempoMove();
        
    }
    if(mChica->isSelected() && max_time<mChica->getTiempoMove()){
        max_time=mChica->getTiempoMove();
        
    }
    if(mPirata->isSelected() && max_time<mPirata->getTiempoMove()){
        max_time=mPirata->getTiempoMove();
        
    }
   
    return max_time;
}

void BridgeScene::addHumanToSelected(HumanB* human){
    

    HumanB* valor1=(HumanB*) mSelectedHumans.at(0);
    HumanB* valor2= (HumanB*)mSelectedHumans.at(1);
    if(valor1!=NULL && valor2!=NULL)
    {
   
        //machaco el 1
        valor1->setSelected(false);
        mSelectedHumans.insert(0, human);
        if(mIconSelected1!=NULL){
            mIconSelected1->removeFromParent();
            mIconSelected1=NULL;
            
        }
        mIconSelected1=Sprite::create(human->getNameOfHuman().c_str());
        mIconSelected1->setScale(0.4f);
        mIconSelected1->setPosition(convertirAVisible(Point(430,580)));
        this->addChild(mIconSelected1);
        return;
    }
    
    else if(valor1==NULL)
    {
        mSelectedHumans.insert(0, human);
        if(mIconSelected1!=NULL){
            mIconSelected1->removeFromParent();
            mIconSelected1=NULL;
           
        }
        mIconSelected1=Sprite::create(human->getNameOfHuman().c_str());
        mIconSelected1->setScale(0.4f);
        mIconSelected1->setPosition(convertirAVisible(Point(430,580)));
        this->addChild(mIconSelected1);
        
    }
    else if(valor2==NULL)
    {
        mSelectedHumans.insert(1, human);
        if(mIconSelected2!=NULL){
            mIconSelected2->removeFromParent();
            mIconSelected2=NULL;
           
        }
        mIconSelected2=Sprite::create(human->getNameOfHuman().c_str());
        mIconSelected2->setScale(0.4f);
        mIconSelected2->setPosition(convertirAVisible(Point(560,580)));
        this->addChild(mIconSelected2);
    }
    human->setSelected(true);
    
    /*
    //siempre meto en el ultimo hueco vacio y sino en el primero
    if(mSelectedHumans->objectForKey(0)!=human &&
       mSelectedHumans->objectForKey(1)!=human){
        if(mSelectedHumans->count()==0){
            mSelectedHumans->setObject(human, 0);
            if(mIconSelected1!=NULL){
                removeChild(mIconSelected1);
            }
            mIconSelected1=Sprite::create(human->getNameOfHuman().c_str());
            mIconSelected1->setScale(0.4f);
            mIconSelected1->setPosition(convertirAVisible(Point(430,580)));
            addChild(mIconSelected1);
            
        }else if(mSelectedHumans->count()==1){
            int index=0;
            HumanB* temp=(HumanB*)mSelectedHumans->objectForKey(index);
            if(temp!=NULL){
                index=1;
            }
            mSelectedHumans->setObject(human, index);
            if(index==0){
                mIconSelected1=Sprite::create(human->getNameOfHuman().c_str());
                mIconSelected1->setScale(0.4f);
                mIconSelected1->setPosition(convertirAVisible(Point(430,580)));

            }else{
                mIconSelected2=Sprite::create(human->getNameOfHuman().c_str());
                mIconSelected2->setScale(0.4f);
                mIconSelected2->setPosition(convertirAVisible(Point(560,580)));
            }
            addChild(mIconSelected2);

        }
        else if(mSelectedHumans->count()==2){
            HumanB* temp=(HumanB*)mSelectedHumans->objectForKey(0);
            temp->setSelected(false);
            removeHumanToSelected(temp);
            mSelectedHumans->setObject(human, 0);
            if(mIconSelected1!=NULL){
                removeChild(mIconSelected1);
            }
            mIconSelected1=Sprite::create(human->getNameOfHuman().c_str());
            mIconSelected1->setScale(0.4f);
            mIconSelected1->setPosition(convertirAVisible(Point(430,580)));
            addChild(mIconSelected1);
        }
    }*/
  
}
void BridgeScene::removeHumanFromSelected(HumanB* human)
{
    if(human==NULL){
        return;
    }
    if(((HumanB*)mSelectedHumans.at(0))==human){
        mSelectedHumans.erase(0);
        if(mIconSelected1!=NULL){
            mIconSelected1->removeFromParent();
            mIconSelected1=NULL;
        }
        
    }else if(((HumanB*)mSelectedHumans.at(1))==human){
        mSelectedHumans.erase(1);
        if(mIconSelected2!=NULL){
            mIconSelected2->removeFromParent();
            mIconSelected2=NULL;
        }
    }
}

/*void BridgeScene::removeHumanToSelected(HumanB* human){
    //siempre meto en el ultimo hueco vacio y sino en el primero
    if(human==NULL){
        return;
    }
    if(mSelectedHumans->objectForKey(0)==human){
        mSelectedHumans->removeObjectForKey(0);
        if(mIconSelected1!=NULL){
            removeChild(mIconSelected1);
        }
        
    }
    else if(mSelectedHumans->objectForKey(1)==human){
        mSelectedHumans->removeObjectForKey(1);
        if(mIconSelected2!=NULL){
            removeChild(mIconSelected2);
        }
    }
    
    
}*/


void BridgeScene::descenderSegundos(float dt){
    if(mNumberOfSeconds<31 ){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevatormove.mp3", false);

    }
    mNumberOfSeconds--;
    if(mNumberOfSeconds<0){
        BaseScene::GameOver();
        return;
    }
    char temp[200];
    if(mNumberOfSeconds>=10){
         sprintf(temp, "00:%d",mNumberOfSeconds);
    }else{
         sprintf(temp, "00:0%d",mNumberOfSeconds);
    }
     mCountDown->setString(temp);
  
   
    
}

void BridgeScene::Retry(Ref* sender){
    Scene* scene=BridgeScene::scene();
    Director *pDirector = Director::getInstance();
    pDirector->replaceScene(TransitionCrossFade::create(0.5f, scene));

}

void BridgeScene::HintStartTemp(float dt){
    HintStart(NULL);
}

void BridgeScene::HintStart(Ref* sender){
    if(mAbuelo->isMoving() ||
       mChica->isMoving() ||
       mBebe->isMoving() ||
       mAtleta->isMoving()||
       mPirata->isMoving())
    {
        CCLOG("Is running");
        return;
    }
  //mAbuelo,*mChica,*mBebe,*mAtleta,*mPirata
    switch(mHintStep){
        case 1:
            mAbuelo->click(NULL);
            mAtleta->click(NULL);
            break;
        case 2:
            Go(NULL);
            break;
        case 3:
            mAtleta->click(NULL);
            break;
        case 4:
            Go(NULL);
            break;
        case 5:
            mPirata->click(NULL);
            mBebe->click(NULL);
            break;
        case 6:
            Go(NULL);
            break;
        case 7:
            mAbuelo->click(NULL);
            break;
        case 8:
            Go(NULL);
            break;
        case 9:
            mAtleta->click(NULL);
            mChica->click(NULL);
            break;
        case 10:
            Go(NULL);
            break;
        case 11:
            mAtleta->click(NULL);
            break;
        case 12:
            Go(NULL);
            break;
        case 13:
            mAbuelo->click(NULL);
            mAtleta->click(NULL);
            break;
        case 14:
            Go(NULL);
            break;
            
    }
    mHintStep++;
}



void BridgeScene::Hint(Ref* sender){
    BaseScene::Hint(NULL);
}

void BridgeScene::HintCallback(bool accepted){
    if(accepted){
        BaseScene::HintCallback(accepted);
        crearPersonajes();
        mHintStep=1;
        mSituacionHint=true;
        LayerHintOk::mostrar(true);
        mAntorchaIzquierda=true;
        mAntorchaMoving=false;
        mAntorcha->setPosition(mPointAntorchaIz);
        
        mScheudleHint=CC_SCHEDULE_SELECTOR(BridgeScene::HintStartTemp);
     //   mScheudleHint=;
        this->schedule(mScheudleHint,2.f);
        
    }else{
        CCLOG("CANCELADO");
    }
}

void BridgeScene::onEnterTransitionDidFinish(){
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("sounds/bridge/windy.mp3", true);
    BaseScene::onEnterTransitionDidFinish();
}



