//
//  HumanB.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__HumanB__
#define __logicmaster2__HumanB__

#include <iostream>
#include "../widgets/Boton.h"
#include "HumanB.fwd.h"
#include "BridgeScene.h"


typedef enum HumanBState
{
    kLadoIzquierdo,
    kLadoDerecho
    
} HumanBState;

class HumanB: public Boton
{
    
    
    public:
    
        HumanB(Point mPointIzq,Point mPointDch,std::string name,int z_index,int tiempo_move);
        virtual ~HumanB(void);
        static cocos2d::Scene* scene();
    
   
        static HumanB* create(Node* parent,std::string imagen,Point mPointIzq,Point mPointDch,std::string name,int z_index,int tiempo_move);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        bool isMoving();
        std::string mName;
        std::string getNameOfHuman();
        HumanBState mEstado;
        int mZIndex;
        bool isSelected();
        void setSelected(bool selected);
        int getTiempoMove();
        Point mPosicionIzquierda,mPosicionDerecha;
        int mTiempoMove;
        void move();
    private:
        bool mEstaIzquierda;
        bool mIsMoving;
        unsigned int mSoundEffect;
        bool mSelected;
        void finMove();
   
    
    
    
};


#endif /* defined(__logicmaster2__HumanB__) */
