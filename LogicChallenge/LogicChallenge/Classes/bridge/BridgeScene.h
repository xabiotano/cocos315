#ifndef __BridgeScene_SCENE_H__
#define __BridgeScene_SCENE_H__

#include "cocos2d.h"
#include "../BaseScene.h"
#include "../widgets/Boton.h"
#include "HumanB.fwd.h"

USING_NS_CC;


class BridgeScene : public BaseScene
{
public:
    virtual void Retry(Ref* sender);
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    ~BridgeScene(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static Scene* scene();
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(BridgeScene);
    static BridgeScene* getInstance();
    
    virtual void checkFinish(Ref* sender);
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
     virtual void TutorialStart(Ref* sender){};
    
    
    bool estaAntorchaEnIzquierda();
    bool isAntorchaMoving();
    void finMoveAntorcha();
    int getMaxTimeOfMove();
    void addHumanToSelected(HumanB* human);
    //void removeHumanToSelected(HumanB* human);
    void onEnterTransitionDidFinish();
    void removeHumanFromSelected(HumanB* human);
private:
    Label* mLabel1,*mLabel2;
    Sprite *mIconSelected1,*mIconSelected2;
    cocos2d::Map<int,Sprite*> mSelectedHumans;
    Label* mCounter;
    Boton* mBotonMove;
    bool mAntorchaMoving;
    Sprite* mPanel_selected;
    bool mAntorchaIzquierda;
    Sprite* mAntorcha;
    Point mPointAntorchaIz,mPointAntorchaDc;
    void Go(Ref* sender);
    HumanB* mAbuelo,*mChica,*mBebe,*mAtleta,*mPirata;
    Label* mCountDown;
    int mNumberOfSeconds;
    void descenderSegundos(float dt);
    void crearPersonajes();
    void HintStartTemp(float dt);
   
protected:

};


#endif // __BridgeScene_SCENE_H__
