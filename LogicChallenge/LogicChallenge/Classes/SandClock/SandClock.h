//
//  SandClock.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__SandClock__
#define __logicmaster2__SandClock__

#include <iostream>
#include "../widgets/Boton.h"
#include "SandClock.fwd.h"

class SandClock: public Boton
{
    
public:
    SandClock(float TimeCapacity);
    virtual ~SandClock(void);
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    
    static SandClock* create(Node* parent, SEL_MenuHandler accion,int TimeCapacity,float defaultScale);
   
    void toggleVerticalPosition();
    void initClocks();
    void stopTime(float tiempoGastado);
    void startTime();
    float getTimeLeft();
    
private:
    
    bool mLockRotate;
    Size mSize;
    Boton* _boton;
    float mDefaultScale;
    bool mPosicion;
    float mTimeCapacity;
    float mTimeLeft;
    ProgressFromTo *mTiempoProgresoArriba,*mTiempoProgresoAbajo;
    ProgressTimer* mTiempoRestanteArriba,*mTiempoRestanteAbajo;
    void finTiempoArriba();
    void finTiempoAbajo();
    void finRotacion();
    void resetAppearance();
    
    
};


#endif /* defined(__logicmaster2__SandClock__) */
