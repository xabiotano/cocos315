//
//  SandClock.cpp
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#include "SandClock.h"
#include "SandClockScene.h"
#include "SimpleAudioEngine.h"

SandClock::SandClock(float TimeCapacity)
{
    mTimeCapacity=TimeCapacity*1.f;
    mTimeLeft=TimeCapacity*1.f;
    mLockRotate=false;
}
SandClock::~SandClock(void)
{
    
}

SandClock* SandClock::create(Node* parent, SEL_MenuHandler accion,int TimeCapacity,float defaultScale)
{
   
    SandClock *devolver= new SandClock(TimeCapacity);
    devolver->mDefaultScale=defaultScale;
    devolver->setScale(defaultScale);
    devolver->initBoton(parent, accion, "sandclock/sanclock.png");
    devolver->autorelease();
    devolver->initClocks();
    char temp[200];
    sprintf(temp,"%i",TimeCapacity);
    Label* label= Label::createWithTTF(temp, CCGetFont(), 40);
    label->setTextColor(Color4B(0,0,0,255));
    label->enableOutline(Color4B(255,255,255,255));
    devolver->addChild(label,10);
    label->setPosition(Point(devolver->getContentSize().width/2,devolver->getContentSize().height*0.25f));
    
    return devolver;
}



void SandClock::initClocks()
{
    if(mTiempoRestanteArriba){
        this->removeChild(mTiempoRestanteArriba, true);
    }
    if(mTiempoRestanteAbajo){
        this->removeChild(mTiempoRestanteAbajo, true);
    }
    //ARRIBA
    float actualPercentage=mTimeLeft/mTimeCapacity *100.f;
    mTiempoRestanteArriba= ProgressTimer::create(Sprite::create("sandclock/sandclock_filled.png"));
    mTiempoRestanteArriba->setType(ProgressTimer::Type::BAR);
    mTiempoRestanteArriba->setMidpoint(Point(0,100));
    mTiempoRestanteArriba->setBarChangeRate(Point(0,1));
    mTiempoRestanteArriba->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height*0.67f));
    mTiempoRestanteArriba->setRotation(180.f);
    this->addChild(mTiempoRestanteArriba,1);
    mTiempoRestanteArriba->setPercentage(actualPercentage);
    CCLOG("mTiempoRestanteArriba %f",actualPercentage);
   
    //ABAJO
    mTiempoRestanteAbajo= ProgressTimer::create(Sprite::create("sandclock/sandclock_filled.png"));
    mTiempoRestanteAbajo->setType(ProgressTimer::Type::BAR);
    
   
    mTiempoRestanteAbajo->setMidpoint(Point(100,0));
    mTiempoRestanteAbajo->setBarChangeRate(Point(0,1));
    mTiempoRestanteAbajo->setPosition(Point(this->getContentSize().width/2,this->getContentSize().height*0.32f));
    this->addChild(mTiempoRestanteAbajo,1);
    mTiempoRestanteAbajo->setPercentage(100.f-actualPercentage);
    CCLOG("mTiempoRestanteAbajo %f",100.f-actualPercentage);
    
    this->runAction(RepeatForever::create(CCEaseBounceOut::create(Sequence::createWithTwoActions(ScaleTo::create(2.f, (mDefaultScale-0.05f)), ScaleTo::create(2.f,(mDefaultScale+0.05f))))));
    
    
}

void SandClock::finTiempoArriba()
{
    CCLOG("finTiempoArriba");
    SandClockScene* padre=(SandClockScene*)this->getParent();
    padre->pararTiempo(mTimeCapacity,mTimeLeft);
    mTimeLeft=0;
    mLockRotate=false;
    
    
    
}
void SandClock::finTiempoAbajo()
{
    CCLOG("finTiempoAbajo");
}


void SandClock::toggleVerticalPosition()
{
    if(mLockRotate){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/elevators/elevator_badselected.mp3",false);
        return;};
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sounds/sandclock/balanza.mp3", false);
    resetAppearance();
    mLockRotate=true;
    mTimeLeft=mTimeCapacity-mTimeLeft;
    CallFunc *finRotacion = CallFunc::create([this]() { this->finRotacion(); });
    this->runAction(Sequence::createWithTwoActions(RotateBy::create(0.5f, 180.f),finRotacion));
    
    CCLOG("timeLeft %f",mTimeLeft);
}

void SandClock::finRotacion()
{
    mLockRotate=false;
    this->setRotation(0.f);
    initClocks();
    resetAppearance();
}


void SandClock::stopTime(float tiempoGastado)
{
    mTimeLeft-=tiempoGastado;
    if(mTimeLeft<0)
    {
        mTimeLeft=0;
    }
    mTiempoRestanteAbajo->stopAllActions();
    mTiempoRestanteArriba->stopAllActions();
    mLockRotate=false;
    
    this->runAction(RepeatForever::create(EaseBackIn::create(Sequence::createWithTwoActions(ScaleTo::create(2.f, (mDefaultScale-0.05f)), ScaleTo::create(2.f,(mDefaultScale+0.05f))))));
}
void SandClock::resetAppearance()
{
    this->stopAllActions();
    this->setScale(mDefaultScale);
}


void SandClock::startTime()
{
    resetAppearance();
    mLockRotate=true;
    if(mTimeLeft==0)
    {
        return;
    }
    float actualPercentage=mTimeLeft/mTimeCapacity *100.f;
    
    mTiempoProgresoArriba = ProgressFromTo::create(mTimeLeft, actualPercentage, 0.f);
    CallFunc *tiempoFinish = CallFunc::create([this]() { this->finTiempoArriba(); });
    Sequence *asequence = (Sequence *) Sequence::createWithTwoActions(mTiempoProgresoArriba,tiempoFinish);
    mTiempoRestanteArriba->runAction(asequence);
    
   
    mTiempoProgresoAbajo = ProgressFromTo::create(mTimeLeft, 100.f-actualPercentage,100.f);
    CallFunc *tiempoFinishAbajo =CallFunc::create([this]() { this->finTiempoAbajo(); });
    Sequence *asequenceAbajo = (Sequence *) Sequence::createWithTwoActions(mTiempoProgresoAbajo,tiempoFinishAbajo);
    mTiempoRestanteAbajo->runAction(asequenceAbajo);
    
    
    
}
float SandClock::getTimeLeft()
{
    return mTimeLeft;
}


