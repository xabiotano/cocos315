#ifndef __SandClockScene_SCENE_H__
#define __SandClockScene_SCENE_H__

#include "cocos2d.h"
#include "SandClock.h"
#include "../BaseScene.h"



USING_NS_CC;
class SandClockScene : public BaseScene
{
public:
    
    void Retry(Ref* sender) override;
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    virtual ~SandClockScene(void);
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::Scene* scene();
    
        // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(SandClockScene);
    void onClickSandClockLeft(Ref* sender);
    void onClickSandClockRight(Ref* sender);
    
    void pararTiempo(float capacidad,float tiempogastado);
    
    
    
private:
    void iniciarScene();
    Size mSize;
    SandClock* mSandClockLeft,*mSandClockRight;
    Label* mTimeLabel;
    Boton *mBoiler;
   void botonClock(bool vieneDeHint);
    void botonClockButton(Ref* sender);
    int mTime;
    SEL_SCHEDULE mCuentaAtrasSchedule;
    void cuentaAtras(float dt);
    float minTime;
    virtual  void  GameOver();
    virtual  void Success();
    ParticleSystemQuad* mParticle;
    void boilingEffect(bool visible);
    CCArray* mListaPasos;
    virtual void onExit();
    void onEnterTransitionDidFinish();
    void HintStartTemp(float dt);
protected:
    virtual void Hint(Ref* sender);
    virtual void HintCallback(bool accepted);
    virtual void HintStart(Ref* sender);
    virtual void TutorialStart(Ref* sender);
    virtual void TutorialNext(Ref* sender);


};

#endif // __SandClockScene_SCENE_H__
