#include "ObjCCalls.h"
///#import "../cocos2dx/platform/ios/EAGLView.h"
#import "Variables.h"
#import "cocos2d.h"
#import "../widgets/LanguageManager.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#import "FacebookSender.h"
#import "RootViewController.h"
#endif


void ObjCCalls::shareAmigos()
{
    
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    std::string texto= LanguageManager::getInstance()->getStringForKey("SHARE1", "SHARE1");
    std::string texto_uno= LanguageManager::getInstance()->getStringForKey("SHARE2", "SHARE2");
   // std::string texto_dos(LanguageManager::getInstance()->getStringForKey("SHARE3", "SHARE3"));

    cocos2d::Size size=Director::getInstance()->getVisibleSize();
    
    RenderTexture* texture = RenderTexture::create((int)size.width, (int)size.height);
    texture->setPosition(cocos2d::Point(size.width/2, size.height/2));
    texture->begin();
    Director::getInstance()->getRunningScene()->visit();
    texture->end();
    
    
    std::string path = FileUtils::getInstance()->getWritablePath() + "screenshot.png";
    texture->saveToFile(path.c_str());

    CCLOG("path screenshot %s",path.c_str());
    //std::string concat(Variables::URL_DESCARGA);
    texto=texto +"    "+ texto_uno;
    [FacebookSender  tryToPostOnFacebook:texto.c_str()];
    #endif
}



void ObjCCalls::showIntersitial()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    RootViewController* rootView=(RootViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    try{
        [rootView showInterstitial];
    }catch(...){}
    #endif
}
void ObjCCalls::showBanner()
{
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    RootViewController* rootView=(RootViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    try{
        [rootView showBanner];
    }catch(...){}
     #endif
    
}


void ObjCCalls::cacheIntersitial()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    RootViewController* rootView=(RootViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    try{
             [rootView cacheIntersitial];
    }catch(...){}
#endif
}





void ObjCCalls::hideBanner()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    RootViewController* rootView=(RootViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    try{
        [rootView hideBanner];
    }catch(...){}
    
#endif
}








