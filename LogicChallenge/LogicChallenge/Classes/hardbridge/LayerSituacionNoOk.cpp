//
//  LayerSituacionNoOk.cpp
//  trivial
//
//  Created by Xabier on 25/03/13.
//
//

#include "LayerSituacionNoOk.h"
#include "Variables.h"
#include "SimpleAudioEngine.h"
#include "../AdHelper.h"




LayerSituacionNoOk* LayerSituacionNoOk::initLayer(TipoSituacionProblema tipoSituacionProblema){
    LayerSituacionNoOk *layer = LayerSituacionNoOk::create();
    layer->dibujarContent(tipoSituacionProblema);
    return layer;
}


// on "init" you need to initialize your instance
bool LayerSituacionNoOk::init()
{
    //////////////////////////////
    // 1. super init first
    if (!LayerColor::initWithColor(Color4B(100,100,100,180))){
        return false;
    }
   
    return true;
}

void LayerSituacionNoOk::dibujarContent(TipoSituacionProblema tipoSituacionProblema){
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Layer* layer=LayerColor::create(Color4B(0,0,0,10.f),visibleSize.width , visibleSize.height);
    Sprite* fondo= Sprite::create("ui/panel_brown.png");
    fondo->setPosition(Point(visibleSize.width/2,visibleSize.height/2));
    
    layer->addChild(fondo);
    Size tamanoFondo=fondo->getContentSize();
    Point puntoIzq=Point(tamanoFondo.width*0.22f, tamanoFondo.height*0.5f);
    Point puntoMed=Point(tamanoFondo.width*0.5f, tamanoFondo.height*0.5f);
    Point puntoDcha=Point(tamanoFondo.width*0.77f, tamanoFondo.height*0.5f);
    Sprite* itemIzquierda;
    Sprite* itemCentro;
    Sprite* itemDerecha;
    
    if(tipoSituacionProblema==TipoSituacionProblema(kPerroNino)){
        itemIzquierda=Sprite::create("hardbridge/perro.png");
        itemCentro=Sprite::create("hardbridge/error/ladrando.png");
        itemDerecha=Sprite::create("hardbridge/nino.png");
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kPerroNina)){
        itemIzquierda=Sprite::create("hardbridge/perro.png");
        itemCentro=Sprite::create("hardbridge/error/ladrando.png");
        itemDerecha=Sprite::create("hardbridge/nina.png");
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kPerroCastor)){
        itemIzquierda=Sprite::create("hardbridge/perro.png");
        itemCentro=Sprite::create("hardbridge/error/ladrando.png");
        itemDerecha=Sprite::create("hardbridge/error/castor_asustado.png");
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kPerroPato)){
        itemIzquierda=Sprite::create("hardbridge/perro.png");
        itemCentro=Sprite::create("hardbridge/error/ladrando.png");
        itemDerecha=Sprite::create("hardbridge/error/pato_asustado.png");
        
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kNinoCastor)){
        itemIzquierda=Sprite::create("hardbridge/nino.png");
        itemCentro=Sprite::create("hardbridge/error/tirachinas.png");
        itemDerecha=Sprite::create("hardbridge/error/castor_asustado.png");
        
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kNinaPato)){
        itemIzquierda=Sprite::create("hardbridge/nina.png");
        itemCentro=Sprite::create("hardbridge/error/tirachinas.png");
        itemDerecha=Sprite::create("hardbridge/error/pato_asustado.png");
        
    }else if(tipoSituacionProblema==TipoSituacionProblema(kPerroLadrando)){
        itemIzquierda=Sprite::create("hardbridge/perro.png");
        itemCentro=Sprite::create("hardbridge/error/ladrando.png");
        itemDerecha=Sprite::create("hardbridge/error/ladrando.png");
        
    }
    else{
        CCLOG("WHATT!!");
    }
    itemIzquierda->setScale(1.4f);
    itemCentro->setScale(1.4f);
    itemDerecha->setScale(1.4f);
    
    fondo->addChild(itemIzquierda);
    fondo->addChild(itemCentro);
    fondo->addChild(itemDerecha);
    itemIzquierda->setPosition(puntoIzq);
    itemCentro->setPosition(puntoMed);
    itemDerecha->setPosition(puntoDcha);
    
    this->addChild(layer,50);


}



void LayerSituacionNoOk::mostrar(TipoSituacionProblema tipoSituacionProblema)
{
  
    if(!Director::getInstance()->getRunningScene()){
        return;
    }
    if(!Director::getInstance()->getRunningScene()->getChildByTag(TAG_LAYER_SITUACION_NO_OK)){
        LayerSituacionNoOk* layer= LayerSituacionNoOk::initLayer(tipoSituacionProblema);
        layer->setContentSize( Director::getInstance()->getWinSize());
        layer->setTag(TAG_LAYER_SITUACION_NO_OK);
        Director::getInstance()->getRunningScene()->addChild(layer);
        BaseScene* base= (BaseScene*)Director::getInstance()->getRunningScene();
        if(!LogicSQLHelper::getInstance().estaSinPublicidadComprado()){
            AdHelper::showIntersitial();
        }
     
    }
    else{
        CCLOG("Layer loading previamente mostrada");
    }
}

void LayerSituacionNoOk::ocultar(Ref* pSender)
{
    if( Director::getInstance()->getRunningScene()->getChildByTag(TAG_LAYER_SITUACION_NO_OK)){
        Director::getInstance()->getRunningScene()->removeChildByTag(TAG_LAYER_SITUACION_NO_OK,true);
    }
  
}

void LayerSituacionNoOk::onEnter()
{
    mListener = EventListenerTouchOneByOne::create();
    mListener->setSwallowTouches(true);
    mListener->onTouchBegan = CC_CALLBACK_2(LayerSituacionNoOk::onTouchBegan, this);
    mListener->onTouchMoved = CC_CALLBACK_2(LayerSituacionNoOk::onTouchMoved, this);
    mListener->onTouchEnded = CC_CALLBACK_2(LayerSituacionNoOk::onTouchEnded, this);
    
    this->getEventDispatcher()->addEventListenerWithFixedPriority(mListener, -60);
    Layer::onEnter();
}

void LayerSituacionNoOk::onExit()
{
     this->getEventDispatcher()->removeEventListener(mListener);
   
    
    Layer::onExit();
}


bool LayerSituacionNoOk::onTouchBegan(Touch* touch, Event* event)
{
    return true;
}

void LayerSituacionNoOk::onTouchEnded(Touch* touch, Event* event)
{
    ocultar(this);
}
void LayerSituacionNoOk::onTouchMoved(Touch* touch, Event* event)
{
}





