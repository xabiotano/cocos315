//
//  Protagonista.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__TipoSituacionProblema__
#define __logicmaster2__TipoSituacionProblema__

#include <iostream>


typedef enum TipoSituacionProblema
{
    kPerroNino,
    kPerroNina,
    kPerroCastor,
    kPerroPato,
    kNinoCastor,
    kNinaPato,
    kPerroLadrando
    
    
} TipoSituacionProblema;



#endif /* defined(__logicmaster2__Protagonista__) */
