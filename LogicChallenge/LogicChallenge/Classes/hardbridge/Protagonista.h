//
//  Protagonista.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Protagonista__
#define __logicmaster2__Protagonista__

#include <iostream>
#include "../widgets/Boton.h"
#include "Protagonista.fwd.h"
#include "HardBridgeScene.h"

typedef enum TipoProtagonista
{
    kPerro,
    kNino,
    kCastor1,
    kCastor2,
    kPato1,
    kPato2,
    kNina,
    kGranjero
    
} TipoProtagonista;

typedef enum EstadoProtagonista
{
    kProtagonistaIzquierda,
    kProtagonistaDerecha,
    kProtagonistaMontado
    
} EstadoProtagonista;



class Protagonista: public Boton
{
    
    
    public:
    
        Protagonista(Point mPointIzq,Point mPointDch,int z_index,TipoProtagonista tipo);
        virtual ~Protagonista(void);
        static cocos2d::Scene* scene();
    
   
        static Protagonista* create(Node* parent,TipoProtagonista tipo);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        std::string mName;
        std::string getNameOfHuman();
        int mZIndex;
        bool isSelected();
       
        Point mPosicionIzquierda,mPosicionDerecha;
        void move();
        TipoProtagonista mTipo;
        EstadoProtagonista mEstado;
        bool estaEnDerecha();
      
protected:
   
    
    private:
        bool mEstaIzquierda;
        bool mIsMoving;
        unsigned int mSoundEffect;
        bool mSelected;
        void finMove();
        void finUnMount();
        int mPosicionAsiento;
    
    
    
};


#endif /* defined(__logicmaster2__Protagonista__) */
