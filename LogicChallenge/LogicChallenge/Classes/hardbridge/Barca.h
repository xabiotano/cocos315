//
//  Barca.h
//  logicmaster2
//
//  Created by Xabier on 09/11/13.
//
//

#ifndef __logicmaster2__Barca__
#define __logicmaster2__Barca__

#include <iostream>
#include "../widgets/Boton.h"
#include "Barca.fwd.h"
#include "HardBridgeScene.h"




typedef enum BarcaState
{
    kBarcaLadoIzquierdo,
    kBarcaLadoDerecho
    
} BarcaState;

class Barca: public Boton
{
    
    
    public:
        
        Barca(Point mPointIzq,Point mPointDch);
        virtual ~Barca(void);
        static cocos2d::Scene* scene();
        bool mSitioBarca1,mSitioBarca2;
   
        static Barca* create(Node* parent);
        void click(Ref* sender);
        bool estaEnIzquierda();
        void setMoving(bool moving);
        std::string mName;
    
        BarcaState mEstado;
        Point mPosicionIzquierda,mPosicionDerecha;
        void move(Ref* sender);
        bool mIsMoving;
        Point getSitioLibre(bool convertidoAWorld,int &posicion);
        int getNumeroSitiosLibres();
       
        void ocuparAsiento(int posicion);
        void liberarAsiento(int posicion);
        void checkAlertaMover();


    private:
        bool mEstaIzquierda;
        Dictionary* mSitios;
        unsigned int mSoundEffect;
        void finMove();
        Boton* mMover;
    
    
    
};


#endif /* defined(__logicmaster2__Barca__) */
